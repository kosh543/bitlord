#include "torrentdetails.h"
#include "components.h"

TorrentDetails::TorrentDetails(Components *comp, QWidget *parent) :
    QTabWidget(parent)
{
    this->components = comp;

    filesTab = new FilesTab(comp, this);
    statusTab = new StatusTab(comp, this);
    detailsTab = new DetailTab(comp, this);
    peersTab = new PeersTab(comp, this);
    optionsTab = new OptionsTab(comp, this);

    this->addTab(filesTab, "Files");
    this->addTab(statusTab, "Status");
    this->addTab(detailsTab, "Details");
    this->addTab(peersTab, "Peers");
    this->addTab(optionsTab, "Options");

    this->_update_timer = new QTimer(this);
    this->_update_timer->setInterval(2000);
    this->connect(this->_update_timer, SIGNAL(timeout()), this, SLOT(update_details()));
    this->_update_timer->start();
}

void TorrentDetails::update_details()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
        // clear
        return;

    this->filesTab->update_tab();
    this->statusTab->update_tab();
    this->detailsTab->update_tab();
    this->peersTab->update_tab();
    this->optionsTab->update_tab();
}

void TorrentDetails::save_tabs_state()
{
    this->filesTab->save_state();
    this->peersTab->save_state();
}
