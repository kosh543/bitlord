#include "peerstab.h"
#include "components.h"

PeersTabProgressItemDelegate::PeersTabProgressItemDelegate(QObject *parent) : QItemDelegate(parent){}

void PeersTabProgressItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionProgressBarV2 progressBarOption;
    //QRect rect = option.rect;
    //QSize size(rect.width()*3/4,rect.height()*3/4);
    //rect.setSize(size);
    progressBarOption.state = QStyle::State_Enabled;
    progressBarOption.direction = QApplication::layoutDirection();
    progressBarOption.rect = option.rect;
    progressBarOption.fontMetrics = QApplication::fontMetrics();
    //QPalette pal = progressBarOption.palette;
    //QColor col;
    //col.setNamedColor("#05B8CC");
    //pal.setColor(QPalette::Highlight,col);
    //progressBarOption.palette = pal;
    progressBarOption.type = QStyleOption::SO_ProgressBar;
    progressBarOption.version = 2;
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.textAlignment = Qt::AlignCenter;
    progressBarOption.textVisible = true;
    int progress = (int)(index.data(Qt::DisplayRole).toFloat() * 100);
    progressBarOption.progress = progress;
    progressBarOption.text = QString("%1%").arg(progressBarOption.progress);

    // Draw the progress bar onto the view.
    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter, 0);
}

PeersTab::PeersTab(Components *comp, QWidget *parent) :
    QWidget(parent)
{
    this->components = comp;

    seed_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/seeding16.png"));
    peer_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/downloading16.png"));

    QVBoxLayout *vBox = new QVBoxLayout(this);
    this->setLayout(vBox);

    peersView = new QTreeView();
    peersModel = new QStandardItemModel(0, 6);
    peersView->setModel(peersModel);
    peersView->setSelectionBehavior(QAbstractItemView::SelectRows);
    peersView->setRootIsDecorated(false);
    peersView->setSortingEnabled(true);
    //peersView->sortByColumn(1, Qt::AscendingOrder);
    QStringList peers_labels;
    peers_labels << "" << "Address" << "Client" << "Progress" << "Down Speed" << "Up Speed";
    peersModel->setHorizontalHeaderLabels(peers_labels);
    peersView->setItemDelegateForColumn(3, new PeersTabProgressItemDelegate(peersView));
    peersView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    vBox->addWidget(peersView);

    this->load_state();
}

void PeersTab::load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("peers_tab.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        this->columns_width.clear();
        in >> this->columns_width;
        for(int index = 0; index < this->columns_width.count(); ++index)
            this->peersView->setColumnWidth(index, this->columns_width[index]);
        fnew.close();
        return;
    }
    QFile f(QDir(get_default_config_dir()).absoluteFilePath("peers_tab.state"));
    if(f.open(QIODevice::ReadOnly))
    {
        QString s = QString(f.readAll());
        f.close();
        PickleLoader pl(s);
        QVariant res;
        try
        {
            pl.loads(res);
            if(res.type() == QVariant::Map && res.toMap().contains("columns"))
            {
                QMap<QString, QVariant> columns = res.toMap()["columns"].toMap();
                this->columns_width.clear();
                this->columns_width << columns[""].toMap()["width"].toInt()
                                    << columns["Address"].toMap()["width"].toInt()
                                    << columns["Client"].toMap()["width"].toInt()
                                    << columns["Progress"].toMap()["width"].toInt()
                                    << columns["Down Speed"].toMap()["width"].toInt()
                                    << columns["Up Speed"].toMap()["width"].toInt();
                for(int index = 0; index < this->columns_width.count(); ++index)
                    this->peersView->setColumnWidth(index, this->columns_width[index]);
            }
        }
        catch(...){}
        if(this->columns_width.count() < 4)
        {
            this->columns_width.clear();
            this->columns_width << 20 << 100 << 50 << 50 << 50 << 50;
        }
    }
}

void PeersTab::save_state()
{
    for(int index = 0; index < this->columns_width.count(); ++index)
        this->columns_width[index] = this->peersView->columnWidth(index);
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("peers_tab.state"));
    if(fnew.open(QIODevice::WriteOnly))
    {
        QDataStream out(&fnew);
        out << this->columns_width;
        fnew.flush();
        fnew.close();
    }
}

void PeersTab::clear_tab()
{
    this->peersModel->invisibleRootItem()->removeRows(0, this->peersModel->invisibleRootItem()->rowCount());
}

QIcon PeersTab::get_flag_icon(QString country)
{
    if(country.isEmpty() || country == "  ")
        return QIcon();

    if(!this->cached_flag_icons.contains(country))
        this->cached_flag_icons[country] =
            QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/flags/" + country.toLower() + ".png"));
    return this->cached_flag_icons[country];
}

void PeersTab::update_tab()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
    {
        this->clear_tab();
        return;
    }

    QString torrent_id = selected_torrents[0];

    QList<QString> status_keys;
    status_keys << "peers";
    if(torrent_id != this->current_torrent_id)
    {
        this->peersModel->invisibleRootItem()->removeRows(0, this->peersModel->invisibleRootItem()->rowCount());
        this->current_torrent_id = torrent_id;
        this->peers.clear();
    }

    QMap<QString, QVariant> status = (*(this->components->torrentManager))[torrent_id]->get_status(status_keys);

    if(status.isEmpty())
        return;

    QSet<QString> new_ips;
    foreach(QVariantMap peer, status["peers"].value<QList<QVariantMap> >())
    {
        QString ip = peer["ip"].toString();
        new_ips << ip;
        if(this->peers.contains(ip))
        {
            int row = this->peers[ip]->row();
            if(!this->peersModel->hasIndex(row, 0))
            {
                this->peers.remove(ip);
                continue;
            }
            if(peer["down_speed"].toInt() != this->peersModel->data(this->peersModel->index(row, 4)).toInt())
                this->peersModel->setData(this->peersModel->index(row, 4), peer["down_speed"], Qt::DisplayRole);
            if(peer["up_speed"].toInt() != this->peersModel->data(this->peersModel->index(row, 5)).toInt())
                this->peersModel->setData(this->peersModel->index(row, 5), peer["up_speed"], Qt::DisplayRole);
            if(peer["country"].toString() != this->peersModel->data(this->peersModel->index(row, 0), Qt::UserRole).toString())
            {
                this->peersModel->setData(this->peersModel->index(row, 0), peer["country"], Qt::UserRole);
                this->peersModel->item(row, 0)->setIcon(this->get_flag_icon(peer["country"].toString()));
            }
            if(peer["progress"].toFloat() != this->peersModel->data(this->peersModel->index(row, 3)).toFloat())
                this->peersModel->setData(this->peersModel->index(row, 3), peer["progress"], Qt::DisplayRole);

            if(peer["seed"].toUInt())
            {
                if(this->peersModel->data(this->peersModel->index(row, 1), Qt::UserRole).toInt() == 0)
                {
                    this->peersModel->setData(this->peersModel->index(row, 1), 1, Qt::UserRole);
                    this->peersModel->item(row, 1)->setIcon(this->seed_icon);
                }
            }
            else
            {
                if(this->peersModel->data(this->peersModel->index(row, 1), Qt::UserRole).toInt() == 1)
                {
                    this->peersModel->setData(this->peersModel->index(row, 1), 0, Qt::UserRole);
                    this->peersModel->item(row, 1)->setIcon(this->peer_icon);
                }
            }
        }
        else
        {
            QList<QStandardItem*> items;
            items << new QStandardItem(this->get_flag_icon(peer["country"].toString()), "");
            items[0]->setData(peer["country"], Qt::UserRole);
            if(peer["seed"].toUInt())
            {
                QStandardItem *item = new QStandardItem(this->seed_icon, ip);
                item->setData(1, Qt::UserRole);
                items << item;
            }
            else
            {
                QStandardItem *item = new QStandardItem(this->peer_icon, ip);
                item->setData(0, Qt::UserRole);
                items << item;
            }
            items << new QStandardItem(peer["client"].toString());
            items << new QStandardItem();
            items[3]->setData(peer["progress"], Qt::DisplayRole);
            items << new QStandardItem();
            items[4]->setData(peer["down_speed"], Qt::DisplayRole);
            items << new QStandardItem();
            items[5]->setData(peer["up_speed"], Qt::DisplayRole);
            this->peersModel->appendRow(items);

            this->peers[ip] = items[0];
        }
    }

    foreach(QString ip, this->peers.keys().toSet().subtract(new_ips))
    {
        this->peersModel->removeRow(this->peers[ip]->row());
        this->peers.remove(ip);
    }
}
