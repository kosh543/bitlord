#ifndef TREEVIEWITEM_H
#define TREEVIEWITEM_H

#include <QList>
#include <QVariant>
#include <QIcon>

class TreeViewItem
{
public:
    TreeViewItem(const QList<QVariant> &data, TreeViewItem *parent = 0);
    ~TreeViewItem();

    void appendChild(TreeViewItem *child);

    TreeViewItem *child(int row);
    int childCount() const;
    int columnCount() const;
    void removeChild();
    void removeChildren(int index);
    QVariant data(int column) const;
    void setData(int column, QVariant data);
    int addColumn(QVariant data);
    int row() const;
    TreeViewItem *parent();

private:
    QList<TreeViewItem*> childItems;
    QList<QVariant> itemData;
    TreeViewItem *parentItem;
};

#endif // TREEVIEWITEM_H
