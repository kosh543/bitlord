#include "treeviewitem.h"
#include "treeviewmodel.h"
#include "common.h"

#include <QStringList>
#include <QIcon>

TreeViewModel::TreeViewModel(QObject *parent) :
    QAbstractItemModel(parent)
{
    icons["Downloading"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/downloading16.png"));
    icons["Seeding"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/seeding16.png"));
    icons["Paused"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/inactive16.png"));
    icons["Queued"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/queued16.png"));
    icons["Finished"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/finished16.png"));
    icons["Checking"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/checking16.png"));
    icons["Error"] = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/alert16.png"));
    icons["Allocating"] = icons["Checking"];

    QList<QVariant> rootData;
    rootItem = new TreeViewItem(rootData);
    rootIndex = this->index(0, 0);
}

TreeViewModel::~TreeViewModel()
{
    delete rootItem;
}

int TreeViewModel::columnCount(const QModelIndex &parent) const
{
    //if (parent.isValid())
    //    return static_cast<TreeViewItem*>(parent.internalPointer())->columnCount();
    //else
    return rootItem->columnCount();
}

QVariant TreeViewModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DecorationRole && index.column() == NAME_COLUMN)
    {
        TreeViewItem *item = static_cast<TreeViewItem*>(index.internalPointer());
        return this->icons[item->data(STATE_COLUMN).toString()];
    }

    if (role != Qt::DisplayRole)
        return QVariant();

    TreeViewItem *item = static_cast<TreeViewItem*>(index.internalPointer());

    return item->data(index.column());
}

Qt::ItemFlags TreeViewModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeViewModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);
    return QVariant();
}

int TreeViewModel::addColumn(QString text)
{
    //this->beginInsertColumns(QModelIndex(), rootItem->columnCount(), rootItem->columnCount());
    int col = rootItem->addColumn(QVariant(text));
    this->insertColumn(col, rootIndex);
    //this->endInsertColumns();
    return col;
}

QModelIndex TreeViewModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeViewItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeViewItem*>(parent.internalPointer());

    TreeViewItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeViewModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeViewItem *childItem = static_cast<TreeViewItem*>(index.internalPointer());
    TreeViewItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeViewModel::rowCount(const QModelIndex &parent) const
{
    TreeViewItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeViewItem*>(parent.internalPointer());

    return parentItem->childCount();
}

void TreeViewModel::clear()
{
    rootItem->removeChild();
}

void TreeViewModel::appendRow(QList<QVariant> &data)
{
    rootItem->appendChild(new TreeViewItem(data, rootItem));
    //emit dataChanged(this->index(0, 0, rootIndex), this->index(rootItem->childCount()-1, TRACKER_COLUMN, rootIndex));
}

bool TreeViewModel::containTorrentId(QString torrent_id)
{
    for(int i = 0; i < rootItem->childCount(); i++)
    {
        QModelIndex index = this->index(i, TORRENT_ID_COLUMN, rootIndex);
        if(index.data(Qt::DisplayRole).toString() == torrent_id)
            return true;
    }
    return false;
}

void TreeViewModel::startInsertRows(int first, int last)
{
    this->beginInsertRows(rootIndex, first, last);
}

void TreeViewModel::finishInsertRows()
{
    this->endInsertRows();
}

void TreeViewModel::startInsertColumns(int first, int last)
{
    this->beginInsertColumns(rootIndex, first, last);
}

void TreeViewModel::appendColumn(QString text)
{
    this->beginInsertColumns(rootIndex, rootItem->columnCount(), rootItem->columnCount());
    this->addColumn(text);
    this->endInsertColumns();
}

void TreeViewModel::finishInsertColumns()
{
    this->endInsertColumns();
}

void TreeViewModel::update_model(QMap<QString, QMap<QString, QVariant> > status, QList<QString> columns_to_update, QList<QString> filter)
{
    for(int i = 0; i < rootItem->childCount(); i++)
    {
        QString torrent_id = rootItem->child(i)->data(TORRENT_ID_COLUMN).toString();
        if(filter.contains(torrent_id))
            rootItem->child(i)->setData(FILTER_COLUMN, true);
        else
            rootItem->child(i)->setData(FILTER_COLUMN, false);
        if(status.contains(torrent_id))
        {
            rootItem->child(i)->setData(STATE_COLUMN, status[torrent_id]["state"]);
            if(columns_to_update.contains(column_name_from_number(NAME_COLUMN)))
                rootItem->child(i)->setData(NAME_COLUMN, status[torrent_id]["name"]);
            if(columns_to_update.contains(column_name_from_number(PROGRESS_COLUMN)))
                rootItem->child(i)->setData(PROGRESS_COLUMN, status[torrent_id]["progress"]);
            if(columns_to_update.contains(column_name_from_number(NUMBER_COLUMN)))
            {
                int num = status[torrent_id]["queue"].toInt();
                if(num < 0)
                    rootItem->child(i)->setData(NUMBER_COLUMN, QVariant(""));
                else
                    rootItem->child(i)->setData(NUMBER_COLUMN, QVariant(num + 1));
            }
            if(columns_to_update.contains(column_name_from_number(SIZE_COLUMN)))
                rootItem->child(i)->setData(SIZE_COLUMN, QVariant(fsize(status[torrent_id]["total_wanted"].toInt())));
            if(columns_to_update.contains(column_name_from_number(SEEDERS_COLUMN)))
            {
                int first = status[torrent_id]["num_seeds"].toInt();
                int second = status[torrent_id]["total_seeds"].toInt();
                QString seeders;
                if(second > -1)
                    seeders = QString::number(first) + " (" + QString::number(second) + ")";
                else
                {
                    if(first > -1)
                        seeders = QString::number(first);
                    else
                        seeders = "";
                }
                rootItem->child(i)->setData(SEEDERS_COLUMN, seeders);
            }
            if(columns_to_update.contains(column_name_from_number(PEERS_COLUMN)))
            {
                int first = status[torrent_id]["num_peers"].toInt();
                int second = status[torrent_id]["total_peers"].toInt();
                QString peers;
                if(second > -1)
                    peers = QString::number(first) + " (" + QString::number(second) + ")";
                else
                {
                    if(first > -1)
                        peers = QString::number(first);
                    else
                        peers = "";
                }
                rootItem->child(i)->setData(PEERS_COLUMN, peers);
            }
            if(columns_to_update.contains(column_name_from_number(DOWN_SPEED_COLUMN)))
            {
                int speed = status[torrent_id]["download_payload_rate"].toInt();
                QString val;
                if(speed > 0)
                    val = fspeed(speed);
                else
                    val = "";
                rootItem->child(i)->setData(DOWN_SPEED_COLUMN, QVariant(val));
            }
            if(columns_to_update.contains(column_name_from_number(UP_SPEED_COLUMN)))
            {
                int speed = status[torrent_id]["upload_payload_rate"].toInt();
                QString val;
                if(speed > 0)
                    val = fspeed(speed);
                else
                    val = "";
                rootItem->child(i)->setData(UP_SPEED_COLUMN, QVariant(val));
            }
            if(columns_to_update.contains(column_name_from_number(ETA_COLUMN)))
            {
                int time = status[torrent_id]["eta"].toInt();
                QString time_str;
                if(time <= 0)
                    time_str = "";
                else
                    time_str = ftime_from_seconds(time);
                rootItem->child(i)->setData(ETA_COLUMN, QVariant(time_str));
            }
            if(columns_to_update.contains(column_name_from_number(RATIO_COLUMN)))
            {
                double ratio = status[torrent_id]["ratio"].toDouble();
                QString ratio_str;
                if(ratio < -1)
                    ratio_str = " ";
                else if(ratio == -1)
                    ratio_str = "∞";
                else
                    ratio_str = QString::number(ratio, 'f', 3);
                rootItem->child(i)->setData(RATIO_COLUMN, QVariant(ratio_str));
            }
            if(columns_to_update.contains(column_name_from_number(AVAIL_COLUMN)))
            {
                double avail = status[torrent_id]["distributed_copies"].toDouble();
                QString avail_str;
                if(avail < -1)
                    avail_str = " ";
                else if(avail == -1)
                    avail_str = "∞";
                else
                    avail_str = QString::number(avail, 'f', 3);
                rootItem->child(i)->setData(AVAIL_COLUMN, QVariant(avail_str));
            }
            if(columns_to_update.contains(column_name_from_number(ADDED_COLUMN)))
            {
                double date = status[torrent_id]["time_added"].toDouble();
                QString date_str;
                if(date > 0)
                    date_str = fdate(date);
                else
                    date_str = " ";
                rootItem->child(i)->setData(ADDED_COLUMN, QVariant(date_str));
            }
            if(columns_to_update.contains(column_name_from_number(TRACKER_COLUMN)))
                rootItem->child(i)->setData(TRACKER_COLUMN, status[torrent_id]["tracker_host"]);
        }
        else
        {
            this->beginRemoveRows(rootIndex, rootItem->child(i)->row(), rootItem->child(i)->row());
            this->removeRow(rootItem->child(i)->row());
            rootItem->removeChildren(i);
            this->endRemoveRows();
        }
    }

    emit dataChanged(this->index(0, 0, rootIndex), this->index(rootItem->childCount()-1, TRACKER_COLUMN, rootIndex));
}

/*TreeViewSortFilterModel::TreeViewSortFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{

}*/
