#include "filtertreeview.h"
#include "components.h"

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMimeDatabase>
#include <QMimeType>
#include <QDirIterator>

AddRSSFeedDialog::AddRSSFeedDialog(QWidget *parent) :
    QDialog(parent)
{
    QLabel *feedLabel = new QLabel("Feed URL:");

    feedURLEdit = new QLineEdit();

    QHBoxLayout *feedLayout = new QHBoxLayout();
    feedLayout->addWidget(feedLabel);
    feedLayout->addWidget(feedURLEdit);

    QCheckBox *aliasCheck = new QCheckBox("Custom Alias:");
    this->connect(aliasCheck, SIGNAL(toggled(bool)), this, SLOT(aliasCheckToggled(bool)));

    aliasEdit = new QLineEdit();
    aliasEdit->setDisabled(true);

    QHBoxLayout *aliasLayout = new QHBoxLayout();
    aliasLayout->addWidget(aliasCheck);
    aliasLayout->addWidget(aliasEdit);

    QVBoxLayout *feedFrameLayout = new QVBoxLayout();
    feedFrameLayout->addLayout(feedLayout);
    feedFrameLayout->addLayout(aliasLayout);

    QGroupBox *feedFrame = new QGroupBox("Feed");
    feedFrame->setLayout(feedFrameLayout);

    QPushButton *okButton = new QPushButton("OK");
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(okClicked()));

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancelClicked()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(okButton);
    buttonsLayout->addWidget(cancelButton);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(feedFrame);
    mainLayout->addLayout(buttonsLayout);

    this->setLayout(mainLayout);
}

void AddRSSFeedDialog::closeEvent(QCloseEvent *event)
{
    this->deleteLater();
    event->accept();
}

void AddRSSFeedDialog::okClicked()
{
    QString url = this->feedURLEdit->text().trimmed();
    if(!url.isNull() && !url.isEmpty())
        emit this->rssFeedAdded(url, this->aliasEdit->text().simplified());
    this->close();
    this->deleteLater();
}

void AddRSSFeedDialog::cancelClicked()
{
    this->close();
    this->deleteLater();
}

void AddRSSFeedDialog::aliasCheckToggled(bool state)
{
    if(state)
    {
        this->aliasEdit->setEnabled(true);
    }
    else
    {
        this->aliasEdit->clear();
        this->aliasEdit->setDisabled(true);
    }
}

FilterTreeView::FilterTreeView(Components *comp, QWidget *parent) :
    QTreeView(parent)
{
    this->components = comp;

    status_count["All Downloads"] = 0;
    status_count["Downloading"] = 0;
    status_count["Seeding"] = 0;
    status_count["Active"] = 0;
    status_count["Paused"] = 0;
    status_count["Queued"] = 0;
    status_count["Finished"] = 0;
    status_count["Checking"] = 0;
    status_count["Error"] = 0;

    QStringList fields;
    fields << "All Downloads";
    fields << "Downloading";
    fields << "Seeding";
    fields << "Active";
    fields << "Paused";
    fields << "Queued";
    fields << "Finished";
    fields << "Checking";
    fields << "Error";

    QStringList icons;
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/all16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/downloading16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/seeding16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/active16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/inactive16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/queued16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/finished16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/checking16.png");
    icons << QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/alert16.png");

    model = new QStandardItemModel(this);
    model->appendRow(new QStandardItem("Browser"));
    model->item(0)->setEditable(false);
    model->appendRow(new QStandardItem("Top List"));
    model->item(1)->setEditable(false);
    model->appendRow(new QStandardItem("Torrents"));
    model->item(2)->setEditable(false);
    for(int i = 0; i < fields.length(); ++i)
    {
        QStandardItem *item = new QStandardItem(QIcon(icons[i]), fields[i] + " (0)");
        item->setEditable(false);
        model->item(2)->appendRow(item);
        this->status_row[fields[i]] = i;
    }
    model->appendRow(new QStandardItem("RSS"));
    model->item(3)->setEditable(false);
    QStandardItem *addFeedItem = new QStandardItem(QIcon( QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/addfeedicon16.png")), "Add a feed");
    addFeedItem->setFlags(Qt::ItemIsEnabled);
    model->item(3)->appendRow(addFeedItem);
    model->appendRow(new QStandardItem("Playlists"));
    model->item(4)->setEditable(false);
    QStandardItem *addPlaylistItem = new QStandardItem(QIcon( QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/addfeedicon16.png")), "New playlist");
    addPlaylistItem->setFlags(Qt::ItemIsEnabled);
    model->item(4)->appendRow(addPlaylistItem);

    this->torrentsIndex = model->index(2, 0);
    this->rssIndex = model->index(3, 0);
    this->playlistsIndex = model->index(4, 0);

    this->setItemDelegate(new filterTreeViewItemDelegate(this));
    this->setModel(model);
    this->expandAll();

    this->current_main_page = 1;
    this->lastFilter = this->torrentsIndex.child(0, 0);

    QSettings settings;
    if(!settings.value("uiconfig/sidebar_show_zero").toBool())
        this->show_zero_hits(false);

    this->setHeaderHidden(true);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setRootIsDecorated(false);
    this->setIndentation(0);
    //this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setStyleSheet("background-color:transparent;");
    this->setStyleSheet("background-image:url("
                        + QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/app-bot-overlay.png")
                        + ");");

    this->selectionModel()->setCurrentIndex(this->model->index(0, 0, this->torrentsIndex),
                                            QItemSelectionModel::ClearAndSelect);

    connect(this->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
            this, SLOT(on_current_row_changed(QModelIndex,QModelIndex)));
    connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(on_index_clicked(QModelIndex)));

    QAction *selectAllAction = new QAction("Select All", this);
    QAction *pauseAllAction = new QAction("Pause All", this);
    QAction *resumeAllAction = new QAction("Resume All", this);

    torrents_context_menu = new QMenu(this);
    torrents_context_menu->addAction(selectAllAction);
    torrents_context_menu->addAction(pauseAllAction);
    torrents_context_menu->addAction(resumeAllAction);

    QAction *removeRSSAction = new QAction("Remove", this);
    QAction *updateRSSAction = new QAction("Update", this);

    rss_context_menu = new QMenu(this);
    rss_context_menu->addAction(removeRSSAction);
    rss_context_menu->addAction(updateRSSAction);

    QAction *playPlaylistAction = new QAction("Play", this);
    QAction *removePlaylistAction = new QAction("Remove", this);
    QAction *addFileToPlaylistAction = new QAction("Add File", this);
    QAction *addFolderToPlaylistAction = new QAction("Add Folder", this);

    playlists_context_menu = new QMenu(this);
    playlists_context_menu->addAction(playPlaylistAction);
    playlists_context_menu->addAction(removePlaylistAction);
    playlists_context_menu->addAction(addFileToPlaylistAction);
    playlists_context_menu->addAction(addFolderToPlaylistAction);

    this->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(selectAllAction, SIGNAL(triggered()), this, SLOT(selectAllActivated()));
    this->connect(pauseAllAction, SIGNAL(triggered()), this, SLOT(pauseAllActivated()));
    this->connect(resumeAllAction, SIGNAL(triggered()), this, SLOT(resumeAllActivated()));
    this->connect(removeRSSAction, SIGNAL(triggered()), this, SLOT(removeRSSActivated()));
    this->connect(updateRSSAction, SIGNAL(triggered()), this, SLOT(updateRSSActivated()));
    this->connect(playPlaylistAction, SIGNAL(triggered()), this, SLOT(playPlaylistActivated()));
    this->connect(removePlaylistAction, SIGNAL(triggered()), this, SLOT(removePlaylistActivated()));
    this->connect(addFileToPlaylistAction, SIGNAL(triggered()), this, SLOT(addFileToPlaylistActivated()));
    this->connect(addFolderToPlaylistAction, SIGNAL(triggered()), this, SLOT(addFolderToPlaylistActivated()));
    this->connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowContextMenu(QPoint)));
    this->connect(this->components->rssManager, SIGNAL(rssFeedAddedToManager(QString)), this, SLOT(rssFeedAddedToManager(QString)));
    this->connect(this->itemDelegate(), SIGNAL(editingFinished(QString,QString,QModelIndex)),
                  this, SLOT(editingFinished(QString,QString,QModelIndex)));
    this->connect(this->model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(filterItemChanged(QStandardItem*)));

    this->_update_count = new QTimer(this);
    this->_update_count->setInterval(2000);
    this->connect(this->_update_count, SIGNAL(timeout()), this, SLOT(count_status_fields()));
    this->_update_count->start();
}

void FilterTreeView::count_status_fields()
{
    QMap<QString, int> new_count;
    new_count["All Downloads"] = this->components->torrentManager->torrents.count();
    new_count["Downloading"] = 0;
    new_count["Seeding"] = 0;
    new_count["Active"] = 0;
    new_count["Paused"] = 0;
    new_count["Queued"] = 0;
    new_count["Finished"] = 0;
    new_count["Checking"] = 0;
    new_count["Error"] = 0;
    foreach(QString torrent_id, this->components->torrentManager->torrents.keys())
    {
        QString state = this->components->torrentManager->torrents[torrent_id]->state;
        if(state != "Allocating")
            new_count[state] += 1;
        if(this->components->torrentManager->torrents[torrent_id]->handle.status().download_payload_rate > 0 ||
                this->components->torrentManager->torrents[torrent_id]->handle.status().upload_payload_rate > 0)
            new_count["Active"] += 1;
    }
    this->components->preventHibernation->check_prevent(new_count["Active"]);
    QSettings settings;
    bool show_zero = settings.value("uiconfig/sidebar_show_zero").toBool();
    foreach(QString state, new_count.keys())
    {
        if(state != "Allocating")
        {
            if(new_count[state] != this->status_count[state])
            {
                if(!show_zero)
                {
                    if(this->status_count[state] == 0 && new_count[state] > 0)
                        this->setRowHidden(this->status_row[state], this->torrentsIndex, false);
                    else if(this->status_count[state] > 0 && new_count[state] == 0)
                        this->setRowHidden(this->status_row[state], this->torrentsIndex, true);
                }
                this->status_count[state] = new_count[state];
                this->model->setData(this->model->index(this->status_row[state], 0, this->torrentsIndex),
                                     QVariant(state + " (" + QString::number(new_count[state]) + ")"), Qt::DisplayRole);
            }
        }
    }
}

void FilterTreeView::show_zero_hits(bool show)
{
    foreach(QString state, this->status_count.keys())
    {
        if(this->status_count[state] == 0)
            this->setRowHidden(this->status_row[state], this->torrentsIndex, !show);
    }
}

void FilterTreeView::set_default_filter()
{
    QModelIndexList selected = this->selectionModel()->selectedRows();
    if(selected.count() > 0)
    {
        if(selected[0].data() != "All Downloads")
            this->selectionModel()->setCurrentIndex(this->model->index(0, 0, this->torrentsIndex),
                                                    QItemSelectionModel::ClearAndSelect);
        else
            this->components->torrentView->set_filter("All Downloads");
    }
}

void FilterTreeView::set_last_filter()
{
    QModelIndexList selected = this->selectionModel()->selectedRows();
    if(selected.count() > 0 && selected[0] != this->lastFilter)
        this->selectionModel()->setCurrentIndex(this->lastFilter, QItemSelectionModel::ClearAndSelect);
}

void FilterTreeView::show_browser()
{
    QModelIndexList selected = this->selectionModel()->selectedRows();
    if(selected.count() > 0 && selected[0].data() != "Browser")
        this->selectionModel()->setCurrentIndex(this->model->index(0, 0), QItemSelectionModel::ClearAndSelect);
}

void FilterTreeView::add_playlist(QString playlist)
{
    QStandardItem *newPlaylistItem =
            new QStandardItem(QIcon(QDir(
            QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/playlist16.png")), playlist);
    newPlaylistItem->setEditable(true);
    model->itemFromIndex(this->playlistsIndex)->insertRow(0, newPlaylistItem);
}

void FilterTreeView::on_current_row_changed(QModelIndex selected, QModelIndex deselected)
{
    int new_main_page;
    if(selected.parent().isValid())
    {
        new_main_page = selected.parent().row();
        if(selected.parent().data().toString() == "Torrents")
        {
            QString state = this->status_row.key(selected.row());
            this->components->torrentView->set_filter(state);
        }
        else if(selected.parent().data().toString() == "RSS")
        {
            QString selectedData = selected.data().toString();
            if(selectedData == "Add a feed")
                return;
            this->components->rssManager->setCurrentFeed(selectedData);
            this->components->rssView->setFeed(selectedData);
        }
        else if(selected.parent().data().toString() == "Playlists")
        {
            QString selectedData = selected.data().toString();
            if(selectedData == "New playlist" || selectedData.isEmpty())
                return;
            this->components->playlistsView->setPlaylist(selectedData);
        }
        this->lastFilter = selected;
        this->components->browserTabs->set_first_icon(this->model->itemFromIndex(selected)->icon());
        this->components->browserTabs->set_first_tab();
    }
    else
    {
        new_main_page = selected.row();
        if(new_main_page == 0)
            this->components->browserTabs->set_browser_tab();
        else if(new_main_page == 1)
            this->components->browser->add_tab("http://www.bitlordsearch.com", "");
    }

    if(new_main_page == 1)
        new_main_page = 0;
    else if(new_main_page > 1)
        new_main_page--;

    if(new_main_page != this->current_main_page)
    {
        this->current_main_page = new_main_page;
        this->components->mainWindow->notebook->setCurrentIndex(new_main_page);
    }
}

void FilterTreeView::on_index_clicked(QModelIndex index)
{
    QString data = index.data().toString();
    if(data == "Add a feed")
    {
        AddRSSFeedDialog *dialog = new AddRSSFeedDialog(this);
        QObject::connect(dialog, SIGNAL(rssFeedAdded(QString,QString)),
                         this->components->rssManager, SLOT(rssFeedAdded(QString,QString)));
        dialog->show();
    }
    else if(data == "New playlist")
    {
        QStandardItem *newPlaylistItem =
                new QStandardItem(QIcon(QDir(
                QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/playlist16.png")), "");
        newPlaylistItem->setEditable(true);
        model->itemFromIndex(this->playlistsIndex)->insertRow(0, newPlaylistItem);
        this->selectionModel()->select(newPlaylistItem->index(), QItemSelectionModel::ClearAndSelect);
        this->edit(newPlaylistItem->index());
    }
}

void FilterTreeView::rssFeedAddedToManager(QString alias)
{
    QStandardItem *addFeedItem =
            new QStandardItem(QIcon(QDir(
            QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/rss16.png")), alias);
    addFeedItem->setEditable(false);
    model->itemFromIndex(this->rssIndex)->insertRow(0, addFeedItem);
}

void FilterTreeView::selectAllActivated()
{
    this->components->torrentView->selectAll();
}

void FilterTreeView::pauseAllActivated()
{
    this->components->torrentView->selectAll();
    this->components->torrentManager->pause_selected();
}

void FilterTreeView::resumeAllActivated()
{
    this->components->torrentView->selectAll();
    this->components->torrentManager->resume_selected();
}

void FilterTreeView::removeRSSActivated()
{
    this->components->rssView->clearRSSView();
    this->components->rssManager->removeFeed(this->lastFilter.data().toString());
    QModelIndexList selected = this->selectionModel()->selectedIndexes();
    if(selected.count() > 0)
        this->model->removeRow(selected[0].row(), selected[0].parent());
}

void FilterTreeView::updateRSSActivated()
{
    this->components->rssManager->updateOneFeed(this->lastFilter.data().toString());
}

void FilterTreeView::playPlaylistActivated()
{
    this->components->player->show_media_list(
                this->components->playlistsView->getCurrentMediaList());
}

void FilterTreeView::removePlaylistActivated()
{
    this->components->playlistsView->clearPlaylistsView();
    this->components->playlistsView->removePlaylist(this->lastFilter.data().toString());
    QModelIndexList selected = this->selectionModel()->selectedIndexes();
    if(selected.count() > 0)
        this->model->removeRow(selected[0].row(), selected[0].parent());
}

void FilterTreeView::addFileToPlaylistActivated()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Choose a file",
                                                    "", "All files (*.*)");
    if(!fileName.isEmpty())
    {
        QMimeDatabase db;
        QMimeType type = db.mimeTypeForFile(fileName);
        QModelIndexList selected = this->selectionModel()->selectedIndexes();
        if((type.name().startsWith("audio") || type.name().startsWith("video")) && selected.count() > 0)
            this->components->playlistsView->addFile(selected[0].data().toString(), fileName);
    }
}

void FilterTreeView::addFolderToPlaylistActivated()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");

    if(!folder.isEmpty())
    {
        QMimeDatabase db;
        QDir root(folder);
        root.setFilter(QDir::Files | QDir::NoDot | QDir::NoDotDot);
        QDirIterator it(root, QDirIterator::Subdirectories);
        QList<QString> files;
        while(it.hasNext())
        {
            QString fileName = it.next();
            QMimeType type = db.mimeTypeForFile(fileName);
            if(type.name().startsWith("audio") || type.name().startsWith("video"))
                files.append(fileName);
        }
        if(!files.isEmpty())
        {
            QModelIndexList selected = this->selectionModel()->selectedIndexes();
            if(selected.count() > 0)
                this->components->playlistsView->addFiles(selected[0].data().toString(), files);
        }
    }
}

void FilterTreeView::ShowContextMenu(const QPoint& pos)
{
    QModelIndex index = this->indexAt(pos);
    if(index.parent().isValid())
    {
        if(index.parent().row() == this->torrentsIndex.row())
            this->torrents_context_menu->exec(this->mapToGlobal(pos));
        else if(index.parent().row() == this->rssIndex.row())
        {
            if(index.data() != "Add a feed")
                this->rss_context_menu->exec(this->mapToGlobal(pos));
        }
        else if(index.parent().row() == this->playlistsIndex.row())
        {
            if(index.data() != "New playlist")
                this->playlists_context_menu->exec(this->mapToGlobal(pos));
        }
    }
}

void FilterTreeView::editingFinished(QString newText, QString oldText, QModelIndex index)
{
    if(newText.isEmpty())
    {
        if(oldText.isEmpty())
            this->model->removeRow(index.row(), index.parent());
        else
            this->playlistsWithWrongName.insert(index, oldText);
    }
    else
    {
        QString uniqName = this->components->playlistsView->checkPlaylistName(newText);
        if(uniqName != newText)
            this->playlistsWithWrongName.insert(index, uniqName);
        this->components->playlistsView->addPlaylist(uniqName, oldText);
    }
}

void FilterTreeView::filterItemChanged(QStandardItem *item)
{
    if(this->playlistsWithWrongName.contains(item->index()))
    {
        item->setData(this->playlistsWithWrongName[item->index()], Qt::DisplayRole);
        this->playlistsWithWrongName.remove(item->index());
    }
}
