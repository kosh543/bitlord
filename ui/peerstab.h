#ifndef PEERSTAB_H
#define PEERSTAB_H

#include <QWidget>
#include <QTreeView>
#include <QStandardItemModel>
#include <QItemDelegate>
#include <QString>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QVariantMap>
#include <QIcon>

class Components;

class PeersTabProgressItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    PeersTabProgressItemDelegate(QObject *parent);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class PeersTab : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QIcon seed_icon;
    QIcon peer_icon;
    QTreeView *peersView;
    QStandardItemModel *peersModel;
    QString current_torrent_id;
    QMap<QString, QStandardItem*> peers;
    QMap<QString, QIcon> cached_flag_icons;
    QList<int> columns_width;
    QIcon get_flag_icon(QString country);

public:
    explicit PeersTab(Components *comp, QWidget *parent = 0);
    void update_tab();
    void clear_tab();
    void load_state();
    void save_state();

};

#endif // PEERSTAB_H
