#ifndef PLAYLISTVIEW_H
#define PLAYLISTVIEW_H

#include <QCoreApplication>
#include <QTreeView>
#include <QStandardItemModel>
#include <QTime>
#include <QTimer>
#include <vlc/vlc.h>
#include <vlc-qt/Media.h>
#include <vlc-qt/MediaList.h>
#include <vlc-qt/MetaManager.h>

class Components;

class PlaylistView : public QTreeView
{
    Q_OBJECT
private:
    Components *components;
    QStandardItemModel* model;
    QTimer *real_load_timer;
    QString currentPlaylist;
    QMap<QString, VlcMediaList*> playlists;
    QMap<QString, QList<QString> > filesLocation;
    QMenu *contextMenu;

public:
    explicit PlaylistView(Components *comp, QWidget *parent = 0);
    void load_state();
    void save_state();
    void addPlaylist(QString playlistName, QString oldPlaylistName);
    void setPlaylist(QString playlist);
    void updateViewContent();
    void setColumnsName();
    QString checkPlaylistName(QString name);
    void clearPlaylistsView();
    void removePlaylist(QString name);
    void appendRowFromMedia(VlcMedia* media);
    void addFile(QString playlist, QString file);
    void addFiles(QString playlist, QList<QString> files);
    VlcMediaList* getCurrentMediaList();

private slots:
    void ShowContextMenu(const QPoint& pos);
    void real_load_state();
    void playFileActivated();
    void removeFileActivated();

};

#endif // PLAYLISTVIEW_H
