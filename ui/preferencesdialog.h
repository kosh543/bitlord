#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QStackedWidget>
#include <QModelIndex>
#include <QLineEdit>
#include <QCheckBox>
#include <QRadioButton>
#include <QPushButton>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>

class Components;

class PreferencesDialog : public QDialog
{
    Q_OBJECT
private:
    Components *components;
    QStackedWidget *notebook;
    QLineEdit *downloadToText;
    QLineEdit *moveCompletedToText;
    QLineEdit *autoAddTorrentsText;
    QLineEdit *copyTorrentFilesToText;
    QLineEdit *listenInterfaceText;
    QLineEdit *peerTOSText;
    QCheckBox *moveCompletedToCheck;
    QCheckBox *autoAddTorrentsCheck;
    QCheckBox *copyTorrentFilesToCheck;
    QCheckBox *deleteCopyTorrentCheck;
    QCheckBox *prioritiseFirstLastCheck;
    QCheckBox *addPausedTorrentsCheck;
    QCheckBox *queueNewToTopCheck;
    QCheckBox *dontCountSlowTorrentsCheck;
    QCheckBox *stopRatioCheck;
    QCheckBox *removeIfStopRatioCheck;
    QCheckBox *ignoreLimitsOnLocalCheck;
    QCheckBox *rateLimitIPOverheadCheck;
    QCheckBox *checkAssociationOnStartup;
    QCheckBox *startOnStartupCheck;
    QCheckBox *preventHibernationCheck;
    QCheckBox *useRandomPortsCheck;
    QCheckBox *useRandomOutgoingPortsCheck;
    QCheckBox *upnpCheck;
    QCheckBox *natpmpCheck;
    QCheckBox *peerExchangeCheck;
    QCheckBox *lsdCheck;
    QCheckBox *dhtCheck;
    QCheckBox *encryptEntireStreamCheck;
    QRadioButton *fullAllocationRButton;
    QRadioButton *compactAllocationRButton;
    QPushButton *associateTorrentFilesButton;
    QPushButton *associateMagnetsButton;
    QSpinBox *totalActiveSpin;
    QSpinBox *totalActiveDownloadingSpin;
    QSpinBox *totalActiveSeedingSpin;
    QSpinBox *seedTimeSpin;
    QSpinBox *maxConnectionsSpin;
    QSpinBox *maxUploadSlotsSpin;
    QSpinBox *maxDownSpeedSpin;
    QSpinBox *maxUpSpeedSpin;
    QSpinBox *maxHalfOpenConnectionsSpin;
    QSpinBox *maxConnectionsPerSecondSpin;
    QSpinBox *maxConnectionsPerTorrentSpin;
    QSpinBox *maxUploadSlotsPerTorrentSpin;
    QSpinBox *maxDownSpeedPerTorrentSpin;
    QSpinBox *maxUpSpeedPerTorrentSpin;
    QSpinBox *incomingPortsFromSpin;
    QSpinBox *incomingPortsToSpin;
    QSpinBox *outgoingPortsFromSpin;
    QSpinBox *outgoingPortsToSpin;
    QDoubleSpinBox *shareRatioLimitSpin;
    QDoubleSpinBox *seedTimeRatioSpin;
    QDoubleSpinBox *stopRatioSpin;
    QPushButton *moveCompletedToButton;
    QPushButton *autoAddTorrentsButton;
    QPushButton *copyTorrentFilesToButton;
    QComboBox *inEncryptionCombo;
    QComboBox *outEncryptionCombo;
    QComboBox *levelEncryptionCombo;
    QLabel *activePortValueLabel;
    void load_state();
    void set_config();

public:
    explicit PreferencesDialog(Components *comp, QWidget *parent = 0);
    void show_preferences_dialog();

private slots:
    void cancelButtonClicked();
    void applyButtonClicked();
    void okButtonClicked();
    void changePage(QModelIndex selected, QModelIndex deselected);
    void downloadToClicked();
    void moveCompletedToClicked();
    void autoAddTorrentsClicked();
    void copyTorrentFilesToClicked();
    void associateTorrentFilesClicked();
    void associateMagnetsClicked();
    void moveCompletedToToggled(bool checked);
    void autoAddTorrentsToggled(bool checked);
    void copyTorrentFilesToToggled(bool checked);
    void stopRatioToggled(bool checked);
    void useRandomPortsToggled(bool checked);
    void useRandomOutgoingPortsToggled(bool checked);
};

#endif // PREFERENCESDIALOG_H
