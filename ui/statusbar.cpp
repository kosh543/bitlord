#include "statusbar.h"
#include "components.h"
#include "common.h"

#include "libtorrent/session_status.hpp"

#include <QCoreApplication>
#include <QDir>
#include <QSettings>

StatusBar::StatusBar(Components *comp, QWidget *parent) :
    QStatusBar(parent)
{
    this->components = comp;

    QSettings settings;

    num_connections = 0;
    num_connections_max = settings.value("coreconfig/max_connections_global").toInt();
    down_speed = 0;
    down_speed_max = settings.value("coreconfig/max_download_speed").toInt();
    up_speed = 0;
    up_speed_max = settings.value("coreconfig/max_upload_speed").toInt();
    download_protocol_rate = 1.0;
    upload_protocol_rate = 1.0;
    dht_state = settings.value("coreconfig/dht").toBool();
    dht_nodes = 0;
    health = false;

    QLabel *connPix = new QLabel();
    connPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/connections16.png")));
    connPix->setToolTip("Connections");
    connText = new QLabel();
    this->update_num_connections();
    connText->setToolTip("Connections");

    QLabel *downPix = new QLabel();
    downPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/downarrowbottom16.png")));
    downPix->setToolTip("Download Speed");
    downText = new QLabel("0.0 KiB/s");
    this->update_down_speed();
    downText->setToolTip("Download Speed");

    QLabel *upPix = new QLabel();
    upPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/uparrowbottom16.png")));
    upPix->setToolTip("Upload Speed");
    upText = new QLabel();
    this->update_up_speed();
    upText->setToolTip("Upload Speed");

    QLabel *updownPix = new QLabel();
    updownPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/upandownratio16.png")));
    updownPix->setToolTip("Protocol Traffic Download/Upload");
    updownText = new QLabel("1.0/1.0 KiB/s");
    updownText->setToolTip("Protocol Traffic Download/Upload");

    noincomPix = new QLabel();
    noincomPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/noincomingconnections16.png")));
    noincomPix->setToolTip("No Incoming Connections!");

    dhtPix = new QLabel();
    dhtPix->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/dht16.png")));
    dhtPix->setToolTip("DHT Nodes");
    dhtText = new QLabel("0");
    dhtText->setToolTip("DHT Nodes");
    if(!dht_state)
    {
        dhtPix->hide();
        dhtText->hide();
    }

    this->setStyleSheet(
                "QStatusBar::item { border: 0px; } "
                );
    this->addWidget(connPix);
    this->addWidget(connText);
    this->addWidget(downPix);
    this->addWidget(downText);
    this->addWidget(upPix);
    this->addWidget(upText);
    this->addWidget(updownPix);
    this->addWidget(updownText);
    this->addWidget(noincomPix);
    this->addWidget(dhtPix);
    this->addWidget(dhtText);

    this->update_timer = new QTimer(this);
    this->update_timer->setInterval(3000);
    this->connect(this->update_timer, SIGNAL(timeout()), this, SLOT(update_timer_tick()));

    this->update_timer->start();
}

void StatusBar::update_timer_tick()
{
    int new_num_connections = this->components->core->session->num_connections();
    if(new_num_connections != this->num_connections)
    {
        this->num_connections = new_num_connections;
        this->update_num_connections();
    }
    libtorrent::session_status status = this->components->core->session->status();
    int new_down_speed = status.payload_download_rate;
    if(new_down_speed != this->down_speed)
    {
        this->down_speed = new_down_speed;
        this->update_down_speed();
    }
    int new_up_speed = status.payload_upload_rate;
    if(new_up_speed != this->up_speed)
    {
        this->up_speed = new_up_speed;
        this->update_up_speed();
    }
    double new_download_protocol_rate = (double)(status.download_rate - status.payload_download_rate) / 1024.0;
    double new_upload_protocol_rate = (double)(status.upload_rate - status.payload_upload_rate) / 1024.0;
    if(new_download_protocol_rate != this->download_protocol_rate || new_upload_protocol_rate != this->upload_protocol_rate)
    {
        this->download_protocol_rate = new_download_protocol_rate;
        this->upload_protocol_rate = new_upload_protocol_rate;
        this->update_traffic();
    }
    if(this->dht_state)
    {
        int new_dht_nodes = status.dht_nodes;
        if(new_dht_nodes != this->dht_nodes)
        {
            this->dht_nodes = new_dht_nodes;
            this->update_dht_nodes();
        }
    }
    if(!this->health)
    {
        this->health = status.has_incoming_connections;
        if(this->health)
            this->noincomPix->hide();
    }
}

void StatusBar::update_num_connections()
{
    if(this->num_connections_max < 0)
        this->connText->setText(QString::number(this->num_connections));
    else
        this->connText->setText(QString::number(this->num_connections) + " (" +
                                QString::number(this->num_connections_max) + ")");
}

void StatusBar::update_down_speed()
{
    if(this->down_speed_max <= 0)
        this->downText->setText(fsize(this->down_speed) + "/s");
    else
        this->downText->setText(fsize(this->down_speed) + "/s (" +
                                QString::number(this->down_speed_max) + " KiB/s)");
}

void StatusBar::update_up_speed()
{
    if(this->up_speed_max <= 0)
        this->upText->setText(fsize(this->up_speed) + "/s");
    else
        this->upText->setText(fsize(this->up_speed) + "/s (" +
                              QString::number(this->up_speed_max) + " KiB/s)");
}

void StatusBar::update_traffic()
{
    this->updownText->setText(QString::number(this->download_protocol_rate, 'f', 1) + "/" +
                              QString::number(this->upload_protocol_rate, 'f', 1) + " KiB/s");
}

void StatusBar::update_dht_nodes()
{
    this->dhtText->setText(QString::number(this->dht_nodes));
}

void StatusBar::max_connections_global_changed(int value)
{
    this->num_connections_max = value;
    this->update_num_connections();
}

void StatusBar::max_download_speed_changed(int value)
{
    this->down_speed_max = value;
    this->update_down_speed();
}

void StatusBar::max_upload_speed_changed(int value)
{
    this->up_speed_max = value;
    this->update_up_speed();
}

void StatusBar::dht_state_changed(bool value)
{
    this->dht_state = value;
    if(value)
    {
        this->dhtPix->show();
        this->dhtText->show();
        this->update_dht_nodes();
    }
    else
    {
        this->dhtPix->hide();
        this->dhtText->hide();
    }
}
