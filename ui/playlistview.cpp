#include "playlistview.h"
#include "components.h"

PlaylistView::PlaylistView(Components *comp, QWidget *parent) :
    QTreeView(parent)
{
    this->components = comp;

    this->model = new QStandardItemModel(this);

    this->setModel(this->model);

    this->setRootIsDecorated(false);
    this->setStyleSheet("background-color:transparent;");
    this->setStyleSheet("background-image:url("
                        + QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/app-bot-overlay.png")
                        + ");");

    QAction *playFileAction = new QAction("Play", this);
    QAction *removeFileAction = new QAction("Remove", this);

    this->contextMenu = new QMenu(this);
    this->contextMenu->addAction(playFileAction);
    this->contextMenu->addAction(removeFileAction);

    this->setContextMenuPolicy(Qt::CustomContextMenu);

    this->real_load_timer = new QTimer(this);
    this->real_load_timer->setSingleShot(true);
    this->real_load_timer->setInterval(0);
    this->connect(this->real_load_timer, SIGNAL(timeout()), this, SLOT(real_load_state()));

    this->connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowContextMenu(QPoint)));
    this->connect(playFileAction, SIGNAL(triggered()), this, SLOT(playFileActivated()));
    this->connect(removeFileAction, SIGNAL(triggered()), this, SLOT(removeFileActivated()));
}

void PlaylistView::load_state()
{
    this->real_load_timer->start();
}

void PlaylistView::real_load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("playlists.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        QMap<QString, QList<QString> > files;
        in >> files;
        foreach(QString playlist, files.keys())
        {
            this->addPlaylist(playlist, "");
            foreach(QString file, files[playlist])
            {
                this->addFile(playlist, file);
                QCoreApplication::processEvents();
            }
            this->components->filterTreeView->add_playlist(playlist);
        }
        fnew.close();
    }
}

void PlaylistView::save_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);

    QFile file(QDir(dir).absoluteFilePath("playlists.state"));
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream out(&file);
        out << this->filesLocation;
        file.flush();
        file.close();
    }
}

void PlaylistView::addPlaylist(QString playlistName, QString oldPlaylistName)
{
    if(oldPlaylistName.isEmpty() || !this->playlists.contains(oldPlaylistName))
    {
        this->playlists.insert(playlistName, new VlcMediaList(this->components->player->get_instance()));
        this->filesLocation.insert(playlistName, QList<QString>());
    }
    else
    {
        this->playlists.insert(playlistName, this->playlists[oldPlaylistName]);
        this->playlists.remove(oldPlaylistName);
        this->filesLocation.insert(playlistName, this->filesLocation[oldPlaylistName]);
        this->filesLocation.remove(oldPlaylistName);
    }
}

void PlaylistView::setPlaylist(QString playlist)
{
    this->currentPlaylist = playlist;
    this->updateViewContent();
}

void PlaylistView::setColumnsName()
{
    QStringList labels;
    labels << "Track";
    labels << "Title";
    labels << "Duration";
    this->model->setColumnCount(3);
    this->model->setHorizontalHeaderLabels(labels);
}

void PlaylistView::updateViewContent()
{
    if(this->playlists.contains(this->currentPlaylist))
    {
        this->model->clear();
        this->setColumnsName();
        for(int i = 0; i < this->playlists[this->currentPlaylist]->count(); ++i)
        {
            VlcMedia *media = this->playlists[this->currentPlaylist]->at(i);
            this->appendRowFromMedia(media);
        }
    }
}

QString PlaylistView::checkPlaylistName(QString name)
{
    if(!this->playlists.contains(name))
        return name;
    int i = 1;
    while(true)
    {
        QString uniqName = name + QString::number(i);
        if(!this->playlists.contains(uniqName))
            return uniqName;
        i++;
    }
}

void PlaylistView::clearPlaylistsView()
{
    this->model->clear();
}

void PlaylistView::removePlaylist(QString name)
{
    this->playlists.remove(name);
    this->filesLocation.remove(name);
}

void PlaylistView::appendRowFromMedia(VlcMedia* media)
{
    VlcMetaManager meta(media);
    libvlc_media_t *media_t = media->core();
    if(!libvlc_media_is_parsed(media_t))
        libvlc_media_parse(media_t);
    int d = libvlc_media_get_duration(media_t);
    QTime durationTime = QTime(0, 0).addMSecs(d);
    QString timeFormat;
    if(durationTime.hour() > 0)
        timeFormat = "hh:mm:ss";
    else
        timeFormat = "mm:ss";
    QList<QStandardItem*> items;
    items << new QStandardItem(QString::number(meta.number()));
    items << new QStandardItem(meta.title());
    items << new QStandardItem(durationTime.toString(timeFormat));
    items[1]->setData(media->currentLocation(), Qt::UserRole);
    this->model->appendRow(items);
}

void PlaylistView::addFile(QString playlist, QString file)
{
    if(this->playlists.contains(playlist) && !this->filesLocation[playlist].contains(file))
    {
        VlcMedia *media = new VlcMedia(file, true, this->components->player->get_instance());
        this->playlists[playlist]->addMedia(media);
        this->filesLocation[playlist].append(file);
        this->appendRowFromMedia(media);
    }
}

void PlaylistView::addFiles(QString playlist, QList<QString> files)
{
    foreach(QString file, files)
    {
        this->addFile(playlist, file);
        QCoreApplication::processEvents();
    }
}

VlcMediaList* PlaylistView::getCurrentMediaList()
{
    if(this->playlists.contains(this->currentPlaylist))
        return this->playlists[this->currentPlaylist];
    return NULL;
}

void PlaylistView::playFileActivated()
{
    QModelIndex index = this->selectionModel()->currentIndex();
    //this->components->player->show_media(this->playlists[this->currentPlaylist]->at(index.row()));
    this->components->player->show_media_list(this->playlists[this->currentPlaylist], index.row());
}

void PlaylistView::removeFileActivated()
{
    /*QModelIndex index = this->selectionModel()->currentIndex();
    this->playlists[this->currentPlaylist].removeOne(index.data().toString());
    this->model->removeRow(index.row(), index.parent());*/
    QModelIndex index = this->selectionModel()->currentIndex();
    this->playlists[this->currentPlaylist]->removeMedia(index.row());
    this->filesLocation[this->currentPlaylist].removeAt(index.row());
    this->model->removeRow(index.row(), index.parent());
}

void PlaylistView::ShowContextMenu(const QPoint& pos)
{
    if(this->indexAt(pos).row() > -1)
        this->contextMenu->exec(this->mapToGlobal(pos));
}
