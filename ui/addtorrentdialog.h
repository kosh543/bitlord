#ifndef ADDTORRENTDIALOG_H
#define ADDTORRENTDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QListView>
#include <QTreeView>
#include <QStandardItemModel>
#include <QTabWidget>
#include <QRadioButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QLabel>
#include <QClipboard>

#include "libtorrent/torrent_info.hpp"

#include "common.h"
#include "core/torrentoptions.h"

class Components;

class AddFromURLDialog : public QDialog
{
    Q_OBJECT
private:
    QClipboard* cb;
    QLineEdit* URLEntry;

public:
    explicit AddFromURLDialog(QWidget *parent = 0);
    QString show_add_from_url_dialog();

public slots:

};

class AddTorrentDialog : public QDialog
{
    Q_OBJECT
private:
    Components *components;
    int previous_selected_torrent;
    QIcon folder_icon;
    QIcon file_icon;
    bool compact;
    QMap<QString, QList<QMap<QString, QVariant> > > files;
    QMap<QString, libtorrent::torrent_info*> infos;
    QMap<QString, TorrentOptions*> options;
    QStandardItemModel *torrentsModel;
    QTreeView *torrentsListView;
    QStandardItemModel *filesModel;
    QTreeView *filesView;
    QLineEdit *downloadLocationText;
    QRadioButton *fullRButton;
    QRadioButton *compactRButton;
    QSpinBox *maxDownSpeedSpin;
    QSpinBox *maxUpSpeedSpin;
    QSpinBox *maxConnectionsSpin;
    QSpinBox *maxUploadSlotsSpin;
    QCheckBox *addPauseCheck;
    QCheckBox *priorFLCheck;
    QCheckBox *streamingCheck;
    AddFromURLDialog* addFromURLDialog;
    void set_default_options();
    void save_torrent_options(int row = -1);
    void build_priorities(QStandardItem* parent, QMap<int, bool> &priorities);
    void prepare_file_store(QList<QMap<QString, QVariant> > files);
    void prepare_file(QMap<QString, QVariant> &file, QString file_name, int file_num,
                      bool download, QMap<QString, QVariant> &files_storage);
    void add_files(QStandardItem *parent_index, QMap<QString, QVariant> split_files);
    void set_correct_check_state(QStandardItem* item);
    void set_checked_for_all(QStandardItem* parent);
    void update_torrent_options(QString torrent_id);
    QVector<int> get_file_priorities(QString torrent_id);
    QString get_file_path(QModelIndex index, QString path = "");
public:
    explicit AddTorrentDialog(Components *comp, QWidget *parent = 0);
    void show_add_torrent_dialog();
    void hide_add_torrent_dialog();
    void add_from_files(QList<QString> filenames);
    void add_from_magnets(QList<QString> magnets);
    void add_from_url(QString url);
    void receive_message(QString msg);
    void addFromRSS(QString msg);

signals:

public slots:
    void initialize();
    void torrent_file_downloaded(QString filePath);
    void _on_dialog_destroyed(int result);
    void _on_files_item_changed(QStandardItem* item);
    void _on_torrent_changed(QModelIndex selected, QModelIndex deselected);
    void _on_compact_toggled(bool checked);
    void _on_button_file_clicked();
    void _on_button_url_clicked();
    void _on_button_hash_clicked();
    void _on_button_remove_clicked();
    void _on_button_trackers_clicked();
    void _on_button_cancel_clicked();
    void _on_button_add_clicked();
    void _on_button_apply_clicked();
    void _on_button_revert_clicked();
    void _on_button_select_all_clicked();
    void _on_button_unselect_all_clicked();
    void _on_button_browse_location_clicked();
    void _on_chk_streaming_toggled(bool checked);

};

#endif // ADDTORRENTDIALOG_H
