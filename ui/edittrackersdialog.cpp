#include "edittrackersdialog.h"
#include "components.h"
#include "common.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

EditTrackerDialog::EditTrackerDialog(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowTitle("Edit Tracker");

    trackerEditLine = new QLineEdit(this);

    QHBoxLayout *editLayout = new QHBoxLayout();
    editLayout->addWidget(new QLabel("Tracker:", this));
    editLayout->addWidget(trackerEditLine);

    QPushButton *cancelButton = new QPushButton("Cancel", this);
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *okButton = new QPushButton("OK", this);
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(okButton);
    buttonsLayout->setAlignment(Qt::AlignRight);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(editLayout);
    mainLayout->addLayout(buttonsLayout);

    this->setLayout(mainLayout);
    this->resize(400, this->height());
}

QString EditTrackerDialog::show_edit_tracker_dialog(QString tracker)
{
    this->trackerEditLine->setText(tracker);
    this->exec();
    if(this->result() == QDialog::Accepted)
        return this->trackerEditLine->text();
    return "";
}

AddTrackersDialog::AddTrackersDialog(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowTitle("Add Trackers");

    QLabel *label = new QLabel("Trackers:", this);
    label->setAlignment(Qt::AlignTop);

    trackersEdit = new QTextEdit(this);

    QHBoxLayout *editLayout = new QHBoxLayout();
    editLayout->addWidget(label);
    editLayout->addWidget(trackersEdit);

    QPushButton *cancelButton = new QPushButton("Cancel", this);
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *okButton = new QPushButton("OK", this);
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(okButton);
    buttonsLayout->setAlignment(Qt::AlignRight);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(editLayout);
    mainLayout->addLayout(buttonsLayout);

    this->setLayout(mainLayout);
    this->resize(400, 200);
}

QList<QString> AddTrackersDialog::show_add_trackers_dialog()
{
    this->trackersEdit->clear();
    this->exec();
    if(this->result() == QDialog::Accepted)
        return this->trackersEdit->toPlainText().split("\n");
    return QList<QString>();
}

EditTrackersDialog::EditTrackersDialog(Components *comp, QString torrent_id, QWidget *parent) :
    QDialog(parent)
{
    this->components = comp;
    this->torrent_id = torrent_id;

    this->editTrackerDialog = new EditTrackerDialog(this);
    this->addTrackersDialog = new AddTrackersDialog(this);

    this->setWindowTitle("Edit Trackers");

    trackersView = new QTreeView(this);
    trackersModel = new QStandardItemModel(0, 2);
    trackersView->setModel(trackersModel);
    trackersView->setSelectionBehavior(QAbstractItemView::SelectRows);
    trackersView->setRootIsDecorated(false);
    trackersView->setSelectionMode(QAbstractItemView::SingleSelection);
    trackersView->sortByColumn(0, Qt::AscendingOrder);
    QStringList trackers_labels;
    trackers_labels << "Tier" << "Tracker";
    trackersModel->setHorizontalHeaderLabels(trackers_labels);
    trackersView->setColumnWidth(0, 30);
    trackersView->header()->setSectionResizeMode(0, QHeaderView::Fixed);

    QPushButton *upTracker = new QPushButton("Up", this);
    this->connect(upTracker, SIGNAL(clicked()), this, SLOT(on_button_up_clicked()));

    QPushButton *addTracker = new QPushButton("Add", this);
    this->connect(addTracker, SIGNAL(clicked()), this, SLOT(on_button_add_clicked()));

    QPushButton *editTracker = new QPushButton("Edit", this);
    this->connect(editTracker, SIGNAL(clicked()), this, SLOT(on_button_edit_clicked()));

    QPushButton *removeTracker = new QPushButton("Remove", this);
    this->connect(removeTracker, SIGNAL(clicked()), this, SLOT(on_button_remove_clicked()));

    QPushButton *downTracker = new QPushButton("Down", this);
    this->connect(downTracker, SIGNAL(clicked()), this, SLOT(on_button_down_clicked()));

    QVBoxLayout *controlsLayout = new QVBoxLayout();
    controlsLayout->addWidget(upTracker);
    controlsLayout->addWidget(addTracker);
    controlsLayout->addWidget(editTracker);
    controlsLayout->addWidget(removeTracker);
    controlsLayout->addWidget(downTracker);

    QHBoxLayout *trackersLayout = new QHBoxLayout();
    trackersLayout->addWidget(trackersView);
    trackersLayout->addLayout(controlsLayout);

    QPushButton *cancelButton = new QPushButton("Cancel", this);
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *okButton = new QPushButton("OK", this);
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(okButton);
    buttonsLayout->setAlignment(Qt::AlignRight);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(trackersLayout);
    mainLayout->addLayout(buttonsLayout);

    this->setLayout(mainLayout);
    this->resize(400, 270);
}

void EditTrackersDialog::show_edit_trackers_dialog()
{
    if(this->torrent_id.isEmpty())
        return;

    QList<QString> status_keys;
    status_keys << "trackers";
    QList<QMap<QString, QVariant> > trackers = this->components->torrentManager->torrents[this->torrent_id]->trackers;
    QMap<QString, QVariant> tracker;
    foreach(tracker, trackers)
    {
        QList<QStandardItem*> items;
        items << new QStandardItem();
        items << new QStandardItem(tracker["url"].toString());
        items[0]->setData(tracker["tier"], Qt::DisplayRole);
        this->trackersModel->appendRow(items);
    }
    this->trackersView->sortByColumn(0, Qt::AscendingOrder);
    this->exec();
    if(this->result() == QDialog::Accepted)
    {
        QList<QMap<QString, QVariant> > trackers;
        for(int i = 0; i < this->trackersModel->rowCount(); ++i)
        {
            QMap<QString, QVariant> tracker;
            tracker["url"] = this->trackersModel->data(this->trackersModel->index(i, 1));
            tracker["tier"] = this->trackersModel->data(this->trackersModel->index(i, 0));
            trackers.append(tracker);
        }
        this->components->torrentManager->torrents[this->torrent_id]->set_trackers(trackers);
    }
}

void EditTrackersDialog::on_button_up_clicked()
{
    QModelIndexList selected = this->trackersView->selectionModel()->selectedRows();
    if(!selected.isEmpty())
    {
        int num_rows = this->trackersModel->rowCount();
        if(num_rows > 1)
        {
            int tier = selected[0].data().toInt() + 1;
            this->trackersModel->setData(selected[0], tier, Qt::DisplayRole);
            this->trackersView->sortByColumn(0, Qt::AscendingOrder);
        }
    }
}

void EditTrackersDialog::on_button_add_clicked()
{
    foreach(QString new_tracker, this->addTrackersDialog->show_add_trackers_dialog())
    {
        if(is_url(new_tracker) && this->trackersModel->findItems(new_tracker, Qt::MatchExactly, 1).isEmpty())
        {
            int tier = this->trackersModel->index(this->trackersModel->rowCount()-1, 0).data().toInt() + 1;
            QList<QStandardItem*> items;
            items << new QStandardItem();
            items << new QStandardItem(new_tracker);
            items[0]->setData(tier, Qt::DisplayRole);
            this->trackersModel->appendRow(items);
        }
    }
}

void EditTrackersDialog::on_button_edit_clicked()
{
    QModelIndexList selected = this->trackersView->selectionModel()->selectedRows(1);
    if(!selected.isEmpty())
    {
        QString new_tracker = this->editTrackerDialog->show_edit_tracker_dialog(selected[0].data().toString());
        if(!new_tracker.isEmpty())
            this->trackersModel->setData(selected[0], new_tracker, Qt::DisplayRole);
    }
}

void EditTrackersDialog::on_button_remove_clicked()
{
    QModelIndexList selected = this->trackersView->selectionModel()->selectedRows();
    if(!selected.isEmpty())
        this->trackersModel->removeRow(selected[0].row());
}

void EditTrackersDialog::on_button_down_clicked()
{
    QModelIndexList selected = this->trackersView->selectionModel()->selectedRows();
    if(!selected.isEmpty())
    {
        int num_rows = this->trackersModel->rowCount();
        if(num_rows > 1)
        {
            int tier = selected[0].data().toInt();
            if(tier == 0)
                return;
            tier -= 1;
            this->trackersModel->setData(selected[0], tier, Qt::DisplayRole);
            this->trackersView->sortByColumn(0, Qt::AscendingOrder);
        }
    }
}
