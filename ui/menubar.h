#ifndef MENUBAR_H
#define MENUBAR_H

#include <QApplication>
#include <QMenuBar>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QObject>

class Components;

class MenuBar : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QMenu *torrentMenu;
public:
    MenuBar(Components *comp, QWidget *parent = 0);
    QMenu* getTorrentMenu();

private slots:
    void addTorrentActivate();
    void openFolderActivate();
    void pauseTorrentActivate();
    void resumeTorrentActivate();
    void downloadSpeedSetUnlimitedActivate();
    void downloadSpeedSetOtherActivate();
    void uploadSpeedSetUnlimitedActivate();
    void uploadSpeedSetOtherActivate();
    void connectionSetUnlimitedActivate();
    void connectionSetOtherActivate();
    void uploadSlotSetUnlimitedActivate();
    void uploadSlotSetOtherActivate();
    void autoManagedOnActivate();
    void autoManagedOffActivate();
    void queueTopActivate();
    void queueUpActivate();
    void queueDownActivate();
    void queueBottomActivate();
    void updateTrackerActivate();
    void editTrackersActivate();
    void removeTorrentActivate();
    void forceRecheckActivate();
    void moveStorageActivate();
    void sidebarActivate(bool checked);
    void nameColumnActivate(bool checked);
    void progressColumnActivate(bool checked);
    void numColumnActivate(bool checked);
    void sizeColumnActivate(bool checked);
    void seedersColumnActivate(bool checked);
    void peersColumnActivate(bool checked);
    void downSpeedColumnActivate(bool checked);
    void upSpeedColumnActivate(bool checked);
    void etaColumnActivate(bool checked);
    void ratioColumnActivate(bool checked);
    void availColumnActivate(bool checked);
    void addedColumnActivate(bool checked);
    void trackerColumnActivate(bool checked);
    void showZeroHitsActivate(bool checked);
    void preferencesActivate();
};

#endif // MENUBAR_H
