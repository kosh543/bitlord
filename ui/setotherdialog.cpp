#include "setotherdialog.h"

#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>

SetOtherDialog::SetOtherDialog(QString main_text, QString type_text, QWidget *parent) :
    QDialog(parent)
{
    this->setWindowTitle(" ");

    QLabel *main_label = new QLabel(main_text, this);

    result_spin = new QSpinBox(this);
    result_spin->setRange(-1, INT_MAX);

    QLabel *type_label = new QLabel(type_text, this);

    QHBoxLayout *result_layout = new QHBoxLayout();
    result_layout->addWidget(result_spin);
    result_layout->addWidget(type_label);

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *okButton = new QPushButton("OK");
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

    QHBoxLayout *buttons_layout = new QHBoxLayout();
    buttons_layout->addWidget(cancelButton);
    buttons_layout->addWidget(okButton);
    buttons_layout->setAlignment(Qt::AlignRight);

    QVBoxLayout *main_layout = new QVBoxLayout();
    main_layout->addWidget(main_label);
    main_layout->addLayout(result_layout);
    main_layout->addLayout(buttons_layout);

    this->setLayout(main_layout);
}

int SetOtherDialog::show_set_other_dialog()
{
    this->exec();
    if(this->result() == QDialog::Accepted)
        return this->result_spin->value();
    return -2;
}
