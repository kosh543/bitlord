#include "statustab.h"
#include "common.h"
#include "components.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QList>
#include <QString>

QString fratio(double value)
{
    if(value < 0)
        return "∞";
    return QString::number(value, 'f', 3);
}

QString fspeed_local(int value, int max_value = -1)
{
    if(max_value > -1)
        return fspeed(value) + " [" + QString::number(max_value) + " KiB/s]";
    return fspeed(value);
}

StatusTab::StatusTab(Components *comp, QWidget *parent) :
    QWidget(parent)
{
    this->components = comp;

    QVBoxLayout *vBox = new QVBoxLayout(this);
    this->setLayout(vBox);

    progressBar = new QProgressBar(this);
    progressBar->setMinimum(0);
    progressBar->setMaximum(100);
    vBox->addWidget(progressBar);

    QLabel *activeTimeLabel = new QLabel("<b>Active Time:</b>");

    activeTime = new QLabel("");

    QHBoxLayout *activeTimeLayout = new QHBoxLayout();
    activeTimeLayout->addWidget(activeTimeLabel);
    activeTimeLayout->addWidget(activeTime);

    QLabel *downloadedLabel = new QLabel("<b>Downloaded:</b>");

    downloaded = new QLabel("");

    QHBoxLayout *downloadedLayout = new QHBoxLayout();
    downloadedLayout->addWidget(downloadedLabel);
    downloadedLayout->addWidget(downloaded);

    QLabel *uploadedLabel = new QLabel("<b>Uploaded:</b>");

    uploaded = new QLabel("");

    QHBoxLayout *uploadedLayout = new QHBoxLayout();
    uploadedLayout->addWidget(uploadedLabel);
    uploadedLayout->addWidget(uploaded);

    QLabel *remainingLabel = new QLabel("<b>Remaining:</b>");

    remaining = new QLabel("");

    QHBoxLayout *remainingLayout = new QHBoxLayout();
    remainingLayout->addWidget(remainingLabel);
    remainingLayout->addWidget(remaining);

    QLabel *nextAnnounceLabel = new QLabel("<b>Next Announce:</b>");

    nextAnnounce = new QLabel("");

    QHBoxLayout *nextAnnounceLayout = new QHBoxLayout();
    nextAnnounceLayout->addWidget(nextAnnounceLabel);
    nextAnnounceLayout->addWidget(nextAnnounce);

    QVBoxLayout *firstColumn = new QVBoxLayout();
    firstColumn->addLayout(activeTimeLayout);
    firstColumn->addLayout(downloadedLayout);
    firstColumn->addLayout(uploadedLayout);
    firstColumn->addLayout(remainingLayout);
    firstColumn->addLayout(nextAnnounceLayout);

    QLabel *etaLabel = new QLabel("<b>ETA:</b>");

    eta = new QLabel("");

    QHBoxLayout *etaLayout = new QHBoxLayout();
    etaLayout->addWidget(etaLabel);
    etaLayout->addWidget(eta);

    QLabel *downSpeedLabel = new QLabel("<b>Down Speed:</b>");

    downSpeed = new QLabel("");

    QHBoxLayout *downSpeedLayout = new QHBoxLayout();
    downSpeedLayout->addWidget(downSpeedLabel);
    downSpeedLayout->addWidget(downSpeed);

    QLabel *upSpeedLabel = new QLabel("<b>Up Speed:</b>");

    upSpeed = new QLabel("");

    QHBoxLayout *upSpeedLayout = new QHBoxLayout();
    upSpeedLayout->addWidget(upSpeedLabel);
    upSpeedLayout->addWidget(upSpeed);

    QLabel *piecesLabel = new QLabel("<b>Pieces:</b>");

    pieces = new QLabel("");

    QHBoxLayout *piecesLayout = new QHBoxLayout();
    piecesLayout->addWidget(piecesLabel);
    piecesLayout->addWidget(pieces);

    QLabel *trackerStatusLabel = new QLabel("<b>Tracker Status:</b>");

    trackerStatus = new QLabel("");

    QHBoxLayout *trackerStatusLayout = new QHBoxLayout();
    trackerStatusLayout->addWidget(trackerStatusLabel);
    trackerStatusLayout->addWidget(trackerStatus);

    QVBoxLayout *secondColumn = new QVBoxLayout();
    secondColumn->addLayout(etaLayout);
    secondColumn->addLayout(downSpeedLayout);
    secondColumn->addLayout(upSpeedLayout);
    secondColumn->addLayout(piecesLayout);
    secondColumn->addLayout(trackerStatusLayout);

    QLabel *seedersLabel = new QLabel("<b>Seeders:</b>");

    seeders = new QLabel("");

    QHBoxLayout *seedersLayout = new QHBoxLayout();
    seedersLayout->addWidget(seedersLabel);
    seedersLayout->addWidget(seeders);

    QLabel *peersLabel = new QLabel("<b>Peers:</b>");

    peers = new QLabel("");

    QHBoxLayout *peersLayout = new QHBoxLayout();
    peersLayout->addWidget(peersLabel);
    peersLayout->addWidget(peers);

    QLabel *shareRatioLabel = new QLabel("<b>Share Ratio:</b>");

    shareRatio = new QLabel("");

    QHBoxLayout *shareRatioLayout = new QHBoxLayout();
    shareRatioLayout->addWidget(shareRatioLabel);
    shareRatioLayout->addWidget(shareRatio);

    QLabel *autoManagedLabel = new QLabel("<b>Auto Managed:</b>");

    autoManaged = new QLabel("");

    QHBoxLayout *autoManagedLayout = new QHBoxLayout();
    autoManagedLayout->addWidget(autoManagedLabel);
    autoManagedLayout->addWidget(autoManaged);

    QVBoxLayout *thirdColumn = new QVBoxLayout();
    thirdColumn->addLayout(seedersLayout);
    thirdColumn->addLayout(peersLayout);
    thirdColumn->addLayout(shareRatioLayout);
    thirdColumn->addLayout(autoManagedLayout);

    QLabel *availabilityLabel = new QLabel("<b>Availability:</b>");

    availability = new QLabel("");

    QHBoxLayout *availabilityLayout = new QHBoxLayout();
    availabilityLayout->addWidget(availabilityLabel);
    availabilityLayout->addWidget(availability);

    QLabel *seedingTimeLabel = new QLabel("<b>Seeding Time:</b>");

    seedingTime = new QLabel("");

    QHBoxLayout *seedingTimeLayout = new QHBoxLayout();
    seedingTimeLayout->addWidget(seedingTimeLabel);
    seedingTimeLayout->addWidget(seedingTime);

    QLabel *seedRankLabel = new QLabel("<b>Seed Rank:</b>");

    seedRank = new QLabel("");

    QHBoxLayout *seedRankLayout = new QHBoxLayout();
    seedRankLayout->addWidget(seedRankLabel);
    seedRankLayout->addWidget(seedRank);

    QLabel *dateAddedLabel = new QLabel("<b>Date Added:</b>");

    dateAdded = new QLabel("");

    QHBoxLayout *dateAddedLayout = new QHBoxLayout();
    dateAddedLayout->addWidget(dateAddedLabel);
    dateAddedLayout->addWidget(dateAdded);

    QVBoxLayout *fourthColumn = new QVBoxLayout();
    fourthColumn->addLayout(availabilityLayout);
    availabilityLayout->setAlignment(Qt::AlignTop);
    fourthColumn->addLayout(seedingTimeLayout);
    seedingTimeLayout->setAlignment(Qt::AlignTop);
    fourthColumn->addLayout(seedRankLayout);
    seedRankLayout->setAlignment(Qt::AlignTop);
    fourthColumn->addLayout(dateAddedLayout);
    dateAddedLayout->setAlignment(Qt::AlignTop);

    QHBoxLayout *statusBox = new QHBoxLayout();
    statusBox->addLayout(firstColumn);
    statusBox->addLayout(secondColumn);
    statusBox->addLayout(thirdColumn);
    statusBox->addLayout(fourthColumn);

    vBox->addLayout(statusBox);

}

void StatusTab::update_tab()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
    {
        this->clear_tab();
        return;
    }

    QString selected = selected_torrents[0];

    QList<QString> status_keys;
    status_keys << "progress" << "num_pieces" << "piece_length";
    status_keys << "distributed_copies" << "all_time_download" << "total_payload_download";
    status_keys << "total_uploaded" << "total_payload_upload" << "download_payload_rate";
    status_keys << "upload_payload_rate" << "num_peers" << "num_seeds" << "total_peers";
    status_keys << "total_seeds" << "eta" << "ratio" << "next_announce";
    status_keys << "tracker_status" << "max_connections" << "max_upload_slots";
    status_keys << "max_upload_speed" << "max_download_speed" << "active_time";
    status_keys << "seeding_time" << "seed_rank" << "is_auto_managed" << "time_added" << "remaining";

    QMap<QString, QVariant> status = (*(this->components->torrentManager))[selected]->get_status(status_keys);

    activeTime->setText(ftime_from_seconds(status["active_time"].toInt()));
    downloaded->setText(fsize(status["all_time_download"].toInt()) + " ("
                + fsize(status["total_payload_download"].toInt()) + ")");
    uploaded->setText(fsize(status["total_uploaded"].toInt()) + " ("
            + fsize(status["total_payload_upload"].toInt()) + ")");
    remaining->setText(fsize(status["remaining"].toInt()));
    nextAnnounce->setText(ftime_from_seconds(status["next_announce"].toInt()));
    eta->setText(ftime_from_seconds(status["eta"].toInt()));
    downSpeed->setText(fspeed_local(status["download_payload_rate"].toInt(),
                       status["max_download_speed"].toInt()));
    upSpeed->setText(fspeed_local(status["upload_payload_rate"].toInt(),
                     status["max_upload_speed"].toInt()));
    pieces->setText(QString::number(status["num_pieces"].toInt())
            + " (" + fsize(status["piece_length"].toInt()) + ")");
    trackerStatus->setText(status["tracker_status"].toString());
    seeders->setText(fpeer(status["num_seeds"].toInt(), status["total_seeds"].toInt()));
    peers->setText(fpeer(status["num_peers"].toInt(), status["total_peers"].toInt()));
    shareRatio->setText(fratio(status["ratio"].toDouble()));
    autoManaged->setText(status["is_auto_managed"].toString());
    availability->setText(fratio(status["distributed_copies"].toDouble()));
    seedingTime->setText(ftime_from_seconds(status["seeding_time"].toInt()));
    seedRank->setText(status["seed_rank"].toString());
    dateAdded->setText(fdate(status["time_added"].toDouble()));

    progressBar->setValue(status["progress"].toDouble());
}

void StatusTab::clear_tab()
{
    activeTime->setText("");
    downloaded->setText("");
    uploaded->setText("");
    remaining->setText("");
    nextAnnounce->setText("");
    eta->setText("");
    downSpeed->setText("");
    upSpeed->setText("");
    pieces->setText("");
    trackerStatus->setText("");
    seeders->setText("");
    peers->setText("");
    shareRatio->setText("");
    autoManaged->setText("");
    availability->setText("");
    seedingTime->setText("");
    seedRank->setText("");
    dateAdded->setText("");
    progressBar->setValue(0);
}
