#ifndef TORRENTDETAILS_H
#define TORRENTDETAILS_H

#include "ui/filestab.h"
#include "ui/statustab.h"
#include "ui/detailtab.h"
#include "ui/peerstab.h"
#include "ui/optionstab.h"

#include <QTabWidget>

class Components;

class TorrentDetails : public QTabWidget
{
    Q_OBJECT
public:
    Components *components;
    QTimer *_update_timer;
    FilesTab *filesTab;
    StatusTab *statusTab;
    DetailTab *detailsTab;
    PeersTab *peersTab;
    OptionsTab *optionsTab;
    explicit TorrentDetails(Components *comp, QWidget *parent = 0);
    void save_tabs_state();

public slots:
    void update_details();
};

#endif // TORRENTDETAILS_H
