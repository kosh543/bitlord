#ifndef FILESTAB_H
#define FILESTAB_H

#include <QWidget>
#include <QTreeView>
#include <QStandardItemModel>
#include <QItemDelegate>
#include <QMenu>

class Components;

class FilesTabProgressItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    FilesTabProgressItemDelegate(QObject *parent);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class FilesTab : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QIcon folder_icon;
    QIcon file_icon;
    QMenu *context_menu;
    QMap<QString, QList<QMap<QString, QVariant> > > files_list;
    bool compact;
    QTreeView *filesView;
    QStandardItemModel *filesModel;
    QString current_torrent_id;
    QList<int> columns_width;

    int add_files(QStandardItem *parent_index, QMap<QString, QVariant> split_files);
    void prepare_file_store(QList<QMap<QString, QVariant> > files);
    void prepare_file(QMap<QString, QVariant> &file, QString file_name, int file_num,
                      bool download, QMap<QString, QVariant> &files_storage);
    void get_files_from_tree(QStandardItem *parent, QList<QStandardItem *> &files);
    QString get_file_path(QModelIndex index, QString path);
    void set_file_priorities_on_user_change(QModelIndexList selected, int priority);

public:
    explicit FilesTab(Components *comp, QWidget *parent = 0);
    void update_tab();
    void update_files();
    void clear_tab();
    void load_state();
    void save_state();

private slots:
    void ShowContextMenu(const QPoint& pos);
    void openPathActivated();
    void doNotDownloadActivated();
    void normalPriorityActivated();
    void highPriorityActivated();
    void highestPriorityActivated();
    void onItemChanged(QStandardItem *item);
    void onTorrentRemovedEvent(QString torrent_id);
    void onTorrentFileRenamedEvent(QString torrent_id, int index, QString name);
    void onTorrentFolderRenamedEvent(QString torrent_id, QString old_folder, QString new_folder);
};

#endif // FILESTAB_H
