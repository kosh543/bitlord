#include "rssview.h"
#include "components.h"

RSSView::RSSView(Components *comp, QWidget *parent) :
    QListView(parent)
{
    this->components = comp;

    this->setStyleSheet("background-color:transparent;");
    this->setStyleSheet("background-image:url("
                        + QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/app-bot-overlay.png")
                        + ");");

    this->model = new QStandardItemModel(this);
    this->setModel(this->model);

    this->connect(this, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(rowDoubleClicked(QModelIndex)));
}

void RSSView::setFeed(QString feed)
{
    this->model->clear();
    foreach(QString feedTitle, this->components->rssManager->getFeedTitles(feed))
    {
        QStandardItem *item = new QStandardItem(feedTitle);
        item->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/feed_item16.png")));
        item->setEditable(false);
        this->model->appendRow(item);
    }
}

void RSSView::rowDoubleClicked(QModelIndex index)
{
    this->components->addTorrentDialog->addFromRSS(
                this->components->rssManager->getUrlByIndex(index.row()));
}

void RSSView::clearRSSView()
{
    this->model->clear();
}
