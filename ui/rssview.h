#ifndef RSSVIEW_H
#define RSSVIEW_H

#include <QListView>
#include <QStandardItemModel>

class Components;

class RSSView : public QListView
{
    Q_OBJECT
private:
    Components *components;
    QStandardItemModel* model;

public:
    explicit RSSView(Components *comp, QWidget *parent = 0);
    void setFeed(QString feed);
    void clearRSSView();

private slots:
    void rowDoubleClicked(QModelIndex index);

};

#endif // RSSVIEW_H
