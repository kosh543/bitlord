#ifndef BLPLAYER_H
#define BLPLAYER_H

#include <QDialog>
#include <QMenuBar>
#include <QTimer>
#include <QUrl>
#include <QLabel>
#include <QPixmap>
#include <QStackedWidget>
#include <QPushButton>
#include <QCursor>
#include <QEvent>
#include <QCloseEvent>
#include <QMouseEvent>

#include <vlc/vlc.h>
#include <vlc-qt/Common.h>
#include <vlc-qt/Instance.h>
#include <vlc-qt/Media.h>
#include <vlc-qt/MediaList.h>
#include <vlc-qt/MediaPlayer.h>
#include <vlc-qt/MetaManager.h>
#include <vlc-qt/Video.h>
#include <vlc-qt/Audio.h>
#include <vlc-qt/WidgetVideo.h>
#include <vlc-qt/WidgetVolumeSlider.h>
#include <vlc-qt/WidgetSeek.h>

class Components;

class BLWidgetVideo : public VlcWidgetVideo
{
    Q_OBJECT

public:
    explicit BLWidgetVideo(QWidget *parent = 0);

protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

signals:
    void videoDoubleClicked();
    void videoMouseMove();
    void videoMouseEnter();
    void videoMouseLeave();

};

class BLPlayer : public QDialog
{
    Q_OBJECT
private:
    Components *components;
    QCursor normalCursor;
    QTimer *hideBottomPanelTimer;
    QTimer *completeAudioTracksIdle;
    QTimer *completeSubtitlesIdle;
    bool isAudioTracksCompleted;
    bool isSubtitlesCompleted;
    int currentAudioTrack;
    int currentSubtitle;
    QList<int> audioTracksIndexes;
    QList<int> subtitlesIndexes;
    QMenuBar *mainMenu;
    QMenu *audioTracksMenu;
    QMenu *subtitlesMenu;
    QAction *playPauseAction;
    QPushButton *playPauseButton;
    QPushButton *stopButton;
    QPushButton *prevButton;
    QPushButton *nextButton;
    QLabel *artLabel;
    QStackedWidget *mainView;
    VlcInstance *instance;
    VlcMedia *media;
    VlcMediaList *mediaList;
    VlcMediaPlayer *player;
    BLWidgetVideo *video;
    VlcWidgetSeek *seek;
    VlcWidgetVolumeSlider *volume;
    QWidget *bottomPanel;
    int currentMediaInList;
    bool isPlaying;

public:
    explicit BLPlayer(Components *comp, QWidget *parent = 0);
    ~BLPlayer();
    void show_player();
    void show_file(QString file);
    void show_media(VlcMedia *newMedia);
    void show_media_list(VlcMediaList *newMediaList, int index = 0);
    void clear_media_list();
    void changeToPlay();
    void changeToPause();
    void rateUp(float step = 0.1);
    void rateNormal();
    void rateDown(float step = 0.1);
    void toggleMute();
    void hideControls();
    void showControls();
    void setTitleFromMedia(libvlc_media_t *media_t);
    void clearMenu();
    VlcInstance* get_instance();
    QList<int> getSubtitleIds() const;
    QStringList getSubtitleDescription() const;

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void completeAudioTracks();
    void completeSubtitles();
    void openFileActivate();
    void playPauseButtonClicked();
    void stopButtonClicked();
    void prevButtonClicked();
    void nextButtonClicked();
    void playerOpening();
    void playerPlaying();
    void playerStopped();
    void playerMediaChanged(libvlc_media_t *media_t);
    void mediaEnded();
    void doubleClickedOnVideo();
    void increaseVolumeButtonClicked();
    void decreaseVolumeButtonClicked();
    void muteButtonClicked();
    void mouseMoveOnVideo();
    void mouseEnterOnVideo();
    void mouseLeaveOnVideo();
    void speedFasterActivated();
    void speedNormalActivated();
    void speedSlowerActivated();
    void changeAudioTrackActivated();
    void changeSubtitleActivated();
    void hideBottomPanelTimerTimeout();

};

#endif // BLPLAYER_H
