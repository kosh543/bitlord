#include "blplayer.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMenu>
#include <QFileDialog>
#include <QDesktopWidget>
#include <QMimeDatabase>
#include <QMimeType>

#include <QDebug>

BLWidgetVideo::BLWidgetVideo(QWidget *parent) :
    VlcWidgetVideo(parent)
{
    this->setMouseTracking(true);
}

void BLWidgetVideo::mouseDoubleClickEvent(QMouseEvent *event)
{
    emit this->videoDoubleClicked();
    event->accept();
}

void BLWidgetVideo::mouseMoveEvent(QMouseEvent *event)
{
    emit this->videoMouseMove();
    event->accept();
}

void BLWidgetVideo::enterEvent(QEvent *event)
{
    emit videoMouseEnter();
    event->accept();
}

void BLWidgetVideo::leaveEvent(QEvent *event)
{
    emit videoMouseLeave();
    event->accept();
}

BLPlayer::BLPlayer(Components *comp, QWidget *parent) :
    QDialog(parent)
{
    this->components = comp;

    this->normalCursor = this->cursor();

    media = NULL;
    mediaList = NULL;
    currentMediaInList = 0;
    isPlaying = false;
    currentAudioTrack = -1;
    currentSubtitle = -1;
    isAudioTracksCompleted = false;
    isSubtitlesCompleted = false;

    QAction *openFileAction = new QAction("Open", this);
    this->connect(openFileAction, SIGNAL(triggered()), this, SLOT(openFileActivate()));

    QAction *quitPlayer = new QAction("Quit", this);
    this->connect(quitPlayer, SIGNAL(triggered()), this, SLOT(close()));

    QAction *speedFasterAction = new QAction("Faster", this);
    this->connect(speedFasterAction, SIGNAL(triggered()), this, SLOT(speedFasterActivated()));

    QAction *speedNormalAction = new QAction("Normal", this);
    this->connect(speedNormalAction, SIGNAL(triggered()), this, SLOT(speedNormalActivated()));

    QAction *speedSlowerAction = new QAction("Slower", this);
    this->connect(speedSlowerAction, SIGNAL(triggered()), this, SLOT(speedSlowerActivated()));

    playPauseAction = new QAction("Play", this);
    this->connect(playPauseAction, SIGNAL(triggered()), this, SLOT(playPauseButtonClicked()));

    QAction *stopAction = new QAction("Stop", this);
    this->connect(stopAction, SIGNAL(triggered()), this, SLOT(stopButtonClicked()));

    QAction *prevAction = new QAction("Previous", this);
    this->connect(prevAction, SIGNAL(triggered()), this, SLOT(prevButtonClicked()));

    QAction *nextAction = new QAction("Next", this);
    this->connect(nextAction, SIGNAL(triggered()), this, SLOT(nextButtonClicked()));

    QAction *fullscreenAction = new QAction("Fullscreen", this);
    this->connect(fullscreenAction, SIGNAL(triggered()), this, SLOT(doubleClickedOnVideo()));

    QAction *increaseVolumeAction = new QAction("Increase Volume", this);
    this->connect(increaseVolumeAction, SIGNAL(triggered()), this, SLOT(increaseVolumeButtonClicked()));

    QAction *decreaseVolumeAction = new QAction("Decrease Volume", this);
    this->connect(decreaseVolumeAction, SIGNAL(triggered()), this, SLOT(decreaseVolumeButtonClicked()));

    QAction *muteAction = new QAction("Mute", this);
    this->connect(muteAction, SIGNAL(triggered()), this, SLOT(muteButtonClicked()));

    mainMenu = new QMenuBar(this);
    QMenu *fileMenu = mainMenu->addMenu("File");
    fileMenu->addAction(openFileAction);
    fileMenu->addAction(quitPlayer);

    QMenu *playbackMenu = mainMenu->addMenu("Playback");
    QMenu* speedMenu = playbackMenu->addMenu("Speed");
    speedMenu->addAction(speedFasterAction);
    speedMenu->addAction(speedNormalAction);
    speedMenu->addAction(speedSlowerAction);
    playbackMenu->addSeparator();
    playbackMenu->addAction(playPauseAction);
    playbackMenu->addAction(stopAction);
    playbackMenu->addAction(prevAction);
    playbackMenu->addAction(nextAction);

    QMenu *videoMenu = mainMenu->addMenu("Video");
    videoMenu->addAction(fullscreenAction);

    QMenu *audioMenu = mainMenu->addMenu("Audio");
    audioTracksMenu = audioMenu->addMenu("Audio Track");
    audioMenu->addSeparator();
    audioMenu->addAction(increaseVolumeAction);
    audioMenu->addAction(decreaseVolumeAction);
    audioMenu->addAction(muteAction);

    subtitlesMenu = mainMenu->addMenu("Subtitles");
    subtitlesMenu->addAction("Open file...")->setDisabled(true);
    subtitlesMenu->addSeparator();

    instance = new VlcInstance(VlcCommon::args(), this);

    video = new BLWidgetVideo(this);

    player = new VlcMediaPlayer(instance);
    player->setVideoWidget(video);
    player->setParent(this);

    video->setMediaPlayer(player);
    video->setParent(this);
    video->setStyleSheet("background-color:black;");

    artLabel = new QLabel(this);

    mainView = new QStackedWidget(this);
    mainView->addWidget(video);
    mainView->addWidget(artLabel);
    mainView->setCurrentIndex(0);

    seek = new VlcWidgetSeek(this);
    seek->setMediaPlayer(player);
    seek->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    volume = new VlcWidgetVolumeSlider(this);
    volume->setMediaPlayer(player);
    volume->setRange(0, 200);
    volume->setVolume(100);
    volume->setMaximumHeight(15);
    volume->setMaximumWidth(50);
    volume->setOrientation(Qt::Horizontal);

    playPauseButton = new QPushButton("Play", this);
    this->connect(playPauseButton, SIGNAL(clicked()), this, SLOT(playPauseButtonClicked()));

    stopButton = new QPushButton("Stop", this);
    this->connect(stopButton, SIGNAL(clicked()), this, SLOT(stopButtonClicked()));

    prevButton = new QPushButton("Prev", this);
    this->connect(prevButton, SIGNAL(clicked()), this, SLOT(prevButtonClicked()));

    nextButton = new QPushButton("Next", this);
    this->connect(nextButton, SIGNAL(clicked()), this, SLOT(nextButtonClicked()));

    QHBoxLayout *controlsLayout = new QHBoxLayout();
    controlsLayout->addWidget(playPauseButton);
    controlsLayout->addWidget(stopButton);
    controlsLayout->addWidget(prevButton);
    controlsLayout->addWidget(nextButton);
    controlsLayout->addStretch();
    controlsLayout->addWidget(volume);

    QVBoxLayout *bottomPanelLayout = new QVBoxLayout();
    bottomPanelLayout->setMargin(0);
    bottomPanelLayout->addWidget(seek, 0, Qt::AlignBottom);
    bottomPanelLayout->addLayout(controlsLayout, 0);

    bottomPanel = new QWidget(this);
    bottomPanel->setMinimumHeight(10);
    bottomPanel->setLayout(bottomPanelLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setMenuBar(mainMenu);
    mainLayout->addWidget(mainView, 1);
    mainLayout->addWidget(bottomPanel);

    this->setLayout(mainLayout);
    this->setMinimumSize(320, 240);

    hideBottomPanelTimer = new QTimer(this);
    hideBottomPanelTimer->setInterval(1000);
    this->connect(hideBottomPanelTimer, SIGNAL(timeout()), this, SLOT(hideBottomPanelTimerTimeout()));

    completeAudioTracksIdle = new QTimer(this);
    completeAudioTracksIdle->setSingleShot(true);
    completeAudioTracksIdle->setInterval(0);
    this->connect(completeAudioTracksIdle, SIGNAL(timeout()), this, SLOT(completeAudioTracks()));

    completeSubtitlesIdle = new QTimer(this);
    completeSubtitlesIdle->setSingleShot(true);
    completeSubtitlesIdle->setInterval(0);
    this->connect(completeSubtitlesIdle, SIGNAL(timeout()), this, SLOT(completeSubtitles()));

    this->connect(player, SIGNAL(opening()), this, SLOT(playerOpening()));
    this->connect(player, SIGNAL(playing()), this, SLOT(playerPlaying()));
    this->connect(player, SIGNAL(stopped()), this, SLOT(playerStopped()));
    this->connect(player, SIGNAL(mediaChanged(libvlc_media_t*)),
                  this, SLOT(playerMediaChanged(libvlc_media_t*)));
    this->connect(player, SIGNAL(end()), this, SLOT(mediaEnded()));
    this->connect(video, SIGNAL(videoDoubleClicked()), this, SLOT(doubleClickedOnVideo()));
    this->connect(video, SIGNAL(videoMouseMove()), this, SLOT(mouseMoveOnVideo()));
    this->connect(video, SIGNAL(videoMouseEnter()), this, SLOT(mouseEnterOnVideo()));
    this->connect(video, SIGNAL(videoMouseLeave()), this, SLOT(mouseLeaveOnVideo()));
}

BLPlayer::~BLPlayer()
{
    player->stop();

    //delete seek;
    //delete volume;
    //delete player;
    //delete listPlayer;
    //delete media;
    //delete mediaList;
    //delete instance;
    //delete video;
}

void BLPlayer::closeEvent(QCloseEvent *event)
{
    this->player->stop();
    event->accept();
}

VlcInstance* BLPlayer::get_instance()
{
    return this->instance;
}

void BLPlayer::show_player()
{
    this->show();
}

void BLPlayer::show_file(QString file)
{
    if (file.isEmpty())
        return;

    this->show();
    //this->mediaList->lock();
    this->clear_media_list();
    this->media = new VlcMedia(file, true, this->instance);
    this->media->setParent(this);
    this->mediaList = NULL;
    //this->mediaList->unlock();
    this->changeToPause();
    this->player->open(this->media);
}

void BLPlayer::show_media(VlcMedia *newMedia)
{
    this->show();
    this->media = newMedia;
    this->mediaList = NULL;
    this->changeToPause();
    this->player->open(newMedia);
}

void BLPlayer::show_media_list(VlcMediaList *newMediaList, int index)
{
    this->media = NULL;
    this->mediaList = newMediaList;
    if(this->mediaList && this->mediaList->count() > 0)
    {
        this->show();
        this->currentMediaInList = index;
        this->changeToPause();
        this->player->open(this->mediaList->at(index));
    }
}

void BLPlayer::clear_media_list()
{
    for(int i = this->mediaList->count()-1; i >= 0; --i)
        this->mediaList->removeMedia(i);
}

void BLPlayer::openFileActivate()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Open file"));

    if (file.isEmpty())
        return;

    this->media = new VlcMedia(file, true, this->instance);
    this->mediaList = NULL;
    libvlc_media_parse(this->media->core());
    this->changeToPause();
    this->player->open(this->media);
}

void BLPlayer::changeToPlay()
{
    if(this->isPlaying)
    {
        this->isPlaying = false;
        this->playPauseButton->setText("Play");
        this->playPauseAction->setText("Play");
    }
}

void BLPlayer::changeToPause()
{
    if(!this->isPlaying)
    {
        this->isPlaying = true;
        this->playPauseButton->setText("Pause");
        this->playPauseAction->setText("Pause");
    }
}

void BLPlayer::rateUp(float step)
{
    float rate = libvlc_media_player_get_rate(this->player->core());
    if(rate < 30.0)
    {
        rate += step;
        libvlc_media_player_set_rate(this->player->core(), rate);
    }
    QString text = "Speed: " + QString::number(rate, 'f', 2);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 0);
    libvlc_video_set_marquee_string(this->player->core(), libvlc_marquee_Text,
                                    text.toLocal8Bit().constData());
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Position,
                                 libvlc_position_top_left);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Timeout, 1000);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 1);
}

void BLPlayer::rateNormal()
{
    float rate = 1.0;
    libvlc_media_player_set_rate(this->player->core(), rate);
    QString text = "Speed: " + QString::number(rate, 'f', 2);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 0);
    libvlc_video_set_marquee_string(this->player->core(), libvlc_marquee_Text,
                                    text.toLocal8Bit().constData());
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Position,
                                 libvlc_position_top_left);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Timeout, 1000);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 1);
}

void BLPlayer::rateDown(float step)
{
    float rate = libvlc_media_player_get_rate(this->player->core());
    if(rate > step)
    {
        rate -= step;
        libvlc_media_player_set_rate(this->player->core(), rate);
    }
    QString text = "Speed: " + QString::number(rate, 'f', 2);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 0);
    libvlc_video_set_marquee_string(this->player->core(), libvlc_marquee_Text,
                                    text.toLocal8Bit().constData());
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Position,
                                 libvlc_position_top_left);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Timeout, 1000);
    libvlc_video_set_marquee_int(this->player->core(), libvlc_marquee_Enable, 1);
}

void BLPlayer::playPauseButtonClicked()
{
    if(this->isPlaying)
    {
        this->changeToPlay();
        this->player->pause();
    }
    else
    {
        if(this->player->currentMedia())
        {
            this->changeToPause();
            this->player->play();
        }
    }
}

void BLPlayer::stopButtonClicked()
{
    this->changeToPlay();
    this->player->stop();
}

void BLPlayer::prevButtonClicked()
{
    VlcMedia *newMedia = NULL;
    if(this->mediaList)
    {
        if(this->currentMediaInList > 0)
            this->currentMediaInList--;
        else
            this->currentMediaInList = this->mediaList->count() - 1;
        newMedia = this->mediaList->at(this->currentMediaInList);
    }
    if(newMedia)
    {
        this->changeToPause();
        this->player->open(newMedia);
    }
}

void BLPlayer::nextButtonClicked()
{
    VlcMedia *newMedia = NULL;
    if(this->mediaList)
    {
        if(this->currentMediaInList < this->mediaList->count() - 1)
            this->currentMediaInList++;
        else
            this->currentMediaInList = 0;
        newMedia = this->mediaList->at(this->currentMediaInList);
    }
    if(newMedia)
    {
        this->changeToPause();
        this->player->open(newMedia);
    }
}

void BLPlayer::increaseVolumeButtonClicked()
{
    this->volume->setValue(this->volume->value() + 10);
}

void BLPlayer::decreaseVolumeButtonClicked()
{
    this->volume->setValue(this->volume->value() - 10);
}

void BLPlayer::muteButtonClicked()
{
    this->toggleMute();
}

void BLPlayer::toggleMute()
{
    if(this->player->audio()->toggleMute())
        this->volume->setDisabled(true);
    else
        this->volume->setEnabled(true);
}

void BLPlayer::completeAudioTracks()
{
    if(!this->isAudioTracksCompleted)
    {
        int trackCount = this->player->audio()->trackCount();
        if(trackCount > 0)
        {
            this->isAudioTracksCompleted = true;
            this->audioTracksMenu->clear();
            this->audioTracksIndexes = this->player->audio()->trackIds();
            foreach(QString trackName, this->player->audio()->trackDescription())
            {
                QAction *newTrack = this->audioTracksMenu->addAction(trackName);
                newTrack->setCheckable(true);
                this->connect(newTrack, SIGNAL(triggered()), this, SLOT(changeAudioTrackActivated()));
            }
            this->currentAudioTrack = -1;
            int track = this->player->audio()->track();
            if(track > -1)
            {
                this->currentAudioTrack = this->audioTracksIndexes.indexOf(this->player->audio()->track());
                this->audioTracksMenu->actions()[this->currentAudioTrack]->setChecked(true);
            }
        }
    }
}

void BLPlayer::completeSubtitles()
{
    if(!this->isSubtitlesCompleted)
    {
        int subtitlesCount = libvlc_video_get_spu_count(this->player->core());
        if(subtitlesCount > 0)
        {
            this->isSubtitlesCompleted = true;
            for(int i = this->subtitlesMenu->actions().count()-1; i > 1; --i)
                this->subtitlesMenu->removeAction(this->subtitlesMenu->actions()[i]);
            this->subtitlesIndexes = this->getSubtitleIds();
            foreach(QString subtitleName, this->getSubtitleDescription())
            {
                QAction *newSubtitle = this->subtitlesMenu->addAction(subtitleName);
                newSubtitle->setCheckable(true);
                this->connect(newSubtitle, SIGNAL(triggered()), this, SLOT(changeSubtitleActivated()));
            }
            this->currentSubtitle = -1;
            int track = libvlc_video_get_spu(this->player->core());
            if(track > -1)
            {
                this->currentSubtitle = this->subtitlesIndexes.indexOf(
                            libvlc_video_get_spu(this->player->core()));
                this->subtitlesMenu->actions()[this->currentSubtitle+2]->setChecked(true);
            }
        }
    }
}

QList<int> BLPlayer::getSubtitleIds() const
{
    QList<int> ids;

    libvlc_track_description_t *desc;
    desc = libvlc_video_get_spu_description(this->player->core());

    ids << desc->i_id;
    if (libvlc_video_get_spu_count(this->player->core()) > 1)
    {
        for(int i = 1; i < libvlc_video_get_spu_count(this->player->core()); i++)
        {
            desc = desc->p_next;
            ids << desc->i_id;
        }
    }

    return ids;
}

QStringList BLPlayer::getSubtitleDescription() const
{
    QStringList descriptions;

    libvlc_track_description_t *desc;
    desc = libvlc_video_get_spu_description(this->player->core());

    descriptions << QString().fromUtf8(desc->psz_name);
    if (libvlc_video_get_spu_count(this->player->core()) > 1)
    {
        for(int i = 1; i < libvlc_video_get_spu_count(this->player->core()); i++)
        {
            desc = desc->p_next;
            descriptions << QString().fromUtf8(desc->psz_name);
        }
    }

    return descriptions;
}

void BLPlayer::playerOpening()
{
    this->isAudioTracksCompleted = false;
    this->isSubtitlesCompleted = false;

    QMimeDatabase db;
    QMimeType type = db.mimeTypeForFile(this->player->currentMedia()->currentLocation());
    if(type.name().startsWith("video"))
    {
        unsigned x = 320;
        unsigned y = 240;
        libvlc_video_get_size(this->player->core(), 0, &x, &y);
        y = this->mainMenu->height() + y + this->bottomPanel->height();
        QDesktopWidget widget;
        QRect availSpace = widget.availableGeometry(widget.primaryScreen());
        if(y > availSpace.height())
            y = availSpace.height();
        if(x > availSpace.width())
            x = availSpace.width();
        this->resize(x, y);
    }
}

void BLPlayer::playerPlaying()
{
    if(!this->isAudioTracksCompleted)
        this->completeAudioTracksIdle->start();
    if(!this->isSubtitlesCompleted)
        this->completeSubtitlesIdle->start();
}

void BLPlayer::playerStopped()
{
    this->changeToPlay();
    this->clearMenu();
}

void BLPlayer::setTitleFromMedia(libvlc_media_t *media_t)
{
    if(!libvlc_media_is_parsed(media_t))
        libvlc_media_parse(media_t);

    QString newTitle = " - BitLord player";
    QString artist = QString::fromUtf8(libvlc_media_get_meta(media_t, libvlc_meta_Artist));
    if(!artist.isNull() && !artist.isEmpty())
        newTitle = " - " + artist.simplified() + newTitle;
    QString title = QString::fromUtf8(libvlc_media_get_meta(media_t, libvlc_meta_Title));
    newTitle = title.simplified() + newTitle;
    this->setWindowTitle(newTitle);
}

void BLPlayer::playerMediaChanged(libvlc_media_t *media_t)
{
    this->setTitleFromMedia(media_t);
    QMimeDatabase db;
    QMimeType type = db.mimeTypeForFile(this->player->currentMedia()->currentLocation());
    if(type.name().startsWith("video"))
        this->mainView->setCurrentIndex(0);
    else
    {
        this->mainView->setCurrentIndex(1);
        QUrl artURL(QString::fromUtf8(libvlc_media_get_meta(media_t, libvlc_meta_ArtworkURL)));
        qDebug() << "ART: " << artURL.toLocalFile();
        if(!artURL.isEmpty())
            this->artLabel->setPixmap(QPixmap(artURL.toLocalFile()));
    }
    this->clearMenu();
}

void BLPlayer::clearMenu()
{
    this->isAudioTracksCompleted = false;
    this->currentAudioTrack = -1;
    this->audioTracksMenu->clear();
    this->isSubtitlesCompleted = false;
    this->currentSubtitle = -1;
    for(int i = this->subtitlesMenu->actions().count()-1; i > 1; --i)
        this->subtitlesMenu->removeAction(this->subtitlesMenu->actions()[i]);
}

void BLPlayer::mediaEnded()
{
    if(this->mediaList)
        this->nextButtonClicked();
}

void BLPlayer::hideControls()
{
    this->mainMenu->hide();
    this->bottomPanel->hide();
    this->setCursor(Qt::BlankCursor);
}

void BLPlayer::showControls()
{
    this->mainMenu->show();
    this->bottomPanel->show();
    this->setCursor(this->normalCursor);
}

void BLPlayer::doubleClickedOnVideo()
{
    if(this->isFullScreen())
    {
        this->showNormal();
        this->showControls();
    }
    else
    {
        this->hideBottomPanelTimer->start();
        this->showFullScreen();
    }
}

void BLPlayer::mouseMoveOnVideo()
{
    if(this->isFullScreen())
    {
        this->hideBottomPanelTimer->stop();
        this->hideBottomPanelTimer->start();
        if(!this->bottomPanel->isVisible())
        {
            this->showControls();
        }
    }
}

void BLPlayer::mouseEnterOnVideo()
{
    if(this->isFullScreen())
        this->hideBottomPanelTimer->start();
}

void BLPlayer::mouseLeaveOnVideo()
{
    if(this->isFullScreen())
        this->hideBottomPanelTimer->stop();
}

void BLPlayer::speedFasterActivated()
{
    this->rateUp();
}

void BLPlayer::speedNormalActivated()
{
    this->rateNormal();
}

void BLPlayer::speedSlowerActivated()
{
    this->rateDown();
}

void BLPlayer::changeAudioTrackActivated()
{
    QObject* sender = QObject::sender();
    if(sender)
    {
        QAction *newTrack = (QAction*)sender;
        int newTrackIndex = this->audioTracksMenu->actions().indexOf(newTrack);
        if(newTrackIndex != this->currentAudioTrack)
        {
            if(this->currentAudioTrack > -1)
                this->audioTracksMenu->actions()[this->currentAudioTrack]->setChecked(false);
            this->currentAudioTrack = newTrackIndex;
            newTrack->setChecked(true);
            this->player->audio()->setTrack(this->audioTracksIndexes[newTrackIndex]);
        }
        else
            this->audioTracksMenu->actions()[newTrackIndex]->setChecked(true);
    }
}

void BLPlayer::changeSubtitleActivated()
{
    QObject* sender = QObject::sender();
    if(sender)
    {
        QAction *newSubtitle = (QAction*)sender;
        int newSubtitleIndex = this->subtitlesMenu->actions().indexOf(newSubtitle)-2;
        if(newSubtitleIndex != this->currentSubtitle)
        {
            if(this->currentSubtitle > -1)
                this->subtitlesMenu->actions()[this->currentSubtitle+2]->setChecked(false);
            this->currentSubtitle = newSubtitleIndex;
            newSubtitle->setChecked(true);
            libvlc_video_set_spu(this->player->core(), this->subtitlesIndexes[newSubtitleIndex]);
        }
        else
            this->subtitlesMenu->actions()[newSubtitleIndex+2]->setChecked(true);
    }
}

void BLPlayer::hideBottomPanelTimerTimeout()
{
    if(this->isFullScreen())
        this->hideControls();
}
