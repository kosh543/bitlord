#include "ui/mainwindow.h"
#include "components.h"

#include <QDateTime>
#include <QString>
#include <QSettings>
#include <QStatusBar>

MainWindow::MainWindow(Components *comp, QWidget *parent)
    : QMainWindow(parent)
{
    this->components = comp;

    QSettings settings;

    QWidget *centralWidget = new QWidget(this);

    this->setStyleSheet(
        "QMainWindow{"
        "background-color:rgb(1, 225, 254);"
        "background-image:url("
                + QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/app-bot.png")
                + ");"
        "background-position:bottom left;"
        "background-repeat:none;"
        "background-attachment: fixed;"
        "}");
    this->setFocusPolicy(Qt::StrongFocus);

    QVBoxLayout *vBox = new QVBoxLayout;
    centralWidget->setLayout(vBox);

    QPushButton *b = new QPushButton("PLAYER");
    vBox->addWidget(b);

    QSplitter *vSplitter = new QSplitter();
    vSplitter->setOrientation(Qt::Vertical);

    QHBoxLayout *browserTabsLayout = new QHBoxLayout();
    browserTabsLayout->addWidget(this->components->browserTabs); //, Qt::AlignRight);
    browserTabsLayout->addWidget(this->components->browserTabs->plusButton); //, Qt::AlignRight);
    browserTabsLayout->addStretch();
    browserTabsLayout->setStretchFactor(this->components->browserTabs, 0);

    vBox->addLayout(browserTabsLayout);
    vBox->addWidget(vSplitter);

    this->notebook = new QStackedWidget(this);
    this->notebook->addWidget(this->components->browser);
    this->notebook->addWidget(this->components->torrentView);
    this->notebook->addWidget(this->components->rssView);
    this->notebook->addWidget(this->components->playlistsView);

    this->notebook->setCurrentIndex(1);

    QSplitter *hSplitter = new QSplitter();
    hSplitter->setOrientation(Qt::Horizontal);
    hSplitter->addWidget(this->components->filterTreeView);
    hSplitter->addWidget(this->notebook);
    hSplitter->setStretchFactor(0, 0);
    hSplitter->setStretchFactor(1, 1);
    vSplitter->addWidget(hSplitter);
    vSplitter->addWidget(this->components->torrentDetails);
    vSplitter->setStretchFactor(0, 1);
    vSplitter->setStretchFactor(1, 0);

    statusbar = new StatusBar(this->components);
    this->setStatusBar(statusbar);

    if(!settings.value("uiconfig/show_sidebar").toBool())
        this->components->filterTreeView->hide();

    this->setCentralWidget(centralWidget);

    connect(b, SIGNAL(clicked()), this, SLOT(updateTorrent()));

    int x = settings.value("uiconfig/window_x_pos").toInt();
    if(x < 0)
        x = 0;
    int y = settings.value("uiconfig/window_y_pos").toInt();
    if(y < 0)
        y = 0;
    int w = settings.value("uiconfig/window_width").toInt();
    int h = settings.value("uiconfig/window_height").toInt();
    this->move(x, y);
    this->resize(w, h);
    QList<int> view_sizes;
    int ls = 170; //((QVariant)(*this->config)["window_pane_position"]).toInt();
    view_sizes << ls << w-ls;
    hSplitter->setSizes(view_sizes);
    if(settings.value("uiconfig/window_maximized").toBool())
        this->setWindowState(Qt::WindowMaximized);
    /*elif self.config["window_half_maximized"]:
        self.maximize_to_half_screen_window()*/
}

MainWindow::~MainWindow()
{

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("uiconfig/window_x_pos", this->x());
    settings.setValue("uiconfig/window_y_pos", this->y());
    settings.setValue("uiconfig/window_width", this->width());
    settings.setValue("uiconfig/window_height", this->height());
    settings.setValue("uiconfig/window_maximized",
                      (this->windowState() == Qt::WindowMaximized));

    /*if self.config["window_maximized"]:
        #self.window.maximize()
        self.maximize_window()
    elif self.config["window_half_maximized"]:
        self.maximize_to_half_screen_window()*/

    event->accept();
}

void MainWindow::updateTorrent()
{
    this->components->player->show_player();
}

void MainWindow::about()
{
    QLabel *text = new QLabel;
    text->setWordWrap(true);
    text->setText("<p>The <b>Torrent Client</b> example demonstrates how to"
                  " write a complete peer-to-peer file sharing"
                  " application using Qt's network and thread classes.</p>"
                  "<p>This feature complete client implementation of"
                  " the BitTorrent protocol can efficiently"
                  " maintain several hundred network connections"
                  " simultaneously.</p>");

    QPushButton *quitButton = new QPushButton("OK");

    QHBoxLayout *topLayout = new QHBoxLayout;
    topLayout->setMargin(10);
    topLayout->setSpacing(10);
    topLayout->addWidget(text);

    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addStretch();
    bottomLayout->addWidget(quitButton);
    bottomLayout->addStretch();

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(bottomLayout);

    QDialog abt(this);
    abt.setModal(true);
    abt.setWindowTitle(tr("About Torrent Client"));
    abt.setLayout(mainLayout);

    connect(quitButton, SIGNAL(clicked()), &abt, SLOT(close()));

    abt.exec();
}
