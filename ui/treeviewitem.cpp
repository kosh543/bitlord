#include <QStringList>

#include "treeviewitem.h"

TreeViewItem::TreeViewItem(const QList<QVariant> &data, TreeViewItem *parent)
{
    parentItem = parent;
    itemData = data;
}

TreeViewItem::~TreeViewItem()
{
    qDeleteAll(childItems);
}

void TreeViewItem::appendChild(TreeViewItem *item)
{
    childItems.append(item);
}

TreeViewItem *TreeViewItem::child(int row)
{
    return childItems.value(row);
}

int TreeViewItem::childCount() const
{
    return childItems.count();
}

void TreeViewItem::removeChild()
{
    childItems.clear();
}

void TreeViewItem::removeChildren(int index)
{
    childItems.removeAt(index);
}

int TreeViewItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeViewItem::data(int column) const
{
    return itemData.value(column);
}

void TreeViewItem::setData(int column, QVariant data)
{
    itemData[column] = data;
}

int TreeViewItem::addColumn(QVariant data)
{
    itemData.append(data);
    return itemData.indexOf(data);
}

TreeViewItem *TreeViewItem::parent()
{
    return parentItem;
}

int TreeViewItem::row() const
{
    return 0;
}
