#include "torrentview.h"
#include "core/torrentmanager.h"
#include "core/torrent.h"
#include "components.h"
#include "common.h"

TorrentViewProgressItemDelegate::TorrentViewProgressItemDelegate(QObject *parent) : QItemDelegate(parent){}

void TorrentViewProgressItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionProgressBarV2 progressBarOption;
    //QRect rect = option.rect;
    //QSize size(rect.width()*3/4,rect.height()*3/4);
    //rect.setSize(size);
    progressBarOption.state = QStyle::State_Enabled;
    progressBarOption.direction = QApplication::layoutDirection();
    progressBarOption.rect = option.rect;
    progressBarOption.fontMetrics = QApplication::fontMetrics();
    //QPalette pal = progressBarOption.palette;
    //QColor col;
    //col.setNamedColor("#05B8CC");
    //pal.setColor(QPalette::Highlight,col);
    //progressBarOption.palette = pal;
    progressBarOption.type = QStyleOption::SO_ProgressBar;
    progressBarOption.version = 2;
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.textAlignment = Qt::AlignCenter;
    progressBarOption.textVisible = true;
    int progress = (int)(index.data(Qt::DisplayRole).toFloat());
    progressBarOption.progress = progress;
    progressBarOption.text = QString("%1%").arg(progressBarOption.progress);

    // Draw the progress bar onto the view.
    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter, 0);
}

TorrentView::TorrentView(Components *comp, QWidget *parent) :
    QTreeView(parent)
{
    this->components = comp;

    model = new TreeViewModel(this);
    proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setSourceModel(model);
    proxyModel->setFilterKeyColumn(FILTER_COLUMN);
    proxyModel->setFilterFixedString("true");
    this->setModel(proxyModel);

    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setAlternatingRowColors(true);
    this->setRootIsDecorated(false);
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);

    this->setStyleSheet("background-color:transparent;");
    this->setStyleSheet("background-image:url("
                        + QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/app-bot-overlay.png")
                        + ");");

    this->load_state();

    model->startInsertColumns(0, 15);

    QString column_name = "filter";
    columns[column_name].name = column_name;
    columns[column_name].index = 0;
    columns[column_name].position = 0;
    columns[column_name].visible = false;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    model->addColumn(column_name);

    column_name = "torrent_id";
    columns[column_name].name = column_name;
    columns[column_name].index = 1;
    columns[column_name].position = 1;
    columns[column_name].visible = false;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    model->addColumn(column_name);

    column_name = "state";
    columns[column_name].name = column_name;
    columns[column_name].index = 2;
    columns[column_name].position = 2;
    columns[column_name].visible = false;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    model->addColumn(column_name);

    column_name = "Name";
    columns[column_name].name = column_name;
    columns[column_name].index = 3;
    columns[column_name].position = 3;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "state" << "name";
    model->addColumn(column_name);

    column_name = "Progress";
    columns[column_name].name = column_name;
    columns[column_name].index = 4;
    columns[column_name].position = 4;
    columns[column_name].visible = true;
    columns[column_name].width = 138;
    columns[column_name].fix_width = true;
    columns[column_name].status_field << "progress" << "state";
    model->addColumn(column_name);

    column_name = "#";
    columns[column_name].name = column_name;
    columns[column_name].index = 5;
    columns[column_name].position = 5;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "queue";
    model->addColumn(column_name);

    column_name = "Size";
    columns[column_name].name = column_name;
    columns[column_name].index = 6;
    columns[column_name].position = 6;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "total_wanted";
    model->addColumn(column_name);

    column_name = "Seeders";
    columns[column_name].name = column_name;
    columns[column_name].index = 7;
    columns[column_name].position = 7;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "num_seeds" << "total_seeds";
    model->addColumn(column_name);

    column_name = "Peers";
    columns[column_name].name = column_name;
    columns[column_name].index = 8;
    columns[column_name].position = 8;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "num_peers" << "total_peers";
    model->addColumn(column_name);

    column_name = "Down Speed";
    columns[column_name].name = column_name;
    columns[column_name].index = 9;
    columns[column_name].position = 9;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "download_payload_rate";
    model->addColumn(column_name);

    column_name = "Up Speed";
    columns[column_name].name = column_name;
    columns[column_name].index = 10;
    columns[column_name].position = 10;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "upload_payload_rate";
    model->addColumn(column_name);

    column_name = "ETA";
    columns[column_name].name = column_name;
    columns[column_name].index = 11;
    columns[column_name].position = 11;
    columns[column_name].visible = true;
    columns[column_name].width = 40;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "eta";
    model->addColumn(column_name);

    column_name = "Ratio";
    columns[column_name].name = column_name;
    columns[column_name].index = 12;
    columns[column_name].position = 12;
    columns[column_name].visible = true;
    columns[column_name].width = 50;
    columns[column_name].fix_width = true;
    columns[column_name].status_field << "ratio";
    model->addColumn(column_name);

    column_name = "Avail";
    columns[column_name].name = column_name;
    columns[column_name].index = 13;
    columns[column_name].position = 13;
    columns[column_name].visible = true;
    columns[column_name].width = 60;
    columns[column_name].fix_width = true;
    columns[column_name].status_field << "distributed_copies";
    model->addColumn(column_name);

    column_name = "Added";
    columns[column_name].name = column_name;
    columns[column_name].index = 14;
    columns[column_name].position = 14;
    columns[column_name].visible = true;
    columns[column_name].width = 70;
    columns[column_name].fix_width = true;
    columns[column_name].status_field << "time_added";
    model->addColumn(column_name);

    column_name = "Tracker";
    columns[column_name].name = column_name;
    columns[column_name].index = 15;
    columns[column_name].position = 15;
    columns[column_name].visible = true;
    columns[column_name].width = 10;
    columns[column_name].fix_width = false;
    columns[column_name].status_field << "tracker_host";
    model->addColumn(column_name);

    model->finishInsertColumns();

    QMap<QString, QVariant>  i;
    foreach(i, this->state)
    {
        QString column_name = i["name"].toString();
        if(!columns.contains(column_name))
        {
            continue;
            /*model->appendColumn(column_name);
            columns[column_name].name = column_name;
            columns[column_name].index = this->state.indexOf(i);
            columns[column_name].fix_width = false;*/
        }
        columns[column_name].position = i["position"].toInt();
        columns[column_name].visible = i["visible"].toBool();
        this->setColumnHidden(columns[column_name].index, !columns[column_name].visible);
        int width = i["width"].toInt();
        if(width != columns[column_name].width && width != 0)
        {
            columns[column_name].width = width;
        }
    }

    if(this->state.isEmpty())
    {
        foreach(QString name, this->columns.keys())
        {
            QMap<QString, QVariant> elem;
            elem["name"] = name;
            elem["position"] = this->header()->visualIndex(columns[name].index);
            elem["visible"] = this->columns[name].visible ? 1 : 0;
            elem["width"] = this->columnWidth(columns[name].index);
            this->state.append(elem);
        }
    }

    foreach(TorrentViewColumn col, columns.values())
    {
        int pos = this->header()->visualIndex(col.index);
        if(pos != col.position)
            this->header()->moveSection(pos, col.position);
        this->setColumnHidden(col.index, !col.visible);
        if(col.width != 0)
            this->setColumnWidth(col.index, col.width);
        //if(col.fix_width)
        //    this->header()->setSectionResizeMode(col.index, QHeaderView::Fixed);

    }
    //this->header()->setSectionsClickable(true);
    //this->header()->setSectionsMovable(true);

    this->setItemDelegateForColumn(4, new TorrentViewProgressItemDelegate(this));

    this->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(this->components->eventManager, SIGNAL(TorrentAddedEvent(QString)), this, SLOT(on_torrentadded_event(QString)));
    this->connect(this->components->eventManager, SIGNAL(TorrentRemovedEvent(QString)), this, SLOT(on_torrentremoved_event(QString)));
    this->connect(this->components->eventManager, SIGNAL(TorrentStateChangedEvent(QString,QString)), this, SLOT(on_torrentstatechanged_event(QString,QString)));
    this->connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowContextMenu(QPoint)));
    this->connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(on_torrentview_clicked(QModelIndex)));
    this->connect(this->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(on_torrentview_selection_changed(QItemSelection,QItemSelection)));

    // idle for send status request
    this->_st_idle_timer = new QTimer(this);
    this->_st_idle_timer->setSingleShot(true);
    this->_st_idle_timer->setInterval(0);
    this->connect(this->_st_idle_timer, SIGNAL(timeout()), this, SLOT(send_status_request()));

    this->_update_view_idle_timer = new QTimer(this);
    this->_update_view_idle_timer->setSingleShot(true);
    this->_update_view_idle_timer->setInterval(0);
    this->connect(this->_update_view_idle_timer, SIGNAL(timeout()), this, SLOT(update_view()));

    this->_update_timer = new QTimer(this);
    this->_update_timer->setInterval(2000);
    this->connect(this->_update_timer, SIGNAL(timeout()), this, SLOT(update_data()));
    this->_update_timer->start();
}

void TorrentView::start()
{
    // Start the torrentview
    //this->model->clear();

    // We need to get the core session state to know which torrents are in
    // the session so we can add them to our list.
    //QList<QString> state = this->components->torrentManager->torrents.keys();

    /*QStringList torrents_for_add;
    foreach(QString torrent_id, state)
    {
        if(!this->model->containTorrentId(torrent_id))
            torrents_for_add.append(torrent_id);
    }*/

    this->set_filter("All Downloads");

    /*this->model->startInsertRows(0, state.length()-1);
    foreach(QString torrent_id, state) //torrents_for_add)
    {
        this->add_row(torrent_id, false);
    }
    //self.treeview.set_model(model)
    this->model->finishInsertRows();
    this->update_data();*/
}

void TorrentView::update_data()
{
    this->_st_idle_timer->start();
}

void TorrentView::update_view()
{
    this->model->update_model(this->status, this->columns_to_update, this->filtered_torrents);
}

void TorrentView::send_status_request()
{
    QList<QString> status_keys;
    this->columns_to_update.clear();

    foreach (QString column, this->columns.keys())
    {
        if(this->columns[column].visible && !this->columns[column].status_field.empty())
        {
            foreach (QString field, this->columns[column].status_field)
                if(!status_keys.contains(field))
                    status_keys.append(field);
            this->columns_to_update.append(column);
        }
    }

    if(status_keys.empty())
        return;

    this->status.clear();
    foreach(QString torrent_id, this->components->torrentManager->torrents.keys())
        this->status[torrent_id] = (*(this->components->torrentManager))[torrent_id]->get_status(status_keys);

    this->_update_view_idle_timer->start();
}

void TorrentView::load_state()
{
    // Load the listview state from filename.
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    QFile file(QDir(dir).absoluteFilePath("torrentview.state"));
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in(&file);
        in >> this->state;
        file.close();
        return;
    }

    QFile f(QDir(get_default_config_dir()).absoluteFilePath("torrentview.state"));
    if(f.open(QIODevice::ReadOnly))
    {
        QString s = QString(f.readAll());
        f.close();
        PickleLoader pl(s);
        QVariant res;
        try
        {
            pl.loads(res);
            QList<QMap<QString, QVariant> > state;
            foreach(QVariant m, res.toList())
            {
                state.append(m.toList()[0].toMap());
            }
            this->state = state;
        }
        catch(...){}
    }
}

void TorrentView::save_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);

    QFile file(QDir(dir).absoluteFilePath("torrentview.state"));
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);   // we will serialize the data into the file

    for(int index; index < this->state.count(); ++index)
    {
        QString name = this->state[index]["name"].toString();
        this->state[index]["position"] = this->header()->visualIndex(columns[name].index);
        this->state[index]["visible"] = this->columns[name].visible ? 1 : 0;
        this->state[index]["width"] = this->columnWidth(columns[name].index);
    }

    out << this->state;
    file.flush();
    file.close();
}

void TorrentView::add_column(QString text)
{
    int col = model->addColumn(text);
    this->setColumnHidden(col, false);
    //this->setColumnWidth(col, 100);
    //this->header()->moveSection(col, 1);
    /*this->setColumnHidden(TRACKER_COLUMN, false);
    if(this->columnWidth(TRACKER_COLUMN) == 0)
        this->setColumnWidth(TRACKER_COLUMN, 20);*/
    //this->state.clear();
    //this->model->clear();
    //this->start();
}

void TorrentView::add_row(QString torrent_id, bool update)
{
    // Adds a new torrent row to the treeview
    // Make sure this torrent isn't already in the list
    if(this->model->containTorrentId(torrent_id))
        return;

    QStringList keys;
    keys << "name" << "progress" << "total_size";
    QMap<QString, QVariant> st = (*(this->components->torrentManager))[torrent_id]->get_status(keys);
    QList<QVariant> items;
    items << torrent_id;
    items << st["state"];
    items << false;
    items << st["name"];
    items << st["progress"];
    items << "";
    items << st["total_size"];
    items << "";
    items << "";
    items << "";
    items << "";
    items << "";
    items << "";
    items << "";
    items << "";
    items << "";
    this->model->appendRow(items);
    // Insert a new row to the liststore
    //row = self.liststore.append()
    // Store the torrent id)
    //self.liststore.set_value(
    //            row,
    //            self.columns["torrent_id"].column_indices[0],
    //            torrent_id)
    //if(update)
    //    this->update();
}
/*
void TorrentView::remove_row(self, torrent_id):
    """Removes a row with torrent_id"""
    for row in self.liststore:
        if row[self.columns["torrent_id"].column_indices[0]] == torrent_id:
            self.liststore.remove(row.iter)
            # Force an update of the torrentview
            self.update()
            break*/

bool TorrentView::is_column_visible(QString name)
{
    if(this->columns.contains(name))
        return this->columns[name].visible;
    return false;
}

void TorrentView::set_column_visible(QString name, bool visible)
{
    if(this->columns.contains(name))
    {
        this->columns[name].visible = visible;
        this->setColumnHidden(this->columns[name].index, !visible);
    }
}

void TorrentView::set_filter(QString filter)
{
    this->current_filter = filter;
    this->filtered_torrents.clear();
    QList<QString> torrents = this->components->torrentManager->torrents.keys();
    if(filter == "All Downloads")
        this->filtered_torrents = torrents;
    else
    {
        QList<QString> status_keys;
        status_keys << "state" << "download_payload_rate" << "upload_payload_rate";
        foreach(QString torrent_id, torrents)
        {
            QMap<QString, QVariant> st = (*(this->components->torrentManager))[torrent_id]->get_status(status_keys);
            if(filter == "Downloading")
            {
                if(st["state"].toString() == "Downloading")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Seeding")
            {
                if(st["state"].toString() == "Seeding")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Active")
            {
                if(st["download_payload_rate"].toFloat() > 0 || st["upload_payload_rate"].toFloat() > 0)
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Paused")
            {
                if(st["state"].toString() == "Paused")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Queued")
            {
                if(st["state"].toString() == "Queued")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Finished")
            {
                if(st["state"].toString() == "Finished")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Checking")
            {
                if(st["state"].toString() == "Checking" || st["state"].toString() == "Allocating")
                    this->filtered_torrents.append(torrent_id);
            }
            else if(filter == "Error")
            {
                if(st["state"].toString() == "Error")
                    this->filtered_torrents.append(torrent_id);
            }
        }
    }
    this->update_data();
}

QList<QString> TorrentView::get_selected_torrents()
{
    QList<QString> selected_torrents;
    foreach(QModelIndex ind, this->selectionModel()->selectedRows())
    {
        if(ind.column() == 0)
            selected_torrents.append(ind.data().toString());
        else
            selected_torrents.append(this->model->index(ind.row(), 0).data().toString());
    }
    return selected_torrents;
}

void TorrentView::on_torrentremoved_event(QString torrent_id)
{
    if(this->status.contains(torrent_id))
        this->status.remove(torrent_id);
    if(this->filtered_torrents.contains(torrent_id))
        this->filtered_torrents.removeOne(torrent_id);
    this->update_data();
}

bool TorrentView::check_state(QString torrent_id, QString state)
{
    if(this->current_filter == "All Downloads")
        return true;
    else
    {
        QList<QString> status_keys;
        status_keys << "download_payload_rate" << "upload_payload_rate";
        QMap<QString, QVariant> st = (*(this->components->torrentManager))[torrent_id]->get_status(status_keys);
        if(this->current_filter == "Downloading")
        {
            if(state == "Downloading")
                return true;
        }
        else if(this->current_filter == "Seeding")
        {
            if(state == "Seeding")
                return true;
        }
        else if(this->current_filter == "Active")
        {
            if(st["download_payload_rate"].toFloat() > 0 || st["upload_payload_rate"].toFloat() > 0)
                return true;
        }
        else if(this->current_filter == "Paused")
        {
            if(state == "Paused")
                return true;
        }
        else if(this->current_filter == "Queued")
        {
            if(state == "Queued")
                return true;
        }
        else if(this->current_filter == "Finished")
        {
            if(state == "Finished")
                return true;
        }
        else if(this->current_filter == "Checking")
        {
            if(state == "Checking" || state == "Allocating")
                return true;
        }
        else if(this->current_filter == "Error")
        {
            if(state == "Error")
                return true;
        }
    }
    return false;
}

void TorrentView::on_torrentstatechanged_event(QString torrent_id, QString state)
{
    if(!this->check_state(torrent_id, state))
    {
        if(this->filtered_torrents.contains(torrent_id))
            this->filtered_torrents.removeOne(torrent_id);
    }
    else if(!this->filtered_torrents.contains(torrent_id))
        this->filtered_torrents.append(torrent_id);
}

void TorrentView::on_torrentadded_event(QString torrent_id) //, feed_id);
{
    this->model->startInsertRows(0, 0);
    this->add_row(torrent_id, false);
    this->model->finishInsertRows();
    this->components->filterTreeView->set_default_filter();
}

void TorrentView::on_sessionpaused_event()
{
    qDebug() << "SESSION PAUSED";
}

void TorrentView::on_sessionresumed_event()
{
    qDebug() << "SESSION RESUMED";
}

void TorrentView::on_torrentqueuechanged_event()
{
    qDebug() << "QUEUE CHANGED";
}

void TorrentView::on_torrentfinished_event(QString torrent_id)
{
    qDebug() << "FINISHED: " << torrent_id;
}

void TorrentView::ShowContextMenu(const QPoint& pos)
{
    if(this->indexAt(pos).row() > -1)
        this->components->menubar->getTorrentMenu()->exec(this->mapToGlobal(pos));
}

void TorrentView::on_torrentview_clicked(const QModelIndex index)
{
    qDebug() << "!!!" << index.row() << index.column();
}

void TorrentView::on_torrentview_selection_changed(QItemSelection selected, QItemSelection deselected)
{
    this->components->torrentDetails->update_details();
}
