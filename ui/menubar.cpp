#include "menubar.h"
#include "components.h"
#include "ui/setotherdialog.h"
#include "ui/edittrackersdialog.h"

#include <QSettings>

MenuBar::MenuBar(Components *comp, QWidget *parent)
    : QWidget(parent)
{
    this->components = comp;

    QSettings settings;

    MainWindow *mainWindow = this->components->mainWindow;

    // Torrent actions
    QAction *addTorrentAction = new QAction("Add Torrent", this);
    QAction *openFolderAction = new QAction("Open Folder", this);
    QAction *pauseTorrentAction = new QAction("Pause", this);
    QAction *resumeTorrentAction = new QAction("Resume", this);
    QAction *downloadSpeedSetUnlimitedAction = new QAction("Set Unlimited", this);
    QAction *downloadSpeedSetOtherAction = new QAction("Other...", this);
    QAction *uploadSpeedSetUnlimitedAction = new QAction("Set Unlimited", this);
    QAction *uploadSpeedSetOtherAction = new QAction("Other...", this);
    QAction *connectionSetUnlimitedAction = new QAction("Set Unlimited", this);
    QAction *connectionSetOtherAction = new QAction("Other...", this);
    QAction *uploadSlotSetUnlimitedAction = new QAction("Set Unlimited", this);
    QAction *uploadSlotSetOtherAction = new QAction("Other...", this);
    QAction *autoManagedOnAction = new QAction("On", this);
    QAction *autoManagedOffAction = new QAction("Off", this);
    QAction *queueTopAction = new QAction("Top", this);
    QAction *queueUpAction = new QAction("Up", this);
    QAction *queueDownAction = new QAction("Down", this);
    QAction *queueBottomAction = new QAction("Bottom", this);
    QAction *updateTrackerAction = new QAction("Update Tracker", this);
    QAction *editTrackersAction = new QAction("Edit Trackers", this);
    QAction *removeTorrentAction = new QAction("Remove", this);
    QAction *forceRecheckAction = new QAction("Force Re-check", this);
    QAction *moveStorageAction = new QAction("Move Storage", this);

    // View actions
    QAction *sidebarAction = new QAction("Sidebar", this);
    sidebarAction->setCheckable(true);
    sidebarAction->setChecked(settings.value("uiconfig/show_sidebar").toBool());
    QAction *nameColumnAction = new QAction("Name", this);
    nameColumnAction->setCheckable(true);
    nameColumnAction->setChecked(this->components->torrentView->is_column_visible("Name"));
    QAction *progressColumnAction = new QAction("Progress", this);
    progressColumnAction->setCheckable(true);
    progressColumnAction->setChecked(this->components->torrentView->is_column_visible("Progress"));
    QAction *numColumnAction = new QAction("#", this);
    numColumnAction->setCheckable(true);
    numColumnAction->setChecked(this->components->torrentView->is_column_visible("#"));
    QAction *sizeColumnAction = new QAction("Size", this);
    sizeColumnAction->setCheckable(true);
    sizeColumnAction->setChecked(this->components->torrentView->is_column_visible("Size"));
    QAction *seedersColumnAction = new QAction("Seeders", this);
    seedersColumnAction->setCheckable(true);
    seedersColumnAction->setChecked(this->components->torrentView->is_column_visible("Seeders"));
    QAction *peersColumnAction = new QAction("Peers", this);
    peersColumnAction->setCheckable(true);
    peersColumnAction->setChecked(this->components->torrentView->is_column_visible("Peers"));
    QAction *downSpeedColumnAction = new QAction("Down Speed", this);
    downSpeedColumnAction->setCheckable(true);
    downSpeedColumnAction->setChecked(this->components->torrentView->is_column_visible("Down Speed"));
    QAction *upSpeedColumnAction = new QAction("Up Speed", this);
    upSpeedColumnAction->setCheckable(true);
    upSpeedColumnAction->setChecked(this->components->torrentView->is_column_visible("Up Speed"));
    QAction *etaColumnAction = new QAction("ETA", this);
    etaColumnAction->setCheckable(true);
    etaColumnAction->setChecked(this->components->torrentView->is_column_visible("ETA"));
    QAction *ratioColumnAction = new QAction("Ratio", this);
    ratioColumnAction->setCheckable(true);
    ratioColumnAction->setChecked(this->components->torrentView->is_column_visible("Ratio"));
    QAction *availColumnAction = new QAction("Avail", this);
    availColumnAction->setCheckable(true);
    availColumnAction->setChecked(this->components->torrentView->is_column_visible("Avail"));
    QAction *addedColumnAction = new QAction("Added", this);
    addedColumnAction->setCheckable(true);
    addedColumnAction->setChecked(this->components->torrentView->is_column_visible("Added"));
    QAction *trackerColumnAction = new QAction("Tracker", this);
    trackerColumnAction->setCheckable(true);
    trackerColumnAction->setChecked(this->components->torrentView->is_column_visible("Tracker"));
    QAction *showZeroHitsAction = new QAction("Show Zero Hits", this);
    showZeroHitsAction->setCheckable(true);
    showZeroHitsAction->setChecked(settings.value("uiconfig/sidebar_show_zero").toBool());

    QAction *preferencesAction = new QAction("Preferences", this);

    QMenuBar *mainMenu = mainWindow->menuBar();
    QMenu *fileMenu = mainMenu->addMenu("File");
    fileMenu->addAction(addTorrentAction);
    fileMenu->addSeparator();
    fileMenu->addAction("Exit", qApp, SLOT(quit()));

    QMenu *editMenu = mainMenu->addMenu("Edit");
    editMenu->addAction(preferencesAction);

    // Torrent menu
    torrentMenu = mainMenu->addMenu("Torrent");
    torrentMenu->addAction(openFolderAction);
    torrentMenu->addSeparator();
    torrentMenu->addAction(pauseTorrentAction);
    torrentMenu->addAction(resumeTorrentAction);
    torrentMenu->addSeparator();
    QMenu* optionsMenu = torrentMenu->addMenu("Options");
    QMenu* downloadSpeedLimitMenu = optionsMenu->addMenu("Download Speed Limit");
    downloadSpeedLimitMenu->addAction(downloadSpeedSetUnlimitedAction);
    downloadSpeedLimitMenu->addAction(downloadSpeedSetOtherAction);
    QMenu* uploadSpeedLimitMenu = optionsMenu->addMenu("Upload Speed Limit");
    uploadSpeedLimitMenu->addAction(uploadSpeedSetUnlimitedAction);
    uploadSpeedLimitMenu->addAction(uploadSpeedSetOtherAction);
    QMenu* connectionLimitMenu = optionsMenu->addMenu("Connection Limit");
    connectionLimitMenu->addAction(connectionSetUnlimitedAction);
    connectionLimitMenu->addAction(connectionSetOtherAction);
    QMenu* uploadSlotLimitMenu = optionsMenu->addMenu("Upload Slot Limit");
    uploadSlotLimitMenu->addAction(uploadSlotSetUnlimitedAction);
    uploadSlotLimitMenu->addAction(uploadSlotSetOtherAction);
    QMenu* autoManagedMenu = optionsMenu->addMenu("Auto Managed");
    autoManagedMenu->addAction(autoManagedOnAction);
    autoManagedMenu->addAction(autoManagedOffAction);
    torrentMenu->addSeparator();
    QMenu* queueMenu = torrentMenu->addMenu("Queue");
    queueMenu->addAction(queueTopAction);
    queueMenu->addAction(queueUpAction);
    queueMenu->addAction(queueDownAction);
    queueMenu->addAction(queueBottomAction);
    torrentMenu->addSeparator();
    torrentMenu->addAction(updateTrackerAction);
    torrentMenu->addAction(editTrackersAction);
    torrentMenu->addSeparator();
    torrentMenu->addAction(removeTorrentAction);
    torrentMenu->addSeparator();
    torrentMenu->addAction(forceRecheckAction);
    torrentMenu->addAction(moveStorageAction);

    // View menu
    QMenu *viewMenu = mainMenu->addMenu("View");
    viewMenu->addAction(sidebarAction);
    viewMenu->addSeparator();
    QMenu *columnsMenu = viewMenu->addMenu("Columns");
    columnsMenu->addAction(nameColumnAction);
    columnsMenu->addAction(progressColumnAction);
    columnsMenu->addAction(numColumnAction);
    columnsMenu->addAction(sizeColumnAction);
    columnsMenu->addAction(seedersColumnAction);
    columnsMenu->addAction(peersColumnAction);
    columnsMenu->addAction(downSpeedColumnAction);
    columnsMenu->addAction(upSpeedColumnAction);
    columnsMenu->addAction(etaColumnAction);
    columnsMenu->addAction(ratioColumnAction);
    columnsMenu->addAction(availColumnAction);
    columnsMenu->addAction(addedColumnAction);
    columnsMenu->addAction(trackerColumnAction);
    QMenu *sidebarMenu = viewMenu->addMenu("Sidebar");
    sidebarMenu->addAction(showZeroHitsAction);

    // Help menu
    QMenu *helpMenu = mainMenu->addMenu("Help");
    helpMenu->addAction("About", mainWindow, SLOT(about()));
    helpMenu->addAction("About Qt", qApp, SLOT(aboutQt()));

    this->connect(addTorrentAction, SIGNAL(triggered()), this, SLOT(addTorrentActivate()));
    this->connect(openFolderAction, SIGNAL(triggered()), this, SLOT(openFolderActivate()));
    this->connect(pauseTorrentAction, SIGNAL(triggered()), this, SLOT(pauseTorrentActivate()));
    this->connect(resumeTorrentAction, SIGNAL(triggered()), this, SLOT(resumeTorrentActivate()));
    this->connect(downloadSpeedSetUnlimitedAction, SIGNAL(triggered()), this, SLOT(downloadSpeedSetUnlimitedActivate()));
    this->connect(downloadSpeedSetOtherAction, SIGNAL(triggered()), this, SLOT(downloadSpeedSetOtherActivate()));
    this->connect(uploadSpeedSetUnlimitedAction, SIGNAL(triggered()), this, SLOT(uploadSpeedSetUnlimitedActivate()));
    this->connect(uploadSpeedSetOtherAction, SIGNAL(triggered()), this, SLOT(uploadSpeedSetOtherActivate()));
    this->connect(connectionSetUnlimitedAction, SIGNAL(triggered()), this, SLOT(connectionSetUnlimitedActivate()));
    this->connect(connectionSetOtherAction, SIGNAL(triggered()), this, SLOT(connectionSetOtherActivate()));
    this->connect(uploadSlotSetUnlimitedAction, SIGNAL(triggered()), this, SLOT(uploadSlotSetUnlimitedActivate()));
    this->connect(uploadSlotSetOtherAction, SIGNAL(triggered()), this, SLOT(uploadSlotSetOtherActivate()));
    this->connect(autoManagedOnAction, SIGNAL(triggered()), this, SLOT(autoManagedOnActivate()));
    this->connect(autoManagedOffAction, SIGNAL(triggered()), this, SLOT(autoManagedOffActivate()));
    this->connect(queueTopAction, SIGNAL(triggered()), this, SLOT(queueTopActivate()));
    this->connect(queueUpAction, SIGNAL(triggered()), this, SLOT(queueUpActivate()));
    this->connect(queueDownAction, SIGNAL(triggered()), this, SLOT(queueDownActivate()));
    this->connect(queueBottomAction, SIGNAL(triggered()), this, SLOT(queueBottomActivate()));
    this->connect(updateTrackerAction, SIGNAL(triggered()), this, SLOT(updateTrackerActivate()));
    this->connect(editTrackersAction, SIGNAL(triggered()), this, SLOT(editTrackersActivate()));
    this->connect(removeTorrentAction, SIGNAL(triggered()), this, SLOT(removeTorrentActivate()));
    this->connect(forceRecheckAction, SIGNAL(triggered()), this, SLOT(forceRecheckActivate()));
    this->connect(moveStorageAction, SIGNAL(triggered()), this, SLOT(moveStorageActivate()));
    this->connect(sidebarAction, SIGNAL(triggered(bool)), this, SLOT(sidebarActivate(bool)));
    this->connect(nameColumnAction, SIGNAL(triggered(bool)), this, SLOT(nameColumnActivate(bool)));
    this->connect(progressColumnAction, SIGNAL(triggered(bool)), this, SLOT(progressColumnActivate(bool)));
    this->connect(numColumnAction, SIGNAL(triggered(bool)), this, SLOT(numColumnActivate(bool)));
    this->connect(sizeColumnAction, SIGNAL(triggered(bool)), this, SLOT(sizeColumnActivate(bool)));
    this->connect(seedersColumnAction, SIGNAL(triggered(bool)), this, SLOT(seedersColumnActivate(bool)));
    this->connect(peersColumnAction, SIGNAL(triggered(bool)), this, SLOT(peersColumnActivate(bool)));
    this->connect(downSpeedColumnAction, SIGNAL(triggered(bool)), this, SLOT(downSpeedColumnActivate(bool)));
    this->connect(upSpeedColumnAction, SIGNAL(triggered(bool)), this, SLOT(upSpeedColumnActivate(bool)));
    this->connect(etaColumnAction, SIGNAL(triggered(bool)), this, SLOT(etaColumnActivate(bool)));
    this->connect(ratioColumnAction, SIGNAL(triggered(bool)), this, SLOT(ratioColumnActivate(bool)));
    this->connect(availColumnAction, SIGNAL(triggered(bool)), this, SLOT(availColumnActivate(bool)));
    this->connect(addedColumnAction, SIGNAL(triggered(bool)), this, SLOT(addedColumnActivate(bool)));
    this->connect(trackerColumnAction, SIGNAL(triggered(bool)), this, SLOT(trackerColumnActivate(bool)));
    this->connect(showZeroHitsAction, SIGNAL(triggered(bool)), this, SLOT(showZeroHitsActivate(bool)));
    this->connect(preferencesAction, SIGNAL(triggered()), this, SLOT(preferencesActivate()));
}

QMenu* MenuBar::getTorrentMenu()
{
    return this->torrentMenu;
}

void MenuBar::addTorrentActivate()
{
    this->components->addTorrentDialog->show_add_torrent_dialog();
}

void MenuBar::openFolderActivate()
{
    this->components->torrentManager->open_folder_for_selected();
}

void MenuBar::pauseTorrentActivate()
{
    this->components->torrentManager->pause_selected();
}

void MenuBar::resumeTorrentActivate()
{
    this->components->torrentManager->resume_selected();
}

void MenuBar::downloadSpeedSetUnlimitedActivate()
{
    this->components->torrentManager->set_download_speed_for_selected(-1);
}

void MenuBar::downloadSpeedSetOtherActivate()
{
    int value = SetOtherDialog("Set Maximum Download Speed", "KiB/s", this).show_set_other_dialog();
    if(value > -2)
        this->components->torrentManager->set_download_speed_for_selected(value);
}

void MenuBar::uploadSpeedSetUnlimitedActivate()
{
    this->components->torrentManager->set_upload_speed_for_selected(-1);
}

void MenuBar::uploadSpeedSetOtherActivate()
{
    int value = SetOtherDialog("Set Maximum Upload Speed", "KiB/s", this).show_set_other_dialog();
    if(value > -2)
        this->components->torrentManager->set_upload_speed_for_selected(value);
}

void MenuBar::connectionSetUnlimitedActivate()
{
    this->components->torrentManager->set_connection_for_selected(-1);
}

void MenuBar::connectionSetOtherActivate()
{
    int value = SetOtherDialog("Set Maximum Connections", "", this).show_set_other_dialog();
    if(value > -2)
        this->components->torrentManager->set_connection_for_selected(value);
}

void MenuBar::uploadSlotSetUnlimitedActivate()
{
    this->components->torrentManager->set_upload_slot_for_selected(-1);
}

void MenuBar::uploadSlotSetOtherActivate()
{
    int value = SetOtherDialog("Set Maximum Upload Slots", "", this).show_set_other_dialog();
    if(value > -2)
        this->components->torrentManager->set_upload_slot_for_selected(value);
}

void MenuBar::autoManagedOnActivate()
{
    this->components->torrentManager->set_auto_managed_for_selected(true);
}

void MenuBar::autoManagedOffActivate()
{
    this->components->torrentManager->set_auto_managed_for_selected(false);
}

void MenuBar::queueTopActivate()
{
    this->components->torrentManager->queue_top_selected();
}

void MenuBar::queueUpActivate()
{
    this->components->torrentManager->queue_up_selected();
}

void MenuBar::queueDownActivate()
{
    this->components->torrentManager->queue_down_selected();
}

void MenuBar::queueBottomActivate()
{
    this->components->torrentManager->queue_bottom_selected();
}

void MenuBar::updateTrackerActivate()
{
    this->components->torrentManager->force_reannounce_selected();
}

void MenuBar::editTrackersActivate()
{
    QList<QString> selected = this->components->torrentView->get_selected_torrents();
    if(!selected.isEmpty())
        EditTrackersDialog(this->components, selected[0]).show_edit_trackers_dialog();
}

void MenuBar::removeTorrentActivate()
{
    this->components->torrentManager->remove_selected();
}

void MenuBar::forceRecheckActivate()
{
    this->components->torrentManager->force_recheck_selected();
}

void MenuBar::moveStorageActivate()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");
    this->components->torrentManager->move_storage_for_selected(folder);
}

void MenuBar::sidebarActivate(bool checked)
{
    QSettings settings;
    settings.setValue("uiconfig/show_sidebar", checked);
    if(checked)
        this->components->filterTreeView->show();
    else
        this->components->filterTreeView->hide();
}

void MenuBar::nameColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Name", checked);
}

void MenuBar::progressColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Progress", checked);
}

void MenuBar::numColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("#", checked);
}

void MenuBar::sizeColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Size", checked);
}

void MenuBar::seedersColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Seeders", checked);
}

void MenuBar::peersColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Peers", checked);
}

void MenuBar::downSpeedColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Down Speed", checked);
}

void MenuBar::upSpeedColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Up Speed", checked);
}

void MenuBar::etaColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("ETA", checked);
}

void MenuBar::ratioColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Ratio", checked);
}

void MenuBar::availColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Avail", checked);
}

void MenuBar::addedColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Added", checked);
}

void MenuBar::trackerColumnActivate(bool checked)
{
    this->components->torrentView->set_column_visible("Tracker", checked);
}

void MenuBar::showZeroHitsActivate(bool checked)
{
    QSettings settings;
    settings.setValue("uiconfig/sidebar_show_zero", checked);
    this->components->filterTreeView->show_zero_hits(checked);
}

void MenuBar::preferencesActivate()
{
    this->components->preferencesDialog->show_preferences_dialog();
}
