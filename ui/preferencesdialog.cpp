#include "preferencesdialog.h"
#include "components.h"

#include <QSettings>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeView>
#include <QStandardItemModel>

PreferencesDialog::PreferencesDialog(Components *comp, QWidget *parent) :
    QDialog(parent)
{
    this->components = comp;

    this->setWindowTitle("Preferences");

    QTreeView *categoriesView = new QTreeView();
    QStandardItemModel *categoriesModel = new QStandardItemModel(0, 1);
    categoriesView->setModel(categoriesModel);
    categoriesView->setHeaderHidden(true);
    categoriesView->setRootIsDecorated(false);
    categoriesView->setSelectionBehavior(QAbstractItemView::SelectRows);
    categoriesView->setFixedWidth(100);
    categoriesView->setColumnWidth(0, 90);
    categoriesView->header()->setSectionResizeMode(0, QHeaderView::Fixed);
    categoriesModel->appendRow(new QStandardItem("General"));
    categoriesModel->appendRow(new QStandardItem("Downloads"));
    categoriesModel->appendRow(new QStandardItem("Queue"));
    categoriesModel->appendRow(new QStandardItem("Network"));
    categoriesModel->appendRow(new QStandardItem("Bandwidth"));
    categoriesView->selectionModel()->setCurrentIndex(categoriesModel->index(0, 0), QItemSelectionModel::ClearAndSelect);
    this->connect(categoriesView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
                  this, SLOT(changePage(QModelIndex,QModelIndex)));

    // General page
    QWidget *generalPage = new QWidget(this);

    QLabel *generalMainLabel = new QLabel("<b>General</b>");
    generalMainLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QFrame *generalSeparator = new QFrame();
    generalSeparator->setFrameShape(QFrame::HLine);
    generalSeparator->setFrameShadow(QFrame::Sunken);

    QLabel *defaultTorrentDownloaderLabel = new QLabel("<b>Make BitLord your default torrent downloader</b>");
    defaultTorrentDownloaderLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    associateTorrentFilesButton = new QPushButton("Associate .torrent files with BitLord");
    this->connect(associateTorrentFilesButton, SIGNAL(clicked()), this, SLOT(associateTorrentFilesClicked()));

    associateMagnetsButton = new QPushButton("Associate magnet URIs with BitLord");
    this->connect(associateMagnetsButton, SIGNAL(clicked()), this, SLOT(associateMagnetsClicked()));

    QVBoxLayout *assocButtonsLayout = new QVBoxLayout();
    assocButtonsLayout->addWidget(associateTorrentFilesButton);
    assocButtonsLayout->addWidget(associateMagnetsButton);

    checkAssociationOnStartup = new QCheckBox("Check associations on startup");

    startOnStartupCheck = new QCheckBox("Start BitLord on system startup");

    QVBoxLayout *generalChecksLayout = new QVBoxLayout();
    generalChecksLayout->addWidget(checkAssociationOnStartup);
    generalChecksLayout->addWidget(startOnStartupCheck);

    QHBoxLayout *defaultTorrentDownloaderLayout = new QHBoxLayout();
    defaultTorrentDownloaderLayout->addLayout(assocButtonsLayout);
    defaultTorrentDownloaderLayout->addLayout(generalChecksLayout);
    defaultTorrentDownloaderLayout->addStretch();

    QLabel *whenDownloadingLabel = new QLabel("<b>When downloading</b>");
    whenDownloadingLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    preventHibernationCheck = new QCheckBox("Prevent PC standby if active downloading/uploading");

    QHBoxLayout *preventHibernationLayout = new QHBoxLayout();
    preventHibernationLayout->addSpacing(10);
    preventHibernationLayout->addWidget(preventHibernationCheck);

    QVBoxLayout *generalLayout = new QVBoxLayout();
    generalLayout->addWidget(generalMainLabel);
    generalLayout->addWidget(generalSeparator);
    generalLayout->addWidget(defaultTorrentDownloaderLabel);
    generalLayout->addLayout(defaultTorrentDownloaderLayout);
    generalLayout->addSpacing(20);
    generalLayout->addWidget(whenDownloadingLabel);
    generalLayout->addLayout(preventHibernationLayout);
    generalLayout->setAlignment(Qt::AlignTop);
    generalPage->setLayout(generalLayout);

    // Downloads page
    QWidget *downloadsPage = new QWidget(this);

    QLabel *downloadsMainLabel = new QLabel("<b>Downloads</b>");
    downloadsMainLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QFrame *downloadsSeparator = new QFrame();
    downloadsSeparator->setFrameShape(QFrame::HLine);
    downloadsSeparator->setFrameShadow(QFrame::Sunken);

    QLabel *foldersLabel = new QLabel("<b>Folders</b>");
    foldersLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QLabel *downloadToLabel = new QLabel("Download to:");

    downloadToText = new QLineEdit();
    downloadToText->setReadOnly(true);

    QPushButton *downloadToButton = new QPushButton("...");
    this->connect(downloadToButton, SIGNAL(clicked()), this, SLOT(downloadToClicked()));

    QHBoxLayout *downloadToLayout = new QHBoxLayout();
    downloadToLayout->addSpacing(10);
    downloadToLayout->addWidget(downloadToLabel);
    downloadToLayout->addSpacing(78);
    downloadToLayout->addWidget(downloadToText);
    downloadToLayout->addWidget(downloadToButton);

    moveCompletedToCheck = new QCheckBox("Move completed to:");
    this->connect(moveCompletedToCheck, SIGNAL(toggled(bool)), this, SLOT(moveCompletedToToggled(bool)));

    moveCompletedToText = new QLineEdit();
    moveCompletedToText->setReadOnly(true);

    moveCompletedToButton = new QPushButton("...");
    this->connect(moveCompletedToButton, SIGNAL(clicked()), this, SLOT(moveCompletedToClicked()));

    QHBoxLayout *moveCompletedToLayout = new QHBoxLayout();
    moveCompletedToLayout->addSpacing(10);
    moveCompletedToLayout->addWidget(moveCompletedToCheck);
    moveCompletedToLayout->addSpacing(24);
    moveCompletedToLayout->addWidget(moveCompletedToText);
    moveCompletedToLayout->addWidget(moveCompletedToButton);

    autoAddTorrentsCheck = new QCheckBox("Auto add .torrents from:");
    this->connect(autoAddTorrentsCheck, SIGNAL(toggled(bool)), this, SLOT(autoAddTorrentsToggled(bool)));

    autoAddTorrentsText = new QLineEdit();
    autoAddTorrentsText->setReadOnly(true);

    autoAddTorrentsButton = new QPushButton("...");
    this->connect(autoAddTorrentsButton, SIGNAL(clicked()), this, SLOT(autoAddTorrentsClicked()));

    QHBoxLayout *autoAddTorrentsLayout = new QHBoxLayout();
    autoAddTorrentsLayout->addSpacing(10);
    autoAddTorrentsLayout->addWidget(autoAddTorrentsCheck);
    autoAddTorrentsLayout->addWidget(autoAddTorrentsText);
    autoAddTorrentsLayout->addWidget(autoAddTorrentsButton);

    copyTorrentFilesToCheck = new QCheckBox("Copy of .torrent files to:");
    this->connect(copyTorrentFilesToCheck, SIGNAL(toggled(bool)), this, SLOT(copyTorrentFilesToToggled(bool)));

    copyTorrentFilesToText = new QLineEdit();
    copyTorrentFilesToText->setReadOnly(true);

    copyTorrentFilesToButton = new QPushButton("...");
    this->connect(copyTorrentFilesToButton, SIGNAL(clicked()), this, SLOT(copyTorrentFilesToClicked()));

    QHBoxLayout *copyTorrentFilesToLayout = new QHBoxLayout();
    copyTorrentFilesToLayout->addSpacing(10);
    copyTorrentFilesToLayout->addWidget(copyTorrentFilesToCheck);
    copyTorrentFilesToLayout->addSpacing(1);
    copyTorrentFilesToLayout->addWidget(copyTorrentFilesToText);
    copyTorrentFilesToLayout->addWidget(copyTorrentFilesToButton);

    deleteCopyTorrentCheck = new QCheckBox("Delete copy of torrent file on remove");

    QHBoxLayout *deleteCopyTorrentLayout = new QHBoxLayout();
    deleteCopyTorrentLayout->addSpacing(20);
    deleteCopyTorrentLayout->addWidget(deleteCopyTorrentCheck);

    QLabel *allocationLabel = new QLabel("<b>Allocation</b>");
    allocationLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    fullAllocationRButton = new QRadioButton("Use Full Allocation");
    fullAllocationRButton->setChecked(true);

    compactAllocationRButton = new QRadioButton("Use Compact Allocation");

    QHBoxLayout *allocationLayout = new QHBoxLayout();
    allocationLayout->addSpacing(10);
    allocationLayout->addWidget(fullAllocationRButton);
    allocationLayout->addWidget(compactAllocationRButton);
    allocationLayout->setAlignment(Qt::AlignLeft);

    QLabel *optionsLabel = new QLabel("<b>Options</b>");
    optionsLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    prioritiseFirstLastCheck = new QCheckBox("Prioritise first and last pieces of torrent");

    QHBoxLayout *prioritiseFirstLastLayout = new QHBoxLayout();
    prioritiseFirstLastLayout->addSpacing(10);
    prioritiseFirstLastLayout->addWidget(prioritiseFirstLastCheck);

    addPausedTorrentsCheck = new QCheckBox("Add torrents in Paused state");

    QHBoxLayout *addPausedTorrentsLayout = new QHBoxLayout();
    addPausedTorrentsLayout->addSpacing(10);
    addPausedTorrentsLayout->addWidget(addPausedTorrentsCheck);

    QVBoxLayout *downloadsLayout = new QVBoxLayout();
    downloadsLayout->addWidget(downloadsMainLabel);
    downloadsLayout->addWidget(downloadsSeparator);
    downloadsLayout->addWidget(foldersLabel);
    downloadsLayout->addLayout(downloadToLayout);
    downloadsLayout->addLayout(moveCompletedToLayout);
    downloadsLayout->addLayout(autoAddTorrentsLayout);
    downloadsLayout->addLayout(copyTorrentFilesToLayout);
    downloadsLayout->addLayout(deleteCopyTorrentLayout);
    downloadsLayout->addSpacing(20);
    downloadsLayout->addWidget(allocationLabel);
    downloadsLayout->addLayout(allocationLayout);
    downloadsLayout->addSpacing(20);
    downloadsLayout->addWidget(optionsLabel);
    downloadsLayout->addLayout(prioritiseFirstLastLayout);
    downloadsLayout->addLayout(addPausedTorrentsLayout);
    downloadsLayout->setAlignment(Qt::AlignTop);
    downloadsPage->setLayout(downloadsLayout);

    // Queue page
    QWidget *queuePage = new QWidget(this);

    QLabel *queueMainLabel = new QLabel("<b>Queue</b>");
    queueMainLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QFrame *queueSeparator = new QFrame();
    queueSeparator->setFrameShape(QFrame::HLine);
    queueSeparator->setFrameShadow(QFrame::Sunken);

    QLabel *generalLabel = new QLabel("<b>General</b>");
    generalLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    queueNewToTopCheck = new QCheckBox("Queue new torrents to top");

    QHBoxLayout *queueNewToTopLayout = new QHBoxLayout();
    queueNewToTopLayout->addSpacing(10);
    queueNewToTopLayout->addWidget(queueNewToTopCheck);

    QLabel *activeTorrentsLabel = new QLabel("<b>Active Torrents</b>");
    activeTorrentsLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QLabel *totalActiveLabel = new QLabel("Total active:");

    totalActiveSpin = new QSpinBox();
    totalActiveSpin->setRange(-1, 9999);

    QHBoxLayout *totalActiveLayout = new QHBoxLayout();
    totalActiveLayout->addSpacing(10);
    totalActiveLayout->addWidget(totalActiveLabel);
    totalActiveLayout->addSpacing(60);
    totalActiveLayout->addWidget(totalActiveSpin);
    totalActiveLayout->addStretch();

    QLabel *totalActiveDownloadingLabel = new QLabel("Total activedownloading:");

    totalActiveDownloadingSpin = new QSpinBox();
    totalActiveDownloadingSpin->setRange(-1, 9999);

    QHBoxLayout *totalActiveDownloadingLayout = new QHBoxLayout();
    totalActiveDownloadingLayout->addSpacing(10);
    totalActiveDownloadingLayout->addWidget(totalActiveDownloadingLabel);
    totalActiveDownloadingLayout->addWidget(totalActiveDownloadingSpin);
    totalActiveDownloadingLayout->addStretch();

    QLabel *totalActiveSeedingLabel = new QLabel("Total active:");

    totalActiveSeedingSpin = new QSpinBox();
    totalActiveSeedingSpin->setRange(-1, 9999);

    QHBoxLayout *totalActiveSeedingLayout = new QHBoxLayout();
    totalActiveSeedingLayout->addSpacing(10);
    totalActiveSeedingLayout->addWidget(totalActiveSeedingLabel);
    totalActiveSeedingLayout->addSpacing(60);
    totalActiveSeedingLayout->addWidget(totalActiveSeedingSpin);
    totalActiveSeedingLayout->addStretch();

    dontCountSlowTorrentsCheck = new QCheckBox("Do not count slow torrents");

    QHBoxLayout *dontCountSlowTorrentsLayout = new QHBoxLayout();
    dontCountSlowTorrentsLayout->addSpacing(10);
    dontCountSlowTorrentsLayout->addWidget(dontCountSlowTorrentsCheck);

    QLabel *seedingLabel = new QLabel("<b>Seeding</b>");
    seedingLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QLabel *shareRatioLimitLabel = new QLabel("Share Ratio Limit:");

    shareRatioLimitSpin = new QDoubleSpinBox();
    shareRatioLimitSpin->setRange(-1.0, 100.0);
    shareRatioLimitSpin->setSingleStep(0.1);
    shareRatioLimitSpin->setMinimumWidth(60);

    QHBoxLayout *shareRatioLimitLayout = new QHBoxLayout();
    shareRatioLimitLayout->addSpacing(10);
    shareRatioLimitLayout->addWidget(shareRatioLimitLabel);
    shareRatioLimitLayout->addWidget(shareRatioLimitSpin);
    shareRatioLimitLayout->addStretch();

    QLabel *seedTimeRatioLabel = new QLabel("Seed Time Ratio:");

    seedTimeRatioSpin = new QDoubleSpinBox();
    seedTimeRatioSpin->setRange(-1.0, 100.0);
    seedTimeRatioSpin->setSingleStep(0.1);
    seedTimeRatioSpin->setMinimumWidth(60);

    QHBoxLayout *seedTimeRatioLayout = new QHBoxLayout();
    seedTimeRatioLayout->addSpacing(10);
    seedTimeRatioLayout->addWidget(seedTimeRatioLabel);
    seedTimeRatioLayout->addSpacing(3);
    seedTimeRatioLayout->addWidget(seedTimeRatioSpin);
    seedTimeRatioLayout->addStretch();

    QLabel *seedTimeLabel = new QLabel("Seed Time (m):");

    seedTimeSpin = new QSpinBox();
    seedTimeSpin->setRange(-1, 9999);
    seedTimeSpin->setMinimumWidth(60);

    QHBoxLayout *seedTimeLayout = new QHBoxLayout();
    seedTimeLayout->addSpacing(10);
    seedTimeLayout->addWidget(seedTimeLabel);
    seedTimeLayout->addSpacing(12);
    seedTimeLayout->addWidget(seedTimeSpin);
    seedTimeLayout->addStretch();

    stopRatioCheck = new QCheckBox("Stop seeding when share ratio reaches:");
    this->connect(stopRatioCheck, SIGNAL(toggled(bool)), this, SLOT(stopRatioToggled(bool)));

    stopRatioSpin = new QDoubleSpinBox();
    stopRatioSpin->setRange(0.5, 100.0);
    stopRatioSpin->setSingleStep(0.1);

    QHBoxLayout *stopRatioLayout = new QHBoxLayout();
    stopRatioLayout->addSpacing(10);
    stopRatioLayout->addWidget(stopRatioCheck);
    stopRatioLayout->addWidget(stopRatioSpin);
    stopRatioLayout->addStretch();

    removeIfStopRatioCheck = new QCheckBox("Remove torrent when share ratio reached");

    QHBoxLayout *removeIfStopRatioLayout = new QHBoxLayout();
    removeIfStopRatioLayout->addSpacing(20);
    removeIfStopRatioLayout->addWidget(removeIfStopRatioCheck);

    QVBoxLayout *queueLayout = new QVBoxLayout();
    queueLayout->addWidget(queueMainLabel);
    queueLayout->addWidget(queueSeparator);
    queueLayout->addWidget(generalLabel);
    queueLayout->addLayout(queueNewToTopLayout);
    queueLayout->addSpacing(20);
    queueLayout->addWidget(activeTorrentsLabel);
    queueLayout->addLayout(totalActiveLayout);
    queueLayout->addLayout(totalActiveDownloadingLayout);
    queueLayout->addLayout(totalActiveSeedingLayout);
    queueLayout->addLayout(dontCountSlowTorrentsLayout);
    queueLayout->addSpacing(20);
    queueLayout->addWidget(seedingLabel);
    queueLayout->addLayout(shareRatioLimitLayout);
    queueLayout->addLayout(seedTimeRatioLayout);
    queueLayout->addLayout(seedTimeLayout);
    queueLayout->addLayout(stopRatioLayout);
    queueLayout->addLayout(removeIfStopRatioLayout);
    queueLayout->setAlignment(Qt::AlignTop);
    queuePage->setLayout(queueLayout);

    // Network page
    QWidget *networkPage = new QWidget(this);

    QLabel *networkMainLabel = new QLabel("<b>Network</b>");
    networkMainLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QFrame *networkSeparator = new QFrame();
    networkSeparator->setFrameShape(QFrame::HLine);
    networkSeparator->setFrameShadow(QFrame::Sunken);

    QLabel *incomingPortsLabel = new QLabel("<b>Incoming Ports</b>");

    useRandomPortsCheck = new QCheckBox("Use Random Ports");
    this->connect(useRandomPortsCheck, SIGNAL(toggled(bool)), this, SLOT(useRandomPortsToggled(bool)));

    QLabel *activePortLabel = new QLabel("Active Port:");

    activePortValueLabel = new QLabel("...");

    QHBoxLayout *activePortLayout = new QHBoxLayout();
    activePortLayout->addSpacing(10);
    activePortLayout->addWidget(useRandomPortsCheck);
    activePortLayout->addSpacing(20);
    activePortLayout->addWidget(activePortLabel);
    activePortLayout->addWidget(activePortValueLabel);
    activePortLayout->addStretch();

    QLabel *incomingPortsFromLabel = new QLabel("From:");

    incomingPortsFromSpin = new QSpinBox();
    incomingPortsFromSpin->setRange(0, 65535);

    QLabel *incomingPortsToLabel = new QLabel("To:");

    incomingPortsToSpin = new QSpinBox();
    incomingPortsToSpin->setRange(0, 65535);

    QHBoxLayout *incomingPortsLayout = new QHBoxLayout();
    incomingPortsLayout->addSpacing(10);
    incomingPortsLayout->addWidget(incomingPortsFromLabel);
    incomingPortsLayout->addWidget(incomingPortsFromSpin);
    incomingPortsLayout->addSpacing(10);
    incomingPortsLayout->addWidget(incomingPortsToLabel);
    incomingPortsLayout->addWidget(incomingPortsToSpin);
    incomingPortsLayout->addStretch();

    QLabel *outgoingPortsLabel = new QLabel("<b>Outgoing Ports</b>");

    useRandomOutgoingPortsCheck = new QCheckBox("Use Random Ports");
    this->connect(useRandomOutgoingPortsCheck, SIGNAL(toggled(bool)), this, SLOT(useRandomOutgoingPortsToggled(bool)));

    QHBoxLayout *useRandomOutgoingPortsLayout = new QHBoxLayout();
    useRandomOutgoingPortsLayout->addSpacing(10);
    useRandomOutgoingPortsLayout->addWidget(useRandomOutgoingPortsCheck);

    QLabel *outgoingPortsFromLabel = new QLabel("From:");

    outgoingPortsFromSpin = new QSpinBox();
    outgoingPortsFromSpin->setRange(0, 65535);

    QLabel *outgoingPortsToLabel = new QLabel("To:");

    outgoingPortsToSpin = new QSpinBox();
    outgoingPortsToSpin->setRange(0, 65535);

    QHBoxLayout *outgoingPortsLayout = new QHBoxLayout();
    outgoingPortsLayout->addSpacing(10);
    outgoingPortsLayout->addWidget(outgoingPortsFromLabel);
    outgoingPortsLayout->addWidget(outgoingPortsFromSpin);
    outgoingPortsLayout->addSpacing(10);
    outgoingPortsLayout->addWidget(outgoingPortsToLabel);
    outgoingPortsLayout->addWidget(outgoingPortsToSpin);
    outgoingPortsLayout->addStretch();

    QLabel *listenInterfaceLabel = new QLabel("<b>Interface</b>");

    listenInterfaceText = new QLineEdit();
    listenInterfaceText->setFixedWidth(220);

    QHBoxLayout *listenInterfaceLayout = new QHBoxLayout();
    listenInterfaceLayout->addSpacing(10);
    listenInterfaceLayout->addWidget(listenInterfaceText);
    listenInterfaceLayout->addStretch();

    QLabel *tosLabel = new QLabel("<b>TOS</b>");

    QLabel *peerTOSLabel = new QLabel("Peer TOS Byte:");

    peerTOSText = new QLineEdit();
    peerTOSText->setFixedWidth(40);

    QHBoxLayout *peerTOSLayout = new QHBoxLayout();
    peerTOSLayout->addSpacing(10);
    peerTOSLayout->addWidget(peerTOSLabel);
    peerTOSLayout->addWidget(peerTOSText);
    peerTOSLayout->addStretch();

    QLabel *networkExtrasLabel = new QLabel("<b>Network Extras</b>");

    upnpCheck = new QCheckBox("UPnP");

    natpmpCheck = new QCheckBox("NAT-PMP");

    peerExchangeCheck = new QCheckBox("Peer Exchange");

    lsdCheck = new QCheckBox("LSD");

    dhtCheck = new QCheckBox("DHT");

    QHBoxLayout *networkExtrasFirstLayout = new QHBoxLayout();
    networkExtrasFirstLayout->addSpacing(10);
    networkExtrasFirstLayout->addWidget(upnpCheck);
    networkExtrasFirstLayout->addWidget(natpmpCheck);
    networkExtrasFirstLayout->addWidget(peerExchangeCheck);
    networkExtrasFirstLayout->addStretch();

    QHBoxLayout *networkExtrasSecondLayout = new QHBoxLayout();
    networkExtrasSecondLayout->addSpacing(10);
    networkExtrasSecondLayout->addWidget(lsdCheck);
    networkExtrasSecondLayout->addSpacing(7);
    networkExtrasSecondLayout->addWidget(dhtCheck);
    networkExtrasSecondLayout->addStretch();

    QLabel *encryptionLabel = new QLabel("<b>Encryption</b>");

    QLabel *inEncryptionLabel = new QLabel("Inbound:");

    inEncryptionCombo = new QComboBox();
    inEncryptionCombo->setFixedWidth(80);
    inEncryptionCombo->addItem("Forced");
    inEncryptionCombo->addItem("Enabled");
    inEncryptionCombo->addItem("Disabled");

    QLabel *outEncryptionLabel = new QLabel("Outbound:");

    outEncryptionCombo = new QComboBox();
    outEncryptionCombo->setFixedWidth(80);
    outEncryptionCombo->addItem("Forced");
    outEncryptionCombo->addItem("Enabled");
    outEncryptionCombo->addItem("Disabled");

    QHBoxLayout *boundEncryptionLayout = new QHBoxLayout();
    boundEncryptionLayout->addSpacing(10);
    boundEncryptionLayout->addWidget(inEncryptionLabel);
    boundEncryptionLayout->addWidget(inEncryptionCombo);
    boundEncryptionLayout->addSpacing(10);
    boundEncryptionLayout->addWidget(outEncryptionLabel);
    boundEncryptionLayout->addWidget(outEncryptionCombo);
    boundEncryptionLayout->addStretch();

    QLabel *levelEncryptionLabel = new QLabel("Level:");

    levelEncryptionCombo = new QComboBox();
    levelEncryptionCombo->setFixedWidth(80);
    levelEncryptionCombo->addItem("Handshake");
    levelEncryptionCombo->addItem("Full Stream");
    levelEncryptionCombo->addItem("Either");

    encryptEntireStreamCheck = new QCheckBox("Encrypt entire stream");

    QHBoxLayout *levelEncryptionLayout = new QHBoxLayout();
    levelEncryptionLayout->addSpacing(10);
    levelEncryptionLayout->addWidget(levelEncryptionLabel);
    levelEncryptionLayout->addSpacing(15);
    levelEncryptionLayout->addWidget(levelEncryptionCombo);
    levelEncryptionLayout->addSpacing(10);
    levelEncryptionLayout->addWidget(encryptEntireStreamCheck);
    levelEncryptionLayout->addStretch();

    QVBoxLayout *networkLayout = new QVBoxLayout();
    networkLayout->addWidget(networkMainLabel);
    networkLayout->addWidget(networkSeparator);
    networkLayout->addWidget(incomingPortsLabel);
    networkLayout->addLayout(activePortLayout);
    networkLayout->addLayout(incomingPortsLayout);
    networkLayout->addSpacing(10);
    networkLayout->addWidget(outgoingPortsLabel);
    networkLayout->addLayout(useRandomOutgoingPortsLayout);
    networkLayout->addLayout(outgoingPortsLayout);
    networkLayout->addSpacing(10);
    networkLayout->addWidget(listenInterfaceLabel);
    networkLayout->addLayout(listenInterfaceLayout);
    networkLayout->addSpacing(10);
    networkLayout->addWidget(tosLabel);
    networkLayout->addLayout(peerTOSLayout);
    networkLayout->addSpacing(10);
    networkLayout->addWidget(networkExtrasLabel);
    networkLayout->addLayout(networkExtrasFirstLayout);
    networkLayout->addLayout(networkExtrasSecondLayout);
    networkLayout->addSpacing(10);
    networkLayout->addWidget(encryptionLabel);
    networkLayout->addLayout(boundEncryptionLayout);
    networkLayout->addLayout(levelEncryptionLayout);
    networkLayout->setAlignment(Qt::AlignTop);
    networkPage->setLayout(networkLayout);

    // Bandwidth page
    QWidget *bandwidthPage = new QWidget(this);

    QLabel *bandwidthMainLabel = new QLabel("<b>Bandwidth</b>");
    bandwidthMainLabel->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QFrame *bandwidthSeparator = new QFrame();
    bandwidthSeparator->setFrameShape(QFrame::HLine);
    bandwidthSeparator->setFrameShadow(QFrame::Sunken);

    QLabel *globalBandwidthUsageLabel = new QLabel("<b>Global Bandwidth Usage</b>");

    QLabel *maxConnectionsLabel = new QLabel("Maximum Connections:");

    maxConnectionsSpin = new QSpinBox();
    maxConnectionsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxConnectionsLayout = new QHBoxLayout();
    maxConnectionsLayout->addSpacing(10);
    maxConnectionsLayout->addWidget(maxConnectionsLabel);
    maxConnectionsLayout->addSpacing(99);
    maxConnectionsLayout->addWidget(maxConnectionsSpin);
    maxConnectionsLayout->addStretch();

    QLabel *maxUploadSlotsLabel = new QLabel("Maximum Upload Slots:");

    maxUploadSlotsSpin = new QSpinBox();
    maxUploadSlotsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUploadSlotsLayout = new QHBoxLayout();
    maxUploadSlotsLayout->addSpacing(10);
    maxUploadSlotsLayout->addWidget(maxUploadSlotsLabel);
    maxUploadSlotsLayout->addSpacing(99);
    maxUploadSlotsLayout->addWidget(maxUploadSlotsSpin);
    maxUploadSlotsLayout->addStretch();

    QLabel *maxDownSpeedLabel = new QLabel("Maximum Download Speed (KiB/s):");

    maxDownSpeedSpin = new QSpinBox();
    maxDownSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxDownSpeedLayout = new QHBoxLayout();
    maxDownSpeedLayout->addSpacing(10);
    maxDownSpeedLayout->addWidget(maxDownSpeedLabel);
    maxDownSpeedLayout->addSpacing(44);
    maxDownSpeedLayout->addWidget(maxDownSpeedSpin);
    maxDownSpeedLayout->addStretch();

    QLabel *maxUpSpeedLabel = new QLabel("Maximum Upload Speed (KiB/s):");

    maxUpSpeedSpin = new QSpinBox();
    maxUpSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUpSpeedLayout = new QHBoxLayout();
    maxUpSpeedLayout->addSpacing(10);
    maxUpSpeedLayout->addWidget(maxUpSpeedLabel);
    maxUpSpeedLayout->addSpacing(58);
    maxUpSpeedLayout->addWidget(maxUpSpeedSpin);
    maxUpSpeedLayout->addStretch();

    QLabel *maxHalfOpenConnectionsLabel = new QLabel("Maximum Half-Open Connections:");

    maxHalfOpenConnectionsSpin = new QSpinBox();
    maxHalfOpenConnectionsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxHalfOpenConnectionsLayout = new QHBoxLayout();
    maxHalfOpenConnectionsLayout->addSpacing(10);
    maxHalfOpenConnectionsLayout->addWidget(maxHalfOpenConnectionsLabel);
    maxHalfOpenConnectionsLayout->addSpacing(47);
    maxHalfOpenConnectionsLayout->addWidget(maxHalfOpenConnectionsSpin);
    maxHalfOpenConnectionsLayout->addStretch();

    QLabel *maxConnectionsPerSecondLabel = new QLabel("Maximum Connection Attempts per Second:");

    maxConnectionsPerSecondSpin = new QSpinBox();
    maxConnectionsPerSecondSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxConnectionsPerSecondLayout = new QHBoxLayout();
    maxConnectionsPerSecondLayout->addSpacing(10);
    maxConnectionsPerSecondLayout->addWidget(maxConnectionsPerSecondLabel);
    maxConnectionsPerSecondLayout->addWidget(maxConnectionsPerSecondSpin);
    maxConnectionsPerSecondLayout->addStretch();

    ignoreLimitsOnLocalCheck = new QCheckBox("Ignore limits on local network");

    QHBoxLayout *ignoreLimitsOnLocalLayout = new QHBoxLayout();
    ignoreLimitsOnLocalLayout->addSpacing(20);
    ignoreLimitsOnLocalLayout->addWidget(ignoreLimitsOnLocalCheck);

    rateLimitIPOverheadCheck = new QCheckBox("Rate limit IP overhead");

    QHBoxLayout *rateLimitIPOverheadLayout = new QHBoxLayout();
    rateLimitIPOverheadLayout->addSpacing(20);
    rateLimitIPOverheadLayout->addWidget(rateLimitIPOverheadCheck);

    QLabel *perTorrentBandwidthUsageLabel = new QLabel("<b>Per Torrent Bandwidth Usage</b>");

    QLabel *maxConnectionsPerTorrentLabel = new QLabel("Maximum Connections:");

    maxConnectionsPerTorrentSpin = new QSpinBox();
    maxConnectionsPerTorrentSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxConnectionsPerTorrentLayout = new QHBoxLayout();
    maxConnectionsPerTorrentLayout->addSpacing(10);
    maxConnectionsPerTorrentLayout->addWidget(maxConnectionsPerTorrentLabel);
    maxConnectionsPerTorrentLayout->addSpacing(99);
    maxConnectionsPerTorrentLayout->addWidget(maxConnectionsPerTorrentSpin);
    maxConnectionsPerTorrentLayout->addStretch();

    QLabel *maxUploadSlotsPerTorrentLabel = new QLabel("Maximum Upload Slots:");

    maxUploadSlotsPerTorrentSpin = new QSpinBox();
    maxUploadSlotsPerTorrentSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUploadSlotsPerTorrentLayout = new QHBoxLayout();
    maxUploadSlotsPerTorrentLayout->addSpacing(10);
    maxUploadSlotsPerTorrentLayout->addWidget(maxUploadSlotsPerTorrentLabel);
    maxUploadSlotsPerTorrentLayout->addSpacing(99);
    maxUploadSlotsPerTorrentLayout->addWidget(maxUploadSlotsPerTorrentSpin);
    maxUploadSlotsPerTorrentLayout->addStretch();

    QLabel *maxDownSpeedPerTorrentLabel = new QLabel("Maximum Download Speed (KiB/s):");

    maxDownSpeedPerTorrentSpin = new QSpinBox();
    maxDownSpeedPerTorrentSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxDownSpeedPerTorrentLayout = new QHBoxLayout();
    maxDownSpeedPerTorrentLayout->addSpacing(10);
    maxDownSpeedPerTorrentLayout->addWidget(maxDownSpeedPerTorrentLabel);
    maxDownSpeedPerTorrentLayout->addSpacing(44);
    maxDownSpeedPerTorrentLayout->addWidget(maxDownSpeedPerTorrentSpin);
    maxDownSpeedPerTorrentLayout->addStretch();

    QLabel *maxUpSpeedPerTorrentLabel = new QLabel("Maximum Upload Speed (KiB/s):");

    maxUpSpeedPerTorrentSpin = new QSpinBox();
    maxUpSpeedPerTorrentSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUpSpeedPerTorrentLayout = new QHBoxLayout();
    maxUpSpeedPerTorrentLayout->addSpacing(10);
    maxUpSpeedPerTorrentLayout->addWidget(maxUpSpeedPerTorrentLabel);
    maxUpSpeedPerTorrentLayout->addSpacing(58);
    maxUpSpeedPerTorrentLayout->addWidget(maxUpSpeedPerTorrentSpin);
    maxUpSpeedPerTorrentLayout->addStretch();

    QVBoxLayout *bandwidthLayout = new QVBoxLayout();
    bandwidthLayout->addWidget(bandwidthMainLabel);
    bandwidthLayout->addWidget(bandwidthSeparator);
    bandwidthLayout->addWidget(globalBandwidthUsageLabel);
    bandwidthLayout->addLayout(maxConnectionsLayout);
    bandwidthLayout->addLayout(maxUploadSlotsLayout);
    bandwidthLayout->addLayout(maxDownSpeedLayout);
    bandwidthLayout->addLayout(maxUpSpeedLayout);
    bandwidthLayout->addLayout(maxHalfOpenConnectionsLayout);
    bandwidthLayout->addLayout(maxConnectionsPerSecondLayout);
    bandwidthLayout->addLayout(ignoreLimitsOnLocalLayout);
    bandwidthLayout->addLayout(rateLimitIPOverheadLayout);
    bandwidthLayout->addSpacing(20);
    bandwidthLayout->addWidget(perTorrentBandwidthUsageLabel);
    bandwidthLayout->addLayout(maxConnectionsPerTorrentLayout);
    bandwidthLayout->addLayout(maxUploadSlotsPerTorrentLayout);
    bandwidthLayout->addLayout(maxDownSpeedPerTorrentLayout);
    bandwidthLayout->addLayout(maxUpSpeedPerTorrentLayout);
    bandwidthLayout->setAlignment(Qt::AlignTop);
    bandwidthPage->setLayout(bandwidthLayout);

    notebook = new QStackedWidget(this);
    notebook->addWidget(generalPage);
    notebook->addWidget(downloadsPage);
    notebook->addWidget(queuePage);
    notebook->addWidget(networkPage);
    notebook->addWidget(bandwidthPage);

    QHBoxLayout *mainHLayout = new QHBoxLayout();
    mainHLayout->addWidget(categoriesView);
    mainHLayout->addWidget(notebook);

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancelButtonClicked()));

    QPushButton *applyButton = new QPushButton("Apply");
    this->connect(applyButton, SIGNAL(clicked()), this, SLOT(applyButtonClicked()));

    QPushButton *okButton = new QPushButton("OK");
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(okButtonClicked()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addStretch();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(applyButton);
    buttonsLayout->addWidget(okButton);

    QVBoxLayout *mainVLayout = new QVBoxLayout();
    mainVLayout->addLayout(mainHLayout);
    mainVLayout->addLayout(buttonsLayout);

    this->setLayout(mainVLayout);
}

void PreferencesDialog::show_preferences_dialog()
{
    this->load_state();
    this->show();
}

void PreferencesDialog::cancelButtonClicked()
{
    this->hide();
}

void PreferencesDialog::applyButtonClicked()
{
    this->set_config();
}

void PreferencesDialog::okButtonClicked()
{
    this->set_config();
    this->hide();
}

void PreferencesDialog::load_state()
{
    QSettings settings;

    // General page
    this->checkAssociationOnStartup->setChecked(settings.value("uiconfig/check_associations").toBool());
    this->startOnStartupCheck->setChecked(settings.value("uiconfig/run_at_startup").toBool());
    this->preventHibernationCheck->setChecked(settings.value("uiconfig/prevent_hibernation").toBool());
    if(settings.value("uiconfig/associate_torrents").toBool())
        this->associateTorrentFilesButton->setDisabled(true);
    else
        this->associateTorrentFilesButton->setEnabled(true);
    if(settings.value("uiconfig/associate_magnets").toBool())
        this->associateMagnetsButton->setDisabled(true);
    else
        this->associateMagnetsButton->setEnabled(true);

    // Downloads page
    this->downloadToText->setText(settings.value("coreconfig/download_location").toString());
    this->moveCompletedToCheck->setChecked(settings.value("coreconfig/move_completed").toBool());
    this->moveCompletedToText->setText(settings.value("coreconfig/move_completed_path").toString());
    this->moveCompletedToToggled(this->moveCompletedToCheck->isChecked());
    this->autoAddTorrentsCheck->setChecked(settings.value("coreconfig/autoadd_enable").toBool());
    this->autoAddTorrentsText->setText(settings.value("coreconfig/autoadd_location").toString());
    this->autoAddTorrentsToggled(this->autoAddTorrentsCheck->isChecked());
    this->copyTorrentFilesToCheck->setChecked(settings.value("coreconfig/copy_torrent_file").toBool());
    this->copyTorrentFilesToText->setText(settings.value("coreconfig/torrentfiles_location").toString());
    this->deleteCopyTorrentCheck->setChecked(settings.value("coreconfig/del_copy_torrent_file").toBool());
    this->copyTorrentFilesToToggled(this->copyTorrentFilesToCheck->isChecked());
    this->compactAllocationRButton->setChecked(settings.value("coreconfig/compact_allocation").toBool());
    this->prioritiseFirstLastCheck->setChecked(settings.value("coreconfig/prioritize_first_last_pieces").toBool());
    this->addPausedTorrentsCheck->setChecked(settings.value("coreconfig/add_paused").toBool());

    // Queue page
    this->queueNewToTopCheck->setChecked(settings.value("coreconfig/queue_new_to_top").toBool());
    this->totalActiveSeedingSpin->setValue(settings.value("coreconfig/max_active_seeding").toInt());
    this->totalActiveDownloadingSpin->setValue(settings.value("coreconfig/max_active_downloading").toInt());
    this->totalActiveSpin->setValue(settings.value("coreconfig/max_active_limit").toInt());
    this->dontCountSlowTorrentsCheck->setChecked(settings.value("coreconfig/dont_count_slow_torrents").toBool());
    this->shareRatioLimitSpin->setValue(settings.value("coreconfig/share_ratio_limit").toDouble());
    this->seedTimeRatioSpin->setValue(settings.value("coreconfig/seed_time_ratio_limit").toDouble());
    this->seedTimeSpin->setValue(settings.value("coreconfig/seed_time_limit").toInt());
    this->stopRatioCheck->setChecked(settings.value("coreconfig/stop_seed_at_ratio").toBool());
    this->stopRatioSpin->setValue(settings.value("coreconfig/stop_seed_ratio").toDouble());
    this->removeIfStopRatioCheck->setChecked(settings.value("coreconfig/remove_seed_at_ratio").toBool());
    this->stopRatioToggled(this->stopRatioCheck->isChecked());

    // Network page
    this->useRandomPortsCheck->setChecked(settings.value("coreconfig/random_port").toBool());
    if(this->useRandomPortsCheck->isChecked())
    {
        this->incomingPortsFromSpin->setDisabled(true);
        this->incomingPortsToSpin->setDisabled(true);
    }
    else
    {
        this->incomingPortsFromSpin->setEnabled(true);
        this->incomingPortsToSpin->setEnabled(true);
    }
    this->activePortValueLabel->setText(QString::number(this->components->core->session->listen_port()));
    int lowPort = 6881;
    int highPort = 6889;
    QStringList settings_ports = settings.value("coreconfig/listen_ports").toStringList();
    if(settings_ports.count() == 2)
    {
        lowPort = settings_ports[0].toInt();
        highPort = settings_ports[1].toInt();
    }
    this->incomingPortsFromSpin->setValue(lowPort);
    this->incomingPortsToSpin->setValue(highPort);
    this->useRandomOutgoingPortsCheck->setChecked(settings.value("coreconfig/random_outgoing_ports").toBool());
    if(this->useRandomOutgoingPortsCheck->isChecked())
    {
        this->outgoingPortsFromSpin->setDisabled(true);
        this->outgoingPortsToSpin->setDisabled(true);
    }
    else
    {
        this->outgoingPortsFromSpin->setEnabled(true);
        this->outgoingPortsToSpin->setEnabled(true);
    }
    lowPort = 0;
    highPort = 0;
    settings_ports = settings.value("coreconfig/outgoing_ports").toStringList();
    if(settings_ports.count() == 2)
    {
        lowPort = settings_ports[0].toInt();
        highPort = settings_ports[1].toInt();
    }
    this->outgoingPortsFromSpin->setValue(lowPort);
    this->outgoingPortsToSpin->setValue(highPort);
    this->listenInterfaceText->setText(settings.value("coreconfig/listen_interface").toString());
    this->peerTOSText->setText(settings.value("coreconfig/peer_tos").toString());
    this->upnpCheck->setChecked(settings.value("coreconfig/upnp").toBool());
    this->natpmpCheck->setChecked(settings.value("coreconfig/natpmp").toBool());
    this->peerExchangeCheck->setChecked(settings.value("coreconfig/utpex").toBool());
    this->lsdCheck->setChecked(settings.value("coreconfig/lsd").toBool());
    this->dhtCheck->setChecked(settings.value("coreconfig/dht").toBool());
    this->inEncryptionCombo->setCurrentIndex(settings.value("coreconfig/enc_in_policy").toInt());
    this->outEncryptionCombo->setCurrentIndex(settings.value("coreconfig/enc_out_policy").toInt());
    this->levelEncryptionCombo->setCurrentIndex(settings.value("coreconfig/enc_level").toInt());
    this->encryptEntireStreamCheck->setChecked(settings.value("coreconfig/enc_prefer_rc4").toBool());

    // Bandwidth page
    this->maxConnectionsSpin->setValue(settings.value("coreconfig/max_connections_global").toInt());
    this->maxDownSpeedSpin->setValue(settings.value("coreconfig/max_download_speed").toInt());
    this->maxUpSpeedSpin->setValue(settings.value("coreconfig/max_upload_speed").toInt());
    this->maxUploadSlotsSpin->setValue(settings.value("coreconfig/max_upload_slots_global").toInt());
    this->maxHalfOpenConnectionsSpin->setValue(settings.value("coreconfig/max_half_open_connections").toInt());
    this->maxConnectionsPerSecondSpin->setValue(settings.value("coreconfig/max_connections_per_second").toInt());
    this->maxConnectionsPerTorrentSpin->setValue(settings.value("coreconfig/max_connections_per_torrent").toInt());
    this->maxUploadSlotsPerTorrentSpin->setValue(settings.value("coreconfig/max_upload_slots_per_torrent").toInt());
    this->maxUpSpeedPerTorrentSpin->setValue(settings.value("coreconfig/max_upload_speed_per_torrent").toInt());
    this->maxDownSpeedPerTorrentSpin->setValue(settings.value("coreconfig/max_download_speed_per_torrent").toInt());
    this->ignoreLimitsOnLocalCheck->setChecked(settings.value("coreconfig/ignore_limits_on_local_network").toBool());
    this->rateLimitIPOverheadCheck->setChecked(settings.value("coreconfig/rate_limit_ip_overhead").toBool());
}

void PreferencesDialog::set_config()
{
    QSettings settings;

    // General page
    settings.setValue("uiconfig/check_associations", this->checkAssociationOnStartup->isChecked());
    if(settings.value("uiconfig/run_at_startup").toBool() != this->startOnStartupCheck->isChecked())
    {
        settings.setValue("uiconfig/run_at_startup", this->startOnStartupCheck->isChecked());
        if(this->startOnStartupCheck->isChecked())
            set_run_at_startup();
        else
            remove_run_at_startup();
    }
    if(this->preventHibernationCheck->isChecked() != settings.value("uiconfig/prevent_hibernation").toInt())
    {
        settings.setValue("uiconfig/prevent_hibernation", this->preventHibernationCheck->isChecked());
        this->components->preventHibernation->set_value(this->preventHibernationCheck->isChecked());
    }

    // Downloads page
    settings.setValue("coreconfig/download_location", this->downloadToText->text());
    settings.setValue("coreconfig/move_completed", this->moveCompletedToCheck->isChecked());
    settings.setValue("coreconfig/move_completed_path", this->moveCompletedToText->text());
    if(this->autoAddTorrentsCheck->isChecked() != settings.value("coreconfig/autoadd_enable").toBool())
    {
        settings.setValue("coreconfig/autoadd_enable", this->autoAddTorrentsCheck->isChecked());
        this->components->autoadd->set_autoadd_enable(this->autoAddTorrentsCheck->isChecked());
    }
    if(this->autoAddTorrentsText->text() != settings.value("coreconfig/autoadd_location").toString())
    {
        settings.setValue("coreconfig/autoadd_location", this->autoAddTorrentsText->text());
        this->components->autoadd->set_autoadd_location();
    }
    settings.setValue("coreconfig/copy_torrent_file", this->copyTorrentFilesToCheck->isChecked());
    if(this->copyTorrentFilesToText->text() != settings.value("coreconfig/torrentfiles_location").toString())
    {
        settings.setValue("coreconfig/torrentfiles_location", this->copyTorrentFilesToText->text());
        if(this->copyTorrentFilesToCheck->isChecked())
            QDir().mkdir(this->copyTorrentFilesToText->text());
    }
    settings.setValue("coreconfig/del_copy_torrent_file", this->deleteCopyTorrentCheck->isChecked());
    settings.setValue("coreconfig/compact_allocation", this->compactAllocationRButton->isChecked());
    settings.setValue("coreconfig/prioritize_first_last_pieces", this->prioritiseFirstLastCheck->isChecked());
    settings.setValue("coreconfig/add_paused", this->addPausedTorrentsCheck->isChecked());

    // Queue page
    settings.setValue("coreconfig/queue_new_to_top", this->queueNewToTopCheck->isChecked());
    if(this->totalActiveSeedingSpin->value() != settings.value("coreconfig/max_active_seeding").toInt())
    {
        settings.setValue("coreconfig/max_active_seeding", this->totalActiveSeedingSpin->value());
        this->components->core->set_max_active_seeding(this->totalActiveSeedingSpin->value());
    }
    if(this->totalActiveDownloadingSpin->value() != settings.value("coreconfig/max_active_downloading").toInt())
    {
        settings.setValue("coreconfig/max_active_downloading", this->totalActiveDownloadingSpin->value());
        this->components->core->set_max_active_downloading(this->totalActiveDownloadingSpin->value());
    }
    if(this->totalActiveSpin->value() != settings.value("coreconfig/max_active_limit").toInt())
    {
        settings.setValue("coreconfig/max_active_limit", this->totalActiveSpin->value());
        this->components->core->set_max_active_limit(this->totalActiveSpin->value());
    }
    if(this->dontCountSlowTorrentsCheck->isChecked() != settings.value("coreconfig/dont_count_slow_torrents").toBool())
    {
        settings.setValue("coreconfig/dont_count_slow_torrents", this->dontCountSlowTorrentsCheck->isChecked());
        this->components->core->set_dont_count_slow_torrents(this->dontCountSlowTorrentsCheck->isChecked());
    }
    if(this->shareRatioLimitSpin->value() != settings.value("coreconfig/share_ratio_limit").toDouble())
    {
        settings.setValue("coreconfig/share_ratio_limit", this->shareRatioLimitSpin->value());
        this->components->core->set_share_ratio_limit(this->shareRatioLimitSpin->value());
    }
    if(this->seedTimeRatioSpin->value() != settings.value("coreconfig/seed_time_ratio_limit").toDouble())
    {
        settings.setValue("coreconfig/seed_time_ratio_limit", this->seedTimeRatioSpin->value());
        this->components->core->set_seed_time_ratio_limit(this->seedTimeRatioSpin->value());
    }
    if(this->seedTimeSpin->value() != settings.value("coreconfig/seed_time_limit").toInt())
    {
        settings.setValue("coreconfig/seed_time_limit", this->seedTimeSpin->value());
        this->components->core->set_seed_time_limit(this->seedTimeSpin->value());
    }
    settings.setValue("coreconfig/stop_seed_at_ratio", this->stopRatioCheck->isChecked());
    settings.setValue("coreconfig/stop_seed_ratio", this->stopRatioSpin->value());
    settings.setValue("coreconfig/remove_seed_at_ratio", this->removeIfStopRatioCheck->isChecked());

    // Network page
    if(this->useRandomPortsCheck->isChecked())
    {
        if(!settings.value("coreconfig/random_port").toBool())
        {
            settings.setValue("coreconfig/random_port", true);
            this->components->core->set_random_port(true);
            this->activePortValueLabel->setText(QString::number(this->components->core->session->listen_port()));
        }
    }
    else
    {
        QStringList settings_ports;
        settings_ports << QString::number(this->incomingPortsFromSpin->value());
        settings_ports << QString::number(this->incomingPortsToSpin->value());
        if(settings_ports != settings.value("coreconfig/listen_ports").toStringList()
                || settings.value("coreconfig/random_port").toBool())
        {
            settings.setValue("coreconfig/random_port", false);
            settings.setValue("coreconfig/listen_ports", settings_ports);
            this->components->core->set_listen_ports(this->incomingPortsFromSpin->value(),
                                                     this->incomingPortsToSpin->value());
            this->activePortValueLabel->setText(QString::number(this->components->core->session->listen_port()));
        }
    }
    if(this->useRandomOutgoingPortsCheck->isChecked())
    {
        if(!settings.value("coreconfig/random_outgoing_ports").toBool())
        {
            settings.setValue("coreconfig/random_outgoing_ports", true);
            this->components->core->set_random_outgoing_ports(true);
        }
    }
    else
    {
        QStringList settings_ports;
        settings_ports << QString::number(this->outgoingPortsFromSpin->value());
        settings_ports << QString::number(this->outgoingPortsToSpin->value());
        if(settings_ports != settings.value("coreconfig/outgoing_ports").toStringList()
                || settings.value("coreconfig/random_outgoing_ports").toBool())
        {
            settings.setValue("coreconfig/random_outgoing_ports", false);
            settings.setValue("coreconfig/outgoing_ports", settings_ports);
            this->components->core->set_outgoing_ports(this->outgoingPortsFromSpin->value(),
                                                       this->outgoingPortsToSpin->value());
        }
    }
    if(this->listenInterfaceText->text() != settings.value("coreconfig/listen_interface").toString())
    {
        settings.setValue("coreconfig/listen_interface", this->listenInterfaceText->text());
        this->components->core->set_random_port(this->useRandomPortsCheck->isChecked());
    }
    if(this->peerTOSText->text() != settings.value("coreconfig/peer_tos").toString())
    {
        settings.setValue("coreconfig/peer_tos", this->peerTOSText->text());
        this->components->core->set_peer_tos(this->peerTOSText->text());
    }
    if(this->upnpCheck->isChecked() != settings.value("coreconfig/upnp").toBool())
    {
        settings.setValue("coreconfig/upnp", this->upnpCheck->isChecked());
        this->components->core->set_upnp(this->upnpCheck->isChecked());
    }
    if(this->natpmpCheck->isChecked() != settings.value("coreconfig/natpmp").toBool())
    {
        settings.setValue("coreconfig/natpmp", this->natpmpCheck->isChecked());
        this->components->core->set_natpmp(this->natpmpCheck->isChecked());
    }
    if(this->peerExchangeCheck->isChecked() != settings.value("coreconfig/utpex").toBool())
    {
        settings.setValue("coreconfig/utpex", this->peerExchangeCheck->isChecked());
        this->components->core->set_peer_exchange(this->peerExchangeCheck->isChecked());
    }
    if(this->lsdCheck->isChecked() != settings.value("coreconfig/lsd").toBool())
    {
        settings.setValue("coreconfig/lsd", this->lsdCheck->isChecked());
        this->components->core->set_lsd(this->lsdCheck->isChecked());
    }
    if(this->dhtCheck->isChecked() != settings.value("coreconfig/dht").toBool())
    {
        settings.setValue("coreconfig/dht", this->dhtCheck->isChecked());
        this->components->core->set_dht(this->dhtCheck->isChecked());
        this->components->mainWindow->statusbar->dht_state_changed(this->dhtCheck->isChecked());
    }
    bool encryption_changed = false;
    if(this->inEncryptionCombo->currentIndex() != settings.value("coreconfig/enc_in_policy").toInt())
    {
        settings.setValue("coreconfig/enc_in_policy", this->inEncryptionCombo->currentIndex());
        encryption_changed = true;
    }
    if(this->outEncryptionCombo->currentIndex() != settings.value("coreconfig/enc_out_policy").toInt())
    {
        settings.setValue("coreconfig/enc_out_policy", this->outEncryptionCombo->currentIndex());
        encryption_changed = true;
    }
    if(this->levelEncryptionCombo->currentIndex() != settings.value("coreconfig/enc_level").toInt())
    {
        settings.setValue("coreconfig/enc_level", this->levelEncryptionCombo->currentIndex());
        encryption_changed = true;
    }
    if(this->encryptEntireStreamCheck->isChecked() != settings.value("coreconfig/enc_prefer_rc4").toBool())
    {
        settings.setValue("coreconfig/enc_prefer_rc4", this->encryptEntireStreamCheck->isChecked());
        encryption_changed = true;
    }
    if(encryption_changed)
        this->components->core->set_encryption();

    // Bandwidth page
    if(this->maxConnectionsSpin->value() != settings.value("coreconfig/max_connections_global").toInt())
    {
        settings.setValue("coreconfig/max_connections_global", this->maxConnectionsSpin->value());
        this->components->core->session->set_max_connections(this->maxConnectionsSpin->value());
        this->components->mainWindow->statusbar->max_connections_global_changed(this->maxConnectionsSpin->value());
    }
    if(this->maxDownSpeedSpin->value() != settings.value("coreconfig/max_download_speed").toInt())
    {
        settings.setValue("coreconfig/max_download_speed", this->maxDownSpeedSpin->value());
        this->components->core->set_max_download_speed(this->maxDownSpeedSpin->value());
        this->components->mainWindow->statusbar->max_download_speed_changed(this->maxDownSpeedSpin->value());
    }
    if(this->maxUpSpeedSpin->value() != settings.value("coreconfig/max_upload_speed").toInt())
    {
        settings.setValue("coreconfig/max_upload_speed", this->maxUpSpeedSpin->value());
        this->components->core->set_max_upload_speed(this->maxUpSpeedSpin->value());
        this->components->mainWindow->statusbar->max_upload_speed_changed(this->maxUpSpeedSpin->value());
    }
    if(this->maxUploadSlotsSpin->value() != settings.value("coreconfig/max_upload_slots_global").toInt())
    {
        settings.setValue("coreconfig/max_upload_slots_global", this->maxUploadSlotsSpin->value());
        this->components->core->session->set_max_uploads(this->maxUploadSlotsSpin->value());
    }
    if(this->maxHalfOpenConnectionsSpin->value() != settings.value("coreconfig/max_half_open_connections").toInt())
    {
        settings.setValue("coreconfig/max_half_open_connections", this->maxHalfOpenConnectionsSpin->value());
        this->components->core->session->set_max_half_open_connections(this->maxHalfOpenConnectionsSpin->value());
    }
    if(this->maxConnectionsPerSecondSpin->value() != settings.value("coreconfig/max_connections_per_second").toInt())
    {
        settings.setValue("coreconfig/max_connections_per_second", this->maxConnectionsPerSecondSpin->value());
        this->components->core->set_max_connections_per_second(this->maxConnectionsPerSecondSpin->value());
    }
    if(this->maxConnectionsPerTorrentSpin->value() != settings.value("coreconfig/max_connections_per_torrent").toInt())
    {
        settings.setValue("coreconfig/max_connections_per_torrent", this->maxConnectionsPerTorrentSpin->value());
        this->components->torrentManager->set_max_connections_per_torrent(this->maxConnectionsPerTorrentSpin->value());
    }
    if(this->maxUploadSlotsPerTorrentSpin->value() != settings.value("coreconfig/max_upload_slots_per_torrent").toInt())
    {
        settings.setValue("coreconfig/max_upload_slots_per_torrent", this->maxUploadSlotsPerTorrentSpin->value());
        this->components->torrentManager->set_max_upload_slots_per_torrent(this->maxUploadSlotsPerTorrentSpin->value());
    }
    if(this->maxUpSpeedPerTorrentSpin->value() != settings.value("coreconfig/max_upload_speed_per_torrent").toInt())
    {
        settings.setValue("coreconfig/max_upload_speed_per_torrent", this->maxUpSpeedPerTorrentSpin->value());
        this->components->torrentManager->set_max_upload_speed_per_torrent(this->maxUpSpeedPerTorrentSpin->value());
    }
    if(this->maxDownSpeedPerTorrentSpin->value() != settings.value("coreconfig/max_download_speed_per_torrent").toInt())
    {
        settings.setValue("coreconfig/max_download_speed_per_torrent", this->maxDownSpeedPerTorrentSpin->value());
        this->components->torrentManager->set_max_download_speed_per_torrent(this->maxDownSpeedPerTorrentSpin->value());
    }
    if(this->ignoreLimitsOnLocalCheck->isChecked() != settings.value("coreconfig/ignore_limits_on_local_network").toBool())
    {
        settings.setValue("coreconfig/ignore_limits_on_local_network", this->ignoreLimitsOnLocalCheck->isChecked());
        this->components->core->set_ignore_limits_on_local_network(this->ignoreLimitsOnLocalCheck->isChecked());
    }
    if(this->rateLimitIPOverheadCheck->isChecked() != settings.value("coreconfig/rate_limit_ip_overhead").toBool())
    {
        settings.setValue("coreconfig/rate_limit_ip_overhead", this->rateLimitIPOverheadCheck->isChecked());
        this->components->core->set_rate_limit_ip_overhead(this->rateLimitIPOverheadCheck->isChecked());
    }
}

void PreferencesDialog::changePage(QModelIndex selected, QModelIndex deselected)
{
    this->notebook->setCurrentIndex(selected.row());
}

void PreferencesDialog::downloadToClicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");
    if(!folder.isEmpty())
        this->downloadToText->setText(folder);
}

void PreferencesDialog::moveCompletedToClicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");
    if(!folder.isEmpty())
        this->moveCompletedToText->setText(folder);
}

void PreferencesDialog::autoAddTorrentsClicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");
    if(!folder.isEmpty())
        this->autoAddTorrentsText->setText(folder);
}

void PreferencesDialog::copyTorrentFilesToClicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");
    if(!folder.isEmpty())
        this->copyTorrentFilesToText->setText(folder);
}

void PreferencesDialog::associateTorrentFilesClicked()
{
    QSettings settings;
    settings.setValue("uiconfig/associate_torrents", set_torrent_files_association());
    if(settings.value("uiconfig/associate_torrents").toBool())
        this->associateTorrentFilesButton->setDisabled(true);
}

void PreferencesDialog::associateMagnetsClicked()
{
    QSettings settings;
    settings.setValue("uiconfig/associate_magnets", set_magnet_links_association());
    if(settings.value("uiconfig/associate_magnets").toBool())
        this->associateMagnetsButton->setDisabled(true);
}

void PreferencesDialog::moveCompletedToToggled(bool checked)
{
    if(checked)
    {
        this->moveCompletedToText->setEnabled(true);
        this->moveCompletedToButton->setEnabled(true);
    }
    else
    {
        this->moveCompletedToText->setDisabled(true);
        this->moveCompletedToButton->setDisabled(true);
    }
}

void PreferencesDialog::autoAddTorrentsToggled(bool checked)
{
    if(checked)
    {
        this->autoAddTorrentsText->setEnabled(true);
        this->autoAddTorrentsButton->setEnabled(true);
    }
    else
    {
        this->autoAddTorrentsText->setDisabled(true);
        this->autoAddTorrentsButton->setDisabled(true);
    }
}

void PreferencesDialog::copyTorrentFilesToToggled(bool checked)
{
    if(checked)
    {
        this->copyTorrentFilesToText->setEnabled(true);
        this->copyTorrentFilesToButton->setEnabled(true);
        this->deleteCopyTorrentCheck->setEnabled(true);
    }
    else
    {
        this->copyTorrentFilesToText->setDisabled(true);
        this->copyTorrentFilesToButton->setDisabled(true);
        this->deleteCopyTorrentCheck->setDisabled(true);
    }
}

void PreferencesDialog::stopRatioToggled(bool checked)
{
    if(checked)
    {
        this->stopRatioSpin->setEnabled(true);
        this->removeIfStopRatioCheck->setEnabled(true);
    }
    else
    {
        this->stopRatioSpin->setDisabled(true);
        this->removeIfStopRatioCheck->setDisabled(true);
    }
}

void PreferencesDialog::useRandomPortsToggled(bool checked)
{
    if(checked)
    {
        this->incomingPortsFromSpin->setDisabled(true);
        this->incomingPortsToSpin->setDisabled(true);
    }
    else
    {
        this->incomingPortsFromSpin->setEnabled(true);
        this->incomingPortsToSpin->setEnabled(true);
    }
}

void PreferencesDialog::useRandomOutgoingPortsToggled(bool checked)
{
    if(checked)
    {
        this->outgoingPortsFromSpin->setDisabled(true);
        this->outgoingPortsToSpin->setDisabled(true);
    }
    else
    {
        this->outgoingPortsFromSpin->setEnabled(true);
        this->outgoingPortsToSpin->setEnabled(true);
    }
}
