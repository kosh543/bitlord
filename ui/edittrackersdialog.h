#ifndef EDITTRACKERSDIALOG_H
#define EDITTRACKERSDIALOG_H

#include <QDialog>
#include <QTreeView>
#include <QStandardItemModel>
#include <QString>
#include <QLineEdit>
#include <QTextEdit>

class Components;

class EditTrackerDialog : public QDialog
{
    Q_OBJECT
private:
    QLineEdit *trackerEditLine;

public:
    explicit EditTrackerDialog(QWidget *parent = 0);
    QString show_edit_tracker_dialog(QString tracker);

};

class AddTrackersDialog : public QDialog
{
    Q_OBJECT
private:
    QTextEdit *trackersEdit;

public:
    explicit AddTrackersDialog(QWidget *parent = 0);
    QList<QString> show_add_trackers_dialog();

};

class EditTrackersDialog : public QDialog
{
    Q_OBJECT
private:
    Components *components;
    EditTrackerDialog *editTrackerDialog;
    AddTrackersDialog *addTrackersDialog;
    QString torrent_id;
    QTreeView *trackersView;
    QStandardItemModel *trackersModel;

public:
    explicit EditTrackersDialog(Components *comp, QString torrent_id, QWidget *parent = 0);
    void show_edit_trackers_dialog();

private slots:
    void on_button_up_clicked();
    void on_button_add_clicked();
    void on_button_edit_clicked();
    void on_button_remove_clicked();
    void on_button_down_clicked();

};

#endif // EDITTRACKERSDIALOG_H
