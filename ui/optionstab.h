#ifndef OPTIONSTAB_H
#define OPTIONSTAB_H

#include <QWidget>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <QPushButton>

class Components;

class OptionsTab : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QSpinBox *maxDownSpeedSpin;
    QSpinBox *maxUpSpeedSpin;
    QSpinBox *maxConnectionsSpin;
    QSpinBox *maxUploadSlotsSpin;
    QDoubleSpinBox *stopSeedSpin;
    QCheckBox *autoManagedCheck;
    QCheckBox *shutdownCheck;
    QCheckBox *stopSeedCheck;
    QCheckBox *removeRatioCheck;
    QCheckBox *moveCompletedCheck;
    QCheckBox *privateCheck;
    QCheckBox *priorFLCheck;
    QString prev_torrent_id;
    QPushButton *moveCompletedButton;
    QString moveCompletedPath;
    QMap<QString, QVariant> prev_status;

public:
    explicit OptionsTab(Components *comp, QWidget *parent = 0);
    void update_tab();
    void clear_tab();

private slots:
    void _on_button_edit_trackers_clicked();
    void _on_button_apply_clicked();
    void _on_button_move_completed_clicked();
    void _on_move_completed_toggled(bool checked);
    void _on_stop_seed_toggled(bool checked);

};

#endif // OPTIONSTAB_H
