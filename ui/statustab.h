#ifndef STATUSTAB_H
#define STATUSTAB_H

#include <QWidget>
#include <QProgressBar>
#include <QLabel>

class Components;

class StatusTab : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QProgressBar *progressBar;
    QLabel *activeTime;
    QLabel *downloaded;
    QLabel *uploaded;
    QLabel *remaining;
    QLabel *nextAnnounce;
    QLabel *eta;
    QLabel *downSpeed;
    QLabel *upSpeed;
    QLabel *pieces;
    QLabel *trackerStatus;
    QLabel *seeders;
    QLabel *peers;
    QLabel *shareRatio;
    QLabel *autoManaged;
    QLabel *availability;
    QLabel *seedingTime;
    QLabel *seedRank;
    QLabel *dateAdded;

public:
    explicit StatusTab(Components *comp, QWidget *parent = 0);
    void update_tab();
    void clear_tab();

};

#endif // STATUSTAB_H
