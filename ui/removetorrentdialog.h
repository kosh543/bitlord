#ifndef REMOVETORRENTDIALOG_H
#define REMOVETORRENTDIALOG_H

#include <QDialog>

class RemoveTorrentDialog : public QDialog
{
    Q_OBJECT

private:
    bool with_data;
public:
    explicit RemoveTorrentDialog(QWidget *parent = 0);
    int show_remove_torrent_dialog();

signals:

private slots:
    void _on_remove_with_data_clicked();
    void _on_remove_clicked();
};

#endif // REMOVETORRENTDIALOG_H
