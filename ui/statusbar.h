#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QStatusBar>
#include <QLabel>
#include <QPixmap>
#include <QTimer>

class Components;

class StatusBar : public QStatusBar
{
    Q_OBJECT
private:
    Components *components;
    QTimer *update_timer;
    QLabel *connText;
    QLabel *downText;
    QLabel *upText;
    QLabel *updownText;
    QLabel *dhtPix;
    QLabel *dhtText;
    QLabel *noincomPix;
    bool dht_state;
    bool health;
    int num_connections;
    int num_connections_max;
    int down_speed;
    int down_speed_max;
    int up_speed;
    int up_speed_max;
    int dht_nodes;
    double download_protocol_rate;
    double upload_protocol_rate;

    void update_num_connections();
    void update_down_speed();
    void update_up_speed();
    void update_traffic();
    void update_dht_nodes();

public:
    explicit StatusBar(Components *comp, QWidget *parent = 0);
    void max_connections_global_changed(int value);
    void max_download_speed_changed(int value);
    void max_upload_speed_changed(int value);
    void dht_state_changed(bool value);

private slots:
    void update_timer_tick();
};

#endif // STATUSBAR_H
