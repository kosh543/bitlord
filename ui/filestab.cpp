#include "filestab.h"
#include "common.h"
#include "components.h"

#include <QDesktopServices>

FilesTabProgressItemDelegate::FilesTabProgressItemDelegate(QObject *parent) : QItemDelegate(parent){}

void FilesTabProgressItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.data(Qt::UserRole).toInt())
    {
        QStyleOptionProgressBarV2 progressBarOption;
        //QRect rect = option.rect;
        //QSize size(rect.width()*3/4,rect.height()*3/4);
        //rect.setSize(size);
        progressBarOption.state = QStyle::State_Enabled;
        progressBarOption.direction = QApplication::layoutDirection();
        progressBarOption.rect = option.rect;
        progressBarOption.fontMetrics = QApplication::fontMetrics();
        //QPalette pal = progressBarOption.palette;
        //QColor col;
        //col.setNamedColor("#05B8CC");
        //pal.setColor(QPalette::Highlight,col);
        //progressBarOption.palette = pal;
        progressBarOption.type = QStyleOption::SO_ProgressBar;
        progressBarOption.version = 2;
        progressBarOption.minimum = 0;
        progressBarOption.maximum = 100;
        progressBarOption.textAlignment = Qt::AlignCenter;
        progressBarOption.textVisible = true;
        int progress = (int)(index.data(Qt::DisplayRole).toFloat() * 100);
        progressBarOption.progress = progress;
        progressBarOption.text = QString("%1%").arg(progressBarOption.progress);

        // Draw the progress bar onto the view.
        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter, 0);
    }
    else
    {
        option.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter, option.widget);
    }
}

FilesTab::FilesTab(Components *comp, QWidget *parent) :
    QWidget(parent)
{
    this->components = comp;

    folder_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/folder.png"));
    file_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/file.png"));

    compact = false;

    QVBoxLayout *vBox = new QVBoxLayout(this);
    this->setLayout(vBox);

    filesView = new QTreeView();
    filesModel = new QStandardItemModel(0, 5);
    filesView->setModel(filesModel);
    filesView->setColumnHidden(4, true);
    filesView->setSelectionBehavior(QAbstractItemView::SelectRows);
    QStringList files_labels;
    files_labels << "Filename" << "Size" << "Progress" << "Priority";
    filesModel->setHorizontalHeaderLabels(files_labels);
    filesView->setItemDelegateForColumn(2, new FilesTabProgressItemDelegate(filesView));

    vBox->addWidget(filesView);

    QAction *openPathAction = new QAction("Open", this);
    QAction *expandAllAction = new QAction("Expand All", this);
    QAction *doNotDownloadAction = new QAction("Do Not Download", this);
    QAction *normalPriorityAction = new QAction("Normal Priority", this);
    QAction *highPriorityAction = new QAction("High Priority", this);
    QAction *highestPriorityAction = new QAction("Highest Priority", this);

    context_menu = new QMenu(filesView);
    context_menu->addAction(openPathAction);
    context_menu->addSeparator();
    context_menu->addAction(expandAllAction);
    context_menu->addSeparator();
    context_menu->addAction(doNotDownloadAction);
    context_menu->addAction(normalPriorityAction);
    context_menu->addAction(highPriorityAction);
    context_menu->addAction(highestPriorityAction);

    filesView->setContextMenuPolicy(Qt::CustomContextMenu);

    this->connect(openPathAction, SIGNAL(triggered()), this, SLOT(openPathActivated()));
    this->connect(expandAllAction, SIGNAL(triggered()), filesView, SLOT(expandAll()));
    this->connect(doNotDownloadAction, SIGNAL(triggered()), this, SLOT(doNotDownloadActivated()));
    this->connect(normalPriorityAction, SIGNAL(triggered()), this, SLOT(normalPriorityActivated()));
    this->connect(highPriorityAction, SIGNAL(triggered()), this, SLOT(highPriorityActivated()));
    this->connect(highestPriorityAction, SIGNAL(triggered()), this, SLOT(highestPriorityActivated()));
    this->connect(filesView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ShowContextMenu(QPoint)));
    this->connect(filesModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(onItemChanged(QStandardItem*)));
    this->connect(this->components->eventManager, SIGNAL(TorrentRemovedEvent(QString)),
                  this, SLOT(onTorrentRemovedEvent(QString)));
    this->connect(this->components->eventManager, SIGNAL(TorrentFileRenamedEvent(QString,int,QString)),
                  this, SLOT(onTorrentFileRenamedEvent(QString,int,QString)));
    this->connect(this->components->eventManager, SIGNAL(TorrentFolderRenamedEvent(QString,QString,QString)),
                  this, SLOT(onTorrentFolderRenamedEvent(QString,QString,QString)));

    this->load_state();
}

void FilesTab::load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("files_tab.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        this->columns_width.clear();
        in >> this->columns_width;
        for(int index = 0; index < this->columns_width.count(); ++index)
            this->filesView->setColumnWidth(index, this->columns_width[index]);
        fnew.close();
        return;
    }
    QFile f(QDir(get_default_config_dir()).absoluteFilePath("files_tab.state"));
    if(f.open(QIODevice::ReadOnly))
    {
        QString s = QString(f.readAll());
        f.close();
        PickleLoader pl(s);
        QVariant res;
        try
        {
            pl.loads(res);
            if(res.type() == QVariant::Map && res.toMap().contains("columns"))
            {
                QMap<QString, QVariant> columns = res.toMap()["columns"].toMap();
                this->columns_width.clear();
                this->columns_width << columns["Filename"].toMap()["width"].toInt()
                                    << columns["Size"].toMap()["width"].toInt()
                                    << columns["Progress"].toMap()["width"].toInt()
                                    << columns["Priority"].toMap()["width"].toInt();
                for(int index = 0; index < this->columns_width.count(); ++index)
                    this->filesView->setColumnWidth(index, this->columns_width[index]);
            }
        }
        catch(...){}
        if(this->columns_width.count() < 4)
        {
            this->columns_width.clear();
            this->columns_width << 100 << 20 << 50 << 50;
        }
    }
}

void FilesTab::save_state()
{
    for(int index = 0; index < this->columns_width.count(); ++index)
        this->columns_width[index] = this->filesView->columnWidth(index);
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("files_tab.state"));
    if(fnew.open(QIODevice::WriteOnly))
    {
        QDataStream out(&fnew);
        out << this->columns_width;
        fnew.flush();
        fnew.close();
    }
}

void FilesTab::clear_tab()
{
    this->filesModel->invisibleRootItem()->removeRows(0, this->filesModel->invisibleRootItem()->rowCount());
}

void FilesTab::get_files_from_tree(QStandardItem* parent, QList<QStandardItem*> &files)
{
    if(!parent)
        return;

    if(parent->hasChildren())
    {
        for(int i = 0; i < parent->rowCount(); ++i)
            this->get_files_from_tree(parent->child(i), files);
    }
    if(parent)
    {
        int index;
        if(parent->parent())
            index = this->filesModel->index(parent->row(), 4, parent->parent()->index()).data().toInt();
        else
            index = this->filesModel->index(parent->row(), 4).data().toInt();
        if(index > -1)
            files.append(parent);
    }
}

void FilesTab::update_tab()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
    {
        this->clear_tab();
        return;
    }

    QString torrent_id = selected_torrents[0];

    QList<QString> status_keys;
    status_keys << "file_progress" << "file_priorities";
    if(torrent_id != this->current_torrent_id)
    {
        this->filesModel->invisibleRootItem()->removeRows(0, this->filesModel->invisibleRootItem()->rowCount());
        this->current_torrent_id = torrent_id;
        status_keys << "compact";

        if(!this->files_list.keys().contains(this->current_torrent_id))
            status_keys << "files";
        else
            this->update_files();
    }
    else
        if(this->files_list[this->current_torrent_id].isEmpty())
            status_keys << "files";

    QMap<QString, QVariant> status = (*(this->components->torrentManager))[torrent_id]->get_status(status_keys);

    if(status.isEmpty())
        return;

    if(status.keys().contains("compact"))
        this->compact = status["compact"].toBool();

    if(status.keys().contains("files"))
    {
        this->files_list[this->current_torrent_id] = status["files"].value<QList<QVariantMap> >();
        this->update_files();
    }

    QList<QStandardItem*> lfiles;
    for(int i = 0; i < this->filesModel->invisibleRootItem()->rowCount(); ++i)
        this->get_files_from_tree(this->filesModel->invisibleRootItem()->child(i), lfiles);
    foreach(QStandardItem *item, lfiles)
    {
        int index = this->filesModel->index(item->row(), 4, item->index().parent()).data().toInt();

        QModelIndex modelIndex = this->filesModel->index(item->row(), 2, item->index().parent());
        if(status["file_progress"].value<QList<float> >()[index] != modelIndex.data().toFloat())
        {
            this->filesModel->blockSignals(true);
            this->filesModel->setData(modelIndex, status["file_progress"].value<QList<float> >()[index]);
            this->filesModel->blockSignals(false);
            this->filesView->update(modelIndex);
        }

        modelIndex = this->filesModel->index(item->row(), 3, item->index().parent());
        if(FILE_PRIORITY[status["file_priorities"].value<QVector<int> >()[index]] != modelIndex.data().toString())
        {
            this->filesModel->blockSignals(true);
            this->filesModel->setData(modelIndex, QVariant(FILE_PRIORITY[status["file_priorities"].value<QVector<int> >()[index]]));
            this->filesModel->blockSignals(false);
            this->filesView->update(modelIndex);
        }
    }
}

void FilesTab::update_files()
{
    this->prepare_file_store(this->files_list[this->current_torrent_id]);
}

void FilesTab::prepare_file_store(QList<QMap<QString, QVariant> > files)
{
    this->filesModel->invisibleRootItem()->removeRows(0, this->filesModel->invisibleRootItem()->rowCount());
    QMap<QString, QVariant> split_files;
    int i = 0;
    QMap<QString, QVariant> file;
    foreach(file, files)
    {
        this->prepare_file(file, file["path"].toString(), i,
                      file["download"].toBool(), split_files);
        i += 1;
    }

    this->add_files(this->filesModel->invisibleRootItem(), split_files);
}

void FilesTab::prepare_file(QMap<QString, QVariant> &file, QString file_name, int file_num,
                                    bool download, QMap<QString, QVariant> &files_storage)
{
    int first_slash_index = file_name.indexOf(QDir::separator());
    if(first_slash_index == -1)
    {
        QList<QVariant> elem;
        elem << file_num;
        elem << file;
        elem << download;
        files_storage[file_name] = elem;
    }
    else
    {
        QString file_name_chunk = file_name.left(first_slash_index+1);
        QMap<QString, QVariant> tmp_map;
        if(!files_storage.keys().contains(file_name_chunk))
            files_storage[file_name_chunk] = tmp_map;
        else
            tmp_map = files_storage[file_name_chunk].toMap();
        this->prepare_file(file, file_name.right(file_name.length()-first_slash_index-1),
                           file_num, download, tmp_map);
        files_storage[file_name_chunk] = tmp_map;
    }
}

int FilesTab::add_files(QStandardItem *parent_index, QMap<QString, QVariant> split_files)
{
    int ret = 0;
    foreach(QString key, split_files.keys())
    {
        QVariant value = split_files[key];

        if(key.endsWith(QDir::separator()))
        {
            QList<QStandardItem*> items;
            items << new QStandardItem(this->folder_icon, key);
            items[0]->setEditable(true);
            items << new QStandardItem(); //QString::number(0));
            items[1]->setEditable(false);
            items << new QStandardItem();
            items[2]->setData(0, Qt::UserRole);
            items[2]->setEditable(false);
            items << new QStandardItem();
            items[3]->setEditable(false);
            items << new QStandardItem();
            items[4]->setData(-1, Qt::DisplayRole);
            items[4]->setEditable(false);
            parent_index->appendRow(items);

            int chunk_size = this->add_files(items[0], value.toMap());
            this->filesModel->blockSignals(true);
            items[1]->setData(fsize(chunk_size), Qt::DisplayRole);
            this->filesModel->blockSignals(false);

            ret += chunk_size;
        }
        else
        {
            QList<QStandardItem*> items;
            items << new QStandardItem(this->file_icon, key);
            items[0]->setEditable(true);
            items << new QStandardItem(fsize(value.toList()[1].toMap()["size"].toInt()));
            items[1]->setEditable(false);
            items << new QStandardItem();
            items[2]->setData(0, Qt::DisplayRole);
            items[2]->setData(1, Qt::UserRole);
            items[2]->setEditable(false);
            items << new QStandardItem();
            items[3]->setData(0, Qt::DisplayRole);
            items[3]->setEditable(false);
            items << new QStandardItem();
            items[4]->setData(value.toList()[1].toMap()["index"], Qt::DisplayRole);
            items[4]->setEditable(false);
            parent_index->appendRow(items);

            ret += value.toList()[1].toMap()["size"].toInt();
        }
    }
    return ret;
}

QString FilesTab::get_file_path(QModelIndex index, QString path = "")
{
    if(!index.isValid())
        return path;

    path = index.data().toString() + path;
    return this->get_file_path(index.parent(), path);
}

void FilesTab::openPathActivated()
{
    QList<QString> status_keys;
    status_keys << "save_path";
    QMap<QString, QVariant> status = this->components->torrentManager->torrents[this->current_torrent_id]->get_status(status_keys);

    foreach(QModelIndex ind, this->filesView->selectionModel()->selectedRows())
        QDesktopServices::openUrl(QUrl("file:///" + QDir(status["save_path"].toString()).absoluteFilePath(this->get_file_path(ind))));
}

void FilesTab::set_file_priorities_on_user_change(QModelIndexList selected, int priority)
{
    QList<QString> status_keys;
    status_keys << "file_priorities";
    QVector<int> priorities = this->components->torrentManager->torrents[this->current_torrent_id]->get_status(status_keys)["file_priorities"].value<QVector<int> >();
    QList<QStandardItem*> lfiles;
    foreach(QModelIndex ind, selected)
        this->get_files_from_tree(this->filesModel->itemFromIndex(ind), lfiles);
    foreach(QStandardItem* it, lfiles)
        priorities[this->filesModel->index(it->index().row(), 4, it->index().parent()).data().toInt()] = priority;
    this->components->torrentManager->torrents[this->current_torrent_id]->set_file_priorities(priorities);
}

void FilesTab::doNotDownloadActivated()
{
    this->set_file_priorities_on_user_change(this->filesView->selectionModel()->selectedRows(), 0);
}

void FilesTab::normalPriorityActivated()
{
    this->set_file_priorities_on_user_change(this->filesView->selectionModel()->selectedRows(), 1);
}

void FilesTab::highPriorityActivated()
{
    this->set_file_priorities_on_user_change(this->filesView->selectionModel()->selectedRows(), 2);
}

void FilesTab::highestPriorityActivated()
{
    this->set_file_priorities_on_user_change(this->filesView->selectionModel()->selectedRows(), 5);
}

void FilesTab::ShowContextMenu(const QPoint& pos)
{
    if(this->filesView->indexAt(pos).row() > -1)
        this->context_menu->exec(this->filesView->mapToGlobal(pos));
}

void FilesTab::onItemChanged(QStandardItem *item)
{
    if(item->column() == 0)
    {
        int index = this->filesModel->index(item->row(), 4, item->index().parent()).data().toInt();
        if(index > -1)
        {
            QString filepath;
            if(item->parent())
                filepath = this->get_file_path(item->index());
            else
                filepath = item->data(Qt::DisplayRole).toString();
            this->components->torrentManager->torrents[this->current_torrent_id]->rename_file(index, filepath);
        }
        else
        {
            QString new_folder = this->get_file_path(item->index());
            int deep = new_folder.count(QDir::separator());
            if(new_folder.endsWith(QDir::separator()))
                deep -= 1;
            QString old_folder;
            if(!item->hasChildren())
                return;
            QStandardItem *child = item->child(0);
            while(this->filesModel->index(child->index().row(), 4, child->index().parent()).data().toInt() == -1)
            {
                if(!child->hasChildren())
                    return;
                child = child->child(0);
            }
            int child_index = this->filesModel->index(child->index().row(), 4, child->index().parent()).data().toInt();
            for(int i = 0; i < this->files_list[this->current_torrent_id].count(); ++i)
            {
                if(this->files_list[this->current_torrent_id][i]["index"].toInt() == child_index)
                {
                    QString child_path = this->files_list[this->current_torrent_id][i]["path"].toString();
                    QList<QString> split_path = child_path.split(QDir::separator());
                    old_folder = split_path[0];
                    for(int j = 1; j <= deep; ++j)
                        old_folder += QDir::separator() + split_path[j];
                    old_folder += QDir::separator();
                    break;
                }
            }
            if(!new_folder.endsWith(QDir::separator()))
                new_folder += QDir::separator();
            if(new_folder != old_folder)
                this->components->torrentManager->torrents[this->current_torrent_id]->rename_folder(old_folder, new_folder);
        }
    }
}

void FilesTab::onTorrentRemovedEvent(QString torrent_id)
{
    if(this->files_list.contains(torrent_id))
        this->files_list.remove(torrent_id);
}

void FilesTab::onTorrentFileRenamedEvent(QString torrent_id, int index, QString name)
{
    if(this->files_list.contains(torrent_id))
    {
        for(int i = 0; i < this->files_list[torrent_id].count(); ++i)
        {
            if(this->files_list[torrent_id][i]["index"].toInt() == index)
            {
                this->files_list[torrent_id][i]["path"] = name;
                break;
            }
        }
    }
}

void FilesTab::onTorrentFolderRenamedEvent(QString torrent_id, QString old_folder, QString new_folder)
{
    if(this->files_list.contains(torrent_id))
    {
        for(int i = 0; i < this->files_list[torrent_id].count(); ++i)
        {
            QString path = this->files_list[torrent_id][i]["path"].toString();
            if(path.startsWith(old_folder))
            {
                path = path.replace(old_folder, new_folder);
                this->files_list[torrent_id][i]["path"] = path;
            }
        }
    }
}
