#include "removetorrentdialog.h"

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

RemoveTorrentDialog::RemoveTorrentDialog(QWidget *parent) :
    QDialog(parent)
{
    QLabel* firstLabel = new QLabel("Remove the selected torrent?");

    QLabel* secondLabel = new QLabel("If you remove the data, it will be lost permanently.");

    /*QHBoxLayout *URLLayout = new QHBoxLayout();
    URLLayout->addWidget(URLLabel);
    URLLayout->addWidget(URLEntry);*/

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *removeDataButton = new QPushButton("Remove With Data");
    this->connect(removeDataButton, SIGNAL(clicked()), this, SLOT(_on_remove_with_data_clicked()));

    QPushButton *removeButton = new QPushButton("Remove Torrent");
    this->connect(removeButton, SIGNAL(clicked()), this, SLOT(_on_remove_clicked()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(removeDataButton);
    buttonsLayout->addWidget(removeButton);
    buttonsLayout->setAlignment(Qt::AlignRight);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(firstLabel);
    mainLayout->addWidget(secondLabel);
    //mainLayout->addLayout(URLLayout);
    mainLayout->addLayout(buttonsLayout);

    this->setWindowTitle(tr(""));
    this->setLayout(mainLayout);

    with_data = false;
}

int RemoveTorrentDialog::show_remove_torrent_dialog()
{
    if(this->exec() == QDialog::Accepted)
    {
        if(this->with_data)
            return 2;
        return 1;
    }
    return 0;
}

void RemoveTorrentDialog::_on_remove_with_data_clicked()
{
    this->with_data = true;
    this->accept();
}

void RemoveTorrentDialog::_on_remove_clicked()
{
    this->with_data = false;
    this->accept();
}
