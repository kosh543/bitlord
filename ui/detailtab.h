#ifndef DETAILTAB_H
#define DETAILTAB_H

#include <QWidget>
#include <QLabel>

class Components;

class DetailTab : public QWidget
{
    Q_OBJECT
private:
    Components *components;
    QLabel *path;
    QLabel *totalSize;
    QLabel *hash;
    QLabel *comments;
    QLabel *name;
    QLabel *numFiles;
    QLabel *status;
    QLabel *tracker;

public:
    explicit DetailTab(Components *comp, QWidget *parent = 0);
    void update_tab();
    void clear_tab();

};

#endif // DETAILTAB_H
