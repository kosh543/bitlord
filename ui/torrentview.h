#ifndef TORRENTVIEW_H
#define TORRENTVIEW_H

#include <QTreeView>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include <QItemDelegate>

#include "libtorrent/session.hpp"

#include "treeviewmodel.h"
#include "treeviewitem.h"

class Components;

class TorrentViewProgressItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    TorrentViewProgressItemDelegate(QObject *parent);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

class TorrentView : public QTreeView
{
    Q_OBJECT
private:

    struct TorrentViewColumn
    {
     public:
        int index;
        QString name;
        bool visible;
        int position;
        int width;
        bool fix_width;
        QList<QString> status_field;
    };

    Components *components;
    QTimer *_update_timer;
    QTimer *_st_idle_timer;
    QTimer *_update_view_idle_timer;
    TreeViewModel *model;
    QSortFilterProxyModel *proxyModel;
    QList<QMap<QString, QVariant> > state;
    QMap<QString, QMap<QString, QVariant> > status;
    QMap<QString, TorrentViewColumn> columns;
    QList<QString> columns_to_update;
    QList<QString> filtered_torrents;
    QString current_filter;

public:
    explicit TorrentView(Components *comp, QWidget *parent = 0);
    void start();
    void load_state();
    void save_state();
    void add_column(QString text);
    void add_row(QString torrent_id, bool update=false);
    void set_filter(QString filter);
    QList<QString> get_selected_torrents();
    bool check_state(QString torrent_id, QString state);
    bool is_column_visible(QString name);
    void set_column_visible(QString name, bool visible);

private slots:
    void send_status_request();
    void update_view();
    void ShowContextMenu(const QPoint& pos);
    void on_torrentview_clicked(const QModelIndex index);
    void on_torrentview_selection_changed(QItemSelection selected, QItemSelection deselected);

public slots:
    void update_data();
    void on_torrentstatechanged_event(QString torrent_id, QString state);
    void on_torrentadded_event(QString torrent_id); //, feed_id);
    void on_torrentremoved_event(QString torrent_id);
    void on_sessionpaused_event();
    void on_sessionresumed_event();
    void on_torrentqueuechanged_event();
    void on_torrentfinished_event(QString torrent_id);
};

#endif // TORRENTVIEW_H
