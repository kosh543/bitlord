#ifndef SETOTHERDIALOG_H
#define SETOTHERDIALOG_H

#include <QDialog>
#include <QSpinBox>

class SetOtherDialog : public QDialog
{
    Q_OBJECT
private:
    QSpinBox *result_spin;

public:
    explicit SetOtherDialog(QString main_text, QString type_text, QWidget *parent = 0);
    int show_set_other_dialog();

};

#endif // SETOTHERDIALOG_H
