#include "addtorrentdialog.h"
#include "components.h"
#include "core/downloadingdialog.h"

#include <QSettings>

AddFromURLDialog::AddFromURLDialog(QWidget *parent) :
    QDialog(parent)
{
    QLabel* URLLabel = new QLabel("URL:");

    URLEntry = new QLineEdit();

    QHBoxLayout *URLLayout = new QHBoxLayout();
    URLLayout->addWidget(URLLabel);
    URLLayout->addWidget(URLEntry);

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QPushButton *okButton = new QPushButton("OK");
    this->connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(okButton);
    buttonsLayout->setAlignment(Qt::AlignRight);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(URLLayout);
    mainLayout->addLayout(buttonsLayout);

    this->setWindowTitle(tr("Add From URL"));
    this->setLayout(mainLayout);

    cb = QApplication::clipboard();
}

QString AddFromURLDialog::show_add_from_url_dialog()
{
    QString cb_text = this->cb->text();
    if(is_url(cb_text) || is_magnet(cb_text))
        this->URLEntry->setText(cb_text);
    else
        this->URLEntry->setText("");
    this->exec();
    if(this->result() == QDialog::Accepted)
        return this->URLEntry->text();
    return "";
}

AddTorrentDialog::AddTorrentDialog(Components *comp, QWidget *parent) :
    QDialog(parent)
{
    this->components = comp;

    folder_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/folder.png"));
    file_icon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/file.png"));

    previous_selected_torrent = -1;

    downloadLocationText = new QLineEdit();
    downloadLocationText->setReadOnly(true);

    QPushButton *downloadLocationButton = new QPushButton("...");
    this->connect(downloadLocationButton, SIGNAL(clicked()), this, SLOT(_on_button_browse_location_clicked()));

    QHBoxLayout *downloadLocationLayout = new QHBoxLayout();
    downloadLocationLayout->addWidget(downloadLocationText);
    downloadLocationLayout->addWidget(downloadLocationButton);

    QGroupBox *downloadLocationFrame = new QGroupBox();
    downloadLocationFrame->setTitle("Download Location");
    downloadLocationFrame->setLayout(downloadLocationLayout);

    torrentsListView = new QTreeView();
    torrentsModel = new QStandardItemModel(0, 3);
    torrentsListView->setModel(torrentsModel);
    torrentsListView->setColumnHidden(0, true);
    torrentsListView->setColumnHidden(2, true);
    torrentsListView->setHeaderHidden(true);
    torrentsListView->setRootIsDecorated(false);
    torrentsListView->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->connect(torrentsListView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
                  this, SLOT(_on_torrent_changed(QModelIndex,QModelIndex)));

    QPushButton *torrentsAddFile = new QPushButton("Add File");
    this->connect(torrentsAddFile, SIGNAL(clicked()), this, SLOT(_on_button_file_clicked()));

    QPushButton *torrentsAddURL = new QPushButton("Add URL");
    this->connect(torrentsAddURL, SIGNAL(clicked()), this, SLOT(_on_button_url_clicked()));

    QPushButton *torrentsAddInfohash = new QPushButton("Add Infohash");
    this->connect(torrentsAddInfohash, SIGNAL(clicked()), this, SLOT(_on_button_hash_clicked()));

    QPushButton *torrentsRemove = new QPushButton("Remove");
    this->connect(torrentsRemove, SIGNAL(clicked()), this, SLOT(_on_button_remove_clicked()));

    QHBoxLayout *torrentsHLayout = new QHBoxLayout();
    torrentsHLayout->addWidget(torrentsAddFile);
    torrentsHLayout->addWidget(torrentsAddURL);
    torrentsHLayout->addWidget(torrentsAddInfohash);
    torrentsHLayout->addWidget(torrentsRemove);

    QVBoxLayout *torrentsVLayout = new QVBoxLayout();
    torrentsVLayout->addWidget(torrentsListView);
    torrentsVLayout->addLayout(torrentsHLayout);

    QGroupBox *torrentsFrame = new QGroupBox();
    torrentsFrame->setTitle("Torrents");
    torrentsFrame->setLayout(torrentsVLayout);

    filesView = new QTreeView();
    filesModel = new QStandardItemModel(0, 4);
    filesView->setModel(filesModel);
    //filesView->setColumnHidden(0, true);
    //filesView->setColumnHidden(2, true);
    filesView->setHeaderHidden(true);
    filesView->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->connect(filesModel, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(_on_files_item_changed(QStandardItem*)));

    fullRButton = new QRadioButton("Full");

    compactRButton = new QRadioButton("Compact");
    this->compact = false;
    this->connect(compactRButton, SIGNAL(toggled(bool)), this, SLOT(_on_compact_toggled(bool)));

    QVBoxLayout *allocationLayout = new QVBoxLayout();
    allocationLayout->addWidget(fullRButton);
    allocationLayout->addWidget(compactRButton);

    QGroupBox *allocationFrame = new QGroupBox();
    allocationFrame->setTitle("Allocation");
    allocationFrame->setLayout(allocationLayout);

    maxDownSpeedSpin = new QSpinBox();
    maxDownSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxDownSpeedLayout = new QHBoxLayout();
    maxDownSpeedLayout->addWidget(new QLabel("Max Down Speed:"));
    maxDownSpeedLayout->addWidget(maxDownSpeedSpin);

    maxUpSpeedSpin = new QSpinBox();
    maxUpSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUpSpeedLayout = new QHBoxLayout();
    maxUpSpeedLayout->addWidget(new QLabel("Max Up Speed:"));
    maxUpSpeedLayout->addWidget(maxUpSpeedSpin);

    maxConnectionsSpin = new QSpinBox();
    maxConnectionsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxConnectionsLayout = new QHBoxLayout();
    maxConnectionsLayout->addWidget(new QLabel("Max Connections:"));
    maxConnectionsLayout->addWidget(maxConnectionsSpin);

    maxUploadSlotsSpin = new QSpinBox();
    maxUploadSlotsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUploadSlotsLayout = new QHBoxLayout();
    maxUploadSlotsLayout->addWidget(new QLabel("Max Upload Slots:"));
    maxUploadSlotsLayout->addWidget(maxUploadSlotsSpin);

    QVBoxLayout *bandwidthLayout = new QVBoxLayout();
    bandwidthLayout->addLayout(maxDownSpeedLayout);
    bandwidthLayout->addLayout(maxUpSpeedLayout);
    bandwidthLayout->addLayout(maxConnectionsLayout);
    bandwidthLayout->addLayout(maxUploadSlotsLayout);

    QGroupBox *bandwidthFrame = new QGroupBox();
    bandwidthFrame->setTitle("Bandwidth");
    bandwidthFrame->setLayout(bandwidthLayout);

    addPauseCheck = new QCheckBox("Add In Pause State");

    priorFLCheck = new QCheckBox("Prioritise First/Last Pieces");

    QVBoxLayout *generalLayout = new QVBoxLayout();
    generalLayout->addWidget(addPauseCheck);
    generalLayout->addWidget(priorFLCheck);

    QGroupBox *generalFrame = new QGroupBox();
    generalFrame->setTitle("General");
    generalFrame->setLayout(generalLayout);

    QHBoxLayout *optionsH1Layout = new QHBoxLayout();
    optionsH1Layout->addWidget(allocationFrame);
    optionsH1Layout->addWidget(bandwidthFrame);
    optionsH1Layout->addWidget(generalFrame);

    QPushButton *setDefault = new QPushButton("Revert To Defaults");
    this->connect(setDefault, SIGNAL(clicked()), this, SLOT(_on_button_revert_clicked()));

    QPushButton *applyToAll = new QPushButton("Apply To All");
    this->connect(applyToAll, SIGNAL(clicked()), this, SLOT(_on_button_apply_clicked()));

    QHBoxLayout *optionsH2Layout = new QHBoxLayout();
    optionsH2Layout->addWidget(setDefault);
    optionsH2Layout->addWidget(applyToAll);

    QVBoxLayout *optionsVLayout = new QVBoxLayout();
    optionsVLayout->addLayout(optionsH1Layout);
    optionsVLayout->addLayout(optionsH2Layout);

    QWidget *optionsPage = new QWidget();
    optionsPage->setLayout(optionsVLayout);

    QPushButton *selectAllFiles = new QPushButton("Select all");
    this->connect(selectAllFiles, SIGNAL(clicked()), this, SLOT(_on_button_select_all_clicked()));

    QPushButton *unselectAllFiles = new QPushButton("Unselect all");
    this->connect(unselectAllFiles, SIGNAL(clicked()), this, SLOT(_on_button_unselect_all_clicked()));

    QHBoxLayout *selectUnselectLayout = new QHBoxLayout();
    selectUnselectLayout->addWidget(selectAllFiles);
    selectUnselectLayout->addWidget(unselectAllFiles);
    selectUnselectLayout->setMargin(0);
    selectUnselectLayout->setSpacing(0);

    QWidget *selectUnselectWidget = new QWidget();
    selectUnselectWidget->setLayout(selectUnselectLayout);

    QTabWidget *notebook = new QTabWidget();
    notebook->addTab(filesView, "Files");
    notebook->addTab(optionsPage, "Options");
    notebook->setCornerWidget(selectUnselectWidget);

    QPushButton *cancelButton = new QPushButton("Cancel");
    this->connect(cancelButton, SIGNAL(clicked()), this, SLOT(_on_button_cancel_clicked()));

    QPushButton *startButton = new QPushButton("Start download");
    this->connect(startButton, SIGNAL(clicked()), this, SLOT(_on_button_add_clicked()));

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(cancelButton);
    buttonsLayout->addWidget(startButton);

    streamingCheck = new QCheckBox("Enable instant streaming");
    this->connect(streamingCheck, SIGNAL(toggled(bool)), this, SLOT(_on_chk_streaming_toggled(bool)));

    QHBoxLayout *streamingLayout = new QHBoxLayout();
    streamingLayout->addWidget(streamingCheck);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(downloadLocationFrame);
    mainLayout->addWidget(torrentsFrame);
    mainLayout->addWidget(notebook);
    mainLayout->addLayout(buttonsLayout);
    mainLayout->addLayout(streamingLayout);

    this->setWindowTitle(tr("Add Torrents"));
    this->setLayout(mainLayout);

    addFromURLDialog = new AddFromURLDialog(this);

    this->connect(this, SIGNAL(finished(int)), this, SLOT(_on_dialog_destroyed(int)));
}

void AddTorrentDialog::show_add_torrent_dialog()
{
    this->set_default_options();
    this->show();
}

void AddTorrentDialog::initialize()
{
    QStringList arguments = QCoreApplication::arguments();
    if(arguments.size() > 1)
    {
        this->set_default_options();
        arguments.removeAt(0);
        foreach(QString arg, arguments)
        {
            if(is_magnet(arg))
                this->add_from_magnets(arguments.mid(arguments.indexOf(arg), 1));
            else
                this->add_from_files(arguments.mid(arguments.indexOf(arg), 1));
        }
        this->show();
        this->activateWindow();
    }
}

void AddTorrentDialog::receive_message(QString msg)
{
    this->set_default_options();
    QStringList key;
    key << msg;
    if(is_magnet(msg))
        this->add_from_magnets(key);
    else
        this->add_from_files(key);
    this->components->mainWindow->show();
    this->show();
    this->activateWindow();
}

void AddTorrentDialog::addFromRSS(QString msg)
{
    this->set_default_options();
    if(is_magnet(msg))
    {
        QStringList key;
        key << msg;
        this->add_from_magnets(key);
    }
    else if(is_url(msg))
        this->add_from_url(msg);
    else
        return;
    this->components->mainWindow->show();
    this->show();
    this->activateWindow();
}

void AddTorrentDialog::hide_add_torrent_dialog()
{
    this->streamingCheck->setEnabled(true);
    this->hide();
    this->files.clear();
    this->infos.clear();
    this->options.clear();
    this->previous_selected_torrent = -1;
    this->torrentsModel->clear();
    this->filesModel->clear();
}

void AddTorrentDialog::_on_dialog_destroyed(int result)
{
    this->hide_add_torrent_dialog();
}

void AddTorrentDialog::set_default_options()
{
    QSettings settings;
    this->downloadLocationText->setText(settings.value("coreconfig/download_location").toString());
    this->compactRButton->setChecked(settings.value("coreconfig/compact_allocation").toBool());
    this->fullRButton->setChecked(!settings.value("coreconfig/compact_allocation").toBool());
    this->maxDownSpeedSpin->setValue(settings.value("coreconfig/max_download_speed_per_torrent").toInt());
    this->maxUpSpeedSpin->setValue(settings.value("coreconfig/max_upload_speed_per_torrent").toInt());
    this->maxConnectionsSpin->setValue(settings.value("coreconfig/max_connections_per_torrent").toInt());
    this->maxUploadSlotsSpin->setValue(settings.value("coreconfig/max_upload_slots_per_torrent").toInt());
    this->addPauseCheck->setChecked(settings.value("coreconfig/add_paused").toBool());
    this->priorFLCheck->setChecked(settings.value("coreconfig/prioritize_first_last_pieces").toBool());
    this->streamingCheck->setChecked(settings.value("coreconfig/enable_streaming").toBool());
}

void AddTorrentDialog::add_from_files(QList<QString> filenames)
{
    foreach(QString filename, filenames)
    {
        libtorrent::torrent_info *torrent_info = NULL;
        try
        {
            torrent_info = this->components->torrentManager->get_torrent_info_from_file(filename);
        }
        catch(...){}
        if(!torrent_info)
            continue;

        QString info_hash = info_hash_to_string(torrent_info->info_hash());
        if(this->files.keys().contains(info_hash))
        {
            continue;
        }

        QString name = QString::fromStdString(torrent_info->name());

        QList<QStandardItem*> items;
        items << new QStandardItem(info_hash);
        items << new QStandardItem(name);
        items << new QStandardItem(filename);
        this->torrentsModel->appendRow(items);

        if(this->torrentsModel->rowCount() > 1)
        {
            this->streamingCheck->setChecked(false);
            this->streamingCheck->setEnabled(false);
        }

        libtorrent::file_storage files_storage = torrent_info->files();
        QList<QMap<QString, QVariant> > ret;
        for(int index = 0; index < files_storage.num_files(); index++)
        {
            QMap<QString, QVariant> val;
            val["path"] = QString::fromStdString(files_storage.at(index).path);
            val["size"] = files_storage.at(index).size;
            val["download"] = true;
            val["index"] = index;
            ret.append(val);
        }
        this->files[info_hash] = ret;
        this->infos[info_hash] = torrent_info;

        QModelIndex last = this->torrentsModel->indexFromItem(items[1]);
        this->torrentsListView->selectionModel()->setCurrentIndex(last, QItemSelectionModel::ClearAndSelect);

        this->set_default_options();
        this->save_torrent_options(last.row());
    }
    this->torrentsListView->setColumnHidden(0, true);
    this->torrentsListView->setColumnHidden(2, true);
}

void AddTorrentDialog::add_from_magnets(QList<QString> magnets)
{
    foreach(QString magnet, magnets)
    {
        QString info_hash;
        QString s = magnet.split("&")[0].mid(20);
        if(s.length() == 32)
        {
            //info_hash = base64.b32decode(s).encode("hex");
            QByteArray xcode("");
            xcode.append(s);
            QByteArray precode(QByteArray::fromBase64(xcode));
            info_hash = precode.toHex().data();
        }
        else if(s.length() == 40)
            info_hash = s;
        if(this->infos.contains(info_hash))
            continue;

        QString name;
        foreach(QString i, magnet.split("&"))
        {
            if(i.startsWith("dn="))
                name = i.split("=")[1] + "(" + magnet + ")";
        }
        if(name.isEmpty())
            name = magnet;

        QList<QStandardItem*> items;
        items << new QStandardItem(info_hash);
        items << new QStandardItem(name);
        items << new QStandardItem(magnet);
        this->torrentsModel->appendRow(items);

        if(this->torrentsModel->rowCount() > 1)
        {
            this->streamingCheck->setChecked(false);
            this->streamingCheck->setEnabled(false);
        }

        QList<QMap<QString, QVariant> > ret;

        QMap<QString, QVariant> val;
        val["path"] = "Loading files...";
        val["size"] = 0;
        val["download"] = true;
        val["index"] = 0;
        ret.append(val);

        this->files[info_hash] = ret;
        this->infos[info_hash] = NULL;

        QModelIndex last = this->torrentsModel->indexFromItem(items[1]);
        this->torrentsListView->selectionModel()->setCurrentIndex(last, QItemSelectionModel::ClearAndSelect);

        this->set_default_options();
        this->save_torrent_options(last.row());
    }
    this->torrentsListView->setColumnHidden(0, true);
    this->torrentsListView->setColumnHidden(2, true);
}

void AddTorrentDialog::add_from_url(QString url)
{
    DownloadingDialog *dialog = new DownloadingDialog(QUrl(url), "torrent", this);
    this->connect(dialog, SIGNAL(fileDownloaded(QString)), this, SLOT(torrent_file_downloaded(QString)));
    dialog->show_downloading_dialog();
}

void AddTorrentDialog::torrent_file_downloaded(QString filePath)
{
    QList<QString> files_for_adding;
    files_for_adding << filePath;
    this->add_from_files(files_for_adding);
}

void AddTorrentDialog::save_torrent_options(int row)
{
    // Keeps the torrent options dictionary up-to-date with what the user has
    // selected.
    if(row == -1)
    {
        if(this->previous_selected_torrent > -1 && this->torrentsModel->hasIndex(this->previous_selected_torrent, 0))
            row = this->previous_selected_torrent;
        else
            return;
    }

    QString torrent_id = this->torrentsModel->item(row)->data(Qt::DisplayRole).toString();

    TorrentOptions* option;

    if(this->options.keys().contains(torrent_id))
        option = this->options[torrent_id];
    else
        option = new TorrentOptions();

    (*option)["download_location"] = this->downloadLocationText->text();
    (*option)["compact_allocation"] = this->compactRButton->isChecked();

    //if(this->compactRButton->isChecked())
    //    this->set_checked_for_all(this->filesModel->invisibleRootItem());

    (*option)["max_download_speed"] = this->maxDownSpeedSpin->value();
    (*option)["max_upload_speed"] = this->maxUpSpeedSpin->value();
    (*option)["max_connections"] = this->maxConnectionsSpin->value();
    (*option)["max_upload_slots"] = this->maxUploadSlotsSpin->value();
    (*option)["add_paused"] = this->addPauseCheck->isChecked();
    (*option)["prioritize_first_last_pieces"] = this->priorFLCheck->isChecked();
    (*option)["enable_streaming"] = this->streamingCheck->isChecked();

    this->options[torrent_id] = option;

    // Save the file priorities
    QMap<int, bool> files_priorities;
    this->build_priorities(this->filesModel->invisibleRootItem(), files_priorities);

    if(files_priorities.count() > 0)
    {
        for(int i = 0; i < this->files[torrent_id].length(); ++i)
            this->files[torrent_id][i]["download"] = files_priorities[i];
    }
}

void AddTorrentDialog::set_checked_for_all(QStandardItem* parent)
{
    if(!parent)
        return;
    if(parent->hasChildren())
    {
        for(int i = 0; i < parent->rowCount(); ++i)
            this->set_checked_for_all(parent->child(i));
    }
    if(parent->isCheckable() && parent->checkState() != Qt::Checked)
        parent->setCheckState(Qt::Checked);
}

void AddTorrentDialog::build_priorities(QStandardItem* parent, QMap<int, bool> &priorities)
{
    for(int row = 0; row < parent->rowCount(); ++row)
    {
        QStandardItem* child = parent->child(row);
        if(!child)
            continue;
        if(child->hasChildren())
            this->build_priorities(child, priorities);
        else
        {
            bool download = true;
            if(child->checkState() != Qt::Checked)
                download = false;
            priorities[parent->child(row, 3)->data(Qt::DisplayRole).toInt()] = download;
        }
    }
}

void AddTorrentDialog::_on_torrent_changed(QModelIndex selected, QModelIndex deselected)
{
    int row = selected.row();
    if(row == -1)
    {
        return;
    }

    QString torrent_id = this->torrentsModel->item(row)->data(Qt::DisplayRole).toString();

    if(!this->files.keys().contains(torrent_id))
    {
        // this->files_treeview->clear();
        this->previous_selected_torrent = -1;
        return;
    }

    // Save the previous torrents options
    this->save_torrent_options();

    // Update files list
    QList<QMap<QString, QVariant> > files_list = this->files[torrent_id];
    // If some files already renamed:
    try
    {
        /*mapped_files = self.options[model.get_value(row, 0)]["mapped_files"]
        for file_index in mapped_files:
            files_list[file_index]["path"] = mapped_files[file_index]*/
    }
    catch(...){}

    this->prepare_file_store(files_list);

    //if self.core_config == {}:
    //    this->update_core_config()

    // Update the options frame
    this->update_torrent_options(torrent_id);

    this->previous_selected_torrent = row;
}

void AddTorrentDialog::update_torrent_options(QString torrent_id)
{
    if(!this->options.keys().contains(torrent_id))
    {
        this->set_default_options();
        return;
    }

    TorrentOptions* cur_options = this->options[torrent_id];

    this->downloadLocationText->setText((*cur_options)["download_location"].toString());
    this->compactRButton->setChecked((*cur_options)["compact_allocation"].toBool());
    this->fullRButton->setChecked(!(*cur_options)["compact_allocation"].toBool());
    this->maxDownSpeedSpin->setValue((*cur_options)["max_download_speed"].toInt());
    this->maxUpSpeedSpin->setValue((*cur_options)["max_upload_speed"].toInt());
    this->maxConnectionsSpin->setValue((*cur_options)["max_connections"].toInt());
    this->maxUploadSlotsSpin->setValue((*cur_options)["max_upload_slots"].toInt());
    this->addPauseCheck->setChecked((*cur_options)["add_paused"].toBool());
    this->priorFLCheck->setChecked((*cur_options)["prioritize_first_last_pieces"].toBool());
    this->streamingCheck->setChecked((*cur_options)["enable_streaming"].toBool());
}

void AddTorrentDialog::prepare_file_store(QList<QMap<QString, QVariant> > files)
{
    //self.listview_files.set_model(None)
    this->filesModel->clear();
    QMap<QString, QVariant> split_files;
    int i = 0;
    QMap<QString, QVariant> file;
    foreach(file, files)
    {
        this->prepare_file(file, file["path"].toString(), i,
                      file["download"].toBool(), split_files);
        i += 1;
    }

    this->add_files(this->filesModel->invisibleRootItem(), split_files);
    //self.listview_files.set_model(self.files_treestore)
    //self.listview_files.expand_row("0", False)
}

void AddTorrentDialog::prepare_file(QMap<QString, QVariant> &file, QString file_name, int file_num,
                                    bool download, QMap<QString, QVariant> &files_storage)
{
    int first_slash_index = file_name.indexOf(QDir::separator());
    if(first_slash_index == -1)
    {
        QList<QVariant> elem;
        elem << file_num;
        elem << file;
        elem << download;
        files_storage[file_name] = elem;
    }
    else
    {
        QString file_name_chunk = file_name.left(first_slash_index+1);
        QMap<QString, QVariant> tmp_map;
        if(!files_storage.keys().contains(file_name_chunk))
            files_storage[file_name_chunk] = tmp_map;
        else
            tmp_map = files_storage[file_name_chunk].toMap();
        this->prepare_file(file, file_name.right(file_name.length()-first_slash_index-1),
                           file_num, download, tmp_map);
        files_storage[file_name_chunk] = tmp_map;
    }
}

void AddTorrentDialog::add_files(QStandardItem *parent_index, QMap<QString, QVariant> split_files)
{
    foreach(QString key, split_files.keys())
    {
        QVariant value = split_files[key];

        if(key.endsWith(QDir::separator()))
        {
            QList<QStandardItem*> items;
            QStandardItem* check_item = new QStandardItem();
            check_item->setCheckable(true);
            check_item->setCheckState(Qt::Checked);
            items << check_item;
            items[0]->setEditable(false);
            items << new QStandardItem(this->folder_icon, key);
            items[1]->setEditable(true);
            items << new QStandardItem(QString::number(0));
            items[2]->setEditable(false);
            items << new QStandardItem(QString::number(-1));
            items[3]->setEditable(false);
            //items << new QStandardItem(QString::number(false));
            parent_index->appendRow(items);

            //chunk_iter = self.files_treestore.append(parent_iter,
            //                [True, key, 0, -1, False, gtk.STOCK_DIRECTORY])
            this->add_files(items[0], value.toMap());
        }
        else
        {
            QList<QStandardItem*> items;
            QStandardItem* check_item = new QStandardItem();
            check_item->setCheckable(true);
            if(value.toList()[2].toBool())
                check_item->setCheckState(Qt::Checked);
            else
                check_item->setCheckState(Qt::Unchecked);
            items << check_item;
            items[0]->setEditable(false);
            items << new QStandardItem(this->file_icon, key);
            items[1]->setEditable(true);
            items << new QStandardItem(QString::number(value.toList()[1].toMap()["size"].toInt()));
            items[2]->setEditable(false);
            items << new QStandardItem(QString::number(value.toList()[0].toInt()));
            items[3]->setEditable(false);
            //items << new QStandardItem(QString::number(false));
            parent_index->appendRow(items);

            //self.files_treestore.append(parent_iter, [value[2], key,
            //                        value[1]["size"], value[0], False, gtk.STOCK_FILE])
        }

        this->set_correct_check_state(parent_index);
    }
}

void AddTorrentDialog::set_correct_check_state(QStandardItem* item)
{
    if(!item)
        return;
    if(item->hasChildren())
    {
        QList<bool> download;
        bool download_value = false;
        bool inconsistent = false;
        for(int row = 0; row < item->rowCount(); ++row)
        {
            QStandardItem* child = item->child(row);
            Qt::CheckState st = child->checkState();
            if(st == Qt::PartiallyChecked)
            {
                inconsistent = true;
                break;
            }
            if(st == Qt::Checked)
                download.append(true);
            else
                download.append(false);
        }
        if(download.count(true) == download.length())
            download_value = true;
        else if(download.count(true) == 0)
            download_value = false;
        else
            inconsistent = true;

        Qt::CheckState correct_state;
        if(inconsistent)
            correct_state = Qt::PartiallyChecked;
        else if(download_value)
            correct_state = Qt::Checked;
        else
            correct_state = Qt::Unchecked;
        if(correct_state != item->checkState())
            item->setCheckState(correct_state);
    }
}

QVector<int> AddTorrentDialog::get_file_priorities(QString torrent_id)
{
    // A list of priorities
    QVector<int> files_list;

    QMap<QString, QVariant> file_dict;
    foreach(file_dict, this->files[torrent_id])
    {
        if(!file_dict["download"].toBool())
            files_list.append(0);
        else
            files_list.append(1);
    }

    return files_list;
}

void AddTorrentDialog::_on_compact_toggled(bool checked)
{
    this->compact = checked;
    if(checked)
        this->set_checked_for_all(this->filesModel->invisibleRootItem());
}

void AddTorrentDialog::_on_button_file_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Choose a torrent file",
                                                    "", "Torrents (*.torrent);; All files (*.*)");
    if(!fileName.isEmpty())
    {
        QList<QString> files;
        files << fileName;
        this->add_from_files(files);
    }
}

QString AddTorrentDialog::get_file_path(QModelIndex index, QString path)
{
    if(!index.isValid())
        return path;

    path = this->filesModel->index(index.row(), 1, index.parent()).data().toString() + path;
    return this->get_file_path(index.parent(), path);
}

void AddTorrentDialog::_on_files_item_changed(QStandardItem* item)
{
    if(item->isCheckable())
    {
        if(item->checkState() != Qt::PartiallyChecked && item->hasChildren())
        {
            Qt::CheckState state = item->checkState();
            for(int row = 0; row < item->rowCount(); ++row)
            {
                item->child(row)->setCheckState(state);
            }
        }
        this->set_correct_check_state(item->parent());
    }
    if(item->column() == 1)
    {
        QModelIndexList torrents = this->torrentsListView->selectionModel()->selectedIndexes();
        if(torrents.isEmpty())
            return;
        QString torrent_id = this->torrentsModel->data(this->torrentsModel->index(torrents[0].row(), 0)).toString();
        //QString new_text = item->data(Qt::DisplayRole).toString();
        int index = this->filesModel->index(item->row(), 3, item->index().parent()).data().toInt();
        if(index > -1)
        {
            QString file_path = this->get_file_path(item->index());
            /*if(item->index().parent().isValid())
            {
                for(int i = 0; i < item->parent()->rowCount(); ++i)
                {
                    if(item->parent()->child(i, 1) != item &&
                            new_text == item->parent()->child(i, 1)->data(Qt::DisplayRole).toString() &&
                            item->parent()->child(i, 3)->data(Qt::DisplayRole).toInt() > -1)
                        return;
                }
            }*/
            QMap<int, QString> map = (*this->options[torrent_id])["mapped_files"].value<QMap<int, QString> >();
            map[index] = file_path;
            (*this->options[torrent_id])["mapped_files"] = QVariant::fromValue<QMap<int, QString> >(map);
            for(int i = 0; i < this->files[torrent_id].count(); ++i)
            {
                if(this->files[torrent_id][i]["index"].toInt() == index)
                {
                    this->files[torrent_id][i]["path"] = QVariant(file_path);
                    break;
                }
            }
        }
        else
        {
            if(item->parent())
                item = item->parent()->child(item->row());
            else
                item = this->filesModel->item(item->row());
            QString file_path = this->get_file_path(item->index());
            if(!file_path.endsWith(QDir::separator()))
                file_path += QDir::separator();
            if(!item->hasChildren())
                return;
            int deep = file_path.count(QDir::separator());
            if(file_path.endsWith(QDir::separator()))
                deep -= 1;
            QString old_folder;
            QStandardItem *child = item->child(0);
            while(this->filesModel->index(child->index().row(), 3, child->index().parent()).data().toInt() == -1)
            {
                if(!child->hasChildren())
                    return;
                child = child->child(0);
            }
            int child_index = this->filesModel->index(child->index().row(), 3, child->index().parent()).data().toInt();
            for(int i = 0; i < this->files[torrent_id].count(); ++i)
            {
                if(this->files[torrent_id][i]["index"].toInt() == child_index)
                {
                    QString child_path = this->files[torrent_id][i]["path"].toString();
                    QList<QString> split_path = child_path.split(QDir::separator());
                    old_folder = split_path[0];
                    for(int j = 1; j <= deep; ++j)
                        old_folder += QDir::separator() + split_path[j];
                    old_folder += QDir::separator();
                    break;
                }
            }
            if(file_path != old_folder)
            {
                QMap<int, QString> map = (*this->options[torrent_id])["mapped_files"].value<QMap<int, QString> >();
                for(int i = 0; i < this->files[torrent_id].count(); ++i)
                {
                    QString path = this->files[torrent_id][i]["path"].toString();
                    if(path.startsWith(old_folder))
                    {
                        path = path.replace(old_folder, file_path);
                        map[this->files[torrent_id][i]["index"].toInt()] = path;
                        this->files[torrent_id][i]["path"] = QVariant(path);
                    }
                }
                (*this->options[torrent_id])["mapped_files"] = QVariant::fromValue<QMap<int, QString> >(map);
            }
        }
    }
}

void AddTorrentDialog::_on_button_url_clicked()
{
    QString url = this->addFromURLDialog->show_add_from_url_dialog();
    if(!url.isEmpty())
    {
        if(is_url(url))
            this->add_from_url(url);
        else if(is_magnet(url))
        //if(is_magnet(url))
        {
            QList<QString> magnets;
            magnets << url;
            this->add_from_magnets(magnets);
        }
    }
}

void AddTorrentDialog::_on_button_hash_clicked()
{

}

void AddTorrentDialog::_on_button_remove_clicked()
{
    QModelIndexList selected_rows = this->torrentsListView->selectionModel()->selectedIndexes();
    if(selected_rows.isEmpty())
        return;
    QModelIndex selected_row = selected_rows[0];
    int removing_row = selected_row.row();

    QString torrent_id = this->torrentsModel->data(this->torrentsModel->index(removing_row, 0)).toString();

    this->torrentsModel->removeRow(removing_row);
    this->files.remove(torrent_id);
    this->infos.remove(torrent_id);

    if(this->torrentsModel->rowCount() <= 1 && !this->streamingCheck->isEnabled())
        this->streamingCheck->setEnabled(true);
    if(this->torrentsModel->rowCount() == 0)
    {
        this->set_default_options();
        this->filesModel->clear();
    }
}

void AddTorrentDialog::_on_button_trackers_clicked()
{

}

void AddTorrentDialog::_on_button_cancel_clicked()
{
    this->hide_add_torrent_dialog();
}

void AddTorrentDialog::_on_button_add_clicked()
{
    // Save the options for selected torrent prior to adding
    QModelIndexList selected_list = this->torrentsListView->selectionModel()->selectedIndexes();
    if(!selected_list.isEmpty())
        this->save_torrent_options(selected_list[0].row());

    QList<QString*> torrent_filenames;
    QList<QString*> torrent_magnets;
    QList<TorrentOptions*> torrent_magnet_options;
    QList<TorrentOptions*> torrent_options;
    //torrent_magnet_file_priorities = []

    for(int row = 0; row < this->torrentsModel->rowCount(); ++row)
    {
        QString torrent_id = this->torrentsModel->data(this->torrentsModel->index(row, 0)).toString();
        QString* filename = new QString(this->torrentsModel->data(this->torrentsModel->index(row, 2)).toString());
        TorrentOptions* cur_options;
        if(this->options.keys().contains(torrent_id))
            cur_options = this->options[torrent_id];
        else
            cur_options = NULL;

        QVector<int> file_priorities = this->get_file_priorities(torrent_id);
        if(cur_options)
            (*cur_options)["file_priorities"] = QVariant::fromValue<QVector<int> >(file_priorities);

        // if deluge.common.is_magnet(filename):
        //     torrent_magnets.append(filename)
        //     torrent_magnet_file_priorities.append(options["file_priorities"])
        //     del options["file_priorities"]
        //     torrent_magnet_options.append(options)
        // else:
        if(is_magnet(*filename))
        {
            torrent_magnets.append(filename);
            if(cur_options)
                cur_options->get_qmap().remove("file_priorities");
            torrent_magnet_options.append(cur_options);
        }
        else
        {
            torrent_filenames.append(filename);
            torrent_options.append(cur_options);
        }

    }

    for(int i = 0; i < torrent_filenames.length(); ++i)
    {
        libtorrent::torrent_info *torrent_info = NULL;
        try
        {
            torrent_info = this->components->torrentManager->get_torrent_info_from_file(*(torrent_filenames[i]));
        }
        catch(...){}
        if(!torrent_info)
            return;

        QVector<char> *filedump = NULL;
        QFile f(*(torrent_filenames[i]));
        if(f.open(QIODevice::ReadOnly))
        {
            filedump = new QVector<char>();
            const qint64 content_size = f.bytesAvailable();
            filedump->resize(content_size);
            f.read(filedump->data(), f.size());
            f.close();
        }

        //this->components->torrentView->add_new_torrent(
        //            this->components->torrentManager->add(torrent_info, NULL, torrent_options[i], true,
        //                                                  filedump, torrent_filenames[i], NULL, NULL));
        this->components->torrentManager->add(torrent_info, NULL, torrent_options[i],
                                              true, filedump, torrent_filenames[i], NULL, NULL);
    }
    for(int i = 0; i < torrent_magnets.length(); ++i)
    {
        //this->components->torrentView->add_new_torrent(
        //            this->components->torrentManager->add(NULL, NULL, torrent_magnet_options[i], true,
        //                                                  NULL, NULL, torrent_magnets[i], NULL));
        this->components->torrentManager->add(NULL, NULL, torrent_magnet_options[i],
                                              true, NULL, NULL, torrent_magnets[i], NULL);
    }
    //if torrent_magnets:
    //    for i, m in enumerate(torrent_magnets):
    //        client.core.add_torrent_magnet(m, torrent_magnet_options[i], self.feed_id, torrent_magnet_file_priorities[i])

    this->hide_add_torrent_dialog();
}

void AddTorrentDialog::_on_button_apply_clicked()
{

}

void AddTorrentDialog::_on_button_revert_clicked()
{
    this->set_default_options();
}

void AddTorrentDialog::_on_button_select_all_clicked()
{

}

void AddTorrentDialog::_on_button_unselect_all_clicked()
{

}

void AddTorrentDialog::_on_button_browse_location_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder");

    if(!folder.isEmpty())
        this->downloadLocationText->setText(folder);
}

void AddTorrentDialog::_on_chk_streaming_toggled(bool checked)
{
    if(checked)
    {
        this->fullRButton->setChecked(true);
        this->compactRButton->setEnabled(false);
    }
    else
        this->compactRButton->setEnabled(true);
}
