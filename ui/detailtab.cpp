#include "detailtab.h"
#include "common.h"
#include "components.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QList>
#include <QString>

DetailTab::DetailTab(Components *comp, QWidget *parent) :
    QWidget(parent)
{
    this->components = comp;

    QVBoxLayout *vBox = new QVBoxLayout(this);
    this->setLayout(vBox);

    QLabel *pathLabel = new QLabel("<b>Path:</b>");

    path = new QLabel("");

    QHBoxLayout *pathLayout = new QHBoxLayout();
    pathLayout->addWidget(pathLabel);
    pathLayout->addWidget(path);

    QLabel *totalSizeLabel = new QLabel("<b>Total Size:</b>");

    totalSize = new QLabel("");

    QHBoxLayout *totalSizeLayout = new QHBoxLayout();
    totalSizeLayout->addWidget(totalSizeLabel);
    totalSizeLayout->addWidget(totalSize);

    QLabel *hashLabel = new QLabel("<b>Hash:</b>");

    hash = new QLabel("");

    QHBoxLayout *hashLayout = new QHBoxLayout();
    hashLayout->addWidget(hashLabel);
    hashLayout->addWidget(hash);

    QLabel *commentsLabel = new QLabel("<b>Comments:</b>");

    comments = new QLabel("");

    QHBoxLayout *commentsLayout = new QHBoxLayout();
    commentsLayout->addWidget(commentsLabel);
    commentsLayout->addWidget(comments);

    QLabel *nameLabel = new QLabel("<b>Name:</b>");

    name = new QLabel("");

    QHBoxLayout *nameLayout = new QHBoxLayout();
    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(name);

    QLabel *numFilesLabel = new QLabel("<b># of files:</b>");

    numFiles = new QLabel("");

    QHBoxLayout *numFilesLayout = new QHBoxLayout();
    numFilesLayout->addWidget(numFilesLabel);
    numFilesLayout->addWidget(numFiles);

    QLabel *statusLabel = new QLabel("<b>Status:</b>");

    status = new QLabel("");

    QHBoxLayout *statusLayout = new QHBoxLayout();
    statusLayout->addWidget(statusLabel);
    statusLayout->addWidget(status);

    QLabel *trackerLabel = new QLabel("<b>Tracker:</b>");

    tracker = new QLabel("");

    QHBoxLayout *trackerLayout = new QHBoxLayout();
    trackerLayout->addWidget(trackerLabel);
    trackerLayout->addWidget(tracker);

    vBox->addLayout(pathLayout);
    vBox->addLayout(totalSizeLayout);
    vBox->addLayout(hashLayout);
    vBox->addLayout(commentsLayout);
    vBox->addLayout(nameLayout);
    vBox->addLayout(numFilesLayout);
    vBox->addLayout(statusLayout);
    vBox->addLayout(trackerLayout);
}

void DetailTab::update_tab()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
    {
        this->clear_tab();
        return;
    }

    QString selected = selected_torrents[0];

    QList<QString> status_keys;
    status_keys << "name" << "total_size" << "num_files" << "tracker";
    status_keys << "save_path" << "message" << "hash" << "comment";

    QMap<QString, QVariant> torrent_status = (*(this->components->torrentManager))[selected]->get_status(status_keys);

    name->setText(torrent_status["name"].toString());
    totalSize->setText(fsize(torrent_status["total_size"].toInt()));
    numFiles->setText(torrent_status["num_files"].toString());
    tracker->setText(torrent_status["tracker"].toString());
    path->setText(torrent_status["save_path"].toString());
    status->setText(torrent_status["message"].toString());
    hash->setText(torrent_status["hash"].toString());
    comments->setText(torrent_status["comment"].toString());
}

void DetailTab::clear_tab()
{
    path->setText("");
    totalSize->setText("");
    hash->setText("");
    comments->setText("");
    name->setText("");
    numFiles->setText("");
    status->setText("");
    tracker->setText("");
}

