#ifndef FILTERTREEVIEW_H
#define FILTERTREEVIEW_H

#include <QTreeView>
#include <QStandardItemModel>
#include <QItemDelegate>
#include <QMenu>
#include <QDialog>
#include <QSignalMapper>
#include <QLineEdit>
#include <QCloseEvent>

#include <QDebug>

class Components;

class mapObject : public QObject
{
    Q_OBJECT
public:
    QLineEdit *lineEdit;
    QString oldText;
    QModelIndex index;
    mapObject(QLineEdit *lEdit, QString oldt, QModelIndex ind, QObject* parent = 0)
        : QObject(parent), lineEdit(lEdit), oldText(oldt), index(ind) {}
};

class filterTreeViewItemDelegate : public QItemDelegate
{
    Q_OBJECT

public:
    QSignalMapper *mapper;
    filterTreeViewItemDelegate(QObject* parent = 0)
        : QItemDelegate(parent)
    {
        mapper = new QSignalMapper();
    }
    ~filterTreeViewItemDelegate()
    {
        delete mapper;
    }

    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QWidget *editor = QItemDelegate::createEditor(parent, option, index);
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(editor);
        if (lineEdit)
        {
            mapObject *mapObj = new mapObject(lineEdit, index.data().toString(), index);
            connect(lineEdit, SIGNAL(editingFinished()), mapper, SLOT(map()));
            mapper->setMapping(lineEdit, mapObj);
            connect(mapper, SIGNAL(mapped(QObject*)), this, SLOT(finishedEnteringNewTest(QObject*)));
        }
        return editor;
    }

private slots:
    void finishedEnteringNewTest(QObject *mapO)
    {
        mapObject *mapObj = dynamic_cast<mapObject*>(mapO);
        if(mapObj)
        {
            QLineEdit *lineEdit = mapObj->lineEdit;
            if (lineEdit)
            {
                disconnect(mapper, SIGNAL(mapped(QObject*)), this, SLOT(finishedEnteringNewTest(QObject*)));
                mapper->removeMappings(lineEdit);
                disconnect(lineEdit, SIGNAL(editingFinished()), mapper, SLOT(map()));
                QString oldText = mapObj->oldText;
                QModelIndex index = mapObj->index;
                emit this->editingFinished(lineEdit->text().simplified(), oldText, index);
            }
            delete mapObj;
        }
    }

signals:
    void editingFinished(QString newText, QString oldText, QModelIndex index);
};

class AddRSSFeedDialog : public QDialog
{
    Q_OBJECT
private:
    QLineEdit *feedURLEdit;
    QLineEdit *aliasEdit;

public:
    AddRSSFeedDialog(QWidget *parent);

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void aliasCheckToggled(bool state);
    void okClicked();
    void cancelClicked();

signals:
    void rssFeedAdded(QString url, QString alias);
};

class FilterTreeView : public QTreeView
{
    Q_OBJECT
private:
    Components *components;
    QStandardItemModel *model;
    QModelIndex torrentsIndex;
    QModelIndex rssIndex;
    QModelIndex playlistsIndex;
    QModelIndex lastFilter;
    QMenu *torrents_context_menu;
    QMenu *rss_context_menu;
    QMenu *playlists_context_menu;
    QTimer *_update_count;
    QMap<QString, int> status_count;
    QMap<QString, int> status_row;
    QMap<QModelIndex, QString> playlistsWithWrongName;
    int current_main_page;

public:
    FilterTreeView(Components *comp, QWidget *parent = 0);
    void set_default_filter();
    void set_last_filter();
    void show_zero_hits(bool show);
    void show_browser();
    void add_playlist(QString playlist);

signals:

private slots:
    void count_status_fields();
    void ShowContextMenu(const QPoint& pos);
    void on_current_row_changed(QModelIndex selected, QModelIndex deselected);
    void on_index_clicked(QModelIndex index);
    void selectAllActivated();
    void pauseAllActivated();
    void resumeAllActivated();
    void removeRSSActivated();
    void updateRSSActivated();
    void playPlaylistActivated();
    void removePlaylistActivated();
    void addFileToPlaylistActivated();
    void addFolderToPlaylistActivated();
    void rssFeedAddedToManager(QString alias);
    void editingFinished(QString newText, QString oldText, QModelIndex index);
    void filterItemChanged(QStandardItem *item);
};

#endif // FILTERTREEVIEW_H
