#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QDialog>
#include <QFileDialog>
#include <QSplitter>
#include <QStackedWidget>

#include "ui/statusbar.h"

#include "libtorrent/session.hpp"

class Components;

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Components *components;

public:
    StatusBar *statusbar;
    QStackedWidget *notebook;

    MainWindow(Components *comp, QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void about();
    void updateTorrent();

};

#endif // MAINWINDOW_H
