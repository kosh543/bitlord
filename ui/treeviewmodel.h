#ifndef TREEVIEWMODEL_H
#define TREEVIEWMODEL_H

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QVariant>

#define TORRENT_ID_COLUMN 0
#define STATE_COLUMN 1
#define FILTER_COLUMN 2
#define NAME_COLUMN 3
#define PROGRESS_COLUMN 4
#define NUMBER_COLUMN 5
#define SIZE_COLUMN 6
#define SEEDERS_COLUMN 7
#define PEERS_COLUMN 8
#define DOWN_SPEED_COLUMN 9
#define UP_SPEED_COLUMN 10
#define ETA_COLUMN 11
#define RATIO_COLUMN 12
#define AVAIL_COLUMN 13
#define ADDED_COLUMN 14
#define TRACKER_COLUMN 15

static QString column_name_from_number(int number)
{
    switch (number)
    {
    case TORRENT_ID_COLUMN:
        return "torrent_id";
    case STATE_COLUMN:
        return "state";
    case FILTER_COLUMN:
        return "filter";
    case NAME_COLUMN:
        return "Name";
    case PROGRESS_COLUMN:
        return "Progress";
    case NUMBER_COLUMN:
        return "#";
    case SIZE_COLUMN:
        return "Size";
    case SEEDERS_COLUMN:
        return "Seeders";
    case PEERS_COLUMN:
        return "Peers";
    case DOWN_SPEED_COLUMN:
        return "Down Speed";
    case UP_SPEED_COLUMN:
        return "Up Speed";
    case ETA_COLUMN:
        return "ETA";
    case RATIO_COLUMN:
        return "Ratio";
    case AVAIL_COLUMN:
        return "Avail";
    case ADDED_COLUMN:
        return "Added";
    case TRACKER_COLUMN:
        return "Tracker";
    default:
        return QString::null;
    }
}

static int column_number_from_name(QString name)
{
    if(name == "torrent_id")
        return TORRENT_ID_COLUMN;
    if(name == "state")
        return STATE_COLUMN;
    if(name == "filter")
        return FILTER_COLUMN;
    if(name == "Name")
        return NAME_COLUMN;
    if(name == "Progress")
        return PROGRESS_COLUMN;
    if(name == "#")
        return NUMBER_COLUMN;
    if(name == "Size")
        return SIZE_COLUMN;
    if(name == "Seeders")
        return SEEDERS_COLUMN;
    if(name == "Peers")
        return PEERS_COLUMN;
    if(name == "Down Speed")
        return DOWN_SPEED_COLUMN;
    if(name == "Up Speed")
        return UP_SPEED_COLUMN;
    if(name == "ETA")
        return ETA_COLUMN;
    if(name == "Ratio")
        return RATIO_COLUMN;
    if(name == "Avail")
        return AVAIL_COLUMN;
    if(name == "Added")
        return ADDED_COLUMN;
    if(name == "Tracker")
        return TRACKER_COLUMN;
    return -1;
}

class TreeViewItem;

class TreeViewModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeViewModel(/*const QString &data, */QObject *parent = 0);
    ~TreeViewModel();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    void clear();
    void update_model(QMap<QString, QMap<QString, QVariant> > status, QList<QString> columns_to_update, QList<QString> filter);
    int addColumn(QString text);
    void appendRow(QList<QVariant> &data);
    void appendColumn(QString text);
    bool containTorrentId(QString torrent_id);
    void startInsertRows(int first, int last);
    void finishInsertRows();
    void startInsertColumns(int first, int last);
    void finishInsertColumns();

    TreeViewItem *rootItem;
    QModelIndex rootIndex;
    QMap<QString, QIcon> icons;

};

/*class TreeViewSortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    TreeViewSortFilterModel(QObject *parent = 0);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
};*/

#endif // TREEVIEWMODEL_H
