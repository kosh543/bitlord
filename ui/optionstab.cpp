#include "optionstab.h"
#include "components.h"
#include "ui/edittrackersdialog.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QFileSystemModel>

OptionsTab::OptionsTab(Components *comp, QWidget *parent) :
    QWidget(parent)
{
    this->components = comp;

    QHBoxLayout *hBox = new QHBoxLayout(this);
    this->setLayout(hBox);

    maxDownSpeedSpin = new QSpinBox();
    maxDownSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxDownSpeedLayout = new QHBoxLayout();
    maxDownSpeedLayout->addWidget(new QLabel("Max Download Speed:"));
    maxDownSpeedLayout->addWidget(maxDownSpeedSpin);
    maxDownSpeedLayout->addWidget(new QLabel("KiB/s"));

    maxUpSpeedSpin = new QSpinBox();
    maxUpSpeedSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUpSpeedLayout = new QHBoxLayout();
    maxUpSpeedLayout->addWidget(new QLabel("Max Upload Speed:"));
    maxUpSpeedLayout->addWidget(maxUpSpeedSpin);
    maxUpSpeedLayout->addWidget(new QLabel("KiB/s"));

    maxConnectionsSpin = new QSpinBox();
    maxConnectionsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxConnectionsLayout = new QHBoxLayout();
    maxConnectionsLayout->addWidget(new QLabel("Max Connections:"));
    maxConnectionsLayout->addWidget(maxConnectionsSpin);
    maxConnectionsLayout->addWidget(new QLabel(""));

    maxUploadSlotsSpin = new QSpinBox();
    maxUploadSlotsSpin->setRange(-1, INT_MAX);

    QHBoxLayout *maxUploadSlotsLayout = new QHBoxLayout();
    maxUploadSlotsLayout->addWidget(new QLabel("Max Upload Slots:"));
    maxUploadSlotsLayout->addWidget(maxUploadSlotsSpin);
    maxUploadSlotsLayout->addWidget(new QLabel(""));

    QVBoxLayout *bandwidthLayout = new QVBoxLayout();
    bandwidthLayout->addLayout(maxDownSpeedLayout);
    bandwidthLayout->addLayout(maxUpSpeedLayout);
    bandwidthLayout->addLayout(maxConnectionsLayout);
    bandwidthLayout->addLayout(maxUploadSlotsLayout);

    QGroupBox *bandwidthFrame = new QGroupBox();
    bandwidthFrame->setTitle("Bandwidth");
    bandwidthFrame->setLayout(bandwidthLayout);
    bandwidthFrame->setFixedHeight(145);
    bandwidthFrame->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    autoManagedCheck = new QCheckBox("Auto Managed");

    shutdownCheck = new QCheckBox("Shut down PC when torrent finished");
    shutdownCheck->setEnabled(false);

    stopSeedCheck = new QCheckBox("Stop seed at ratio:");
    this->connect(stopSeedCheck, SIGNAL(toggled(bool)), this, SLOT(_on_stop_seed_toggled(bool)));

    stopSeedSpin = new QDoubleSpinBox();
    stopSeedSpin->setSingleStep(0.1);

    QHBoxLayout *stopSeedLayout = new QHBoxLayout();
    stopSeedLayout->addWidget(stopSeedCheck);
    stopSeedLayout->addWidget(stopSeedSpin);

    removeRatioCheck = new QCheckBox("Remove at ratio");

    moveCompletedCheck = new QCheckBox("Move completed:");
    this->connect(moveCompletedCheck, SIGNAL(toggled(bool)), this, SLOT(_on_move_completed_toggled(bool)));

    moveCompletedButton = new QPushButton("...");
    this->connect(moveCompletedButton, SIGNAL(clicked()), this, SLOT(_on_button_move_completed_clicked()));

    QHBoxLayout *moveCompletedLayout = new QHBoxLayout();
    moveCompletedLayout->addWidget(moveCompletedCheck);
    moveCompletedLayout->addWidget(moveCompletedButton);

    QVBoxLayout *queueLayout = new QVBoxLayout();
    queueLayout->addWidget(autoManagedCheck);
    queueLayout->addWidget(shutdownCheck);
    queueLayout->addLayout(stopSeedLayout);
    queueLayout->addWidget(removeRatioCheck);
    queueLayout->addLayout(moveCompletedLayout);

    QGroupBox *queueFrame = new QGroupBox();
    queueFrame->setTitle("Queue");
    queueFrame->setLayout(queueLayout);
    queueFrame->setFixedSize(215, 145);
    queueFrame->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    privateCheck = new QCheckBox("Private");
    privateCheck->setEnabled(false);

    priorFLCheck = new QCheckBox("Prioritise First/Last");

    QPushButton *editTrackersButton = new QPushButton("Edit Trackers");
    this->connect(editTrackersButton, SIGNAL(clicked()), this, SLOT(_on_button_edit_trackers_clicked()));

    QPushButton *applyButton = new QPushButton("Apply");
    this->connect(applyButton, SIGNAL(clicked()), this, SLOT(_on_button_apply_clicked()));

    QVBoxLayout *generalLayout = new QVBoxLayout();
    generalLayout->addWidget(privateCheck);
    generalLayout->addWidget(priorFLCheck);
    generalLayout->addWidget(editTrackersButton);
    generalLayout->addWidget(applyButton);

    QGroupBox *generalFrame = new QGroupBox();
    generalFrame->setTitle("General");
    generalFrame->setLayout(generalLayout);
    generalFrame->setFixedSize(135, 145);
    generalFrame->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    hBox->addWidget(bandwidthFrame, 0, Qt::AlignTop);
    hBox->addWidget(queueFrame, 0, Qt::AlignTop);
    hBox->addWidget(generalFrame, 0, Qt::AlignTop);
}

void OptionsTab::clear_tab()
{
    this->prev_torrent_id.clear();
    this->prev_status.clear();
}

void OptionsTab::update_tab()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();

    if(selected_torrents.isEmpty())
    {
        this->clear_tab();
        this->setDisabled(true);
        return;
    }
    else
        this->setEnabled(true);

    QString torrent_id = selected_torrents[0];

    if(!this->components->torrentManager->torrents.contains(torrent_id))
    {
        this->setEnabled(false);
        return;
    }

    if(torrent_id != this->prev_torrent_id)
        this->prev_status.clear();

    QList<QString> status_keys;
    status_keys << "max_download_speed" << "max_upload_speed" << "max_connections" <<
                   "max_upload_slots" << "stop_ratio"  << "stop_at_ratio"  << "private" <<
                   "is_auto_managed" << "shutdown_pc"  << "prioritize_first_last" <<
                   "remove_at_ratio" << "move_on_completed"  << "move_on_completed_path";
    QMap<QString, QVariant> status = this->components->torrentManager->torrents[torrent_id]->get_status(status_keys);

    bool not_prev = false;
    if(this->prev_status.isEmpty())
        not_prev = true;

    if(status != this->prev_status)
    {
        if(not_prev || status["max_download_speed"].toInt() != this->prev_status["max_download_speed"].toInt())
            this->maxDownSpeedSpin->setValue(status["max_download_speed"].toInt());
        if(not_prev || status["max_upload_speed"].toInt() != this->prev_status["max_upload_speed"].toInt())
            this->maxUpSpeedSpin->setValue(status["max_upload_speed"].toInt());
        if(not_prev || status["max_connections"].toInt() != this->prev_status["max_connections"].toInt())
            this->maxConnectionsSpin->setValue(status["max_connections"].toInt());
        if(not_prev || status["max_upload_slots"].toInt() != this->prev_status["max_upload_slots"].toInt())
            this->maxUploadSlotsSpin->setValue(status["max_upload_slots"].toInt());
        if(not_prev || status["private"].toBool() != this->prev_status["private"].toBool())
            this->privateCheck->setChecked(status["private"].toBool());
        if(not_prev || status["prioritize_first_last"].toBool() != this->prev_status["prioritize_first_last"].toBool())
            this->priorFLCheck->setChecked(status["prioritize_first_last"].toBool());
        if(not_prev || status["is_auto_managed"].toBool() != this->prev_status["is_auto_managed"].toBool())
            this->autoManagedCheck->setChecked(status["is_auto_managed"].toBool());
        if(not_prev || status["shutdown_pc"].toBool() != this->prev_status["shutdown_pc"].toBool())
            this->shutdownCheck->setChecked(status["shutdown_pc"].toBool());
        if(not_prev || status["stop_at_ratio"].toBool() != this->prev_status["stop_at_ratio"].toBool())
        {
            this->stopSeedCheck->setChecked(status["stop_at_ratio"].toBool());
            if(status["stop_at_ratio"].toBool())
            {
                this->stopSeedSpin->setEnabled(true);
                this->removeRatioCheck->setEnabled(true);
            }
            else
            {
                this->stopSeedSpin->setDisabled(true);
                this->removeRatioCheck->setDisabled(true);
            }
        }
        if(not_prev || status["stop_ratio"].toDouble() != this->prev_status["stop_ratio"].toDouble())
            this->stopSeedSpin->setValue(status["stop_ratio"].toDouble());
        if(not_prev || status["remove_at_ratio"].toBool() != this->prev_status["remove_at_ratio"].toBool())
            this->removeRatioCheck->setChecked(status["remove_at_ratio"].toBool());
        if(not_prev || status["move_on_completed"].toBool() != this->prev_status["move_on_completed"].toBool())
            this->moveCompletedCheck->setChecked(status["move_on_completed"].toBool());
        if(not_prev || status["move_on_completed_path"].toString() != this->prev_status["move_on_completed_path"].toString())
        {
            this->moveCompletedPath = status["move_on_completed_path"].toString();
            /*QList<QString> split_path = this->moveCompletedPath.split("/");
            if(split_path.count() == 1)
                split_path = this->moveCompletedPath.split("\\");
            QString button_text = split_path.last();
            if(button_text.isEmpty())
                button_text = "...";
            this->moveCompletedButton->setText(button_text);*/
            if(this->moveCompletedCheck->isChecked())
                this->moveCompletedButton->setEnabled(true);
            else
                this->moveCompletedButton->setDisabled(true);
        }

        this->prev_status = status;
    }

    this->prev_torrent_id = torrent_id;
}

void OptionsTab::_on_button_edit_trackers_clicked()
{
    QList<QString> selected = this->components->torrentView->get_selected_torrents();
    if(!selected.isEmpty())
        EditTrackersDialog(this->components, selected[0]).show_edit_trackers_dialog();
}

void OptionsTab::_on_button_apply_clicked()
{
    if(this->maxDownSpeedSpin->value() != this->prev_status["max_download_speed"].toInt())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_max_download_speed(this->maxDownSpeedSpin->value());
    if(this->maxUpSpeedSpin->value() != this->prev_status["max_upload_speed"].toInt())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_max_upload_speed(this->maxUpSpeedSpin->value());
    if(this->maxConnectionsSpin->value() != this->prev_status["max_connections"].toInt())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_max_connections(this->maxConnectionsSpin->value());
    if(this->maxUploadSlotsSpin->value() != this->prev_status["max_upload_slots"].toInt())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_max_upload_slots(this->maxUploadSlotsSpin->value());
    if(this->priorFLCheck->isChecked() != this->prev_status["prioritize_first_last"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_prioritize_first_last(this->priorFLCheck->isChecked());
    if(this->autoManagedCheck->isChecked() != this->prev_status["is_auto_managed"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_auto_managed(this->autoManagedCheck->isChecked());
    if(this->shutdownCheck->isChecked() != this->prev_status["shutdown_pc"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_shutdown_pc(this->shutdownCheck->isChecked());
    if(this->stopSeedCheck->isChecked() != this->prev_status["stop_at_ratio"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_stop_at_ratio(this->stopSeedCheck->isChecked());
    if(this->stopSeedSpin->value() != this->prev_status["stop_ratio"].toDouble())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_stop_ratio(this->stopSeedSpin->value());
    if(this->removeRatioCheck->isChecked() != this->prev_status["remove_at_ratio"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_remove_at_ratio(this->removeRatioCheck->isChecked());
    if(this->moveCompletedCheck->isChecked() != this->prev_status["move_on_completed"].toBool())
        this->components->torrentManager->torrents[this->prev_torrent_id]->set_move_completed(this->moveCompletedCheck->isChecked());
    if(this->moveCompletedCheck->isChecked())
    {
        if(!this->moveCompletedPath.isEmpty())
            this->components->torrentManager->torrents[this->prev_torrent_id]->set_move_completed_path(this->moveCompletedPath);
    }
}

void OptionsTab::_on_stop_seed_toggled(bool checked)
{
    if(checked)
    {
        this->stopSeedSpin->setEnabled(true);
        this->removeRatioCheck->setEnabled(true);
    }
    else
    {
        this->stopSeedSpin->setDisabled(true);
        this->removeRatioCheck->setDisabled(true);
    }
}

void OptionsTab::_on_button_move_completed_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, "Select A Folder", this->moveCompletedPath);
    QList<QString> split_path = folder.split("/");
    if(split_path.count() == 1)
        split_path = folder.split("\\");
    QString button_text = split_path.last();
    if(!button_text.isEmpty())
    {
        this->moveCompletedButton->setText(button_text);
        this->moveCompletedPath = folder;
    }
}

void OptionsTab::_on_move_completed_toggled(bool checked)
{
    if(checked)
        this->moveCompletedButton->setEnabled(true);
    else
        this->moveCompletedButton->setDisabled(true);
}
