#include "addressentry.h"
#include "core/cpickleparser.h"
#include "common.h"
#include "components.h"

#include <QDebug>

BrowserHistory::BrowserHistory()
{
    this->load_state();
}

void BrowserHistory::load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("browserhistory.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        in >> this->titles;
        fnew.close();
        return;
    }
    QFile f(QDir(get_default_config_dir()).absoluteFilePath("omniboxtitles.state"));
    if(f.open(QIODevice::ReadOnly))
    {
        QString s = QString(f.readAll());
        f.close();
        PickleLoader pl(s);
        QVariant res;
        try
        {
            pl.loads(res);
            foreach(QString url, res.toMap().keys())
            {
                QString value = res.toMap()[url].toString();
                this->titles[url] = value;
                QUrl tmp(url);
                if(!tmp.scheme().isEmpty())
                {
                    url = url.remove(tmp.scheme()+"://");
                    this->titles[url] = value;
                }
                if(url.startsWith("www."))
                    this->titles[url.remove("www.")] = value;
            }
        }
        catch(...){}
    }
}

void BrowserHistory::save_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("browserhistory.state"));
    if(fnew.open(QIODevice::WriteOnly))
    {
        QDataStream out(&fnew);
        out << this->titles;
        fnew.flush();
        fnew.close();
    }
}

void BrowserHistory::add_url(QString url, QString title)
{
    if(!this->titles.contains(url))
        this->titles[url] = title;
    QUrl tmp(url);
    if(!tmp.scheme().isEmpty())
    {
        url = url.remove(tmp.scheme()+"://");
        if(!this->titles.contains(url))
            this->titles[url] = title;
    }
    if(url.startsWith("www."))
    {
        url = url.remove("www.");
        if(!this->titles.contains(url))
            this->titles[url] = title;
    }
}

AddressEntry::AddressEntry(Components *comp, QWidget *parent) :
    QLineEdit(parent)
{
    this->components = comp;

    QCompleter *historyCompleter = new QCompleter(this);
    QStringListModel *model = new QStringListModel(this->components->browserHistory->titles.keys(), this);
    historyCompleter->setModel(model);
    historyCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    this->setCompleter(historyCompleter);
}
