#ifndef BLBROWSER_H
#define BLBROWSER_H

#include <QWidget>
#include <QStackedWidget>
#include <QWebView>
#include <QLineEdit>
#include <QList>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkDiskCache>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QNetworkCookieJar>

#include "webview.h"

class Components;

class BLNetworkCookieJar : public QNetworkCookieJar
{
    Q_OBJECT
public:
    explicit BLNetworkCookieJar(QObject *parent) : QNetworkCookieJar(parent){}
    QList<QNetworkCookie> BLGetAllCookies()
    {
        return this->allCookies();
    }
    void BLSetAllCookies(QList<QNetworkCookie> cookieList)
    {
        this->setAllCookies(cookieList);
    }
};

class BLBrowser : public QStackedWidget
{
    Q_OBJECT
private:
    Components *components;
    QList<WebView*> webViewList;
    QList<WebView*> loadingWebViewList;
    QList<QLineEdit*> addressEditList;
    QList<QIcon> faviconsList;
    QNetworkAccessManager* networkAccessManager;
    QIcon defaultFavicon;

public:
    explicit BLBrowser(Components *comp, QWidget *parent = 0);
    WebView *add_tab(QString url, QString title, bool first_tab=false);
    void remove_page(int index);
    bool check_url(QString url);
    void navigate(QString str_url);
    void load_cookies(BLNetworkCookieJar *cookieJar);
    void save_cookies();
    void stop_all();

signals:

public slots:
    void address_returns_pressed();
    void pageTitleChanged(QString title);
    void pageURLChanged(QUrl url);
    void pageIconChanged();
    void pageLoadStarted();
    void pageLoadFinished(bool ok);
    void getDownloadRequest(QNetworkRequest request);
    void getUnsupportedContent(QNetworkReply *reply);
    void handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors);
    void backNavigateButtonClicked();
    void forwardNavigateButtonClicked();
    void reloadNavigateButtonClicked();
    void stopNavigateButtonClicked();
    void homeNavigateButtonClicked();

};

#endif // BLBROWSER_H
