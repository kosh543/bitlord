#include "browsertabs.h"
#include "components.h"

BrowserTabs::BrowserTabs(Components *comp, QWidget *parent) :
    QTabBar(parent)
{
    this->components = comp;

    this->last_browser_page = 1;

    this->setStyleSheet(
                "QTabBar::tab { background-color:rgb(14, 183, 207); }"
                "QTabBar::tab:selected { background-color:rgb(181, 246, 255); }"
                "QTabBar::tab { max-width: 200px; min-width: 100px; }" //padding: -5px; margin-left: 5; }"
                "QTabBar::tab:first { max-width: 18px; min-width: 10px; padding: 2px; margin: 0px; }");

    this->setTabsClosable(true);
    this->setDrawBase(false);
    this->setElideMode(Qt::ElideRight);
    this->setExpanding(false);

    this->plusButton = new ClickableLabel(this);
    this->plusButton->setPixmap(QPixmap(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/plus_button.png")));

    this->connect(this->plusButton, SIGNAL(clicked()), this, SLOT(plusButtonClicked()));
    this->connect(this, SIGNAL(currentChanged(int)), this, SLOT(pageChanged(int)));
    this->connect(this, SIGNAL(tabCloseRequested(int)), this, SLOT(pageCloseRequested(int)));

    this->addTab(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/all16.png")), QString());
    this->setTabButton(0, QTabBar::RightSide, 0);
    this->setTabButton(0, QTabBar::LeftSide, 0);
}

void BrowserTabs::add_tab(QString title, QIcon icon, bool focus)
{
    this->addTab(icon, title);
    if(focus)
        this->setCurrentIndex(this->count()-1);
}

void BrowserTabs::set_browser_tab()
{
    if(this->currentIndex() != this->last_browser_page)
        this->setCurrentIndex(this->last_browser_page);
}

void BrowserTabs::set_first_tab()
{
    if(this->currentIndex() != 0)
        this->setCurrentIndex(0);
}

void BrowserTabs::set_first_icon(QIcon icon)
{
    this->setTabIcon(0, icon);
}

void BrowserTabs::pageChanged(int page)
{
    if(page != 0)
    {
        this->last_browser_page = page;
        this->components->browser->setCurrentIndex(page-1);
        this->components->filterTreeView->show_browser();
    }
    else
        this->components->filterTreeView->set_last_filter();
}

void BrowserTabs::plusButtonClicked()
{
    this->components->browser->add_tab("http://boxbub.com", "about:blank");
}

void BrowserTabs::pageCloseRequested(int index)
{
    if(index != 0 && this->count() > 2)
    {
        this->components->browser->remove_page(index-1);
        this->removeTab(index);
    }
}

