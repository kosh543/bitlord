#include "webview.h"
#include "components.h"

WebPage::WebPage(QWidget *parent) :
    QWebPage(parent)
{
}

bool WebPage::acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type)
{
    //qDebug() << request.url();
    //qDebug() << type;
    if(request.url().scheme() == "magnet")
    {
        WebView* webView = (WebView*)this->parent();
        webView->get_magnet(request.url().toString());
        return false;
    }
    return QWebPage::acceptNavigationRequest(frame,request,type);
}

WebView::WebView(Components *comp, QWidget *parent) :
    QWebView(parent)
{
    this->components = comp;

    delete(this->page());
    this->setPage(new WebPage(this));
}

void WebView::get_magnet(QString magnet)
{
    this->components->addTorrentDialog->receive_message(magnet);
}

QWebView* WebView::createWindow(QWebPage::WebWindowType type)
{
    Q_UNUSED(type)
    return this->components->browser->add_tab("", "about:blank");
}
