#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QWebView>
#include <QWebPage>
#include <QNetworkRequest>

#include <QDebug>

class Components;

class WebPage : public QWebPage
{
    Q_OBJECT
public:
    explicit WebPage(QWidget *parent = 0);

protected:
    bool acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type);

signals:

public slots:

};

class WebView : public QWebView
{
    Q_OBJECT
private:
    Components *components;

public:
    explicit WebView(Components *comp, QWidget *parent = 0);
    void get_magnet(QString magnet);

protected:
    QWebView* createWindow(QWebPage::WebWindowType type);

signals:

public slots:

};

#endif // WEBVIEW_H
