#ifndef BROWSERTABS_H
#define BROWSERTABS_H

#include <QTabBar>
#include <QLabel>
#include <QIcon>

class Components;

class ClickableLabel : public QLabel

{
Q_OBJECT

public:
    explicit ClickableLabel(QWidget *parent = 0) : QLabel(parent){}
    //~ClickableLabel();

signals:
    void clicked();

protected:
    void mouseReleaseEvent(QMouseEvent *event){emit clicked();}
};

class BrowserTabs : public QTabBar
{
    Q_OBJECT
private:
    Components *components;
    int last_browser_page;

public:
    ClickableLabel *plusButton;
    explicit BrowserTabs(Components *comp, QWidget *parent = 0);
    void set_first_icon(QIcon icon);
    void set_browser_tab();
    void set_first_tab();
    void add_tab(QString title, QIcon icon, bool focus=true);

private slots:
    void plusButtonClicked();
    void pageChanged(int page);
    void pageCloseRequested(int index);

};

#endif // BROWSERTABS_H
