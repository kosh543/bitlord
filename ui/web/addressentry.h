#ifndef ADDRESSENTRY_H
#define ADDRESSENTRY_H

#include <QLineEdit>
#include <QString>
#include <QSettings>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QMap>
#include <QVariant>
#include <QCompleter>
#include <QAbstractItemView>
#include <QStringListModel>
#include <QKeyEvent>

class Components;

class BrowserHistory
{
public:
    QMap<QString, QString> titles;
    explicit BrowserHistory();
    void load_state();
    void save_state();
    void add_url(QString url, QString title);
};

class AddressEntry : public QLineEdit
{
    Q_OBJECT
private:
    Components *components;

public:
    explicit AddressEntry(Components *comp, QWidget *parent = 0);

};

#endif // ADDRESSENTRY_H
