#include "blbrowser.h"
#include "components.h"
#include "ui/web/addressentry.h"

#include <QLabel>
#include <QMovie>
#include <QVBoxLayout>
#include <QTemporaryFile>
#include <QSslConfiguration>
#include <QSslCertificate>

BLBrowser::BLBrowser(Components *comp, QWidget *parent) :
    QStackedWidget(parent)
{
    this->components = comp;

    this->defaultFavicon = QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/default_favicon.png"));

    /*QSslConfiguration sslCfg = QSslConfiguration::defaultConfiguration();
    QList<QSslCertificate> ca_list = sslCfg.caCertificates();
    QList<QSslCertificate> ca_new = QSslCertificate::fromData("CaCertificates");
    ca_list += ca_new;

    sslCfg.setCaCertificates(ca_list);
    sslCfg.setProtocol(QSsl::AnyProtocol);
    QSslConfiguration::setDefaultConfiguration(sslCfg);*/

    QString cachePath = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0];
    if(!QDir(cachePath).exists())
        QDir().mkdir(cachePath);
    cachePath = QDir(cachePath).absoluteFilePath("BrowserCache");
    if(!QDir(cachePath).exists())
        QDir().mkdir(cachePath);

    QWebSettings *webSettings = QWebSettings::globalSettings();
    webSettings->setAttribute(QWebSettings::PluginsEnabled, true);
    webSettings->enablePersistentStorage(cachePath);

    QNetworkDiskCache *browserCache = new QNetworkDiskCache(this);
    browserCache->setCacheDirectory(cachePath);

    BLNetworkCookieJar *cookieJar = new BLNetworkCookieJar(this);
    this->load_cookies(cookieJar);

    this->networkAccessManager = new QNetworkAccessManager(this);
    this->networkAccessManager->setCache(browserCache);
    this->networkAccessManager->setCookieJar(cookieJar);

    this->add_tab("http://boxbub.com", "about:blank", true);
}

WebView* BLBrowser::add_tab(QString url, QString title, bool first_tab)
{
    QPushButton *back = new QPushButton(this);
    back->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/navback.png")));
    this->connect(back, SIGNAL(clicked()), this, SLOT(backNavigateButtonClicked()));

    QPushButton *forward = new QPushButton(this);
    forward->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/navforward.png")));
    this->connect(forward, SIGNAL(clicked()), this, SLOT(forwardNavigateButtonClicked()));

    QPushButton *reload = new QPushButton(this);
    reload->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/navreload.png")));
    this->connect(reload, SIGNAL(clicked()), this, SLOT(reloadNavigateButtonClicked()));

    QPushButton *stop = new QPushButton(this);
    stop->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/navstop.png")));
    this->connect(stop, SIGNAL(clicked()), this, SLOT(stopNavigateButtonClicked()));

    QPushButton *home = new QPushButton(this);
    home->setIcon(QIcon(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/navhome.png")));
    this->connect(home, SIGNAL(clicked()), this, SLOT(homeNavigateButtonClicked()));

    AddressEntry *addressEdit = new AddressEntry(this->components, this);
    this->addressEditList.append(addressEdit);
    this->connect(addressEdit, SIGNAL(returnPressed()), this, SLOT(address_returns_pressed()));

    QHBoxLayout *addressLayout = new QHBoxLayout();
    addressLayout->addWidget(back);
    addressLayout->addWidget(forward);
    addressLayout->addWidget(reload);
    addressLayout->addWidget(stop);
    addressLayout->addWidget(home);
    addressLayout->addWidget(addressEdit);

    WebView *webView = new WebView(this->components, this);
    this->webViewList.append(webView);
    //webView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
    webView->page()->setForwardUnsupportedContent(true);
    webView->page()->setNetworkAccessManager(this->networkAccessManager);
    //QNetworkAccessManager *manager = webView->page()->networkAccessManager();
    //manager->setCache(this->browserCache);

    this->connect(webView, SIGNAL(titleChanged(QString)), this, SLOT(pageTitleChanged(QString)));
    this->connect(webView, SIGNAL(urlChanged(QUrl)), this, SLOT(pageURLChanged(QUrl)));
    this->connect(webView, SIGNAL(iconChanged()), this, SLOT(pageIconChanged()));
    this->connect(webView, SIGNAL(loadStarted()), this, SLOT(pageLoadStarted()));
    this->connect(webView, SIGNAL(loadFinished(bool)), this, SLOT(pageLoadFinished(bool)));
    //this->connect(webView, SIGNAL(linkClicked(QUrl)), this, SLOT(URLClicked(QUrl)));
    this->connect(webView->page(), SIGNAL(downloadRequested(QNetworkRequest)), this, SLOT(getDownloadRequest(QNetworkRequest)));
    this->connect(webView->page(), SIGNAL(unsupportedContent(QNetworkReply*)), this, SLOT(getUnsupportedContent(QNetworkReply*)));
    this->connect(webView->page()->networkAccessManager(), SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )),
                this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));

    QVBoxLayout *pageLayout = new QVBoxLayout();
    pageLayout->addLayout(addressLayout);
    pageLayout->addWidget(webView);

    QWidget *page = new QWidget(this);
    page->setLayout(pageLayout);

    this->addWidget(page);

    int tabIndex = this->webViewList.indexOf(webView)+1;
    this->components->browserTabs->add_tab(title, this->defaultFavicon, !first_tab);
    //this->components->browserTabs->setTabIcon(tabIndex, this->defaultFavicon);
    this->faviconsList.append(this->defaultFavicon);

    QLabel *animationLabel = static_cast<QLabel*>(this->components->browserTabs->tabButton(tabIndex, QTabBar::LeftSide));
    if(!animationLabel)
        animationLabel = new QLabel();
    if(!animationLabel->movie())
    {
        QMovie *movie = new QMovie(QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("pixmaps/loading.gif"),
                                   QByteArray(), animationLabel);
        animationLabel->setMovie(movie);
    }
    this->components->browserTabs->setTabButton(tabIndex, QTabBar::LeftSide, animationLabel);
    animationLabel->hide();

    webView->load(QUrl(url));

    return webView;
}

void BLBrowser::remove_page(int index)
{
    WebView *view = this->webViewList[index];
    view->stop();
    QWidget *page = this->widget(index);
    this->removeWidget(page);
    this->webViewList.removeAt(index);
    delete view;
    QLineEdit *edit = this->addressEditList[index];
    this->addressEditList.removeAt(index);
    delete edit;
    delete page;
}

void BLBrowser::stop_all()
{
    for(int i = this->webViewList.count()-1; i >= 0; --i)
        this->remove_page(i);
}

bool BLBrowser::check_url(QString url)
{
    QStringList split_url = url.split('.');
    if(split_url.count() <= 1 || split_url.last().isEmpty())
        return false;
    return QUrl::fromUserInput(url).isValid();
}

void BLBrowser::navigate(QString str_url)
{
    int index = this->currentIndex();
    QUrl url;
    if(this->check_url(str_url))
        url = QUrl::fromUserInput(str_url);
    else
        url = QUrl::fromUserInput("http://www.mybub.com/search/"+str_url);
    this->webViewList[index]->load(url);
}

void BLBrowser::address_returns_pressed()
{
    int index = this->currentIndex();
    this->navigate(this->addressEditList[index]->text());
    this->addressEditList[index]->clearFocus();
}

void BLBrowser::backNavigateButtonClicked()
{
    this->webViewList[this->currentIndex()]->back();
}

void BLBrowser::forwardNavigateButtonClicked()
{
    this->webViewList[this->currentIndex()]->forward();
}

void BLBrowser::reloadNavigateButtonClicked()
{
    this->webViewList[this->currentIndex()]->reload();
}

void BLBrowser::stopNavigateButtonClicked()
{
    this->webViewList[this->currentIndex()]->stop();
}

void BLBrowser::homeNavigateButtonClicked()
{
    this->webViewList[this->currentIndex()]->load(QUrl("http://www.boxbub.com"));
}

void BLBrowser::pageLoadStarted()
{
    QObject* sender = QObject::sender();
    if(sender)
    {
        WebView *webView = (WebView*)sender;
        if(!this->loadingWebViewList.contains(webView))
            this->loadingWebViewList.append(webView);
        int tabIndex = this->webViewList.indexOf(webView)+1;
        this->components->browserTabs->setTabIcon(tabIndex, QIcon());
        QLabel *animationLabel = static_cast<QLabel*>(this->components->browserTabs->tabButton(tabIndex, QTabBar::LeftSide));
        // HACK:
        if(animationLabel->y()-8 > 0)
            animationLabel->move(animationLabel->x(), animationLabel->y()-8);
        animationLabel->resize(16, 16);
        animationLabel->movie()->start();
        animationLabel->show();
    }
}

void BLBrowser::pageLoadFinished(bool ok)
{
    Q_UNUSED(ok)
    QObject* sender = QObject::sender();
    if(sender)
    {
        WebView *webView = (WebView*)sender;
        this->loadingWebViewList.removeOne(webView);
        int index = this->webViewList.indexOf(webView);
        QLabel *animationLabel = static_cast<QLabel*>(this->components->browserTabs->tabButton(index+1, QTabBar::LeftSide));
        if(animationLabel)
        {
            if(animationLabel->movie())
                animationLabel->movie()->stop();
            animationLabel->resize(0, 0);
            animationLabel->hide();
        }
        this->components->browserTabs->setTabIcon(index+1, this->faviconsList[index]);
    }
}

void BLBrowser::pageIconChanged()
{
    QObject* sender = QObject::sender();
    if(sender)
    {
        WebView *webView = (WebView*)sender;
        int index = this->webViewList.indexOf(webView);
        if(!webView->icon().isNull())
        {
            this->faviconsList[index] = webView->icon();
            if(!this->loadingWebViewList.contains(webView))
                this->components->browserTabs->setTabIcon(index+1, this->faviconsList[index]);
        }
    }
}

void BLBrowser::pageTitleChanged(QString title)
{
    QObject* sender = QObject::sender();
    if(sender && !title.isEmpty())
    {
        WebView *webView = (WebView*)sender;
        int tabIndex = this->webViewList.indexOf(webView)+1;
        this->components->browserTabs->setTabText(tabIndex, title);
        this->components->browserTabs->setTabToolTip(tabIndex, title);
    }
}

void BLBrowser::pageURLChanged(QUrl url)
{
    QObject* sender = QObject::sender();
    if(sender)
    {
        WebView *webView = (WebView*)sender;
        this->components->browserHistory->add_url(url.toString(), webView->title());
        QLineEdit *addressEdit = this->addressEditList[this->webViewList.indexOf(webView)];
        if(!addressEdit->hasFocus())
            addressEdit->setText(url.toString());
    }
}

void BLBrowser::getDownloadRequest(QNetworkRequest request)
{
    //qDebug() << "@@@";
    //qDebug() << request.url();
}

void BLBrowser::getUnsupportedContent(QNetworkReply *reply)
{
    if(reply)
    {
        QList<QByteArray> content_type;
        if(reply->hasRawHeader("Content-Type"))
            content_type = reply->rawHeader("Content-Type").split(';');

        if(content_type.contains("application/x-bittorrent") || reply->url().toString().endsWith(".torrent"))
        {
            QEventLoop loop;
            connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
            loop.exec();

            QTemporaryFile temp_torrent(QDir(QDir::tempPath()).absoluteFilePath("XXXXXXtmp.torrent"));
            temp_torrent.setAutoRemove(false);
            if(temp_torrent.open())
            {
                QByteArray data = reply->readAll();
                temp_torrent.write(data);
                temp_torrent.close();
                this->components->addTorrentDialog->receive_message(temp_torrent.fileName());
            }
        }
    }
}

void BLBrowser::handleSslErrors(QNetworkReply* reply, const QList<QSslError> &errors)
{
    qDebug() << "handleSslErrors: ";
    foreach (QSslError e, errors)
    {
        qDebug() << "ssl error: " << e;
    }

    Q_UNUSED(errors)
    reply->ignoreSslErrors();
}

void BLBrowser::load_cookies(BLNetworkCookieJar *cookieJar)
{
    QString cookiePath = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0];
    cookiePath = QDir(cookiePath).absoluteFilePath("BrowserCache");
    cookiePath = QDir(cookiePath).absoluteFilePath("BLCookies");

    QFile fnew(cookiePath);
    if(fnew.open(QIODevice::ReadOnly))
    {
        QList<QNetworkCookie> list;
        QDataStream in(&fnew);

        int count;
        in >> count;
        for(int i = 0; i < count; ++i)
        {
            QByteArray value;
            in >> value;
            QList<QNetworkCookie> newCookies = QNetworkCookie::parseCookies(value);
            if (newCookies.count() == 0 && value.length() != 0) {
                qWarning() << "CookieJar: Unable to parse saved cookie:" << value;
            }
            for (int j = 0; j < newCookies.count(); ++j)
                list.append(newCookies.at(j));
            if (in.atEnd())
                break;
        }
        cookieJar->BLSetAllCookies(list);

        fnew.close();
    }
}

void BLBrowser::save_cookies()
{
    QString cookiePath = QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0];
    cookiePath = QDir(cookiePath).absoluteFilePath("BrowserCache");
    cookiePath = QDir(cookiePath).absoluteFilePath("BLCookies");

    QFile fnew(cookiePath);
    if(fnew.open(QIODevice::WriteOnly))
    {
        BLNetworkCookieJar *cookieJar = static_cast<BLNetworkCookieJar*>(this->networkAccessManager->cookieJar());
        QDataStream out(&fnew);

        QList<QNetworkCookie> list = cookieJar->BLGetAllCookies();
        out << list.size();
        for (int i = 0; i < list.size(); ++i)
            out << list.at(i).toRawForm();

        fnew.flush();
        fnew.close();
    }
}
