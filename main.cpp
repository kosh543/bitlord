#include <QApplication>
#include <QCoreApplication>
#include <QSettings>
#include <QTimer>
#include <QLocalServer>
#include <QLocalSocket>
#include <QSharedMemory>

#include "components.h"
#include "ipcclass.h"
#include "common.h"
#include "config.h"

int main(int argc, char *argv[])
{
    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(".");
    paths.append("platforms");
    paths.append("iconengines");
    paths.append("imageformats");
    QCoreApplication::setLibraryPaths(paths);

    QApplication a(argc, argv);

    QCoreApplication::setApplicationVersion("2.3.2-255");
    QCoreApplication::setOrganizationDomain("bitlord.com");
    QCoreApplication::setApplicationName("BitLord");

    convert_to_qsettings();

    QSettings settings;
    settings.setValue("uiconfig/associate_torrents", check_torrent_files_association());
    settings.setValue("uiconfig/associate_magnets", check_magnet_links_association());
    if(settings.value("uiconfig/check_associations").toBool())
    {
        if(!settings.value("uiconfig/associate_torrents").toBool())
            settings.setValue("uiconfig/associate_torrents", set_torrent_files_association());
        if(!settings.value("uiconfig/associate_magnets").toBool())
            settings.setValue("uiconfig/associate_magnets", set_magnet_links_association());
    }

    Components comp;
    Components *components = &comp;
    components->start();

    IPCClass ipc(components);
    if(ipc.is_running)
    {
        if(argc > 1)
            ipc.send_message(QByteArray(argv[1]));
        ipc.shutdown_ipc();
        return 0;
    }

    if(argc > 1)
        QTimer::singleShot(0, components->addTorrentDialog, SLOT(initialize()));

    components->mainWindow->show();

    QObject::connect(qApp, SIGNAL(aboutToQuit()), components, SLOT(shutdown_bitlord()));

    int return_value = a.exec();

    ipc.shutdown_ipc();

    return return_value;
}
