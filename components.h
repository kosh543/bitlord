#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <QApplication>
#include <QString>
#include <QDir>

#include "core/preventhibernation.h"
#include "core/core.h"
#include "core/alertmanager.h"
#include "core/eventmanager.h"
#include "core/rssmanager.h"
#include "core/torrentmanager.h"
#include "core/autoadd.h"
#include "ui/web/addressentry.h"
#include "ui/web/browsertabs.h"
#include "ui/web/blbrowser.h"
#include "ui/mainwindow.h"
#include "ui/addtorrentdialog.h"
#include "ui/preferencesdialog.h"
#include "ui/menubar.h"
#include "ui/torrentview.h"
#include "ui/rssview.h"
#include "ui/playlistview.h"
#include "ui/filtertreeview.h"
#include "ui/torrentdetails.h"
#include "ui/removetorrentdialog.h"
#include "ui/player/blplayer.h"
#include "config.h"

class Components : public QObject
{
    Q_OBJECT
public:
    ConfigEvents* configEvents;
    PreventHibernation *preventHibernation;
    AlertManager *alertManager;
    EventManager *eventManager;
    Core *core;
    RSSManager *rssManager;
    TorrentManager *torrentManager;
    AutoAdd *autoadd;
    BrowserHistory *browserHistory;
    BrowserTabs *browserTabs;
    BLBrowser *browser;
    MainWindow *mainWindow;
    AddTorrentDialog *addTorrentDialog;
    PreferencesDialog *preferencesDialog;
    RemoveTorrentDialog *removeTorrentDialog;
    MenuBar *menubar;
    TorrentView *torrentView;
    RSSView *rssView;
    PlaylistView *playlistsView;
    TorrentDetails *torrentDetails;
    FilterTreeView *filterTreeView;
    BLPlayer *player;
    Components(QObject *parent = 0);
    void stop();
    void start();

public slots:
    void shutdown_bitlord();

};

#endif // COMPONENTS_H
