#include "config.h"

#include <QSettings>

QVector< QVector<int> > find_json_objects(QString s)
{
    /*
    Find json objects in a string.

    :param s: the string to find json objects in
    :type s: string

    :returns: a list of tuples containing start and end locations of json objects in the string `s`
    :rtype: [(start, end), ...]

    */
    QVector< QVector<int> > objects;

    int opens = 0;
    int start = s.indexOf("{");
    int offset = start;

    if(start < 0)
        return QVector< QVector<int> >();

    for(int index = offset; index < s.length(); index++)
    {
        QChar c = s[index];
        if(c == '{')
            opens += 1;
        else if(c == '}')
        {
            opens -= 1;
            if(opens == 0)
            {
                QVector<int> elem;
                elem.append(start);
                elem.append(index+offset+1);
                objects.append(elem);
                start = index + offset + 1;
            }
        }
    }

    return objects;
}

void convert_to_qsettings_from_conf_file(QString filename, QString groupname)
{
    QSettings settings;
    QString data;
    QFile file;
    file.setFileName(get_default_config_dir(filename));
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        data = file.readAll();

        file.close();

        QVector< QVector<int> > objects = find_json_objects(data);

        if(!objects.isEmpty())
        {
            int start = 0;
            int end = 0;
            if(objects.count() == 1)
            {
                start = objects[0][0];
                end = objects[0][1];
            }
            else if(objects.count() == 2)
            {
                start = objects[1][0];
                end = objects[1][1];
            }
            QJsonDocument d = QJsonDocument::fromJson(data.mid(start, end-start).toUtf8());
            QMap<QString, QVariant> conf = d.toVariant().toMap();

            settings.beginGroup(groupname);
            foreach(QString key, conf.keys())
            {
                settings.setValue(key, conf[key]);
            }
            settings.endGroup();
        }
    }
}

void convert_to_qsettings()
{
    QSettings settings;
    if(settings.contains("converted"))
        return;

    convert_to_qsettings_from_conf_file("core.conf", "coreconfig");
    convert_to_qsettings_from_conf_file("gtkui.conf", "uiconfig");

    settings.setValue("converted", true);
}

ConfigEvents::ConfigEvents(QObject *parent) : QObject(parent)
{
    this->init_signals_map();
}

void ConfigEvents::emit_signal(QString key, QVariant value)
{
    switch (this->signals_map[key]) {
    case ConfigEvents::autoadd_enable:
        emit this->autoadd_enable_signal(key, value);
        break;
    case ConfigEvents::autoadd_location:
        emit this->autoadd_location_signal(key, value);
        break;
    case ConfigEvents::prevent_hibernation:
        emit this->prevent_hibernation_signal(key, value);
        break;
    case ConfigEvents::max_connections_per_torrent:
        emit this->max_connections_per_torrent_signal(key, value);
        break;
    case ConfigEvents::max_upload_slots_per_torrent:
        emit this->max_upload_slots_per_torrent_signal(key, value);
        break;
    case ConfigEvents::max_upload_speed_per_torrent:
        emit this->max_upload_speed_per_torrent_signal(key, value);
        break;
    case ConfigEvents::max_download_speed_per_torrent:
        emit this->max_download_speed_per_torrent_signal(key, value);
        break;
    case ConfigEvents::show_rate_in_title:
        emit this->show_rate_in_title_signal(key, value);
        break;
    case ConfigEvents::run_at_startup:
        emit this->run_at_startup_signal(key, value);
        break;
    case ConfigEvents::ui_language:
        emit this->ui_language_signal(key, value);
        break;
    case ConfigEvents::classic_mode:
        emit this->classic_mode_signal(key, value);
        break;
    case ConfigEvents::enable_system_tray:
        emit this->enable_system_tray_signal(key, value);
        break;
    case ConfigEvents::torrentfiles_location:
        emit this->torrentfiles_location_signal(key, value);
        break;
    case ConfigEvents::listen_ports:
        emit this->listen_ports_signal(key, value);
        break;
    case ConfigEvents::listen_interface:
        emit this->listen_interface_signal(key, value);
        break;
    case ConfigEvents::random_port:
        emit this->random_port_signal(key, value);
        break;
    case ConfigEvents::outgoing_ports:
        emit this->outgoing_ports_signal(key, value);
        break;
    case ConfigEvents::random_outgoing_ports:
        emit this->random_outgoing_ports_signal(key, value);
        break;
    case ConfigEvents::peer_tos:
        emit this->peer_tos_signal(key, value);
        break;
    case ConfigEvents::dht:
        emit this->dht_signal(key, value);
        break;
    case ConfigEvents::upnp:
        emit this->upnp_signal(key, value);
        break;
    case ConfigEvents::natpmp:
        emit this->natpmp_signal(key, value);
        break;
    case ConfigEvents::utpex:
        emit this->utpex_signal(key, value);
        break;
    case ConfigEvents::lsd:
        emit this->lsd_signal(key, value);
        break;
    case ConfigEvents::enc_in_policy:
        emit this->enc_in_policy_signal(key, value);
        break;
    case ConfigEvents::enc_out_policy:
        emit this->enc_out_policy_signal(key, value);
        break;
    case ConfigEvents::enc_level:
        emit this->enc_level_signal(key, value);
        break;
    case ConfigEvents::enc_prefer_rc4:
        emit this->enc_prefer_rc4_signal(key, value);
        break;
    case ConfigEvents::max_connections_global:
        emit this->max_connections_global_signal(key, value);
        break;
    case ConfigEvents::max_upload_speed:
        emit this->max_upload_speed_signal(key, value);
        break;
    case ConfigEvents::max_download_speed:
        emit this->max_download_speed_signal(key, value);
        break;
    case ConfigEvents::max_upload_slots_global:
        emit this->max_upload_slots_global_signal(key, value);
        break;
    case ConfigEvents::max_half_open_connections:
        emit this->max_half_open_connections_signal(key, value);
        break;
    case ConfigEvents::max_connections_per_second:
        emit this->max_connections_per_second_signal(key, value);
        break;
    case ConfigEvents::ignore_limits_on_local_network:
        emit this->ignore_limits_on_local_network_signal(key, value);
        break;
    case ConfigEvents::share_ratio_limit:
        emit this->share_ratio_limit_signal(key, value);
        break;
    case ConfigEvents::seed_time_ratio_limit:
        emit this->seed_time_ratio_limit_signal(key, value);
        break;
    case ConfigEvents::seed_time_limit:
        emit this->seed_time_limit_signal(key, value);
        break;
    case ConfigEvents::max_active_downloading:
        emit this->max_active_downloading_signal(key, value);
        break;
    case ConfigEvents::max_active_seeding:
        emit this->max_active_seeding_signal(key, value);
        break;
    case ConfigEvents::max_active_limit:
        emit this->max_active_limit_signal(key, value);
        break;
    case ConfigEvents::dont_count_slow_torrents:
        emit this->dont_count_slow_torrents_signal(key, value);
        break;
    case ConfigEvents::send_info:
        emit this->send_info_signal(key, value);
        break;
    case ConfigEvents::proxies:
        emit this->proxies_signal(key, value);
        break;
    case ConfigEvents::new_release_check:
        emit this->new_release_check_signal(key, value);
        break;
    case ConfigEvents::rate_limit_ip_overhead:
        emit this->rate_limit_ip_overhead_signal(key, value);
        break;
    case ConfigEvents::geoip_db_location:
        emit this->geoip_db_location_signal(key, value);
        break;
    case ConfigEvents::cache_size:
        emit this->cache_size_signal(key, value);
        break;
    case ConfigEvents::cache_expiry:
        emit this->cache_expiry_signal(key, value);
        break;
    }
}

void ConfigEvents::init_signals_map()
{
    this->signals_map["autoadd_enable"] = ConfigEvents::autoadd_enable;
    this->signals_map["autoadd_location"] = ConfigEvents::autoadd_location;
    this->signals_map["prevent_hibernation"] = ConfigEvents::prevent_hibernation;
    this->signals_map["max_connections_per_torrent"] = ConfigEvents::max_connections_per_torrent;
    this->signals_map["max_upload_slots_per_torrent"] = ConfigEvents::max_upload_slots_per_torrent;
    this->signals_map["max_upload_speed_per_torrent"] = ConfigEvents::max_upload_speed_per_torrent;
    this->signals_map["max_download_speed_per_torrent"] = ConfigEvents::max_download_speed_per_torrent;
    this->signals_map["show_rate_in_title"] = ConfigEvents::show_rate_in_title;
    this->signals_map["run_at_startup"] = ConfigEvents::run_at_startup;
    this->signals_map["ui_language"] = ConfigEvents::ui_language;
    this->signals_map["classic_mode"] = ConfigEvents::classic_mode;
    this->signals_map["enable_system_tray"] = ConfigEvents::enable_system_tray;
    this->signals_map["torrentfiles_location"] = ConfigEvents::torrentfiles_location;
    this->signals_map["listen_ports"] = ConfigEvents::listen_ports;
    this->signals_map["listen_interface"] = ConfigEvents::listen_interface;
    this->signals_map["random_port"] = ConfigEvents::random_port;
    this->signals_map["outgoing_ports"] = ConfigEvents::outgoing_ports;
    this->signals_map["random_outgoing_ports"] = ConfigEvents::random_outgoing_ports;
    this->signals_map["peer_tos"] = ConfigEvents::peer_tos;
    this->signals_map["dht"] = ConfigEvents::dht;
    this->signals_map["upnp"] = ConfigEvents::upnp;
    this->signals_map["natpmp"] = ConfigEvents::natpmp;
    this->signals_map["utpex"] = ConfigEvents::utpex;
    this->signals_map["lsd"] = ConfigEvents::lsd;
    this->signals_map["enc_in_policy"] = ConfigEvents::enc_in_policy;
    this->signals_map["enc_out_policy"] = ConfigEvents::enc_out_policy;
    this->signals_map["enc_level"] = ConfigEvents::enc_level;
    this->signals_map["enc_prefer_rc4"] = ConfigEvents::enc_prefer_rc4;
    this->signals_map["max_connections_global"] = ConfigEvents::max_connections_global;
    this->signals_map["max_upload_speed"] = ConfigEvents::max_upload_speed;
    this->signals_map["max_download_speed"] = ConfigEvents::max_download_speed;
    this->signals_map["max_upload_slots_global"] = ConfigEvents::max_upload_slots_global;
    this->signals_map["max_half_open_connections"] = ConfigEvents::max_half_open_connections;
    this->signals_map["max_connections_per_second"] = ConfigEvents::max_connections_per_second;
    this->signals_map["ignore_limits_on_local_network"] = ConfigEvents::ignore_limits_on_local_network;
    this->signals_map["share_ratio_limit"] = ConfigEvents::share_ratio_limit;
    this->signals_map["seed_time_ratio_limit"] = ConfigEvents::seed_time_ratio_limit;
    this->signals_map["seed_time_limit"] = ConfigEvents::seed_time_limit;
    this->signals_map["max_active_downloading"] = ConfigEvents::max_active_downloading;
    this->signals_map["max_active_seeding"] = ConfigEvents::max_active_seeding;
    this->signals_map["max_active_limit"] = ConfigEvents::max_active_limit;
    this->signals_map["dont_count_slow_torrents"] = ConfigEvents::dont_count_slow_torrents;
    this->signals_map["send_info"] = ConfigEvents::send_info;
    this->signals_map["proxies"] = ConfigEvents::proxies;
    this->signals_map["new_release_check"] = ConfigEvents::new_release_check;
    this->signals_map["rate_limit_ip_overhead"] = ConfigEvents::rate_limit_ip_overhead;
    this->signals_map["geoip_db_location"] = ConfigEvents::geoip_db_location;
    this->signals_map["cache_size"] = ConfigEvents::cache_size;
    this->signals_map["cache_expiry"] = ConfigEvents::cache_expiry;
}
