#ifndef COMMON_H
#define COMMON_H

/* Common functions for various parts of Deluge to use. */

#include "libtorrent/torrent_info.hpp"

#include <QCoreApplication>
#include <QSysInfo>
#include <QSettings>
#include <QStandardPaths>
#include <QDir>
#include <QMap>
#include <QList>
#include <QString>
#include <QDateTime>
#include <QUrl>

const QMap<QString, int> lt_torrent_state_init();
const QList<QString> torrent_state_init();
const QMap<int, QString> file_priority_init();

static const QMap<QString, int> LT_TORRENT_STATE = lt_torrent_state_init();
static const QList<QString> TORRENT_STATE = torrent_state_init();
static const QMap<int, QString> FILE_PRIORITY = file_priority_init();

QString get_version();
QString get_default_config_dir(QString filename=QString());
QString get_default_download_dir();
bool windows_check();
bool windows_xp_check();
bool vista_check();
bool windows7_check();
bool windows8_check();
bool is_url(QString url);
bool is_magnet(QString magnet);
QString info_hash_to_string(libtorrent::sha1_hash hash);
QString fsize(int fsize_b);
QString fpcnt(double dec);
QString fspeed(int bps);
QString fpeer(int num_peers, int total_peers);
QString ftime_from_seconds(int seconds);
QString fdate(double seconds);
void set_run_at_startup();
void remove_run_at_startup();
bool check_torrent_files_association();
bool check_magnet_links_association();
bool set_torrent_files_association();
bool set_magnet_links_association();

#endif // COMMON_H
