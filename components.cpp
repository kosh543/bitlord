#include "components.h"
#include "common.h"

Components::Components(QObject *parent) :
    QObject(parent)
{
    if(!QDir(get_default_config_dir()).exists())
        QDir().mkdir(get_default_config_dir());

    this->configEvents = new ConfigEvents();
    this->preventHibernation = new PreventHibernation();
    this->core = new Core(this);
    this->alertManager = new AlertManager(this);
    this->eventManager = new EventManager();
    this->rssManager = new RSSManager(this);
    this->torrentManager = new TorrentManager(this);
    this->autoadd = new AutoAdd(this);

    this->filterTreeView = new FilterTreeView(this);
    this->browserHistory = new BrowserHistory();
    this->browserTabs = new BrowserTabs(this);
    this->browser = new BLBrowser(this);
    this->torrentView = new TorrentView(this);
    this->rssView = new RSSView(this);
    this->playlistsView = new PlaylistView(this);
    this->torrentDetails = new TorrentDetails(this);
    this->mainWindow = new MainWindow(this);
    this->addTorrentDialog = new AddTorrentDialog(this);
    this->preferencesDialog = new PreferencesDialog(this);
    this->removeTorrentDialog = new RemoveTorrentDialog();
    this->menubar = new MenuBar(this);

    this->player = new BLPlayer(this);

    this->playlistsView->load_state();
    this->rssManager->load_state();
}

void Components::start()
{
    this->torrentManager->start();

    this->torrentView->start();
}

void Components::stop()
{
    this->torrentManager->stop();
}

void Components::shutdown_bitlord()
{
    this->alertManager->pause();

    if(!this->mainWindow->isHidden())
            this->mainWindow->close();

    this->browserHistory->save_state();
    this->browser->save_cookies();
    this->browser->stop_all();
    this->rssManager->save_state();
    this->playlistsView->save_state();
    this->torrentDetails->save_tabs_state();
    this->torrentView->save_state();
    this->torrentManager->save_state();
    this->torrentManager->save_resume_data();

    std::auto_ptr<libtorrent::alert> alert = this->core->session->pop_alert();
    while(!this->torrentManager->waiting_on_resume_data.isEmpty())
    {
        libtorrent::alert *a = alert.get();
        if(a)
        {
            //qDebug() << "!!!" << a->type() << QString::fromStdString(a->message());
            switch (a->type())
            {
            case libtorrent::save_resume_data_alert::alert_type:
                this->torrentManager->on_alert_save_resume_data(a);
                break;
            }
        }
        alert = this->core->session->pop_alert();
    }

    this->core->save_dht_state();

    delete this->player;
    delete this->browser;
    delete this->torrentManager;
    delete this->core;
}
