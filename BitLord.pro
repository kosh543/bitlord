#-------------------------------------------------
#
# Project created by QtCreator 2013-10-01T09:05:07
#
#-------------------------------------------------

QT       += core gui network webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BitLord
TEMPLATE = app

SOURCES += main.cpp\
        ui/mainwindow.cpp \
    ui/menubar.cpp \
    ui/torrentview.cpp \
    components.cpp \
    ui/filtertreeview.cpp \
    core/core.cpp \
    config.cpp \
    updateqmap.cpp \
    common.cpp \
    core/torrent.cpp \
    core/torrentoptions.cpp \
    core/alertmanager.cpp \
    core/torrentmanager.cpp \
    ui/treeviewitem.cpp \
    ui/treeviewmodel.cpp \
    ui/addtorrentdialog.cpp \
    ui/removetorrentdialog.cpp \
    core/eventmanager.cpp \
    ui/torrentdetails.cpp \
    ui/statustab.cpp \
    ui/detailtab.cpp \
    ui/filestab.cpp \
    ui/optionstab.cpp \
    ui/peerstab.cpp \
    ui/setotherdialog.cpp \
    ui/edittrackersdialog.cpp \
    ui/preferencesdialog.cpp \
    ipcclass.cpp \
    core/preventhibernation.cpp \
    core/autoadd.cpp \
    ui/statusbar.cpp \
    ui/web/blbrowser.cpp \
    ui/web/browsertabs.cpp \
    ui/web/webview.cpp \
    ui/web/addressentry.cpp \
    core/downloadingdialog.cpp \
    core/rssmanager.cpp \
    ui/rssview.cpp \
    ui/player/blplayer.cpp \
    ui/playlistview.cpp

HEADERS  += ui/mainwindow.h \
    ui/menubar.h \
    ui/torrentview.h \
    components.h \
    ui/filtertreeview.h \
    core/core.h \
    config.h \
    updateqmap.h \
    common.h \
    core/torrent.h \
    core/torrentoptions.h \
    UriParser.h \
    core/alertmanager.h \
    core/torrentmanager.h \
    core/torrentstate.h \
    core/cpickleparser.h \
    ui/treeviewitem.h \
    ui/treeviewmodel.h \
    ui/addtorrentdialog.h \
    ui/removetorrentdialog.h \
    core/eventmanager.h \
    ui/torrentdetails.h \
    ui/statustab.h \
    ui/detailtab.h \
    ui/filestab.h \
    ui/optionstab.h \
    ui/peerstab.h \
    ui/setotherdialog.h \
    ui/edittrackersdialog.h \
    ui/preferencesdialog.h \
    ipcclass.h \
    core/preventhibernation.h \
    core/autoadd.h \
    ui/statusbar.h \
    ui/web/blbrowser.h \
    ui/web/browsertabs.h \
    ui/web/webview.h \
    ui/web/addressentry.h \
    core/downloadingdialog.h \
    core/rssmanager.h \
    ui/rssview.h \
    ui/player/blplayer.h \
    ui/playlistview.h


QMAKE_CXXFLAGS += -ftemplate-depth-128 -O3 -finline-functions -Wno-inline -Wall -mthreads -Wno-missing-braces -fno-strict-aliasing
QMAKE_CXXFLAGS += -DBOOST_ALL_NO_LIB -DBOOST_ASIO_ENABLE_CANCELIO -DBOOST_ASIO_HASH_MAP_BUCKETS=1021 -DBOOST_ASIO_SEPARATE_COMPILATION
QMAKE_CXXFLAGS += -DBOOST_EXCEPTION_DISABLE -DBOOST_SYSTEM_STATIC_LINK=1 -DNDEBUG -DTORRENT_DISABLE_GEO_IP -DTORRENT_NO_BOOST_DATE_TIME
QMAKE_CXXFLAGS += -DTORRENT_USE_I2P=1 -DTORRENT_USE_TOMMATH -DUNICODE -DWIN32 -DWIN32_LEAN_AND_MEAN -D_FILE_OFFSET_BITS=64 -D_UNICODE
QMAKE_CXXFLAGS += -D_WIN32 -D_WIN32_WINNT=0x0500 -D__USE_W32_SOCKETS
QMAKE_CXXFLAGS += -I"C:\libtorrent-rasterbar-0.16.10\include" -I"C:\boost\include\boost-1_49"
LIBS += -LC:\libtorrent-rasterbar-0.16.10\bin\gcc-mingw-4.8.0\release\boost-source\fpic-on\threading-multi
LIBS += -LC:\boost\stage\lib
LIBS += -llibtorrent -llibboost_system-mgw48-mt-1_49
LIBS += -lvlc-qt -lvlc-qt-widgets
LIBS += -llibvlc -llibvlccore
LIBS += -lws2_32 -lwsock32 -lpthread
LIBS += -lKernel32

FORMS +=
