#include "rssmanager.h"
#include "components.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QEventLoop>
#include <QByteArray>
#include <QXmlStreamReader>
#include <QDebug>

RSSManager::RSSManager(Components *comp, QObject *parent) :
    QObject(parent)
{
    this->components = comp;

    networkAccessManager = new QNetworkAccessManager(this);

    this->real_load_timer = new QTimer(this);
    this->real_load_timer->setSingleShot(true);
    this->real_load_timer->setInterval(0);
    this->connect(this->real_load_timer, SIGNAL(timeout()), this, SLOT(real_load_state()));

    this->update_feeds_timer = new QTimer(this);
    this->update_feeds_timer->setInterval(300000);
    this->connect(this->update_feeds_timer, SIGNAL(timeout()), this, SLOT(update_feeds()));
    this->update_feeds_timer->start();
}

void RSSManager::load_state()
{
    this->real_load_timer->start();
}

void RSSManager::real_load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile fnew(QDir(dir).absoluteFilePath("rsslinks.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        while(!in.atEnd())
        {
            QString url;
            QString title;
            in >> url;
            in >> title;
            this->addFeed(url, title);
        }
        fnew.close();
    }
}

void RSSManager::save_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);

    QFile file(QDir(dir).absoluteFilePath("rsslinks.state"));
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream out(&file);
        foreach(RSSFeed feed, this->feeds.values())
        {
            out << feed.url;
            out << feed.title;
        }
        file.flush();
        file.close();
    }
}

void RSSManager::update_feeds()
{
    foreach(QString feed_title, this->feeds.keys())
        this->parseFeed(this->feeds[feed_title]);
}

void RSSManager::rssFeedAdded(QString url, QString alias)
{
    this->addFeed(url, alias);
}

void RSSManager::parseFeed(RSSFeed &rssFeed)
{
    rssFeed.titles.clear();
    rssFeed.urls.clear();

    QUrl feedURL(rssFeed.url);
    QNetworkRequest request(feedURL);
    QNetworkReply *reply = this->networkAccessManager->get(request);

    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    QByteArray data = reply->readAll();

    QXmlStreamReader xml;
    xml.addData(data);

    QString currentTag;
    QString feedTitle;
    QString titleString;
    QString linkString;
    bool isItem = false;
    while(!xml.atEnd())
    {
        xml.readNext();
        if(xml.isStartElement())
        {
            if(xml.name() == "item")
                isItem = true;
            currentTag = xml.name().toString();
        }
        else if(xml.isEndElement())
        {
            if(xml.name() == "item")
            {
                rssFeed.titles.append(titleString);
                rssFeed.urls.append(linkString);

                isItem = false;
                titleString.clear();
                linkString.clear();
            }
        }
        else if(xml.isCharacters() && !xml.isWhitespace())
        {
            if(isItem)
            {
                if(currentTag == "title")
                    titleString += xml.text().toString();
                else if (currentTag == "link")
                    linkString += xml.text().toString();
            }
            else
            {
                if(currentTag == "title")
                    feedTitle += xml.text().toString();
            }
        }
    }

    if(rssFeed.title == "")
        rssFeed.title = feedTitle;
    if(rssFeed.title == "")
        rssFeed.title = rssFeed.url;
}

void RSSManager::addFeed(QString url, QString alias)
{
    RSSFeed rssFeed(alias, url);
    if(this->feeds.values().contains(rssFeed))
        return;

    this->parseFeed(rssFeed);

    if(this->feeds.contains(rssFeed.title))
        return;

    this->feeds.insert(rssFeed.title, rssFeed);

    emit this->rssFeedAddedToManager(rssFeed.title);
}

void RSSManager::removeFeed(QString title)
{
    this->feeds.remove(title);
}

void RSSManager::updateOneFeed(QString title)
{
    this->parseFeed(this->feeds[title]);
}

void RSSManager::setCurrentFeed(QString feed)
{
    this->currentFeed = feed;
}

QList<QString> RSSManager::getFeedTitles(QString feed)
{
    if(!this->feeds.contains(feed))
        return QList<QString>();
    return this->feeds[feed].titles;
}

QString RSSManager::getUrlByIndex(int index)
{
    return this->feeds[this->currentFeed].urls.at(index);
}
