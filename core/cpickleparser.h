#ifndef PICKLELOADER_H_

#include <QVector>
#include <QStack>
#include <QVariant>

// Complete reimplementation of the Protocol Loading 2.  The codebase
// has a smaller memory footprint, it's just about as fast (faster in
// some cases), it's is easier to read and maintain, and simpler.

#define PY_MARK        '('
#define PY_STOP        '.'
#define PY_POP         '0'
#define PY_POP_MARK    '1'
#define PY_DUP         '2'
#define PY_FLOAT       'F'
#define PY_BINFLOAT    'G'
#define PY_INT         'I'
#define PY_BININT      'J'
#define PY_BININT1     'K'
#define PY_LONG        'L'
#define PY_BININT2     'M'
#define PY_NONE        'N'
#define PY_PERSID      'P'
#define PY_BINPERSID   'Q'
#define PY_REDUCE      'R'
#define PY_STRING      'S'
#define PY_BINSTRING   'T'
#define PY_SHORT_BINSTRING 'U'
#define PY_UNICODE     'V'
#define PY_BINUNICODE  'X'
#define PY_APPEND      'a'
#define PY_BUILD       'b'
#define PY_GLOBAL      'c'
#define PY_DICT        'd'
#define PY_EMPTY_DICT  '}'
#define PY_APPENDS     'e'
#define PY_GET         'g'
#define PY_BINGET      'h'
#define PY_INST        'i'
#define PY_LONG_BINGET 'j'
#define PY_LIST        'l'
#define PY_EMPTY_LIST  ']'
#define PY_OBJ         'o'
#define PY_PUT         'p'
#define PY_BINPUT      'q'
#define PY_LONG_BINPUT 'r'
#define PY_SETITEM     's'
#define PY_TUPLE       't'
#define PY_EMPTY_TUPLE ')'
#define PY_SETITEMS    'u'
/* Protocol 2. */
#define PY_PROTO    '\x80' /* identify pickle protocol */
#define PY_NEWOBJ   '\x81' /* build object by applying cls.__new__ to argtuple */
#define PY_EXT1     '\x82' /* push object from extension registry; 1-byte index */
#define PY_EXT2     '\x83' /* ditto, but 2-byte index */
#define PY_EXT4     '\x84' /* ditto, but 4-byte index */
#define PY_TUPLE1   '\x85' /* build 1-tuple from stack top */
#define PY_TUPLE2   '\x86' /* build 2-tuple from two topmost stack items */
#define PY_TUPLE3   '\x87' /* build 3-tuple from three topmost stack items */
#define PY_NEWTRUE  '\x88' /* push True */
#define PY_NEWFALSE '\x89' /* push False */
#define PY_LONG1    '\x8a' /* push long from < 256 bytes */
#define PY_LONG4    '\x8b' /* push really big long */

// The PickleLoader is an loader which will load any Protocol, just
// like cPickle.loads().
class PickleLoader {

private:

    // The Value stack: nothing ever goes "straight into" a data
    // structure: it has to sit on the value stack then be reduced into
    // some data structure.
    QStack<QVariant> values_;

    // Every time a memo is made, lookup its associated value.  The
    // AVLTreeT values do not move once they are in the tree.  These
    // are put in order: 1..n, with a "swapInto"
    QMap<int, QVariant> memos_;

    // Mark stack: every a mark is made, indicate where it is on the
    // value stack ... many things pop back to the last mark.
    QStack<int> marks_;

    // The input, stored as some chars.
    QString input_;
    int where_;
    int len_;

public:

    // Construct a Pickleloader with the given input.
    //PickleLoader (const char* buffer, int len)
    PickleLoader(QString buffer)
    {
        this->input_ = buffer;
        this->where_ = 0;
        this->len_ = buffer.length();
    }

    // Reset the input so we can reuse this loader
    //void reset (const char* buffer, int len)
    void reset(QString buffer)
    {
        this->input_ = buffer;
        this->len_ = buffer.length();
        this->where_ = 0;
        this->values_.clear();
        this->marks_.clear();
        this->memos_.clear();
    }

    // Load and return the top value
    void loads (QVariant& return_value)
    {
        this->decode_(return_value);
        //TranslateForNumPyClassesToArray(return_value);
    }

protected:

    // Keep pulling stuff off of input and final thing on top of stack
    // becomes result
    inline void decode_ (QVariant& result);

    // Get the next character from input: may throw exception if at end
    // of buffer.
    inline QChar getChar_()
    {
        if(where_>=len_)
            return QChar::Null;
        return input_[where_++];
    }

    // Advance the input up "n" characters, but keep a pointer back
    // to the original place where we started: this allows us to get
    // the next few characters easily.
    inline int advanceInput_ (int n)
    {
        int start = where_;
        where_ += n;
        if (where_>len_) throw std::exception();
        return start;
    }

    // From the current input, keep going until you get a \n: pass
    // the newline
    QString getUpToNewLine_ (int& length_of_input)
    {
        int start = where_;
        int end = len_;
        int result = start;
        while(start != end && input_[start++] != '\n');
        length_of_input = start - result;
        where_ += length_of_input;

        if (where_>len_) throw std::exception();
        return input_.mid(result, length_of_input);
    }

    // Getting a string is tricky: it can contain embedded ' and " as
    // well as \t\n sequences as well as \x00 sequences.  We have to
    // start with the starting punctutation (' or "), ignore all
    // escaped starting punctuation until the end.
    // Input: something like 'abc\'123'\n
    // Output: input advanced past \n, start points to start quote
    //         len includes quotes and len (so actual string data is 3 less)
    QString getString_ (int& length_of_input)
    {
        int input = where_;
        const int input_length = len_;
        length_of_input = -1;
        // Get start quote
        QChar start_quote = input_[input];
        if (start_quote!='\'' && start_quote!='\"')
        {
            throw std::exception();
        }
        int start_string = input+1;
        // Iterate through escapes
        static QString code = "n\\rt'\"";
        for (int ii=1; ii<input_length; )
        {
            QChar c = input_[input+ii];
            if (c=='\\')
            { // non-printable,so was esc
                if (ii+1>=input_length)
                {
                    throw std::exception();
                }
                bool where = code.contains(input_[input+ii+1]);
                if (where)
                {
                    ii+=2;
                    continue;
                }
                else if (input_[input+ii+1]=='x')
                {
                    if (ii+3>=input_length)
                    {
                        throw std::exception();
                    }
                    ii+=4;
                    continue;
                }
                else
                {
                    throw std::exception();
                }
            }
            else if (c==start_quote)
            {
                // Error checking to made sure we are at end
                if (++ii>=input_length)
                {
                    throw std::exception();
                }
                if(input_[input+ii]!='\n')
                {
                    throw std::exception();
                }
                length_of_input = ii+1;
                where_ += length_of_input;
                break;
            }
            else
            { // printable, so take as is
                ++ii;
            }
        }
        if (length_of_input==-1)
        {
            throw std::exception();
        }
        return input_.mid(start_string, length_of_input-3);
    }


    // Get a 4 byte in from input: a length so int_u4
    //inline int get4ByteInt_();

    // Routines to handle each input token
    inline void hMARK();
    inline void hFLOAT();
    inline void hINT();
    inline void hLONG();
    inline void hNONE();
    inline void hSTRING();
    inline void hUNICODE();
    inline void hAPPEND();
    inline void hBUILD();
    inline void hDICT();
    inline void hGET();
    inline void hINST();
    inline void hLIST();
    inline void hTUPLE();
    inline void hPUT();
    inline void hSETITEM();

    inline void NOT_IMPLEMENTED (char c)
    {
        throw std::exception();
    }

    inline void pushMemo_(int memo_number);
}; // PickleLoader

inline void PickleLoader::decode_ (QVariant &result)
{
    bool done = false;
    while (!done)
    {
        QChar token = getChar_();
        if(token.isNull()) break;
        switch (token.toLatin1())
        {
        case PY_MARK:             hMARK(); break;            // '('
        case PY_STOP:             done = true; break;        // '.'
        case PY_POP:           NOT_IMPLEMENTED('0'); break;  // '0'
        case PY_POP_MARK:      NOT_IMPLEMENTED('1'); break;  // '1'
        case PY_DUP:           NOT_IMPLEMENTED('2'); break;  // '2'
        case PY_FLOAT:            hFLOAT(); break;           // 'F'
        case PY_INT:              hINT(); break;             // 'I'
        case PY_LONG:             hLONG();    break;         // 'L'
        case PY_NONE:             hNONE();    break;         // 'N'
        case PY_PERSID:        NOT_IMPLEMENTED('P'); break;  // 'P'
        case PY_BINPERSID:     NOT_IMPLEMENTED('Q'); break;  // 'Q'
        case PY_STRING:           hSTRING(); break;          // 'S'
        case PY_UNICODE:          hUNICODE(); break;         // 'V'
        case PY_BINUNICODE:    NOT_IMPLEMENTED('X');         // 'X'
        case PY_APPEND:           hAPPEND(); break;          // 'a'
        case PY_BUILD:            hBUILD(); break;           // 'b'
        case PY_DICT:             hDICT();  break;           // 'd'
        case PY_TUPLE:            hTUPLE(); break;           // 't'
        case PY_GET:              hGET();     break;         // 'g'
        case PY_INST:             hINST(); break;            // 'i'
        case PY_LIST:             hLIST(); break;            // 'l'
        case PY_PUT:              hPUT();   break;           // 'p'
        case PY_SETITEM:          hSETITEM(); break;         // 's'
        default:
            throw std::exception();
        }
    }
    // The final result is whatever is on top of the stack!
    QVariant& top = values_.last();
    result.swap(top);
}

inline void PickleLoader::hSTRING ()
{
    int full_len;
    QVariant s = getString_(full_len);
    values_.push(s);
}

inline void PickleLoader::hUNICODE ()
{
    int full_len;
    QString u = getUpToNewLine_(full_len);
    u.remove('\n');
    QVariant s = u;
    values_.push(s);
}

inline void PickleLoader::hMARK ()
{
  // Simulate a "mark" on Python stack with a meta value
  marks_.push(values_.count());
}


// DICT, LIST and TUPLE all use the MARK
// EMPTYDICT, EMPTYLIST, EMPTY_TUPLE do not

inline void PickleLoader::hDICT ()
{
    // Find where top of the stack was at mark time
    int last_mark = marks_.pop();

    // How many items on stack?  The difference between the current
    // length, and where the last mark was
    int items_to_set = values_.count() - last_mark;

    // No actual dict value on stack at this point: it replaces the
    // first thing on the stack
    QMap<QString, QVariant> d;

    // For efficiency, swap the values in
    for (int ii=last_mark; ii<last_mark+items_to_set; ii++)
    {
        //d.swapInto(values_[ii], values_[ii+1]);
    }

    QVariant v(d);
    // Once values in Tab, take em off stack ... all except last,
    // which becomes the Tab itself
    if (items_to_set==2)
    {
        values_.last().swap(v);
    }
    else if (items_to_set-1>0)
    {
        for (int j=0; j<items_to_set-1; j++)
        {
            values_.pop();
        }
        values_.last().swap(v);
    }
    else
    {
        values_.push(v); // If nothing on the stack, push an empty Tab
    }
}

inline void PickleLoader::hLIST ()
{
    // Find where top of the stack was at mark time
    int last_mark = marks_.pop();

    // How many items on stack?  The difference between the current
    // length, and where the last mark was
    int items_to_append = values_.count() - last_mark;

    // No actual tuple value on stack: it replaces the first thing on
    // the stack
    QList<QVariant> a;

    for (int ii=0; ii<items_to_append; ii++)
    {
        a.append(values_[last_mark+ii]);
    }

    // Once values in arr, take em off stack ... all except last,
    // which becomes the arr itself
    QVariant v(a);
    if (items_to_append==1)
    {
        values_.last().swap(v);
    }
    else if (items_to_append-1>0)
    {
        for (int j=0; j<items_to_append-1; j++)
        {
            values_.pop();
        }
        values_.last().swap(v);
    }
    else
    {
        values_.push(v); // If nothing on the stack, push an empty arr
    }
}

inline void PickleLoader::hTUPLE()
{
    hLIST();
}

// EMPY doesn't look at the mark, just plops on the stack.
//inline void PickleLoader::hEMPTY_DICT ()  { values_.push(new Tab()); }
//inline void PickleLoader::hEMPTY_LIST ()  { values_.push(new Arr()); }
//inline void PickleLoader::hEMPTY_TUPLE () { values_.push(new Tup()); }


inline void PickleLoader::hFLOAT ()
{
    int len;
    QString f = getUpToNewLine_(len);
    double rl = f.toDouble();
    QVariant v(rl);
    values_.push(v);
}

inline void PickleLoader::hPUT ()
{
    // Take top of stack and "memoize" it with this value!
    int len;
    QString s = getUpToNewLine_(len);
    int memo_number = s.toInt();

    memos_[memo_number] = values_.last();
}

inline void PickleLoader::pushMemo_ (int memo_number)
{
    QVariant v = memos_[memo_number];
    values_.push(v);
}

inline void PickleLoader::hGET ()
{
    // Take top of stack and "memoize" it with this value!
    int len;
    QString s = getUpToNewLine_(len);
    int memo_number = s.toInt();

    pushMemo_(memo_number);
}

inline void PickleLoader::hAPPEND ()
{
    // Two items on stack: must be list and some value
    QVariant v = values_.pop();
    QList<QVariant> a = values_.pop().toList();

    a.append(v);

    // Leave just array on stack
    values_.push(QVariant(a));
}

inline void PickleLoader::hSETITEM ()
{
    // Three items on stack: must be Tab, key, value
    QVariant value = values_.pop();
    QString key = values_.pop().toString();
    QMap<QString, QVariant> t = values_.pop().toMap();

    // insert key/value
    t[key] = value;

    // Leave just Tab on stack
    values_.push(QVariant(t));
}

inline void PickleLoader::hNONE()
{
    values_.push(QVariant());
}


inline void PickleLoader::hINT ()
{
    int len;
    QString str = getUpToNewLine_(len);
    QVariant s(str.toInt());
    values_.push(s);
}


inline void PickleLoader::hLONG ()
{
    int len;
    QString str = getUpToNewLine_(len);
    QVariant s(str.toInt());
    values_.push(s);
}

inline void PickleLoader::hINST()
{
    int len;
    getUpToNewLine_(len);
    getUpToNewLine_(len);
    hLIST();
}

inline void PickleLoader::hBUILD ()
{
    hAPPEND();
    // Build is interesting: it seems to expect the Type Object on the
    // stack as well as the dictionary
    //QVariant type_object = values_.at(values_.count()-2);

    // Otherwise check
    //if (type_object.tag != 'u')
    //{
    //    throw std::exception();
    //}
    //string name = type_object(0);
    //Val&   tupl = type_object(1);

    //QVariant& dictionary  = values_.last();

    // Look up the name in the registry: use our information
    // to the BUILD the appropriate object if it is registered
    //if (registry_.contains(name))
    //{
        /*FactoryFunction& ff = registry_[name];
        QVariant result;
        ff(type_object, dictionary, env_, result);
        result.swap(type_object);*/
    //}
    //else
    //{
        // Create a tuple since nothing is registered
        // ( type_object, dictionary )
        /*Val not_there = new Tup(None, None);
        Tup& u = not_there;
        u(0).swap(type_object);
        u(1).swap(dictionary);
        not_there.swap(type_object);*/
    //}
    //values_.pop();
}


#define PICKLELOADER_H_
#endif // PICKLELOADER_H_
