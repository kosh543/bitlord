#include "torrentoptions.h"

#include <QSettings>

TorrentOptions::TorrentOptions()
{
    this->options = QMap<QString, QVariant>();
    QMap<QString, QString> options_conf_map;
    options_conf_map["max_connections"] = "max_connections_per_torrent";
    options_conf_map["max_upload_slots"] = "max_upload_slots_per_torrent";
    options_conf_map["max_upload_speed"] = "max_upload_speed_per_torrent";
    options_conf_map["max_download_speed"] = "max_download_speed_per_torrent";
    options_conf_map["prioritize_first_last_pieces"] = "prioritize_first_last_pieces";
    options_conf_map["compact_allocation"] = "compact_allocation";
    options_conf_map["download_location"] = "download_location";
    options_conf_map["auto_managed"] = "auto_managed";
    options_conf_map["shutdown_pc"] = "shutdown_pc";
    options_conf_map["stop_at_ratio"] = "stop_seed_at_ratio";
    options_conf_map["stop_ratio"] = "stop_seed_ratio";
    options_conf_map["remove_at_ratio"] = "remove_seed_at_ratio";
    options_conf_map["move_completed"] = "move_completed";
    options_conf_map["move_completed_path"] = "move_completed_path";
    options_conf_map["add_paused"] = "add_paused";

    QSettings settings;
    foreach (QString opt_k, options_conf_map.keys())
        this->options[opt_k] = settings.value("coreconfig/"+options_conf_map[opt_k]);
    this->options["file_priorities"] = QVariant::fromValue(QVector<int>());
    QMap<int, QString> map;
    this->options["mapped_files"] = QVariant::fromValue<QMap<int, QString> >(map);
    this->options["enable_streaming"] = false;
}

void TorrentOptions::updateOptions(QMap<QString, QVariant> &other)
{
    updateQMap(this->options, other);
}

QVariant& TorrentOptions::operator [](QString key)
{
    return this->options[key];
}

QList<QString> TorrentOptions::keys()
{
    return this->options.keys();
}

QMap<QString, QVariant> TorrentOptions::get_qmap()
{
    return this->options;
}
