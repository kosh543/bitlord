#include "torrent.h"
#include "components.h"

#include <QSettings>

Torrent::Torrent(Components *comp, libtorrent::torrent_handle handle, TorrentOptions *options, TorrentState *state, QString *filename,
                 QString *magnet, QVector<int> magnet_file_priorities)
{
    this->components = comp;

    // Set the libtorrent handle
    this->handle = handle;
    // Set the torrent_id for this torrent
    this->torrent_id = info_hash_to_string(handle.info_hash());

    // Keep a list of file indexes we're waiting for file_rename alerts on
    // This also includes the old_folder and new_folder to know what signal to send
    // This is so we can send one folder_renamed signal instead of multiple
    // file_renamed signals.
    // [(old_folder, new_folder, [*indexes]), ...]
    //this->waiting_on_folder_rename = QList<QList<QVariant> >();

    // We store the filename just in case we need to make a copy of the torrentfile
    if(!filename)
        // If no filename was provided, then just use the infohash
        this->filename = QString(this->torrent_id);
    else
        this->filename = *filename;


    // Store the magnet uri used to add this torrent if available
    this->magnet = magnet;

    // Holds status info so that we don't need to keep getting it from lt
    this->status = this->handle.status();
    this->state = QString("Checking");
    this->magnet_file_priorities = magnet_file_priorities;

    /*try
    {
        this->torrent_info = &this->handle.get_torrent_info();
    }
    catch(...)
    {
        this->torrent_info = NULL;
    }*/

    // Files dictionary
    this->files = this->get_files(); //QList<QMap<QString, QVariant> >();

    // Default total_uploaded to 0, this may be changed by the state
    this->total_uploaded = 0.0;

    // Set the default options
    this->options = TorrentOptions();
    QMap<QString, QVariant> qmap = (*options).get_qmap();
    this->options.updateOptions(qmap);

    // We need to keep track if the torrent is finished in the state to prevent
    // some weird things on state load.
    this->is_finished = false;

    // Load values from state if we have it
    if(state)
    {
        // This is for saving the total uploaded between sessions
        this->total_uploaded = state->total_uploaded;
        // Set the trackers
        this->set_trackers(state->trackers);
        // Set the filename
        this->filename = state->filename;
        this->is_finished = state->is_finished;
    }
    else
    {
        // Tracker list
        //this->trackers = QList<QMap<QString, QVariant> >();
        // Create a list of trackers
        foreach(libtorrent::announce_entry value, this->handle.trackers())
        {
            QMap<QString, QVariant> tracker;
            tracker["url"] = QString::fromStdString(value.url);
            tracker["tier"] = value.tier;
            trackers.append(tracker);
        }
        //for(value in this->handle.trackers())
        //{
            //if lt.version_minor < 15:
            //    tracker = {}
            //    tracker["url"] = value.url
            //    tracker["tier"] = value.tier
            //else:
            //    tracker = value
            //self.trackers.append(tracker)
        //}
    }

    // Various torrent options
    this->handle.resolve_countries(true);

    this->set_options(this->options);

    // Status message holds error info about the torrent
    this->statusmsg = QString("OK");

    // The torrents state
    this->update_state();

    // The tracker status
    this->tracker_status = QString();

    // This gets updated when get_tracker_host is called
    this->tracker_host = QString();

    // URL of last working tracker for this torrent
    this->working_tracker = QString();

    if(state)
        this->time_added = state->time_added;
    else
        this->time_added = QDateTime::currentDateTime().toTime_t();

    // Keep track if we're forcing a recheck of the torrent so that we can
    // repause it after its done if necessary
    this->forcing_recheck = false;
    this->forcing_recheck_paused = false;
}

Torrent::~Torrent()
{
}

void Torrent::set_options(TorrentOptions options)
{
    QList<QString> vals;
    vals << "auto_managed" << "shutdown_pc" << "download_location" << "file_priorities" <<
            "max_connections" << "max_download_speed" << "max_upload_slots" << "max_upload_speed" <<
            "prioritize_first_last_pieces";
    foreach(QString key, options.keys())
    {
        if(!vals.contains(key))
            continue;
        switch (vals.indexOf(key))
        {
        case 0:
            this->set_auto_managed(options[key].toBool());
            break;
        case 1:
            this->set_shutdown_pc(options[key].toBool());
            break;
        case 2:
            this->set_save_path(options[key].toString());
            break;
        case 3:
            this->set_file_priorities(options[key].value<QVector<int> >());
            break;
        case 4:
            this->handle.set_max_connections(options[key].toInt());
            break;
        case 5:
            this->set_max_download_speed(options[key].toInt());
            break;
        case 6:
            this->handle.set_max_uploads(options[key].toInt());
            break;
        case 7:
            this->set_max_upload_speed(options[key].toInt());
            break;
        case 8:
            // set_prioritize_first_last is called by set_file_priorities,
            // so skip if file_priorities is set in options.
            if(!options.keys().contains("file_priorities"))
                this->set_prioritize_first_last(options[key].toBool());
            break;
        default:
            break;
        }
    }
    QMap<QString, QVariant> opt = options.get_qmap();
    this->options.updateOptions(opt);
}

TorrentOptions Torrent::get_options()
{
    return this->options;
}

void Torrent::set_max_connections(int max_connections)
{
    this->options["max_connections"] = max_connections;
    this->handle.set_max_connections(max_connections);
}

void Torrent::set_max_upload_slots(int max_slots)
{
    this->options["max_upload_slots"] = max_slots;
    this->handle.set_max_uploads(max_slots);
}

void Torrent::set_max_upload_speed(int m_up_speed)
{
    this->options["max_upload_speed"] = m_up_speed;
    int v = 0;
    if(m_up_speed < 0)
        v = -1;
    else
        v = m_up_speed * 1024;
    this->handle.set_upload_limit(v);
}

void Torrent::set_max_download_speed(int m_down_speed)
{
    this->options["max_download_speed"] = m_down_speed;
    int v = 0;
    if( m_down_speed < 0)
        v = -1;
    else
        v = m_down_speed * 1024;
    this->handle.set_download_limit(v);
}

void Torrent::set_prioritize_first_last(bool prioritize)
{
    this->options["prioritize_first_last_pieces"] = prioritize;

    if(!prioritize)
    {
        // If we are turning off this option, call set_file_priorities to
        // reset all the piece priorities
        if(this->options["file_priorities"].canConvert<QVector<int> >())
            this->set_file_priorities(this->options["file_priorities"].value<QVector<int> >());
        return;
    }
    if(!this->handle.has_metadata() || this->options["compact_allocation"].toBool())
        return;

    // A list of priorities for each piece in the torrent
    QVector<int> priorities = QVector<int>::fromStdVector(this->handle.piece_priorities());
    libtorrent::torrent_info ti = this->handle.get_torrent_info();
    for(int i = 0; i < ti.num_files(); i++)
    {
        libtorrent::file_entry f = ti.file_at(i);
        int two_percent_bytes = (int)(0.02 * f.size);
        // Get the pieces for the byte offsets
        int first_start = ti.map_file(i, 0, 0).piece;
        int first_end = ti.map_file(i, two_percent_bytes, 0).piece;
        int last_start = ti.map_file(i, f.size - two_percent_bytes, 0).piece;
        int m = f.size - 1;
        if(m < 0)
            m = 0;
        int last_end = ti.map_file(i, m, 0).piece;

        first_end += 1;
        last_end += 1;

        // Set the pieces in our first and last ranges to priority 7
        // if they are not marked as do not download
        for(int j = first_start; j < first_end; j++)
            if(priorities[j] != 0)
                priorities[j] = 7;
        for(int j = last_start; j < last_end; j++)
            if(priorities[j] != 0)
                priorities[j] = 7;
    }
    // Setting the priorites for all the pieces of this torrent
    this->handle.prioritize_pieces(priorities.toStdVector());
}

void Torrent::set_auto_managed(bool auto_managed)
{
    this->options["auto_managed"] = auto_managed;
    if(!(this->handle.is_paused() && !this->handle.is_auto_managed()))
    {
        this->handle.auto_managed(auto_managed);
        this->update_state();
    }
}

void Torrent::set_shutdown_pc(bool shutdown_pc)
{
    this->options["shutdown_pc"] = shutdown_pc;
}

void Torrent::set_stop_ratio(float stop_ratio)
{
    this->options["stop_ratio"] = stop_ratio;
}

void Torrent::set_stop_at_ratio(bool stop_at_ratio)
{
    this->options["stop_at_ratio"] = stop_at_ratio;
}

void Torrent::set_remove_at_ratio(bool remove_at_ratio)
{
    this->options["remove_at_ratio"] = remove_at_ratio;
}

void Torrent::set_move_completed(bool move_completed)
{
    this->options["move_completed"] = move_completed;
}

void Torrent::set_move_completed_path(QString move_completed_path)
{
    this->options["move_completed_path"] = move_completed_path;
}

void Torrent::set_file_priorities(QVector<int> file_priorities)
{
    if(file_priorities.count() != this->files.length() || this->options["compact_allocation"].toBool())
    {
        this->options["file_priorities"] = QVariant::fromValue(QVector<int>::fromStdVector(this->handle.file_priorities()));
        return;
    }

    this->handle.prioritize_files(file_priorities.toStdVector());

    QVector<int> options_file_priorities;
    if(this->options["file_priorities"].canConvert<QVector<int> >())
        options_file_priorities = this->options["file_priorities"].value<QVector<int> >();
    else
        return;

    if(options_file_priorities.contains(0) || file_priorities.contains(0))
    {
        // We have previously or now marked a file 'Do Not Download'
        // Check to see if we have changed any 0's to >0 or any >0 to 0 and change state accordingly
        for(int index = 0; index < options_file_priorities.count(); index++)
        {
            if(options_file_priorities[index] == 0 && file_priorities[index] > 0)
            {
                this->change_file_attribute_and_rename_file(index, 128);
                this->is_finished = false;
                this->update_state();
            }
            else if(options_file_priorities[index] > 0 and file_priorities[index] == 0)
                this->change_file_attribute_and_rename_file(index, 2);
        }
    }

    this->options["file_priorities"] = QVariant::fromValue(file_priorities);

    // Set the first/last priorities if needed
    if(this->options["prioritize_first_last_pieces"].toBool())
        this->set_prioritize_first_last(this->options["prioritize_first_last_pieces"].toBool());
}

void Torrent::change_file_attribute_and_rename_file(int index, int attribute)
{
#ifdef Q_OS_WIN
    if(attribute == 2 || attribute == 128)
    {
        QString name = this->files[index]["path"].toString(); //.decode("utf8", "ignore")
        if((attribute == 2 && name.endsWith(".part")) ||
           (attribute == 2 && this->get_file_progress()[index] == 1.0) ||
           (attribute == 128 && !name.endsWith(".part")))
                return;
        //name = sanitize_filepath(name)
        QString filepath = this->options["download_location"].toString() + "\\" + name;
        filepath = filepath.replace("/", "\\");
        QFileInfo fi = QFileInfo(filepath);
        if(fi.exists())
        {
            int attr = GetFileAttributes(filepath.toStdWString().c_str());
            if(!(attr & attribute))
                SetFileAttributes(filepath.toStdWString().c_str(), attr | attribute);
        }
        if(attribute == 2)
            name += ".part";
        else
            name = name.remove(name.length()-5, 5);
        this->handle.rename_file(index, name.toStdWString());
    }
#endif
}

void Torrent::check_not_downloaded_files()
{
    QVector<int> options_file_priorities;
    if(this->options["file_priorities"].canConvert<QVector<int> >())
        options_file_priorities = this->options["file_priorities"].value<QVector<int> >();
    else
        return;
    if(options_file_priorities.contains(0))
    {
        for(int index = 0; index < options_file_priorities.count(); index++)
        {
            if(options_file_priorities[index] == 0)
            {
                if(this->get_file_progress()[index] == 1.0)
                    this->change_file_attribute_and_rename_file(index, 128);
                else
                    this->change_file_attribute_and_rename_file(index, 2);
            }
        }
    }
}

void Torrent::set_trackers(QList<QMap<QString, QVariant> > trackers)
{
        // Sets trackers
        if(trackers.isEmpty())
        {
            //QList<QMap<QString, QVariant> > trackers;
            foreach(libtorrent::announce_entry value, this->handle.trackers())
            {
                QMap<QString, QVariant> tracker;
                tracker["url"] = QString::fromStdString(value.url);
                tracker["tier"] = value.tier;
                trackers.append(tracker);
            }
            this->trackers = trackers;
            this->tracker_host = QString();
            return;
        }

        QVector<libtorrent::announce_entry> tracker_list;

        QMap<QString, QVariant> tracker;
        foreach(tracker, trackers)
        {
            libtorrent::announce_entry new_entry = libtorrent::announce_entry(tracker["url"].toString().toStdString());
            new_entry.tier = tracker["tier"].toInt();
            tracker_list.append(new_entry);
            tracker.clear();
        }
        this->handle.replace_trackers(tracker_list.toStdVector());

        this->trackers = trackers;
        if(trackers.length() > 0)
            // Force a reannounce if there is at least 1 tracker
            this->force_reannounce();

        this->tracker_host = QString();
}

void Torrent::set_save_path(QString save_path)
{
    this->options["download_location"] = save_path;
}

void Torrent::set_tracker_status(QString status)
{
    // Sets the tracker status
    this->tracker_status = this->get_tracker_host() + ": " + status;
}

void Torrent::update_state()
{
    // Updates the state based on what libtorrent's state for the torrent is

    // Set the initial state based on the lt state
    int ltstate = int(this->handle.status().state);
    // First we check for an error from libtorrent, and set the state to that
    // if any occurred.
    if(!this->handle.status().error.empty())
    {
        // This is an error'd torrent
        this->state = "Error";
        this->set_status_message(QString::fromStdString(this->handle.status().error));
        if(this->handle.is_paused())
            this->handle.auto_managed(false);
        return;
    }

    //if ltstate == LTSTATE["Queued"] or ltstate == LTSTATE["Checking"] or ltstate == LTSTATE["Checking Resume Data"]:
    /*if(ltstate == LT_TORRENT_STATE["Queued"])
    {
        this->state = "Checking";
        this->force_recheck();
        return;
    }*/
    if(ltstate == LT_TORRENT_STATE["Queued"] || ltstate == LT_TORRENT_STATE["Checking"]) //or ltstate == LT_TORRENT_STATE["Checking Resume Data"]:
    {
        if(this->handle.is_paused())
            this->state = "Paused";
        else
            this->state = "Checking";
        return;
    }
    else if(ltstate == LT_TORRENT_STATE["Downloading"] or ltstate == LT_TORRENT_STATE["Downloading Metadata"])
        this->state = "Downloading";
    else if(ltstate == LT_TORRENT_STATE["Finished"] or ltstate == LT_TORRENT_STATE["Seeding"])
    {
        this->is_finished = true;
        if(this->components->core->session->is_paused() or this->handle.is_paused())
            this->state = "Finished";
        else
            this->state = "Seeding";
    }
    else if(ltstate == LT_TORRENT_STATE["Allocating"])
        this->state = "Allocating";

    if(this->handle.is_paused() && this->handle.is_auto_managed() &&
            !this->components->core->session->is_paused())
        this->state = "Queued";
    else if(this->components->core->session->is_paused() or (this->handle.is_paused() && !this->handle.is_auto_managed()))
    {
        if(ltstate == LT_TORRENT_STATE["Seeding"] || ltstate == LT_TORRENT_STATE["Finished"])
            this->state = "Finished";
        else
            this->state = "Paused";
    }
}

void Torrent::set_state(QString state)
{
    // Accepts state strings, ie, "Paused", "Seeding", etc.
    if(!TORRENT_STATE.contains(state))
        return;

    this->state = state;
}

void Torrent::set_status_message(QString message)
{
    this->statusmsg = message;
}

float Torrent::get_eta()
{
    // Returns the ETA in seconds for this torrent
    libtorrent::torrent_status status = this->status;
    QSettings settings;
    if(this->is_finished && (this->options["stop_at_ratio"].toBool() ||
                             settings.value("coreconfig/stop_seed_at_ratio").toBool()))
    {
        // We're a seed, so calculate the time to the 'stop_share_ratio'
        if(!status.upload_payload_rate)
            return 0;
        float stop_ratio =0.0;
        if(settings.value("coreconfig/stop_seed_at_ratio").toBool())
            stop_ratio = settings.value("coreconfig/stop_seed_ratio").toFloat();
        else
            stop_ratio = this->options["stop_ratio"].toFloat();

        return (((float)status.all_time_download * stop_ratio) -
                (float)status.all_time_upload) / (float)status.upload_payload_rate;
    }

    float left =(float) status.total_wanted - (float)status.total_done;

    if(left <= 0 || status.download_payload_rate == 0)
        return 0;

    float eta = left / (float)status.download_payload_rate;

    return eta;
}

float Torrent::get_ratio()
{
    // Returns the ratio for this torrent

    libtorrent::torrent_status status = this->status;

    float downloaded = 0.0;
    if(status.all_time_download > 0)
        downloaded = (float)status.all_time_download;
    else if(status.total_done > 0)
        // We use 'total_done' if the downloaded value is 0
        downloaded = (float)status.total_done;
    else
        // Return -1.0 to signify infinity
        return -1.0;

    return (float)status.all_time_upload / downloaded;
}

QList<QMap<QString, QVariant> > Torrent::get_files()
{
    // Returns a list of files this torrent contains
    if(!this->handle.has_metadata())
        return QList<QMap<QString, QVariant> >();

    libtorrent::torrent_info torrent_info = this->handle.get_torrent_info();

    QList<QMap<QString, QVariant> > ret;
    libtorrent::file_storage files = torrent_info.files();
    //for(std::iterator file = files.begin(); file != files.end(); file++)
    for(int index = 0; index < files.num_files(); index++)
    {
        QMap<QString, QVariant> val;
        val["index"] = index;
        val["path"] = QString::fromStdString(files.at(index).path);
        val["size"] = files.at(index).size;
        val["offset"] = files.at(index).offset;
        ret.append(val);
    }
    return ret;
}

void Torrent::update_files_list()
{
    // Updates list of files for torrentless downloads
    this->files = this->get_files();
    if(!this->magnet_file_priorities.isEmpty())
        this->set_file_priorities(this->magnet_file_priorities);
    else
        this->set_file_priorities(QVector<int>());
}

QList<QMap<QString, QVariant> > Torrent::get_peers()
{
    // Returns a list of peers and various information about them
    QList<QMap<QString, QVariant> > ret;
    std::vector<libtorrent::peer_info> peers;
    this->handle.get_peer_info(peers);

    for(std::vector<libtorrent::peer_info>::iterator peer = peers.begin(); peer != peers.end(); peer++)
    {
        // We do not want to report peers that are half-connected
        if(peer->flags & peer->connecting || peer->flags & peer->handshake)
            continue;
        QString client = QString::fromStdString(peer->client);

        // Make country a proper string
        QString country;
        QChar c(peer->country[0]);
        if(c.isLetter())
            country.append(c);
        else
            country.append(" ");
        c = QChar(peer->country[1]);
        if(c.isLetter())
            country.append(c);
        else
            country.append(" ");
        QMap<QString, QVariant> val;
        val["client"] = client;
        val["country"] = country;
        val["down_speed"] = peer->payload_down_speed; //down_speed;
        val["ip"] = QString::fromStdString(peer->ip.address().to_string()) + ":" + QString::number(peer->ip.port());
        val["progress"] = peer->progress;
        val["seed"] = peer->flags & peer->seed;
        val["up_speed"] = peer->payload_up_speed; //up_speed;
        ret.append(val);
    }

    return ret;
}

int Torrent::get_queue_position()
{
    // Returns the torrents queue position
    return this->handle.queue_position();
}

QList<float> Torrent::get_file_progress()
{
    // Returns the file progress as a list of floats.. 0.0 -> 1.0
    if(!this->handle.has_metadata())
    {
        QList<float> val;
        val << 0.0;
        return val;
    }

    std::vector<libtorrent::size_type> file_progress;
    this->handle.file_progress(file_progress);
    QList<float> ret;
    QMap<QString, QVariant> f;
    foreach(f, this->files)
    {
        if(((int)f["size"].toFloat()) == 0)
            ret.append(0.0);
        else
            ret.append((float)file_progress[this->files.indexOf(f)] / f["size"].toFloat());
        f.clear();
    }

    return ret;
}

QString Torrent::get_tracker_host()
{
    // Returns just the hostname of the currently connected tracker
    // if no tracker is connected, it uses the 1st tracker.
    //if self.tracker_host:
    //    return self.tracker_host

    //if not self.status:
    //    self.status = self.handle.status() #(0xffffffff)

    QString tracker = QString::fromStdString(this->status.current_tracker);
    if(tracker.isEmpty() && !this->trackers.isEmpty())
        tracker = this->trackers[0]["url"].toString();

    if(!tracker.isEmpty())
    {
        std::string tracker_addr = tracker.replace("udp://", "http://").toStdString();
        http::url url = http::ParseHttpUrl(tracker_addr);
        QString host = QString::fromStdString(url.host);
        if(host.isEmpty())
            host = "DHT";
        // Check if hostname is an IP address and just return it if that's the case
        boost::system::error_code ec;
        boost::asio::ip::address::from_string(host.toStdString(), ec);
        if(!ec)
            return host;

        QStringList parts = host.split(".");
        if(parts.length() > 2)
        {
            QStringList doms;
            int parts_len = parts.length();
            doms << "co" << "com" << "net" << "org";
            if(doms.contains(parts[parts_len-2]) || parts[parts_len-1] == "uk")
            {
                QStringList host_parts;
                host_parts << parts.takeAt(parts_len-3) << parts.takeAt(parts_len-2) << parts.takeAt(parts_len-1);
                host = host_parts.join('.');
            }
            else
            {
                QStringList host_parts;
                host_parts << parts.takeAt(parts_len-2) << parts.takeAt(parts_len-1);
                host = host_parts.join('.');
            }
        }
        this->tracker_host = host;
        return host;
    }
    return QString();
}

QMap<QString, QVariant> Torrent::get_status(QList<QString> keys)
{
    // Returns the status of the torrent based on the keys provided

    // Create the full dictionary
    this->status = this->handle.status();

    // Adjust progress to be 0-100 value
    float progress = this->status.progress * 100.0;

    // Adjust status.distributed_copies to return a non-negative value
    float distributed_copies = this->status.distributed_copies;
    if(distributed_copies < 0)
        distributed_copies = 0.0;

    //if you add a key here->add it to core STATUS_KEYS too.
    QMap<QString, QVariant> full_status;
    full_status["active_time"] = this->status.active_time;
    full_status["all_time_download"] = this->status.all_time_download;
    full_status["compact"] = this->options["compact_allocation"];
    full_status["distributed_copies"] = distributed_copies;
    full_status["download_payload_rate"] = this->status.download_payload_rate;
    full_status["file_priorities"] = this->options["file_priorities"];
    full_status["files"] = QVariant::fromValue(this->files);
    full_status["hash"] = this->torrent_id;
    full_status["is_auto_managed"] = this->options["auto_managed"];
    full_status["shutdown_pc"] = this->options["shutdown_pc"];
    full_status["is_finished"] = this->is_finished;
    full_status["max_connections"] = this->options["max_connections"];
    full_status["max_download_speed"] = this->options["max_download_speed"];
    full_status["max_upload_slots"] = this->options["max_upload_slots"];
    full_status["max_upload_speed"] = this->options["max_upload_speed"];
    full_status["message"] = this->statusmsg;
    full_status["move_on_completed_path"] = this->options["move_completed_path"];
    full_status["move_on_completed"] = this->options["move_completed"];
    full_status["next_announce"] = this->status.next_announce.seconds();
    full_status["num_peers"] = this->status.num_peers - this->status.num_seeds;
    full_status["num_seeds"] = this->status.num_seeds;
    full_status["paused"] = this->status.paused;
    full_status["prioritize_first_last"] = this->options["prioritize_first_last_pieces"];
    full_status["progress"] = progress;
    full_status["remove_at_ratio"] = this->options["remove_at_ratio"];
    full_status["save_path"] = this->options["download_location"];
    full_status["seeding_time"] = this->status.seeding_time;
    full_status["seed_rank"] = this->status.seed_rank;
    full_status["state"] = this->state;
    full_status["stop_at_ratio"] = this->options["stop_at_ratio"];
    full_status["stop_ratio"] = this->options["stop_ratio"];
    full_status["time_added"] = this->time_added;
    full_status["total_done"] = this->status.total_done;
    full_status["total_payload_download"] = this->status.total_payload_download;
    full_status["total_payload_upload"] = this->status.total_payload_upload;
    full_status["total_peers"] = this->status.num_incomplete; //self.status.list_peers - self.status.list_seeds;
    full_status["total_seeds"] = this->status.num_complete; //self.status.list_seeds;
    full_status["total_uploaded"] = this->status.all_time_upload;
    full_status["total_wanted"] = this->status.total_wanted;
    full_status["tracker"] = QString::fromStdString(this->status.current_tracker);
    full_status["trackers"] = QVariant::fromValue(this->trackers);
    full_status["tracker_status"] = this->tracker_status;
    full_status["upload_payload_rate"] = this->status.upload_payload_rate;
    full_status["streaming"] = this->options["enable_streaming"];

    QString ti_comment;
    QString ti_name;
    bool ti_priv;
    int ti_total_size;
    int ti_num_files;
    int ti_num_pieces;
    int ti_piece_length;
    if(this->handle.has_metadata())
    {
        libtorrent::torrent_info torrent_info = this->handle.get_torrent_info();
        ti_comment = QString::fromStdString(torrent_info.comment());
        ti_name = QString::fromStdString(torrent_info.name());
        ti_priv = torrent_info.priv();
        ti_total_size = torrent_info.total_size();
        ti_num_files = torrent_info.num_files();
        ti_num_pieces = torrent_info.num_pieces();
        ti_piece_length = torrent_info.piece_length();
    }
    else
    {
        ti_comment = QString();
        if(this->magnet)
            ti_name = QString::fromStdString(this->handle.name());
        else
            ti_name = this->torrent_id;
        ti_priv = false;
        ti_total_size = 0;
        ti_num_files = 0;
        ti_num_pieces = 0;
        ti_piece_length = 0;
    }

    float ti_remaining = (float)(this->status.total_wanted - this->status.total_done);

    full_status["comment"] = ti_comment;
    full_status["eta"] = this->get_eta();
    full_status["file_progress"] = QVariant::fromValue(this->get_file_progress());
    full_status["is_seed"] = this->handle.is_seed();
    full_status["name"] = ti_name;
    full_status["num_files"] = ti_num_files;
    full_status["num_pieces"] = ti_num_pieces;
    full_status["peers"] = QVariant::fromValue(this->get_peers());
    full_status["piece_length"] = ti_piece_length;
    full_status["private"] = ti_priv;
    full_status["queue"] = this->handle.queue_position();
    full_status["ratio"] = this->get_ratio();
    full_status["total_size"] = ti_total_size;
    full_status["tracker_host"] = this->get_tracker_host();
    full_status["remaining"] = ti_remaining;

    // Create the desired status dictionary and return it
    QMap<QString, QVariant> status_dict;

    if(keys.isEmpty())
        return full_status;
    else
    {
        foreach(QString key, keys)
        {
            if(full_status.contains(key))
                status_dict[key] = full_status[key];
        }
    }

    return status_dict;
}

bool Torrent::pause()
{
    // Pause this torrent

    // Turn off auto-management so the torrent will not be unpaused by lt queueing
    this->handle.auto_managed(false);
    if(this->handle.is_paused())
    {
        // This torrent was probably paused due to being auto managed by lt
        // Since we turned auto_managed off, we should update the state which should
        // show it as 'Paused'.  We need to emit a torrent_paused signal because
        // the torrent_paused alert from libtorrent will not be generated.
        this->update_state();
        this->components->eventManager->emitTorrentStateChangedEvent(this->torrent_id, "Paused");
    }
    else
        this->handle.pause();

    return true;
}

bool Torrent::resume()
{
    // Resumes this torrent

    if(this->handle.is_paused() && this->handle.is_auto_managed())
        return false;
    else
    {
        // Reset the status message just in case of resuming an Error'd torrent
        this->set_status_message("OK");

        if(this->handle.is_finished())
        {
            // If the torrent has already reached it's 'stop_seed_ratio' then do not do anything
            QSettings settings;
            if(settings.value("coreconfig/stop_seed_at_ratio").toBool() || this->options["stop_at_ratio"].toBool())
            {
                float ratio = 0.0;
                if(this->options["stop_at_ratio"].toBool())
                    ratio = this->options["stop_ratio"].toFloat();
                else
                    ratio = settings.value("coreconfig/stop_seed_ratio").toFloat();

                if(this->get_ratio() >= ratio)
                    //XXX: This should just be returned in the RPC Response, no event
                    //self.signals.emit_event("torrent_resume_at_stop_ratio")
                    return false;
            }
        }

        if(this->options["auto_managed"].toBool())
        {
            // This torrent is to be auto-managed by lt queueing
            this->handle.auto_managed(true);
        }

        this->handle.resume();

        return true;
    }
}

bool Torrent::connect_peer(QString ip, int port)
{
    // adds manual peer
    try
    {
        boost::asio::ip::tcp::endpoint epoint(boost::asio::ip::address::from_string(ip.toStdString()), port);
        this->handle.connect_peer(epoint, 0);
    }
    catch(...)
    {
        return false;
    }
    return true;
}

bool Torrent::move_storage(QString dest)
{
    // Move a torrent's storage location
    if(dest.isEmpty())
        return false;
    this->handle.move_storage(dest.toStdString());
    return true;
}

void Torrent::enable_streaming()
{
    this->handle.set_sequential_download(true);
    this->options["enable_streaming"] = true;
}

void Torrent::set_piece_priority(int start, int end, int priority)
{
    for(int ind = start; ind < end; ind++)
        this->handle.piece_priority(ind, priority);
}

void Torrent::save_resume_data()
{
    // Signals libtorrent to build resume data for this torrent, it gets
    // returned in a libtorrent alert
    int state = this->handle.status().state;

    if(state == libtorrent::torrent_status::queued_for_checking ||
       state == libtorrent::torrent_status::checking_files ||
       state == libtorrent::torrent_status::checking_resume_data)
        return;
    this->handle.save_resume_data();
}

void Torrent::write_torrentfile()
{
    // Writes the torrent file
    if(this->handle.has_metadata())
    {
        QString path = QDir(get_default_config_dir()).absoluteFilePath("state") +
                QDir::separator() + this->torrent_id + ".torrent";

        libtorrent::torrent_info ti = this->handle.get_torrent_info();
        libtorrent::entry md = libtorrent::bdecode(&ti.metadata()[0], &ti.metadata()[0] + ti.metadata_size());

        libtorrent::entry torrent_file;
        torrent_file["info"] = md;

        std::vector<char> out;

        libtorrent::bencode(std::back_inserter(out), torrent_file);

        QFile f(path);

        if(!out.empty() && f.open(QIODevice::WriteOnly))
        {
          f.write(&out[0], out.size());
          f.close();
        }
    }
}

void Torrent::delete_torrentfile()
{
    // Deletes the .torrent file in the state
    /*QString path = QDir(get_config_dir()).absoluteFilePath("state") +
            QDir::separator() + this->torrent_id + ".torrent";
    QFile::remove(path);*/
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    QFile::remove(QDir(dir + QDir::separator() + "state").absoluteFilePath(this->torrent_id + ".torrent"));
}

bool Torrent::force_reannounce()
{
    // Force a tracker reannounce
    this->handle.force_reannounce();
    return true;
}

bool Torrent::scrape_tracker()
{
    // Scrape the tracker
    this->handle.scrape_tracker();
    return true;
}

bool Torrent::force_recheck()
{
    // Forces a recheck of the torrents pieces
    bool paused = this->handle.is_paused();
    this->handle.force_recheck();
    this->handle.resume();
    this->forcing_recheck = true;
    this->forcing_recheck_paused = paused;
    return true;
}

void Torrent::rename_files(QMap<int, QString> filenames)
{
    // Renames files in the torrent. 'filenames' should be a list of
    // (index, filename) pairs.
    foreach(int index, filenames.keys())
    {
        //filename = sanitize_filepath(filename)
        this->handle.rename_file(index, filenames[index].toStdWString());
    }
}

void Torrent::rename_file(int index, QString filename)
{
    this->handle.rename_file(index, filename.toStdWString());
}

void Torrent::rename_folder(QString folder, QString new_folder)
{
    // Renames a folder within a torrent.  This basically does a file rename
    // on all of the folders children.
    if(new_folder.isEmpty())
        return;

    //new_folder = sanitize_filepath(new_folder, folder=True)

    QList<QVariant> wait_on_folder;
    wait_on_folder << folder << new_folder << QVariant::fromValue(QList<int>());
    QMap<QString, QVariant> f;
    QList<int> tmp_list;
    foreach(f, this->get_files())
    {
        if(f["path"].toString().startsWith(folder))
        {
            // Keep a list of filerenames we're waiting on
            tmp_list.append(f["index"].toInt());
            this->handle.rename_file(f["index"].toInt(), f["path"].toString().replace(folder, new_folder).toStdWString());
        }
        f.clear();
    }
    wait_on_folder[2] = QVariant::fromValue<QList<int> >(tmp_list);
    this->waiting_on_folder_rename.append(wait_on_folder);
}
