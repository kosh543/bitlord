#include "core.h"
#include "components.h"

#include <QTime>

Core::Core(Components *comp)
{
    this->components = comp;

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    QSettings app_settings;

    this->session = new libtorrent::session(libtorrent::fingerprint("DE", 2, 3, 2, 250));
    this->set_random_port(app_settings.value("coreconfig/random_port").toBool());

    this->settings = new libtorrent::session_settings();
    this->settings->user_agent = "BitLord 2.3.2";
    this->settings->alert_queue_size = 10000;
    this->settings->send_redundant_have = true;
    //this->settings->disk_io_write_mode = this->settings->disable_os_cache;
    this->settings->disk_io_read_mode = this->settings->disable_os_cache;
    this->settings->optimize_hashing_for_speed = false;
    this->settings->max_sparse_regions = 0;
    // announce_to_all_tiers - slow connection
    this->settings->announce_to_all_tiers = true;
    //this->settings->announce_to_all_tiers = false;
    this->settings->announce_to_all_trackers = true;
    this->session->set_settings(*this->settings);

    this->session->add_extension(&libtorrent::create_metadata_plugin);
    this->session->add_extension(&libtorrent::create_ut_metadata_plugin);
    this->session->add_extension(&libtorrent::create_smart_ban_plugin);

    this->set_peer_tos(app_settings.value("coreconfig/peer_tos").toString());

    this->set_upnp(app_settings.value("coreconfig/upnp").toBool());
    this->set_natpmp(app_settings.value("coreconfig/natpmp").toBool());
    this->set_peer_exchange(app_settings.value("coreconfig/utpex").toBool());
    this->set_lsd(app_settings.value("coreconfig/lsd").toBool());
    this->set_dht(app_settings.value("coreconfig/dht").toBool(), false);
}

Core::~Core()
{
    delete this->settings;
    delete this->session;
}

int Core::get_random_int(int low, int high)
{
    return qrand() % ((high + 1) - low) + low;
}

void Core::set_listen_ports(int low, int high)
{
    QSettings app_settings;
    if(!app_settings.value("coreconfig/random_port").toBool())
        this->session->listen_on(std::make_pair(low, high), app_settings.value("coreconfig/listen_interface").toString().toStdString().c_str());
}

void Core::set_random_port(bool value)
{
    QSettings app_settings;
    int low = 6881;
    int high = 6889;
    if(value)
    {
        low = this->get_random_int(49152, 65525);
        high = low + 10;
    }
    else
    {
        QStringList settings_ports = app_settings.value("coreconfig/listen_ports").toStringList();
        if(settings_ports.count() == 2)
        {
            low = settings_ports[0].toInt();
            high = settings_ports[1].toInt();
        }
    }

    this->session->listen_on(std::make_pair(low, high), app_settings.value("coreconfig/listen_interface").toString().toStdString().c_str());
}

void Core::set_outgoing_ports(int low, int high)
{
    QSettings app_settings;
    if(!app_settings.value("coreconfig/random_outgoing_ports").toBool())
    {
        this->settings->outgoing_ports = std::make_pair(low, high);
        this->session->set_settings(*this->settings);
    }
}

void Core::set_random_outgoing_ports(bool value)
{
    if(value)
    {
        this->settings->outgoing_ports = std::make_pair(0, 0);
        this->session->set_settings(*this->settings);
    }
}

void Core::set_peer_tos(QString value)
{
    this->settings->peer_tos = (char)(value.toInt(0, 16));
    this->session->set_settings(*this->settings);
}

void Core::set_upnp(bool value)
{
    if(value)
        this->session->start_upnp();
    else
        this->session->stop_upnp();
}

void Core::set_natpmp(bool value)
{
    if(value)
        this->session->start_natpmp();
    else
        this->session->stop_natpmp();
}

void Core::set_peer_exchange(bool value)
{
    if(value)
        this->session->add_extension(&libtorrent::create_ut_pex_plugin);
}

void Core::set_lsd(bool value)
{
    if(value)
        this->session->start_lsd();
    else
        this->session->stop_lsd();
}

void Core::set_dht(bool value, bool save)
{
    if(value)
    {
        QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
        QString dir = QFileInfo(ini.fileName()).absolutePath();
        if(!QDir(dir).exists())
            QDir().mkdir(dir);
        QFile f(QDir(dir).absoluteFilePath("dht.state"));
        if(!f.open(QIODevice::ReadOnly))
        {
            f.setFileName(QDir(get_default_config_dir()).absoluteFilePath("dht.state"));
            if(!f.open(QIODevice::ReadOnly))
            {
                this->session->start_dht();
                return;
            }
        }

        std::vector<char> in;
        const qint64 content_size = f.bytesAvailable();
        in.resize(content_size);
        f.read(&in[0], f.size());
        libtorrent::entry state = libtorrent::bdecode(&in[0], &in[0]+in.size());
        f.close();

        try
        {
            this->session->start_dht(state);
        }
        catch(...)
        {
            this->session->start_dht();
        }

        this->session->add_dht_router(std::make_pair("router.bittorrent.com", 6881));
        this->session->add_dht_router(std::make_pair("router.utorrent.com", 6881));
        this->session->add_dht_router(std::make_pair("router.bitcomet.com", 6881));
    }
    else
    {
        if(save)
            this->save_dht_state();
        this->session->stop_dht();
    }
}

void Core::save_dht_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    QFile f(QDir(dir).absoluteFilePath("dht.state"));

    std::vector<char> out;

    libtorrent::bencode(std::back_inserter(out), this->session->dht_state());

    if(!out.empty() && f.open(QIODevice::WriteOnly))
    {
      f.write(&out[0], out.size());
      f.close();
    }
}

void Core::set_encryption()
{
    QSettings app_settings;
    libtorrent::pe_settings pe_settings;
    libtorrent::pe_settings old_pe_settings = this->session->get_pe_settings();

    switch(app_settings.value("coreconfig/enc_in_policy").toInt())
    {
    case 0:
        pe_settings.in_enc_policy = pe_settings.forced;
        break;
    case 1:
        pe_settings.in_enc_policy = pe_settings.enabled;
        break;
    case 2:
        pe_settings.in_enc_policy = pe_settings.disabled;
        break;
    default:
        pe_settings.in_enc_policy = old_pe_settings.in_enc_policy;
    }

    switch(app_settings.value("coreconfig/enc_out_policy").toInt())
    {
    case 0:
        pe_settings.out_enc_policy = pe_settings.forced;
        break;
    case 1:
        pe_settings.out_enc_policy = pe_settings.enabled;
        break;
    case 2:
        pe_settings.out_enc_policy = pe_settings.disabled;
        break;
    default:
        pe_settings.out_enc_policy = old_pe_settings.out_enc_policy;
    }

    switch(app_settings.value("coreconfig/enc_level").toInt())
    {
    case 0:
        pe_settings.allowed_enc_level = pe_settings.plaintext;
        break;
    case 1:
        pe_settings.allowed_enc_level = pe_settings.rc4;
        break;
    case 2:
        pe_settings.allowed_enc_level = pe_settings.both;
        break;
    default:
        pe_settings.allowed_enc_level = old_pe_settings.allowed_enc_level;
    }

    pe_settings.prefer_rc4 = app_settings.value("coreconfig/enc_prefer_rc4").toBool();

    this->session->set_pe_settings(pe_settings);
}

void Core::set_max_upload_speed(int value)
{
    int v;
    if(value < 0)
        v = -1;
    else
        v = value * 1024;

    this->settings->upload_rate_limit = v;
    this->session->set_settings(*this->settings);
}

void Core::set_max_download_speed(int value)
{
    int v;
    if(value < 0)
        v = -1;
    else
        v = value * 1024;

    this->settings->download_rate_limit = v;
    this->session->set_settings(*this->settings);
}

void Core::set_max_connections_per_second(int value)
{
    this->settings->connection_speed = value;
    this->session->set_settings(*this->settings);
}

void Core::set_ignore_limits_on_local_network(bool value)
{
    this->settings->ignore_limits_on_local_network = value;
    this->session->set_settings(*this->settings);
}

void Core::set_rate_limit_ip_overhead(bool value)
{
    this->settings->rate_limit_ip_overhead = value;
    this->session->set_settings(*this->settings);
}

void Core::set_share_ratio_limit(float value)
{
    this->settings->share_ratio_limit = value;
    this->session->set_settings(*this->settings);
}

void Core::set_seed_time_ratio_limit(float value)
{
    this->settings->seed_time_ratio_limit = value;
    this->session->set_settings(*this->settings);
}

void Core::set_seed_time_limit(int value)
{
    this->settings->seed_time_limit = value * 60;
    this->session->set_settings(*this->settings);
}

void Core::set_max_active_downloading(int value)
{
    this->settings->active_downloads = value;
    this->session->set_settings(*this->settings);
}

void Core::set_max_active_seeding(int value)
{
    this->settings->active_seeds = value;
    this->session->set_settings(*this->settings);
}

void Core::set_max_active_limit(int value)
{
    this->settings->active_limit = value;
    this->session->set_settings(*this->settings);
}

void Core::set_dont_count_slow_torrents(bool value)
{
    this->settings->dont_count_slow_torrents = value;
    this->session->set_settings(*this->settings);
}
