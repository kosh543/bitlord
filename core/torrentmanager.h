#ifndef TORRENTMANAGER_H
#define TORRENTMANAGER_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QVector>
#include <QList>
#include <QMap>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QUrl>
#include <QEventLoop>
#include <QXmlStreamReader>
#include <QTimer>

#include "libtorrent/session.hpp"
#include "libtorrent/magnet_uri.hpp"

#include "core/torrent.h"
#include "core/torrentstate.h"
#include "core/torrentoptions.h"
#include "core/cpickleparser.h"

class Components;

class TorrentManager : public QObject
{
    Q_OBJECT
private:
    Components *components;
    libtorrent::session *session;
    QTimer *save_state_timer;
public:
    QMap<QString, Torrent*> torrents;
    libtorrent::entry resume_data;
    QList<QString> waiting_on_resume_data;
    bool need_save_resume_data_for_all;
    explicit TorrentManager(Components *comp, QObject *parent = 0);
    ~TorrentManager();
    void start();
    void stop();
    void update();
    QList<QString> get_torrent_list();
    libtorrent::torrent_info* get_torrent_info_from_file(QString filepath);
    QList<float> get_torrent_files_progress(QString torrent_id);
    std::vector<char>* legacy_get_resume_data_from_file(QString torrent_id);
    void legacy_delete_resume_data(QString torrent_id);
    QList<QString> get_additional_trackers(QString infohash);
    QString add(libtorrent::torrent_info *torrent_info, TorrentState *state, TorrentOptions *options, bool save_state,
                QVector<char> *filedump, QString *filename, QString *magnet, std::vector<char> *resume_data,
                QVector<int> magnet_file_priorities=QVector<int>());
    libtorrent::torrent_info* load_torrent(QString torrent_id);
    bool remove(QString torrent_id, bool remove_data=false);
    void load_state();
    void save_state();
    void save_resume_data(QList<QString> torrent_ids=QList<QString>());
    libtorrent::entry load_resume_data_file();
    void save_resume_data_file();
    bool queue_top(QString torrent_id);
    bool queue_up(QString torrent_id);
    bool queue_down(QString torrent_id);
    bool queue_bottom(QString torrent_id);
    Torrent* operator[](QString key);
    void open_folder_for_selected();
    void pause_selected();
    void resume_selected();
    void set_download_speed_for_selected(int speed);
    void set_upload_speed_for_selected(int speed);
    void set_connection_for_selected(int connection);
    void set_upload_slot_for_selected(int upload_slots);
    void set_auto_managed_for_selected(bool auto_manage);
    void set_max_connections_per_torrent(int value);
    void set_max_upload_slots_per_torrent(int value);
    void set_max_upload_speed_per_torrent(int value);
    void set_max_download_speed_per_torrent(int value);
    void queue_top_selected();
    void queue_up_selected();
    void queue_down_selected();
    void queue_bottom_selected();
    void force_reannounce_selected();
    void remove_selected();
    void force_recheck_selected();
    void move_storage_for_selected(QString folder);
signals:

public slots:
    void save_state_timer_tick();
    void on_set_max_connections_per_torrent(QString key, QVariant value);
    void on_set_max_upload_slots_per_torrent(QString key, QVariant value);
    void on_set_max_upload_speed_per_torrent(QString key, QVariant value);
    void on_set_max_download_speed_per_torrent(QString key, QVariant value);
    void on_fastresume_rejected_alert(libtorrent::alert* alert);
    void on_alert_torrent_finished(libtorrent::alert* alert);
    void on_alert_torrent_paused(libtorrent::alert* alert);
    void on_alert_torrent_checked(libtorrent::alert* alert);
    void on_alert_tracker_reply(libtorrent::alert* alert);
    void on_alert_tracker_announce(libtorrent::alert* alert);
    void on_alert_tracker_warning(libtorrent::alert* alert);
    void on_alert_tracker_error(libtorrent::alert* alert);
    void on_alert_storage_moved(libtorrent::alert* alert);
    void on_alert_torrent_resumed(libtorrent::alert* alert);
    void on_alert_state_changed(libtorrent::alert* alert);
    void on_alert_save_resume_data(libtorrent::alert* alert);
    void on_alert_save_resume_data_failed(libtorrent::alert* alert);
    void on_alert_file_renamed(libtorrent::alert* alert);
    void on_alert_metadata_received(libtorrent::alert* alert);
    void on_alert_file_error(libtorrent::alert* alert);
};

#endif // TORRENTMANAGER_H
