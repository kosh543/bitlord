#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include <QObject>
#include <QString>
#include <QVariant>

class EventManager : public QObject
{
    Q_OBJECT
public:
    explicit EventManager(QObject *parent = 0);
    void emitSessionStartedEvent();
    void emitNewVersionAvailableEvent(QString new_release, QString new_installer_url, QString changelog);
    void emitSessionResumedEvent();
    void emitTorrentQueueChangedEvent();
    void emitPluginEnabledEvent(QString name);
    void emitPluginDisabledEvent(QString name);
    void emitConfigValueChangedEvent(QString key, QVariant value);
    void emitTorrentStateChangedEvent(QString torrent_id, QString state);
    void emitTorrentAddedEvent(QString torrent_id); //, feed_id);
    void emitPreTorrentRemovedEvent(QString torrent_id);
    void emitTorrentRemovedEvent(QString torrent_id);
    void emitTorrentFinishedEvent(QString torrent_id);
    void emitTorrentResumedEvent(QString torrent_id);
    void emitTorrentFolderRenamedEvent(QString torrent_id, QString old_name, QString new_name);
    void emitTorrentFileRenamedEvent(QString torrent_id, int index, QString name);

signals:
    void SessionStartedEvent();
    void NewVersionAvailableEvent(QString new_release, QString new_installer_url, QString changelog);
    void SessionResumedEvent();
    void TorrentQueueChangedEvent();
    void PluginEnabledEvent(QString name);
    void PluginDisabledEvent(QString name);
    void ConfigValueChangedEvent(QString key, QVariant value);
    void TorrentStateChangedEvent(QString torrent_id, QString state);
    void TorrentAddedEvent(QString torrent_id); //, feed_id);
    void PreTorrentRemovedEvent(QString torrent_id);
    void TorrentRemovedEvent(QString torrent_id);
    void TorrentFinishedEvent(QString torrent_id);
    void TorrentResumedEvent(QString torrent_id);
    void TorrentFolderRenamedEvent(QString torrent_id, QString old_name, QString new_name);
    void TorrentFileRenamedEvent(QString torrent_id, int index, QString name);

public slots:

};

#endif // EVENTMANAGER_H
