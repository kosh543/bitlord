#include "downloadingdialog.h"

#include <QVBoxLayout>
#include <QTemporaryFile>
#include <QByteArray>
#include <QDir>
#include <QDebug>
DownloadingDialog::DownloadingDialog(QUrl url, QString fileExtension, QWidget *parent) :
    QDialog(parent)
{
    this->url = url;
    this->fileExtension = fileExtension;

    this->networkAccessManager = new QNetworkAccessManager(this);
    this->networkReply = NULL;
    this->connect(this->networkAccessManager, SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )),
                this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));

    this->downloadingProgress = new QProgressBar(this);
    this->downloadingProgress->setMaximum(100);
    this->downloadingProgress->setValue(0);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(this->downloadingProgress);

    this->setWindowTitle("Downloading...");
    this->setLayout(mainLayout);
}

void DownloadingDialog::show_downloading_dialog()
{
    this->show();
    this->getReply(this->url);
}

void DownloadingDialog::getReply(QUrl url)
{
    if(this->networkReply)
    {
        this->networkReply->disconnect(this);
        this->networkReply->deleteLater();
    }
    QNetworkRequest request(url);
    this->networkReply = this->networkAccessManager->get(request);
    this->connect(this->networkReply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));
    this->connect(this->networkReply, SIGNAL(finished()), this, SLOT(downloadingFinished()));
}

void DownloadingDialog::metaDataChanged()
{
    QUrl redirectionTarget = this->networkReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    if (redirectionTarget.isValid())
        this->getReply(redirectionTarget);
}

void DownloadingDialog::downloadProgress(qint64 current, qint64 total)
{
    if(total != 0)
        this->downloadingProgress->setValue((int)(((float)current/(float)total)*100.0));
}

void DownloadingDialog::downloadingFinished()
{
    QTemporaryFile temp_torrent(QDir(QDir::tempPath()).absoluteFilePath("XXXXXXtmp." + this->fileExtension));
    temp_torrent.setAutoRemove(false);
    if(temp_torrent.open())
    {
        QByteArray data = this->networkReply->readAll();
        temp_torrent.write(data);
        temp_torrent.close();
        emit this->fileDownloaded(temp_torrent.fileName());
    }
    this->close();
    this->parent()->disconnect(this);
    this->deleteLater();
}

void DownloadingDialog::handleSslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
    Q_UNUSED(errors)
    reply->ignoreSslErrors();
}
