#ifndef TORRENTSTATE_H
#define TORRENTSTATE_H

#include <QString>
#include <QVariant>
#include <QVector>
#include <QList>
#include <QMap>

class TorrentState
{
public:
    QString torrent_id;
    QString filename;
    float total_uploaded;
    QList<QMap<QString, QVariant> >trackers;
    bool compact;
    bool paused;
    QString save_path;
    int max_connections;
    int max_upload_slots;
    float max_upload_speed;
    float max_download_speed;
    bool prioritize_first_last;
    QVector<int> file_priorities;
    int queue;
    bool auto_managed;
    bool shutdown_pc;
    bool is_finished;
    float stop_ratio;
    bool stop_at_ratio;
    bool remove_at_ratio;
    bool move_completed;
    QString move_completed_path;
    QString magnet;
    uint time_added;
    bool streaming;

    TorrentState()
    {
        this->torrent_id = QString();
        this->filename = QString();
        this->total_uploaded = 0;
        this->trackers = QList<QMap<QString, QVariant> >();
        this->compact = false;
        this->paused = false;
        this->save_path = QString();
        this->max_connections = -1;
        this->max_upload_slots = -1;
        this->max_upload_speed = -1.0;
        this->max_download_speed = -1.0;
        this->prioritize_first_last = false;
        this->file_priorities = QVector<int>();
        this->queue = -1;
        this->auto_managed = true;
        this->shutdown_pc = false;
        this->is_finished = false;
        this->stop_ratio = 2.00;
        this->stop_at_ratio = false;
        this->remove_at_ratio = false;
        this->move_completed = false;
        this->move_completed_path = QString();
        this->magnet = QString();
        this->time_added = -1;
        this->streaming = false;
    }

    TorrentState(QString torrent_id, QString filename, float total_uploaded,
                 QList<QMap<QString, QVariant> >trackers, bool compact, bool paused,
                 QString save_path, int max_connections, int max_upload_slots, float max_upload_speed,
                 float max_download_speed, bool prioritize_first_last, QVector<int> file_priorities,
                 int queue, bool auto_managed, bool shutdown_pc, bool is_finished,
                 float stop_ratio, bool stop_at_ratio, bool remove_at_ratio, bool move_completed,
                 QString move_completed_path, QString magnet, uint time_added, bool streaming)
    {
        this->torrent_id = torrent_id;
        this->filename = filename;
        this->total_uploaded = total_uploaded;
        this->trackers = trackers;
        this->queue = queue;
        this->is_finished = is_finished;
        this->magnet = magnet;
        this->time_added = time_added;

        // Options
        this->compact = compact;
        this->paused = paused;
        this->save_path = save_path.replace("\\\\", "\\");
        this->max_connections = max_connections;
        this->max_upload_slots = max_upload_slots;
        this->max_upload_speed = max_upload_speed;
        this->max_download_speed = max_download_speed;
        this->prioritize_first_last = prioritize_first_last;
        this->file_priorities = file_priorities;
        this->auto_managed = auto_managed;
        this->shutdown_pc = shutdown_pc;
        this->stop_ratio = stop_ratio;
        this->stop_at_ratio = stop_at_ratio;
        this->remove_at_ratio = remove_at_ratio;
        this->move_completed = move_completed;
        this->move_completed_path = move_completed_path;
        this->streaming = streaming;
    }
};

#endif // TORRENTSTATE_H
