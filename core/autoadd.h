#ifndef AUTOADD_H
#define AUTOADD_H

#include <QObject>
#include <QTimer>

class Components;

class AutoAdd : public QObject
{
    Q_OBJECT
private:
    Components *components;
    QTimer *update_autoadd_timer;

    void resume_autoadd();
    void pause_autoadd();

public:
    explicit AutoAdd(Components *comp, QObject *parent = 0);
    void set_autoadd_enable(bool value);
    void set_autoadd_location();

private slots:
    void update_autoadd();

};

#endif // AUTOADD_H
