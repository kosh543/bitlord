#include "eventmanager.h"

EventManager::EventManager(QObject *parent) :
    QObject(parent)
{
}

void EventManager::emitSessionStartedEvent()
{
    emit this->SessionStartedEvent();
}

void EventManager::emitSessionResumedEvent()
{
    emit this->SessionResumedEvent();
}

void EventManager::emitNewVersionAvailableEvent(QString new_release, QString new_installer_url, QString changelog)
{
    emit this->NewVersionAvailableEvent(new_release, new_installer_url, changelog);
}

void EventManager::emitTorrentQueueChangedEvent()
{
    emit this->TorrentQueueChangedEvent();
}

void EventManager::emitPluginEnabledEvent(QString name)
{
    emit this->PluginEnabledEvent(name);
}

void EventManager::emitPluginDisabledEvent(QString name)
{
    emit this->PluginDisabledEvent(name);
}

void EventManager::emitConfigValueChangedEvent(QString key, QVariant value)
{
    emit this->ConfigValueChangedEvent(key, value);
}

void EventManager::emitTorrentStateChangedEvent(QString torrent_id, QString state)
{
    emit this->TorrentStateChangedEvent(torrent_id, state);
}

void EventManager::emitTorrentAddedEvent(QString torrent_id) //, feed_id)
{
    emit this->TorrentAddedEvent(torrent_id); //, feed_id);
}

void EventManager::emitPreTorrentRemovedEvent(QString torrent_id)
{
    emit this->PreTorrentRemovedEvent(torrent_id);
}

void EventManager::emitTorrentRemovedEvent(QString torrent_id)
{
    emit this->TorrentRemovedEvent(torrent_id);
}

void EventManager::emitTorrentFinishedEvent(QString torrent_id)
{
    emit this->TorrentFinishedEvent(torrent_id);
}

void EventManager::emitTorrentResumedEvent(QString torrent_id)
{
    emit this->TorrentResumedEvent(torrent_id);
}

void EventManager::emitTorrentFolderRenamedEvent(QString torrent_id, QString old_name, QString new_name)
{
    emit this->TorrentFolderRenamedEvent(torrent_id, old_name, new_name);
}

void EventManager::emitTorrentFileRenamedEvent(QString torrent_id, int index, QString name)
{
    emit this->TorrentFileRenamedEvent(torrent_id, index, name);
}
