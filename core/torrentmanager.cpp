#include "torrentmanager.h"
#include "components.h"
#include "common.h"

#include <QSettings>
#include <QDesktopServices>

TorrentManager::TorrentManager(Components *comp, QObject *parent) :
    QObject(parent)
{
    this->components = comp;
    // Set the libtorrent session
    this->session = this->components->core->session;

    // Make sure the state folder has been created
    if(!QDir(get_default_config_dir()+QDir::separator()+"state").exists())
        QDir().mkdir(get_default_config_dir()+QDir::separator()+"state");

    // Create the torrents dict { torrent_id: Torrent }
    this->torrents = QMap<QString, Torrent*>();

    // This is a map of torrent_ids to Deferreds used to track needed resume data.
    // The Deferreds will be completed when resume data has been saved.
    this->waiting_on_resume_data = QList<QString>();

    // Keeps track of resume data
    //this->resume_data = NULL;

    this->need_save_resume_data_for_all = false;

    // Register set functions
    this->connect(this->components->configEvents, SIGNAL(max_connections_per_torrent_signal(QString,QVariant)),
                  this, SLOT(on_set_max_connections_per_torrent(QString,QVariant)));
    this->connect(this->components->configEvents, SIGNAL(max_upload_slots_per_torrent_signal(QString,QVariant)),
                  this, SLOT(on_set_max_upload_slots_per_torrent(QString,QVariant)));
    this->connect(this->components->configEvents, SIGNAL(max_upload_speed_per_torrent_signal(QString,QVariant)),
                  this, SLOT(on_set_max_upload_speed_per_torrent(QString,QVariant)));
    this->connect(this->components->configEvents, SIGNAL(max_download_speed_per_torrent_signal(QString,QVariant)),
                  this, SLOT(on_set_max_download_speed_per_torrent(QString,QVariant)));

    // Register alert functions
    this->connect(this->components->alertManager, SIGNAL(fastresume_rejected_signal(libtorrent::alert*)),
                  this, SLOT(on_fastresume_rejected_alert(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(torrent_finished_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_torrent_finished(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(torrent_paused_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_torrent_paused(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(torrent_checked_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_torrent_checked(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(tracker_reply_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_tracker_reply(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(tracker_announce_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_tracker_announce(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(tracker_warning_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_tracker_warning(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(tracker_error_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_tracker_error(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(storage_moved_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_storage_moved(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(torrent_resumed_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_torrent_resumed(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(state_changed_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_state_changed(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(save_resume_data_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_save_resume_data(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(save_resume_data_failed_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_save_resume_data_failed(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(file_renamed_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_file_renamed(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(metadata_received_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_metadata_received(libtorrent::alert*)));
    this->connect(this->components->alertManager, SIGNAL(file_error_signal(libtorrent::alert*)),
                  this, SLOT(on_alert_file_error(libtorrent::alert*)));

    this->save_state_timer = new QTimer(this);
    this->save_state_timer->setInterval(200000);
    this->connect(this->save_state_timer, SIGNAL(timeout()), this, SLOT(save_state_timer_tick()));
}

TorrentManager::~TorrentManager()
{
    foreach(QString key, this->torrents.keys())
        delete this->torrents[key];
}

void TorrentManager::start()
{
    this->load_state();

    this->save_state_timer->start();
}

void TorrentManager::stop()
{
    if(this->save_state_timer->isActive())
        this->save_state_timer->stop();

    this->save_state();
}

void TorrentManager::update()
{
    QSettings settings;
    foreach(QString torrent_id, this->torrents.keys())
    {
        if(settings.value("coreconfig/stop_seed_at_ratio").toBool() ||
           this->torrents[torrent_id]->options["stop_at_ratio"].toBool() &&
           this->torrents[torrent_id]->state != "Checking" && this->torrents[torrent_id]->state != "Allocating")
        {
            // If the global setting is set, but the per-torrent isn't.. Just skip to the next torrent
            // This is so that a user can turn-off the stop at ratio option on a per-torrent basis
            if(settings.value("coreconfig/stop_seed_at_ratio").toBool() &&
               !this->torrents[torrent_id]->options["stop_at_ratio"].toBool())
                continue;
            float stop_ratio = settings.value("coreconfig/stop_seed_ratio").toFloat();
            if(this->torrents[torrent_id]->options["stop_at_ratio"].toBool())
                stop_ratio = this->torrents[torrent_id]->options["stop_ratio"].toFloat();
            if(this->torrents[torrent_id]->get_ratio() >= stop_ratio && this->torrents[torrent_id]->is_finished)
            {
                if(settings.value("coreconfig/stop_seed_at_ratio").toBool() ||
                   this->torrents[torrent_id]->options["remove_at_ratio"].toBool())
                {
                    this->remove(torrent_id);
                    break;
                }
                if(!this->torrents[torrent_id]->handle.is_paused())
                    this->torrents[torrent_id]->pause();
            }
        }
    }
}

QList<QString> TorrentManager::get_torrent_list()
{
    // Returns a list of torrent_ids
    return this->torrents.keys();
}

libtorrent::torrent_info* TorrentManager::get_torrent_info_from_file(QString filepath)
{
    // Returns a torrent_info for the file specified or None
    libtorrent::torrent_info *torrent_info = NULL;
    // Get the torrent data from the torrent file
    QFile f(filepath);
    if(f.open(QIODevice::ReadOnly))
    {
        std::vector<char> in;
        const qint64 content_size = f.bytesAvailable();
        in.resize(content_size);
        f.read(&in[0], f.size());
        torrent_info = new libtorrent::torrent_info(libtorrent::bdecode(&in[0], &in[0]+in.size()));
        f.close();
    }

    return torrent_info;
}

QList<float> TorrentManager::get_torrent_files_progress(QString torrent_id)
{
    return this->torrents[torrent_id]->get_file_progress();
}

std::vector<char>* TorrentManager::legacy_get_resume_data_from_file(QString torrent_id)
{
    // Returns an entry with the resume data or None
    std::vector<char> *fastresume = NULL;
    QFile f(QDir(get_default_config_dir() + QDir::separator() + "state").absoluteFilePath(torrent_id + ".fastresume"));
    if(f.open(QIODevice::ReadOnly))
    {
        const QByteArray content = f.readAll();
        const int content_size = content.size();
        fastresume = new std::vector<char>();
        fastresume->resize(content_size);
        memcpy(&((*fastresume)[0]), content.data(), content_size);
        f.close();
    }

    return fastresume;
}

void TorrentManager::legacy_delete_resume_data(QString torrent_id)
{
    // Deletes the .fastresume file
    QString path = QDir(get_default_config_dir() + QDir::separator() + "state").absoluteFilePath(torrent_id + ".fastresume");
    QFile::remove(path);
}

QList<QString> TorrentManager::get_additional_trackers(QString infohash)
{
    QUrl url("http://www.bitlordsearch.com/trackers/trackers.php?hash=" + infohash);

    QNetworkAccessManager *nam = new QNetworkAccessManager(this);

    QEventLoop loop;
    QObject::connect(nam, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

    QTimer timer;
    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(3000);
    QNetworkReply *reply = nam->get(QNetworkRequest(url));
    loop.exec();
    if (!timer.isActive() || reply->error() != QNetworkReply::NoError)
    {
        return QList<QString>();
    }
    timer.stop();

    QString xmlData = QString(reply->readAll());
    QXmlStreamReader  xml(xmlData);
    QList<QString> trackers;
    while(!xml.atEnd() && !xml.hasError())
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if(token == QXmlStreamReader::StartElement && xml.name() == "tracker_name")
        {
            xml.readNext();
            if(xml.tokenType() != QXmlStreamReader::Characters)
                continue;
            trackers.append(xml.text().toString());
        }
    }
    xml.clear();
    return trackers;
}

QString TorrentManager::add(libtorrent::torrent_info *torrent_info, TorrentState *state, TorrentOptions *options, bool save_state,
                            QVector<char> *filedump, QString *filename, QString *magnet, std::vector<char> *resume_data,
                            QVector<int> magnet_file_priorities)
// torrent_info=None, state=None, options=None, save_state=True,
//filedump=None, filename=None, magnet=None, resume_data=None, feed_id=None, magnet_file_priorities=None):
{
    // Add a torrent to the manager and returns it's torrent_id
    if(!torrent_info && !state && !filedump && !magnet)
        return QString();

    libtorrent::add_torrent_params add_torrent_params;
    std::string rd_temp = "";

    if(filedump)
    {
        if(!filedump->isEmpty())
        {
            try
            {
                std::vector<char> in = filedump->toStdVector();
                //torrent_info = lt.torrent_info(lt.bdecode(filedump))
                torrent_info = new libtorrent::torrent_info(libtorrent::bdecode(&in[0], &in[0]+in.size()));
            }
            catch(...)
            {
                return QString();
            }
        }
    }

    if(!torrent_info && state)
    {
        // We have no torrent_info so we need to add the torrent with information
        // from the state object.

        // Populate the options dict from state
        options = new TorrentOptions();
        (*options)["max_connections"] = state->max_connections;
        (*options)["max_upload_slots"] = state->max_upload_slots;
        (*options)["max_upload_speed"] = state->max_upload_speed;
        (*options)["max_download_speed"] = state->max_download_speed;
        (*options)["prioritize_first_last_pieces"] = state->prioritize_first_last;
        (*options)["file_priorities"] = QVariant::fromValue<QVector<int> >(state->file_priorities);
        (*options)["compact_allocation"] = state->compact;
        (*options)["download_location"] = state->save_path;
        (*options)["auto_managed"] = state->auto_managed;
        (*options)["shutdown_pc"] = state->shutdown_pc;
        (*options)["stop_at_ratio"] = state->stop_at_ratio;
        (*options)["stop_ratio"] = state->stop_ratio;
        (*options)["remove_at_ratio"] = state->remove_at_ratio;
        (*options)["move_completed"] = state->move_completed;
        (*options)["move_completed_path"] = state->move_completed_path;
        (*options)["add_paused"] = state->paused;
        (*options)["enable_streaming"] = state->streaming;

        QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
        QString dir = QFileInfo(ini.fileName()).absolutePath();
        if(state->magnet.isEmpty() || QFile::exists(QDir(dir + QDir::separator() +
                                        "state").absoluteFilePath(state->torrent_id + ".torrent"))
                                   || QFile::exists(QDir(get_default_config_dir() + QDir::separator() +
                                        "state").absoluteFilePath(state->torrent_id + ".torrent")))
        {
            if(QFile::exists(QDir(dir + QDir::separator() +
               "state").absoluteFilePath(state->torrent_id + ".torrent")))
                    add_torrent_params.ti = this->get_torrent_info_from_file(QDir(dir + QDir::separator() +
                                                                "state").absoluteFilePath(state->torrent_id + ".torrent"));
            else
                    add_torrent_params.ti = this->get_torrent_info_from_file(QDir(get_default_config_dir() + QDir::separator() +
                                                        "state").absoluteFilePath(state->torrent_id + ".torrent"));

            if(!add_torrent_params.ti)
                return QString();
        }
        else
            magnet = new QString(state->magnet);

        // Handle legacy case with storing resume data in individual files
        // for each torrent
        if(!resume_data)
        {
            resume_data = this->legacy_get_resume_data_from_file(state->torrent_id);
            this->legacy_delete_resume_data(state->torrent_id);
        }

        if(resume_data)
        {
            add_torrent_params.resume_data = resume_data;
            rd_temp = std::string(resume_data->begin(), resume_data->end());
        }
    }
    else
    {
        // We have a torrent_info object so we're not loading from state.
        // Check if options is None and load defaults
        if(!options)
            options = new TorrentOptions();
        else
        {
            TorrentOptions* o = new TorrentOptions();
            QMap<QString, QVariant> qmap = options->get_qmap();
            o->updateOptions(qmap);
            options = o;
        }

        // Check for renamed files and if so, rename them in the torrent_info
        // before adding to the session.
        QMap<int, QString> m = (*options)["mapped_files"].value<QMap<int, QString> >();
        if(!m.isEmpty())
        {
            foreach(int index, m.keys())
            {
                torrent_info->rename_file(index, m[index].toStdString());
            }
        }

        add_torrent_params.ti = torrent_info;
        //add_torrent_params.resume_data = "";
    }

    // HACK for some reason libtorrent_utp crashes with empty resume data, so just remove it
    //if len(add_torrent_params['resume_data']) == 0:
    //    del add_torrent_params['resume_data']

    // Set the right storage_mode
    libtorrent::storage_mode_t storage_mode;
    if((*options)["compact_allocation"].toBool())
        storage_mode = libtorrent::storage_mode_compact;
    else
        storage_mode = libtorrent::storage_mode_allocate;

    // Fill in the rest of the add_torrent_params dictionary
    add_torrent_params.save_path = (*options)["download_location"].toString().toStdString();
    add_torrent_params.storage_mode = storage_mode;
    add_torrent_params.paused = true;
    add_torrent_params.auto_managed = false;
    add_torrent_params.duplicate_is_error = true;

    // We need to pause the AlertManager momentarily to prevent alerts
    // for this torrent being generated before a Torrent object is created.
    this->components->alertManager->pause();

    libtorrent::torrent_handle handle;
    bool handle_added = false;

    if(magnet)
    {
        //std::string m = magnet->toStdString();
        handle = libtorrent::add_magnet_uri(*(this->session), magnet->toStdString(), add_torrent_params);
        handle_added = true;
    }
    else
    {
        handle = this->session->add_torrent(add_torrent_params);
        handle_added = true;
    }

    if(resume_data)
    {
        delete resume_data;
        resume_data = 0;
    }

    //try:
    //    component.get("ToggleAnnounce").toggle_off(filename)
    //    component.get("ToggleAnnounce").toggle_on(handle, filename)
    //except Exception, e:
    //    print e

    if(!handle_added || !handle.is_valid())
    {
        // The torrent was not added to the session
        this->components->alertManager->resume();
        return QString();
    }

    // downloading like a stream
    if((*options)["enable_streaming"].toBool())
        handle.set_sequential_download(true);

    // Set auto_managed to False because the torrent is paused
    handle.auto_managed(false);
    // Create a Torrent object
    Torrent *torrent = new Torrent(this->components, handle, options, state, filename, magnet, magnet_file_priorities);
    // Add trackers from BitlordSearch
    if((filename || magnet) && !state)
    {
        QList<QMap<QString, QVariant> > trackers = torrent->trackers;
        int tier;
        if(!trackers.isEmpty())
            tier = trackers.last()["tier"].toInt();
        else
            tier = -1;

        foreach(QString new_tracker, this->get_additional_trackers(info_hash_to_string(handle.info_hash())))
        {
            QMap<QString, QVariant> t;
            bool already_have = false;
            foreach(t, trackers)
            {
                if(t["url"].toString() == new_tracker)
                {
                    already_have = true;
                    break;
                }
            }
            if(already_have)
                continue;

            tier += 1;
            QMap<QString, QVariant> val;
            val["tier"] = tier;
            val["url"] = new_tracker;
            trackers.append(val);
        }
        torrent->set_trackers(trackers);
    }
    // Add the torrent object to the dictionary
    this->torrents[torrent->torrent_id] = torrent;
    if(resume_data)
        this->resume_data[torrent->torrent_id.toStdString()] = rd_temp;
    QSettings settings;
    if(settings.value("coreconfig/queue_new_to_top").toBool())
        handle.queue_position_top();

    this->components->alertManager->resume();

    // Resume the torrent if needed
    if(!(*options)["add_paused"].toBool())
        torrent->resume();

    // Write the .torrent file to the state directory
    if(filedump)
    {
        QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
        QString dir = QFileInfo(ini.fileName()).absolutePath();
        if(!QDir(dir).exists())
        {
            QDir().mkdir(dir);
            QDir().mkdir(dir + QDir::separator() + "state");
        }

        QFile save_file(QDir(dir + QDir::separator() +
                   "state").absoluteFilePath(torrent->torrent_id + ".torrent"));
        if(save_file.open(QIODevice::WriteOnly))
        {
            save_file.write(filedump->data(), filedump->size());
            save_file.close();
        }

        // If the user has requested a copy of the torrent be saved elsewhere
        // we need to do that.
        if(settings.value("coreconfig/copy_torrent_file").toBool() && filename)
        {
            save_file.setFileName(QDir(settings.value("coreconfig/torrentfiles_location").toString()).absoluteFilePath(*filename));
            if(save_file.open(QIODevice::WriteOnly))
            {
                save_file.write(QString(filedump->data()).toLatin1());
                save_file.close();
            }
        }
    }

    if(save_state)
        // Save the session state
        this->save_state();

    // Emit the torrent_added signal
    this->components->eventManager->emitTorrentAddedEvent(torrent->torrent_id); //, feed_id);

    // Restore main window if it is minimized or iconified
    // component.get("MainWindow").restore_window()

    if(options)
        delete options;
    if(filename)
        delete filename;
    if(state)
        delete state;

    return torrent->torrent_id;
}

libtorrent::torrent_info* TorrentManager::load_torrent(QString torrent_id)
{
    // Load a torrent file from state and return it's torrent info

    // Get the torrent data from the torrent file
    QFile f(QDir(get_default_config_dir() + QDir::separator() +
            "state").absoluteFilePath(torrent_id + ".torrent"));
    if(f.open(QIODevice::ReadOnly))
    {
        std::vector<char> in;
        const qint64 content_size = f.bytesAvailable();
        in.resize(content_size);
        f.read(&in[0], f.size());
        return new libtorrent::torrent_info(libtorrent::bdecode(&in[0], &in[0]+in.size()));
        f.close();
    }
    return NULL;
}

void TorrentManager::open_folder_for_selected()
{
    QList<QString> status_keys;
    status_keys << "name" << "save_path";
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
    {
        QMap<QString, QVariant> status = this->torrents[selected_torrent]->get_status(status_keys);
        if(status.isEmpty())
            continue;
        QString fullpath = QDir(status["save_path"].toString()).absoluteFilePath(status["name"].toString());
        if(QFileInfo(fullpath).isDir())
            QDesktopServices::openUrl(QUrl("file:///" + fullpath));
        else
            QDesktopServices::openUrl(QUrl("file:///" + status["save_path"].toString()));
    }
}

void TorrentManager::pause_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->pause();
}

void TorrentManager::resume_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->resume();
}

void TorrentManager::set_download_speed_for_selected(int speed)
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->set_max_download_speed(speed);
}

void TorrentManager::set_upload_speed_for_selected(int speed)
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->set_max_upload_speed(speed);
}

void TorrentManager::set_connection_for_selected(int connection)
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->set_max_connections(connection);
}

void TorrentManager::set_upload_slot_for_selected(int upload_slots)
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->set_max_upload_slots(upload_slots);
}

void TorrentManager::set_auto_managed_for_selected(bool auto_manage)
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->set_auto_managed(auto_manage);
}

void TorrentManager::set_max_connections_per_torrent(int value)
{
    foreach(QString torrent_id, this->torrents.keys())
        this->torrents[torrent_id]->set_max_connections(value);
}

void TorrentManager::set_max_upload_slots_per_torrent(int value)
{
    foreach(QString torrent_id, this->torrents.keys())
        this->torrents[torrent_id]->set_max_upload_slots(value);
}

void TorrentManager::set_max_upload_speed_per_torrent(int value)
{
    foreach(QString torrent_id, this->torrents.keys())
        this->torrents[torrent_id]->set_max_upload_speed(value);
}

void TorrentManager::set_max_download_speed_per_torrent(int value)
{
    foreach(QString torrent_id, this->torrents.keys())
        this->torrents[torrent_id]->set_max_download_speed(value);
}

bool queueUpLessThan(const QPair<QString, int> &s1, const QPair<QString, int> &s2)
{
    return s1.second < s2.second;
}

bool queueDownLessThan(const QPair<QString, int> &s1, const QPair<QString, int> &s2)
{
    return s1.second > s2.second;
}

void TorrentManager::queue_top_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
    {
        if(this->torrents[selected_torrent]->get_queue_position() != 0)
        {
            this->torrents[selected_torrent]->handle.queue_position_top();
            this->components->eventManager->emitTorrentQueueChangedEvent();
        }
    }
}

void TorrentManager::queue_up_selected()
{
    QList<QPair<QString, int> > list;
    foreach(QString t, this->components->torrentView->get_selected_torrents())
        list.append(QPair<QString, int>(t, this->torrents[t]->get_queue_position()));
    qSort(list.begin(), list.end(), queueUpLessThan);
    QPair<QString, int> selected_torrent;
    foreach(selected_torrent, list)
    {
        if(selected_torrent.second != 0)
        {
            this->torrents[selected_torrent.first]->handle.queue_position_up();
            this->components->eventManager->emitTorrentQueueChangedEvent();
        }
    }
}

void TorrentManager::queue_down_selected()
{
    QList<QPair<QString, int> > list;
    foreach(QString t, this->components->torrentView->get_selected_torrents())
        list.append(QPair<QString, int>(t, this->torrents[t]->get_queue_position()));
    qSort(list.begin(), list.end(), queueDownLessThan);
    QPair<QString, int> selected_torrent;
    foreach(selected_torrent, list)
    {
        if(selected_torrent.second != this->torrents.count()-1)
        {
            this->torrents[selected_torrent.first]->handle.queue_position_down();
            this->components->eventManager->emitTorrentQueueChangedEvent();
        }
    }
}

void TorrentManager::queue_bottom_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
    {
        if(this->torrents[selected_torrent]->get_queue_position() != this->torrents.count()-1)
        {
            this->torrents[selected_torrent]->handle.queue_position_bottom();
            this->components->eventManager->emitTorrentQueueChangedEvent();
        }
    }
}

void TorrentManager::force_reannounce_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->force_reannounce();
}

void TorrentManager::remove_selected()
{
    QList<QString> selected_torrents = this->components->torrentView->get_selected_torrents();
    int remove_result = this->components->removeTorrentDialog->show_remove_torrent_dialog();
    if(remove_result == 0)
        return;
    foreach(QString selected_torrent, selected_torrents)
        this->remove(selected_torrent, remove_result == 2);
}

void TorrentManager::force_recheck_selected()
{
    foreach(QString selected_torrent, this->components->torrentView->get_selected_torrents())
        this->torrents[selected_torrent]->force_recheck();
}

void TorrentManager::move_storage_for_selected(QString folder)
{
    foreach(QString selected, this->components->torrentView->get_selected_torrents())
    {
        this->torrents[selected]->move_storage(folder);
    }
}

bool TorrentManager::remove(QString torrent_id, bool remove_data)
{
    /*
     * Remove a torrent from the session.
     *
     * :param torrent_id: the torrent to remove
     * :type torrent_id: string
     * :param remove_data: if True, remove the downloaded data
     * :type remove_data: bool
     *
     * :returns: True if removed successfully, False if not
     * :rtype: bool
     */

    if(!this->torrents.contains(torrent_id))
        return false;

    // Emit the signal to the clients
    this->components->eventManager->emitPreTorrentRemovedEvent(torrent_id);

    try
    {
        this->session->remove_torrent(this->torrents[torrent_id]->handle, remove_data ? 1 : 0);
    }
    catch(...)
    {
        return false;
    }

    // Remove fastresume data if it is exists
    this->resume_data.dict().erase(torrent_id.toStdString());

    // Remove the .torrent file in the state
    this->torrents[torrent_id]->delete_torrentfile();

    // Remove the torrent file from the user specified directory
    QString filename = this->torrents[torrent_id]->filename;
    QSettings settings;
    if(settings.value("coreconfig/copy_torrent_file").toBool() &&
            settings.value("coreconfig/del_copy_torrent_file").toBool())
    {
        QFile users_torrent_file(QDir(settings.value("coreconfig/torrentfiles_location").toString()).absoluteFilePath(filename));
        users_torrent_file.remove();
    }

    // Remove the torrent from deluge's session
    this->torrents.remove(torrent_id);

    // Save the session state
    this->save_state();

    // Emit the signal to the clients
    this->components->eventManager->emitTorrentRemovedEvent(torrent_id);

    return true;
}

void TorrentManager::load_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
    {
        QDir().mkdir(dir);
        QDir().mkdir(dir + QDir::separator() + "state");
    }
    QFile fnew(QDir(dir + QDir::separator() + "state").absoluteFilePath("torrents.state"));
    if(fnew.open(QIODevice::ReadOnly))
    {
        QDataStream in(&fnew);
        QMap<QString, QMap<QString, QVariant> > state;
        in >> state;

        try
        {
            libtorrent::entry all_resume_data = this->load_resume_data_file();
            foreach(QString torrent_id, state.keys())
            {
                QList<QMap<QString, QVariant> > trackers;
                foreach(QVariant tr, state[torrent_id]["trackers"].toList())
                    trackers.append(tr.toMap());

                QList<QVariant> fp = state[torrent_id]["file_priorities"].toList();
                QVector<int> file_priorities;
                foreach(QVariant priority, fp)
                    file_priorities.append(priority.toInt());

                TorrentState *torrent_state = new TorrentState(
                        torrent_id,
                        state[torrent_id]["filename"].toString(),
                        state[torrent_id]["total_uploaded"].toFloat(),
                        trackers,
                        state[torrent_id]["compact_allocation"].toBool(),
                        state[torrent_id]["paused"].toBool(),
                        state[torrent_id]["save_path"].toString(),
                        state[torrent_id]["max_connections"].toInt(),
                        state[torrent_id]["max_upload_slots"].toInt(),
                        state[torrent_id]["max_upload_speed"].toFloat(),
                        state[torrent_id]["max_download_speed"].toFloat(),
                        state[torrent_id]["prioritize_first_last_pieces"].toBool(),
                        file_priorities,
                        state[torrent_id]["queue_position"].toInt(),
                        state[torrent_id]["auto_managed"].toBool(),
                        state[torrent_id]["shutdown_pc"].toBool(),
                        state[torrent_id]["is_finished"].toBool(),
                        state[torrent_id]["stop_ratio"].toFloat(),
                        state[torrent_id]["stop_at_ratio"].toBool(),
                        state[torrent_id]["remove_at_ratio"].toBool(),
                        state[torrent_id]["move_completed"].toBool(),
                        state[torrent_id]["move_completed_path"].toString(),
                        state[torrent_id]["magnet"].toString(),
                        state[torrent_id]["time_added"].toUInt(),
                        state[torrent_id]["enable_streaming"].toBool()
                        );

                std::vector<char> *resume_data = NULL;
                try
                {
                    std::string str = all_resume_data[torrent_id.toStdString()].string();
                    resume_data = new std::vector<char>();
                    std::copy(str.begin(), str.end(), back_inserter(*resume_data));
                }
                catch(...){}
                this->add(NULL, torrent_state, NULL, false, NULL, NULL, NULL, resume_data);
            }
        }
        catch(...){}

        fnew.close();
        return;
    }
    QFile f(QDir(get_default_config_dir() + QDir::separator() + "state").absoluteFilePath("torrents.state"));
    if(f.open(QIODevice::ReadOnly))
    {
        QString s = QString(f.readAll());
        f.close();
        PickleLoader pl(s);
        QVariant res;
        try
        {
            pl.loads(res);
            if(res.type() == QVariant::List && res.toList()[0].type() == QVariant::Map &&
                    res.toList()[0].toMap().contains("torrents") &&
                    res.toList()[0].toMap()["torrents"].type() == QVariant::List)
            {
                libtorrent::entry all_resume_data = this->load_resume_data_file();
                foreach(QVariant vts, res.toList()[0].toMap()["torrents"].toList())
                {
                    QMap<QString, QVariant> ts = vts.toList()[0].toMap();
                    QList<QMap<QString, QVariant> > trackers;
                    foreach(QVariant vtracker, ts["trackers"].toList())
                    {
                        trackers.append(vtracker.toMap());
                    }
                    QVector<int> file_priorities;
                    foreach(QVariant priority, ts["file_priorities"].toList())
                    {
                        file_priorities.append(priority.toInt());
                    }
                    TorrentState *torrent_state = new TorrentState(
                                ts["torrent_id"].toString(),
                                ts["filename"].toString(),
                                ts["total_uploaded"].toFloat(),
                                trackers,
                                ts["compact_allocation"].toBool(),
                                ts["paused"].toBool(),
                                ts["save_path"].toString(),
                                ts["max_connections"].toInt(),
                                ts["max_upload_slots"].toInt(),
                                ts["max_upload_speed"].toFloat(),
                                ts["max_download_speed"].toFloat(),
                                ts["prioritize_first_last_pieces"].toBool(),
                                file_priorities,
                                ts["queue_position"].toInt(),
                                ts["auto_managed"].toBool(),
                                ts["shutdown_pc"].toBool(),
                                ts["is_finished"].toBool(),
                                ts["stop_ratio"].toFloat(),
                                ts["stop_at_ratio"].toBool(),
                                ts["remove_at_ratio"].toBool(),
                                ts["move_completed"].toBool(),
                                ts["move_completed_path"].toString(),
                                ts["magnet"].toString(),
                                ts["time_added"].toUInt(),
                                ts["enable_streaming"].toBool()
                            );
                    //self.add(state=torrent_state, save_state=False,
                    //                         resume_data=resume_data.get(torrent_state.torrent_id))
                    std::string str = all_resume_data[torrent_state->torrent_id.toStdString()].string();
                    std::vector<char> *resume_data = new std::vector<char>();
                    std::copy(str.begin(), str.end(), back_inserter(*resume_data));
                    this->add(NULL, torrent_state, NULL, false, NULL, NULL, NULL, resume_data);
                }
            }
        }
        catch(...){}
    }
    this->components->eventManager->emitSessionStartedEvent();
}

void TorrentManager::save_state_timer_tick()
{
    this->save_state();
}

void TorrentManager::save_state()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
    {
        QDir().mkdir(dir);
        QDir().mkdir(dir + QDir::separator() + "state");
    }

    QFile file(QDir(dir + QDir::separator() + "state").absoluteFilePath("torrents.state"));
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);   // we will serialize the data into the file
    QMap<QString, QMap<QString, QVariant> > state;
    foreach(Torrent* torrent, this->torrents.values())
    {
        bool paused = false;
        if(torrent->state == "Paused" || torrent->state == "Finished")
            paused = true;
        QList<QString> st_keys;
        st_keys << "total_uploaded";
        QMap<QString, QVariant> st = torrent->get_status(st_keys);

        //typedef QList<QMap<QString, QVariant> > trackers_type;
        QList<QVariant> trackers;
        QMap<QString, QVariant> tr;
        foreach(tr, torrent->trackers)
            trackers.append(tr);

        QVector<int> fp = torrent->options["file_priorities"].value<QVector<int> >();
        QList<QVariant> file_priorities;
        foreach(int var, fp)
            file_priorities.append(var);

        state[torrent->torrent_id]["filename"] = torrent->filename;
        state[torrent->torrent_id]["total_uploaded"] = st["total_uploaded"];
        state[torrent->torrent_id]["trackers"] = trackers;
        state[torrent->torrent_id]["compact_allocation"] = torrent->options["compact_allocation"];
        state[torrent->torrent_id]["paused"] = paused;
        state[torrent->torrent_id]["save_path"] = torrent->options["download_location"];
        state[torrent->torrent_id]["max_connections"] = torrent->options["max_connections"];
        state[torrent->torrent_id]["max_upload_slots"] = torrent->options["max_upload_slots"];
        state[torrent->torrent_id]["max_upload_speed"] = torrent->options["max_upload_speed"];
        state[torrent->torrent_id]["max_download_speed"] = torrent->options["max_download_speed"];
        state[torrent->torrent_id]["prioritize_first_last_pieces"] = torrent->options["prioritize_first_last_pieces"];
        state[torrent->torrent_id]["file_priorities"] = file_priorities;
        state[torrent->torrent_id]["queue_position"] = torrent->get_queue_position();
        state[torrent->torrent_id]["auto_managed"] = torrent->options["auto_managed"];
        state[torrent->torrent_id]["shutdown_pc"] = torrent->options["shutdown_pc"];
        state[torrent->torrent_id]["is_finished"] = torrent->is_finished;
        state[torrent->torrent_id]["stop_ratio"] = torrent->options["stop_ratio"];
        state[torrent->torrent_id]["stop_at_ratio"] = torrent->options["stop_at_ratio"];
        state[torrent->torrent_id]["remove_at_ratio"] = torrent->options["remove_at_ratio"];
        state[torrent->torrent_id]["move_completed"] = torrent->options["move_completed"];
        state[torrent->torrent_id]["move_completed_path"] = torrent->options["move_completed_path"];
        if(torrent->magnet)
            state[torrent->torrent_id]["magnet"] = *(torrent->magnet);
        else
            state[torrent->torrent_id]["magnet"] = "";
        state[torrent->torrent_id]["time_added"] = torrent->time_added;
        state[torrent->torrent_id]["enable_streaming"] = torrent->options["enable_streaming"];
    }
    out << state;
    file.flush();
    file.close();
}

void TorrentManager::save_resume_data(QList<QString> torrent_ids)
{
    /*
     * Saves resume data for list of torrent_ids or for all torrents
     * needing resume data updated if torrent_ids is empty
     */

    if(torrent_ids.isEmpty())
    {
        foreach(QString torrent_id, this->torrents.keys())
        {
            if(this->torrents[torrent_id]->handle.need_save_resume_data())
                torrent_ids.append(torrent_id);
        }
    }

    if(!this->waiting_on_resume_data.isEmpty())
    {
        // If we are still waiting on resume data from last call, force write and clear the queue
        this->save_resume_data_file();
        this->waiting_on_resume_data.clear();
    }

    foreach(QString torrent_id, torrent_ids)
    {
        if(!this->waiting_on_resume_data.contains(torrent_id))
            this->waiting_on_resume_data.append(torrent_id);
        this->torrents[torrent_id]->save_resume_data();
    }
}

libtorrent::entry TorrentManager::load_resume_data_file()
{
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
    {
        QDir().mkdir(dir);
        QDir().mkdir(dir + QDir::separator() + "state");
    }
    QFile fastresume_file(QDir(dir+QDir::separator()+"state").absoluteFilePath("torrents.fastresume"));
    if(!fastresume_file.exists())
        fastresume_file.setFileName(QDir(get_default_config_dir()+QDir::separator()+"state").absoluteFilePath("torrents.fastresume"));
    if(!fastresume_file.open(QIODevice::ReadOnly))
        return libtorrent::entry();
    std::vector<char> in;
    const qint64 content_size = fastresume_file.bytesAvailable();
    in.resize(content_size);
    fastresume_file.read(&in[0], content_size);
    fastresume_file.close();
    libtorrent::entry resume_data;

    try
    {
        resume_data = libtorrent::bdecode(&in[0], &in[0]+in.size());
    }
    catch(...)
    {
        return libtorrent::entry();
    }

    return resume_data;
}

void TorrentManager::save_resume_data_file()
{
    /*
     * Saves the resume data file with the contents of self.resume_data.
     */
    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
    {
        QDir().mkdir(dir);
        QDir().mkdir(dir + QDir::separator() + "state");
    }

    QFile fastresume_file(QDir(dir + QDir::separator() + "state").absoluteFilePath("torrents.fastresume"));
    if(!fastresume_file.open(QIODevice::WriteOnly))
        return;
    std::vector<char> out;
    libtorrent::bencode(std::back_inserter(out), this->resume_data);
    fastresume_file.write(&out[0], out.size());
    fastresume_file.close();
}

bool TorrentManager::queue_top(QString torrent_id)
{
    // Queue torrent to top
    if(this->torrents[torrent_id]->get_queue_position() == 0)
        return false;

    this->torrents[torrent_id]->handle.queue_position_top();
    return true;
}

bool TorrentManager::queue_up(QString torrent_id)
{
    // Queue torrent up one position
    if(this->torrents[torrent_id]->get_queue_position() == 0)
        return false;

    this->torrents[torrent_id]->handle.queue_position_up();
    return true;
}

bool TorrentManager::queue_down(QString torrent_id)
{
    // Queue torrent down one position
    if(this->torrents[torrent_id]->get_queue_position() == (this->torrents.count() - 1))
        return false;

    this->torrents[torrent_id]->handle.queue_position_down();
    return true;
}

bool TorrentManager::queue_bottom(QString torrent_id)
{
    // Queue torrent to bottom
    if(this->torrents[torrent_id]->get_queue_position() == (this->torrents.count() - 1))
        return false;

    this->torrents[torrent_id]->handle.queue_position_bottom();
    return true;
}

void TorrentManager::on_set_max_connections_per_torrent(QString key, QVariant value)
{
    // Sets the per-torrent connection limit
    //foreach(key, this->torrents.keys())
    //    this->torrents[key]->set_max_connections(value.toInt());
}

void TorrentManager::on_set_max_upload_slots_per_torrent(QString key, QVariant value)
{
    // Sets the per-torrent upload slot limit
    foreach(key, this->torrents.keys())
        this->torrents[key]->set_max_upload_slots(value.toInt());
}

void TorrentManager::on_set_max_upload_speed_per_torrent(QString key, QVariant value)
{
    foreach(key, this->torrents.keys())
        this->torrents[key]->set_max_upload_speed(value.toInt());
}

void TorrentManager::on_set_max_download_speed_per_torrent(QString key, QVariant value)
{
    foreach(key, this->torrents.keys())
        this->torrents[key]->set_max_download_speed(value.toInt());
}

Torrent* TorrentManager::operator [](QString key)
{
    return this->torrents[key];
}

void TorrentManager::on_alert_torrent_finished(libtorrent::alert* alert)
{
    libtorrent::torrent_finished_alert const *a = dynamic_cast<libtorrent::torrent_finished_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    // Get the total_download and if it's 0, do not move.. It's likely
    // that the torrent wasn't downloaded, but just added.
    QList<QString> keys;
    keys << "total_payload_download";
    int total_download = torrent->get_status(keys)["total_payload_download"].toInt();

    // Move completed download to completed folder if needed
    if(!torrent->is_finished && total_download)
    {

        if(torrent->options["move_completed"].toBool())
        {
            QString move_path = torrent->options["move_completed_path"].toString();
            if(torrent->options["download_location"].toString() != move_path)
                torrent->move_storage(move_path);
        }

        this->components->eventManager->emitTorrentFinishedEvent(torrent_id);
    }

    torrent->is_finished = true;
    torrent->update_state();

    // Only save resume data if it was actually downloaded something. Helps
    // on startup with big queues with lots of seeding torrents. Libtorrent
    // emits alert_torrent_finished for them, but there seems like nothing
    // worth really to save in resume data, we just read it up in
    // self.load_state().
    if(total_download)
    {
        torrent->check_not_downloaded_files();

        keys.clear();
        keys << torrent_id;
        this->save_resume_data(keys);

        if(torrent->options["shutdown_pc"].toBool())
        {
            torrent->set_shutdown_pc(false);
            //from deluge.shutdownpc import ShutdownPC
            //ShutdownPC("Your PC will be turned off after 10 seconds")
            //component.get("MainWindow").quit()
        }
    }
}

void TorrentManager::on_alert_torrent_paused(libtorrent::alert* alert)
{
    libtorrent::torrent_paused_alert const *a = dynamic_cast<libtorrent::torrent_paused_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    // Set the torrent state
    QString old_state = torrent->state;
    torrent->update_state();
    if(torrent->state != old_state)
        this->components->eventManager->emitTorrentStateChangedEvent(torrent_id, torrent->state);

    // Write the fastresume file if we are not waiting on a bulk write
    if(!this->waiting_on_resume_data.contains(torrent_id))
    {
        QList<QString> keys;
        keys << torrent_id;
        this->save_resume_data(keys);
    }
}

void TorrentManager::on_alert_torrent_checked(libtorrent::alert* alert)
{
    libtorrent::torrent_checked_alert const *a = dynamic_cast<libtorrent::torrent_checked_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    // Check to see if we're forcing a recheck and set it back to paused
    // if necessary
    if(torrent->forcing_recheck)
    {
        torrent->forcing_recheck = false;
        if(torrent->forcing_recheck_paused)
            torrent->handle.pause();
    }

    // Set the torrent state
    torrent->update_state();
}

void TorrentManager::on_alert_tracker_reply(libtorrent::alert* alert)
{
    libtorrent::tracker_reply_alert const *a = dynamic_cast<libtorrent::tracker_reply_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    // Set the tracker status for the torrent
    if(a->message() != "Got peers from DHT")
    {
        torrent->working_tracker = QString::fromStdString(a->url);
        torrent->set_tracker_status("Announce OK");
    }

    // Check to see if we got any peer information from the tracker
    if(a->handle.status().num_complete == -1 || a->handle.status().num_incomplete == -1)
        // We didn't get peer information, so lets send a scrape request
        torrent->scrape_tracker();
}

void TorrentManager::on_alert_tracker_announce(libtorrent::alert* alert)
{
    libtorrent::tracker_announce_alert const *a = dynamic_cast<libtorrent::tracker_announce_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    // Set the tracker status for the torrent
    this->torrents[torrent_id]->set_tracker_status("Announce Sent");
}

void TorrentManager::on_alert_tracker_warning(libtorrent::alert* alert)
{
    libtorrent::tracker_warning_alert const *a = dynamic_cast<libtorrent::tracker_warning_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    QString tracker_status = "Warning: " + QString::fromStdString(a->message());

    // Set the tracker status for the torrent
    this->torrents[torrent_id]->set_tracker_status(tracker_status);
}

void TorrentManager::on_alert_tracker_error(libtorrent::alert* alert)
{
    libtorrent::tracker_error_alert const *a = dynamic_cast<libtorrent::tracker_error_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    if(torrent->working_tracker != QString::fromStdString(a->url))
        return;
    torrent->working_tracker = QString();
    QString tracker_status = "Error: " + QString::fromStdString(a->msg);
    torrent->set_tracker_status(tracker_status);
}

void TorrentManager::on_alert_storage_moved(libtorrent::alert* alert)
{
    libtorrent::storage_moved_alert const *a = dynamic_cast<libtorrent::storage_moved_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    this->torrents[torrent_id]->set_save_path(QString::fromStdString(a->handle.save_path()));
}

void TorrentManager::on_alert_torrent_resumed(libtorrent::alert* alert)
{
    libtorrent::torrent_resumed_alert const *a = dynamic_cast<libtorrent::torrent_resumed_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    torrent->is_finished = torrent->handle.is_seed();
    QString old_state = torrent->state;
    torrent->update_state();
    if(torrent->state != old_state)
        // We need to emit a TorrentStateChangedEvent too
        this->components->eventManager->emitTorrentStateChangedEvent(torrent_id, torrent->state);
    this->components->eventManager->emitTorrentResumedEvent(torrent_id);
}

void TorrentManager::on_alert_state_changed(libtorrent::alert* alert)
{
    libtorrent::state_changed_alert const *a = dynamic_cast<libtorrent::state_changed_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    QString old_state = torrent->state;
    torrent->update_state();
    // Only emit a state changed event if the state has actually changed
    if(torrent->state != old_state)
        this->components->eventManager->emitTorrentStateChangedEvent(torrent_id, torrent->state);
}

void TorrentManager::on_alert_save_resume_data(libtorrent::alert* alert)
{
    libtorrent::save_resume_data_alert const* a = dynamic_cast<libtorrent::save_resume_data_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());

    if(this->torrents.contains(torrent_id))
    {
        // Libtorrent in add_torrent() expects resume_data to be bencoded
        std::vector<char> out;
        libtorrent::entry* rd = a->resume_data.get();
        libtorrent::bencode(std::back_inserter(out), *rd);
        this->resume_data[torrent_id.toStdString()] = std::string(out.begin(), out.end());
    }

    if(this->waiting_on_resume_data.contains(torrent_id))
    {
        this->waiting_on_resume_data.removeOne(torrent_id);
        if(this->waiting_on_resume_data.isEmpty())
            this->save_resume_data_file();
    }
}

void TorrentManager::on_alert_save_resume_data_failed(libtorrent::alert* alert)
{
    libtorrent::save_resume_data_failed_alert const* a = dynamic_cast<libtorrent::save_resume_data_failed_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());

    if(this->waiting_on_resume_data.contains(torrent_id))
    {
        this->waiting_on_resume_data.removeOne(torrent_id);
        if(this->waiting_on_resume_data.isEmpty())
            this->save_resume_data_file();
    }
}

void TorrentManager::on_alert_file_renamed(libtorrent::alert* alert)
{
    libtorrent::file_renamed_alert const *a = dynamic_cast<libtorrent::file_renamed_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    // We need to see if this file index is in a waiting_on_folder list
    bool folder_rename = false;
    int i = 0;
    foreach(QList<QVariant> wait_on_folder, torrent->waiting_on_folder_rename)
    {
        QList<int> tmp_list = wait_on_folder[2].value<QList<int> >();
        if(tmp_list.contains(a->index))
        {
            folder_rename = true;
            if(tmp_list.length() == 1)
            {
                // This is the last alert we were waiting for, time to send signal
                this->torrents[torrent_id]->update_files_list();
                QDir oldFolder(QDir(this->torrents[torrent_id]->options["download_location"].toString()).absoluteFilePath(wait_on_folder[0].toString()));
                if(oldFolder.exists())
                    oldFolder.removeRecursively(); //.rmdir(oldFolder.absolutePath());
                this->components->eventManager->emitTorrentFolderRenamedEvent(torrent_id,
                                  wait_on_folder[0].toString(), wait_on_folder[1].toString());
                torrent->waiting_on_folder_rename.removeOne(torrent->waiting_on_folder_rename[i]);
                QList<QString> keys;
                keys << torrent_id;
                this->save_resume_data(keys);
                break;
            }
            // This isn't the last file to be renamed in this folder, so just
            // remove the index and continue
            tmp_list.removeOne(a->index);
            torrent->waiting_on_folder_rename[i][2] = QVariant::fromValue<QList<int> >(tmp_list);
        }
        ++i;
    }

    if(!folder_rename)
    {
        // This is just a regular file rename so send the signal
        this->torrents[torrent_id]->update_files_list();
        this->components->eventManager->emitTorrentFileRenamedEvent(torrent_id, a->index, QString::fromStdString(a->name));
        QList<QString> keys;
        keys << torrent_id;
        this->save_resume_data(keys);
    }
}

void TorrentManager::on_alert_metadata_received(libtorrent::alert* alert)
{
    libtorrent::metadata_received_alert const *a = dynamic_cast<libtorrent::metadata_received_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    Torrent *torrent = this->torrents[torrent_id];

    torrent->write_torrentfile();
    torrent->update_files_list();
}

void TorrentManager::on_alert_file_error(libtorrent::alert* alert)
{
    libtorrent::file_error_alert const *a = dynamic_cast<libtorrent::file_error_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    this->torrents[torrent_id]->update_state();
}

void TorrentManager::on_fastresume_rejected_alert(libtorrent::alert* alert)
{
    this->need_save_resume_data_for_all = true;

    libtorrent::fastresume_rejected_alert const *a = dynamic_cast<libtorrent::fastresume_rejected_alert const*>(alert);

    QString torrent_id = info_hash_to_string(a->handle.info_hash());
    if(!this->torrents.contains(torrent_id))
        return;

    qDebug() << "REJECTED: " << this->torrents[torrent_id]->filename;

    this->torrents[torrent_id]->update_state();
}
