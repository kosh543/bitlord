#ifndef CORE_H
#define CORE_H

#include <QString>

#include <stdlib.h>
#include "libtorrent/session.hpp"
#include "libtorrent/bencode.hpp"
#include "libtorrent/entry.hpp"
#include "libtorrent/extensions/metadata_transfer.hpp"
#include "libtorrent/extensions/ut_metadata.hpp"
#include "libtorrent/extensions/smart_ban.hpp"
#include "libtorrent/extensions/ut_pex.hpp"

class Components;

class Core
{
private:
    Components *components;
public:
    libtorrent::session *session;
    libtorrent::session_settings *settings;

    Core(Components *comp);
    ~Core();
    int get_random_int(int low, int high);
    void set_listen_ports(int low, int high);
    void set_random_port(bool value);
    void set_outgoing_ports(int low, int high);
    void set_random_outgoing_ports(bool value);
    void set_peer_tos(QString value);
    void set_upnp(bool value);
    void set_natpmp(bool value);
    void set_peer_exchange(bool value);
    void set_lsd(bool value);
    void set_dht(bool value, bool save = true);
    void save_dht_state();
    void set_encryption();
    void set_max_upload_speed(int value);
    void set_max_download_speed(int value);
    void set_max_connections_per_second(int value);
    void set_ignore_limits_on_local_network(bool value);
    void set_rate_limit_ip_overhead(bool value);
    void set_share_ratio_limit(float value);
    void set_seed_time_ratio_limit(float value);
    void set_seed_time_limit(int value);
    void set_max_active_downloading(int value);
    void set_max_active_seeding(int value);
    void set_max_active_limit(int value);
    void set_dont_count_slow_torrents(bool value);
};

#endif // CORE_H
