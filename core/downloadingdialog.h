#ifndef DOWNLOADINGDIALOG_H
#define DOWNLOADINGDIALOG_H

#include <QDialog>
#include <QUrl>
#include <QString>
#include <QProgressBar>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class DownloadingDialog : public QDialog
{
    Q_OBJECT
private:
    QNetworkAccessManager *networkAccessManager;
    QNetworkReply *networkReply;
    QProgressBar *downloadingProgress;
    QUrl url;
    QString fileExtension;

    void getReply(QUrl url);

public:
    explicit DownloadingDialog(QUrl url, QString fileExtension, QWidget *parent = 0);
    void show_downloading_dialog();

signals:
    void fileDownloaded(QString filePath);

private slots:
    void metaDataChanged();
    void downloadProgress(qint64 current, qint64 total);
    void downloadingFinished();
    void handleSslErrors(QNetworkReply *reply, const QList<QSslError> &errors);

};

#endif // DOWNLOADINGDIALOG_H
