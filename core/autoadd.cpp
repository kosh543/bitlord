#include "autoadd.h"
#include "components.h"

#include <QSettings>
#include <QDir>
#include <QFile>
#include <QString>
#include <QList>

AutoAdd::AutoAdd(Components *comp, QObject *parent) :
    QObject(parent)
{
    this->components = comp;

    this->update_autoadd_timer = new QTimer(this);
    this->update_autoadd_timer->setInterval(5000);
    this->connect(this->update_autoadd_timer, SIGNAL(timeout()), this, SLOT(update_autoadd()));

    QSettings settings;
    this->set_autoadd_enable(settings.value("coreconfig/autoadd_enable").toBool());
}

void AutoAdd::resume_autoadd()
{
    if(!this->update_autoadd_timer->isActive())
        this->update_autoadd_timer->start();
}

void AutoAdd::pause_autoadd()
{
    if(this->update_autoadd_timer->isActive())
        this->update_autoadd_timer->stop();
}

void AutoAdd::update_autoadd()
{
    QSettings settings;
    if(!settings.value("coreconfig/autoadd_enable").toBool())
    {
        this->pause_autoadd();
        return;
    }

    QDir autoaddDir(settings.value("coreconfig/autoadd_location").toString());
    QStringList filters;
    filters << "*.torrent";
    QStringList filesList = autoaddDir.entryList(filters);

    foreach(QString filename, filesList)
    {
        QString filepath = autoaddDir.absoluteFilePath(filename);
        QVector<char> *filedump = NULL;
        QFile f(filepath);
        if(f.open(QIODevice::ReadOnly))
        {
            filedump = new QVector<char>();
            const qint64 content_size = f.bytesAvailable();
            filedump->resize(content_size);
            f.read(filedump->data(), f.size());
            f.close();
        }
        else
            continue;

        QString *filename_for_manager = new QString(filename);
        this->components->torrentManager->add(NULL, NULL, NULL, true, filedump, filename_for_manager, NULL, NULL);
        QFile::remove(filepath);
    }
}

void AutoAdd::set_autoadd_enable(bool value)
{
    if(value)
        this->resume_autoadd();
    else
        this->pause_autoadd();
}

void AutoAdd::set_autoadd_location()
{
    QSettings settings;
    if(settings.value("coreconfig/autoadd_enable").toBool())
        this->resume_autoadd();
}
