#include "preventhibernation.h"
#include "windows.h"

#include <QSettings>

PreventHibernation::PreventHibernation()
{
    numberOfActiveTorrents = 0;
    currentState = false;
}

void PreventHibernation::set_value(bool value)
{
    if(value && this->numberOfActiveTorrents > 0)
    {
        SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
        this->currentState = true;
    }
    else
    {
        SetThreadExecutionState(ES_CONTINUOUS);
        this->currentState = false;
    }
}

void PreventHibernation::set_prevent_hibernation(bool value)
{
    if(value && !this->currentState)
    {
        QSettings settings;
        if(settings.value("uiconfig/prevent_hibernation").toBool())
            this->set_value(value);
    }
    else if(this->currentState)
        this->set_value(value);
}

void PreventHibernation::check_prevent(int newNumberOfActiveTorrent)
{
    if(this->numberOfActiveTorrents == newNumberOfActiveTorrent)
        return;

    if(this->numberOfActiveTorrents == 0 && newNumberOfActiveTorrent > 0)
    {
        this->numberOfActiveTorrents = newNumberOfActiveTorrent;
        this->set_prevent_hibernation(true);
    }
    else if(this->numberOfActiveTorrents > 0 && newNumberOfActiveTorrent == 0)
    {
        this->numberOfActiveTorrents = newNumberOfActiveTorrent;
        this->set_prevent_hibernation(false);
    }
}
