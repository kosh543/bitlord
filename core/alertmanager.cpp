#include "alertmanager.h"
#include "components.h"

AlertManager::AlertManager(Components *comp, QObject *parent) : QObject(parent)
{
    this->components = comp;
    this->session = this->components->core->session;

    this->session->set_alert_mask(
                libtorrent::alert::error_notification |
                libtorrent::alert::port_mapping_notification |
                libtorrent::alert::storage_notification |
                libtorrent::alert::tracker_notification |
                libtorrent::alert::status_notification |
                libtorrent::alert::ip_block_notification |
                libtorrent::alert::performance_warning);

    // handlers is a dictionary of lists {"alert_type": [handler1,h2,..]}
    this->handlers = QMap<QString, QList<void (*)(libtorrent::alert *alert)> >();

    this->alert_timer = new QTimer(this);
    this->alert_timer->setInterval(1000);
    this->connect(this->alert_timer, SIGNAL(timeout()), this, SLOT(alert_timer_timeout()));
    this->alert_timer->start();
}

void AlertManager::register_handler(QString alert_type, void (*handler)(libtorrent::alert *alert))
{
    /*
     * Registers a function that will be called when 'alert_type' is pop'd
     * in handle_alerts.  The handler function should look like: handler(alert)
     * Where 'alert' is the actual alert object from libtorrent.
     *
     * :param alert_type: str, this is string representation of the alert name
     * :param handler: func(alert), the function to be called when the alert is raised
     */

    if(!this->handlers.keys().contains(alert_type))
        // There is no entry for this alert type yet, so lets make it with an
        // empty list.
        this->handlers[alert_type] = QList<void (*)(libtorrent::alert *alert)>();

    // Append the handler to the list in the handlers dictionary
    this->handlers[alert_type].append(handler);
}

void AlertManager::deregister_handler(void (*handler)(libtorrent::alert *alert))
{
    /*
     * De-registers the `:param:handler` function from all alert types.
     *
     * :param handler: func, the handler function to deregister
     */

    // Iterate through all handlers and remove 'handler' where found
    foreach(QString key, this->handlers.keys())
    {
        if(this->handlers[key].contains(handler))
            // Handler is in this alert type list
            this->handlers[key].removeOne(handler);
    }
}

void AlertManager::alert_timer_timeout()
{
    this->handle_alerts();
}

void AlertManager::pause()
{
    this->alert_timer->stop();
}

void AlertManager::resume()
{
    this->alert_timer->start();
}

void AlertManager::handle_alerts(bool wait)
{
    /*
     * Pops all libtorrent alerts in the session queue and handles them
     * appropriately.
     *
     * :param wait: bool, if True then the handler functions will be run right
     * away and waited to return before processing the next alert
     */

    std::auto_ptr<libtorrent::alert> alert = this->session->pop_alert();
    // Loop through all alerts in the queue
    while(alert.get())
    {
        libtorrent::alert *a = alert.get();
        this->emit_torrent_signal(a);
        alert = this->session->pop_alert();
    }
}

QString AlertManager::get_alert_type(int type)
{
    switch (type)
    {
    case libtorrent::fastresume_rejected_alert::alert_type:
    {
        return "fastresume_rejected_alert";
    }
    case libtorrent::torrent_finished_alert::alert_type:
    {
        return "torrent_finished_alert";
    }
    case libtorrent::torrent_paused_alert::alert_type:
    {
        return "torrent_paused_alert";
    }
    case libtorrent::torrent_checked_alert::alert_type:
    {
        return "torrent_checked_alert";
    }
    case libtorrent::tracker_reply_alert::alert_type:
    {
        return "tracker_reply_alert";
    }
    case libtorrent::tracker_announce_alert::alert_type:
    {
        return "tracker_announce_alert";
    }
    case libtorrent::tracker_warning_alert::alert_type:
    {
        return "tracker_warning_alert";
    }
    case libtorrent::tracker_error_alert::alert_type:
    {
        return "tracker_error_alert";
    }
    case libtorrent::storage_moved_alert::alert_type:
    {
        return "storage_moved_alert";
    }
    case libtorrent::torrent_resumed_alert::alert_type:
    {
        return "torrent_resumed_alert";
    }
    case libtorrent::state_changed_alert::alert_type:
    {
        return "state_changed_alert";
    }
    case libtorrent::save_resume_data_alert::alert_type:
    {
        return "save_resume_data_alert";
    }
    case libtorrent::save_resume_data_failed_alert::alert_type:
    {
        return "save_resume_data_failed_alert";
    }
    case libtorrent::file_renamed_alert::alert_type:
    {
        return "file_renamed_alert";
    }
    case libtorrent::metadata_received_alert::alert_type:
    {
        return "metadata_received_alert";
    }
    case libtorrent::file_error_alert::alert_type:
    {
        return "file_error_alert";
    }
    }
    return "";
}

void AlertManager::emit_torrent_signal(libtorrent::alert *alert)
{
    switch (alert->type())
    {
    case libtorrent::fastresume_rejected_alert::alert_type:
        emit this->fastresume_rejected_signal(alert);
        break;
    case libtorrent::torrent_finished_alert::alert_type:
        emit this->torrent_finished_signal(alert);
        break;
    case libtorrent::torrent_paused_alert::alert_type:
        emit this->torrent_paused_signal(alert);
        break;
    case libtorrent::torrent_checked_alert::alert_type:
        emit this->torrent_checked_signal(alert);
        break;
    case libtorrent::tracker_reply_alert::alert_type:
        emit this->tracker_reply_signal(alert);
        break;
    case libtorrent::tracker_announce_alert::alert_type:
        emit this->tracker_announce_signal(alert);
        break;
    case libtorrent::tracker_warning_alert::alert_type:
        emit this->tracker_warning_signal(alert);
        break;
    case libtorrent::tracker_error_alert::alert_type:
        emit this->tracker_error_signal(alert);
        break;
    case libtorrent::storage_moved_alert::alert_type:
        emit this->storage_moved_signal(alert);
        break;
    case libtorrent::torrent_resumed_alert::alert_type:
        emit this->torrent_resumed_signal(alert);
        break;
    case libtorrent::state_changed_alert::alert_type:
        emit this->state_changed_signal(alert);
        break;
    case libtorrent::save_resume_data_alert::alert_type:
        emit this->save_resume_data_signal(alert);
        break;
    case libtorrent::save_resume_data_failed_alert::alert_type:
        emit this->save_resume_data_failed_signal(alert);
        break;
    case libtorrent::file_renamed_alert::alert_type:
        emit this->file_renamed_signal(alert);
        break;
    case libtorrent::metadata_received_alert::alert_type:
        emit this->metadata_received_signal(alert);
        break;
    case libtorrent::file_error_alert::alert_type:
        emit this->file_error_signal(alert);
        break;
    }
}
