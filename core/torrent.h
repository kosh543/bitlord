#ifndef TORRENT_H
#define TORRENT_H

#include <QString>
#include <QVariant>
#include <QMap>
#include <QDir>
#include <QFile>
#include <QDateTime>

#include <stdlib.h>
#include "libtorrent/session.hpp"
#include "libtorrent/peer_info.hpp"

#ifdef Q_OS_WIN
#include "windows.h"
#endif

#include "common.h"
#include "torrentstate.h"
#include "torrentoptions.h"
#include "updateqmap.h"
#include "UriParser.h"

class Components;

class Torrent
{
private:
    Components *components;
public:
    libtorrent::torrent_handle handle;
    QString torrent_id;
    QString filename;
    QString *magnet;
    float total_uploaded;
    QList<QMap<QString, QVariant> > files;
    QVector<int> magnet_file_priorities;
    libtorrent::torrent_status status;
    QString state;
    QList<QMap<QString, QVariant> > trackers;
    QString tracker_host;
    TorrentOptions options;
    uint time_added;
    bool is_finished;
    QString statusmsg;
    QString tracker_status;
    QString working_tracker;
    QList<QList<QVariant> > waiting_on_folder_rename;
    bool forcing_recheck;
    bool forcing_recheck_paused;

    Torrent(Components *comp, libtorrent::torrent_handle handle, TorrentOptions *options, TorrentState *state,
            QString *filename, QString *magnet, QVector<int> magnet_file_priorities=QVector<int>());
    ~Torrent();
    void set_options(TorrentOptions options);
    TorrentOptions get_options();
    void set_max_connections(int max_connections);
    void set_max_upload_slots(int max_slots);
    void set_max_upload_speed(int m_up_speed);
    void set_max_download_speed(int m_down_speed);
    void set_prioritize_first_last(bool prioritize);
    void set_auto_managed(bool auto_managed);
    void set_shutdown_pc(bool shutdown_pc);
    void set_stop_ratio(float stop_ratio);
    void set_stop_at_ratio(bool stop_at_ratio);
    void set_remove_at_ratio(bool remove_at_ratio);
    void set_move_completed(bool move_completed);
    void set_move_completed_path(QString move_completed_path);
    void set_file_priorities(QVector<int> file_priorities);
    void change_file_attribute_and_rename_file(int index, int attribute);
    void check_not_downloaded_files();
    void set_trackers(QList<QMap<QString, QVariant> > trackers = QList<QMap<QString, QVariant> >());
    void set_save_path(QString save_path);
    void set_tracker_status(QString status);
    void update_state();
    void set_state(QString state);
    void set_status_message(QString message);
    float get_eta();
    float get_ratio();
    QList<QMap<QString, QVariant> > get_files();
    void update_files_list();
    QList<QMap<QString, QVariant> > get_peers();
    int get_queue_position();
    QList<float> get_file_progress();
    QString get_tracker_host();
    QMap<QString, QVariant> get_status(QList<QString> keys);
    bool pause();
    bool resume();
    bool connect_peer(QString ip, int port);
    bool move_storage(QString dest);
    void enable_streaming();
    void set_piece_priority(int start, int end, int priority);
    void save_resume_data();
    void write_torrentfile();
    void delete_torrentfile();
    bool force_reannounce();
    bool scrape_tracker();
    bool force_recheck();
    void rename_files(QMap<int, QString> filenames);
    void rename_file(int index, QString filename);
    void rename_folder(QString folder, QString new_folder);
};

#endif // TORRENT_H
