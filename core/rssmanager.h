#ifndef RSSMANAGER_H
#define RSSMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QString>
#include <QMap>
#include <QList>
#include <QTimer>

class Components;

class RSSFeed
{
public:
    QString title;
    QString url;
    QList<QString> titles;
    QList<QString> urls;

    RSSFeed(QString title, QString url) : title(title), url(url) {}
    RSSFeed() : title(""), url("") {}
    bool operator ==(const RSSFeed &other)
    {
        return this->url == other.url;
    }
};

class RSSManager : public QObject
{
    Q_OBJECT
private:
    Components *components;
    QNetworkAccessManager *networkAccessManager;
    QMap<QString, RSSFeed> feeds;
    QString currentFeed;
    QTimer *real_load_timer;
    QTimer *update_feeds_timer;

public:
    explicit RSSManager(Components *comp, QObject *parent = 0);
    void load_state();
    void save_state();
    void parseFeed(RSSFeed &rssFeed);
    void addFeed(QString url, QString alias);
    void removeFeed(QString title);
    void updateOneFeed(QString title);
    QList<QString> getFeedTitles(QString feed);
    QString getUrlByIndex(int index);
    void setCurrentFeed(QString feed);

signals:
    void rssFeedAddedToManager(QString alias);

public slots:
    void update_feeds();
    void real_load_state();
    void rssFeedAdded(QString url, QString alias);

};

#endif // RSSMANAGER_H
