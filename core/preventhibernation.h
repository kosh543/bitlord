#ifndef PREVENTHIBERNATION_H
#define PREVENTHIBERNATION_H

class PreventHibernation
{
private:
    int numberOfActiveTorrents;
    bool currentState;

public:
    PreventHibernation();
    void set_value(bool value);
    void set_prevent_hibernation(bool value);
    void check_prevent(int newNumberOfActiveTorrent);
};

#endif // PREVENTHIBERNATION_H
