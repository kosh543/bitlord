#ifndef ALERTMANAGER_H
#define ALERTMANAGER_H

#include <QObject>
#include <QMap>
#include <QList>
#include <QString>
#include <QTimer>
#include <QtConcurrent/QtConcurrentRun>

#include "libtorrent/session.hpp"
#include "libtorrent/alert_types.hpp"

class Components;

/*
 *
 * The AlertManager handles all the libtorrent alerts.
 *
 * This should typically only be used by the Core.  Plugins should utilize the
 * `:mod:EventManager` for similar functionality.
 *
 */

class AlertManager : public QObject
{
    Q_OBJECT
private:
    Components *components;
    libtorrent::session *session;
    QMap<QString, QList<void (*)(libtorrent::alert *alert)> > handlers;
    QTimer *alert_timer;
public:
    AlertManager(Components *comp, QObject *parent=0);
    void register_handler(QString alert_type, void (*handler)(libtorrent::alert *alert));
    void deregister_handler(void (*handler)(libtorrent::alert *alert));
    void pause();
    void resume();
    QString get_alert_type(int type);
    void emit_torrent_signal(libtorrent::alert *alert);
    void handle_alerts(bool wait=false);
private slots:
    void alert_timer_timeout();
signals:
    void fastresume_rejected_signal(libtorrent::alert *alert);
    void torrent_finished_signal(libtorrent::alert *alert);
    void torrent_paused_signal(libtorrent::alert *alert);
    void torrent_checked_signal(libtorrent::alert *alert);
    void tracker_reply_signal(libtorrent::alert *alert);
    void tracker_announce_signal(libtorrent::alert *alert);
    void tracker_warning_signal(libtorrent::alert *alert);
    void tracker_error_signal(libtorrent::alert *alert);
    void storage_moved_signal(libtorrent::alert *alert);
    void torrent_resumed_signal(libtorrent::alert *alert);
    void state_changed_signal(libtorrent::alert *alert);
    void save_resume_data_signal(libtorrent::alert *alert);
    void save_resume_data_failed_signal(libtorrent::alert *alert);
    void file_renamed_signal(libtorrent::alert *alert);
    void metadata_received_signal(libtorrent::alert *alert);
    void file_error_signal(libtorrent::alert *alert);
};

#endif // ALERTMANAGER_H
