#ifndef TORRENTOPTIONS_H
#define TORRENTOPTIONS_H

#include <QMap>
#include <QList>
#include <QString>
#include <QVariant>
#include <QVector>

#include "updateqmap.h"

typedef QMap<int, QString> QMapIntString;
Q_DECLARE_METATYPE(QVector<int>)
Q_DECLARE_METATYPE(QMapIntString)
Q_DECLARE_METATYPE(QList<float>)
Q_DECLARE_METATYPE(QList<int>)

class TorrentOptions
{
private:
    QMap<QString, QVariant> options;
public:
    TorrentOptions();
    void updateOptions(QMap<QString, QVariant> &other);
    QVariant& operator[](QString key);
    QList<QString> keys();
    QMap<QString, QVariant> get_qmap();
};

#endif // TORRENTOPTIONS_H
