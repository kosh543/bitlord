#ifndef UPDATEQMAP_H
#define UPDATEQMAP_H

#include <QMap>
#include <QString>
#include <QVariant>

void updateQMap(QMap<QString, QVariant> &first, QMap<QString, QVariant> &second);

#endif // UPDATEQMAP_H
