#include "common.h"

const QMap<QString, int> lt_torrent_state_init()
{
    QMap<QString, int> tmp_map;
    tmp_map["Queued"] = 0;
    tmp_map["Checking"] = 1;
    tmp_map["Downloading Metadata"] = 2;
    tmp_map["Downloading"] = 3;
    tmp_map["Finished"] = 4;
    tmp_map["Seeding"] = 5;
    tmp_map["Allocating"] = 6;
    tmp_map["Checking Resume Data"] = 7;
    return tmp_map;
}

const QList<QString> torrent_state_init()
{
    QList<QString> tmp_list;
    tmp_list << "Allocating" << "Checking" << "Downloading" <<
                "Seeding" << "Paused" << "Error" << "Queued";
    return tmp_list;
}

const QMap<int, QString> file_priority_init()
{
    QMap<int, QString> tmp_map;
    tmp_map[0] = "Do Not Download";
    tmp_map[1] = "Normal Priority";
    tmp_map[2] = "High Priority";
    tmp_map[5] = "Highest Priority";
    return tmp_map;
}

QString get_version()
{
    /*
     * Returns the program version
     *
     * :returns: the version of BitLord
     * :rtype: string
     *
     */

    return QCoreApplication::applicationVersion();
}

QString get_default_config_dir(QString filename)
{
    /*
     * :param filename: if None, only the config path is returned, if provided, a path including the filename will be returned
     * :type filename: string
     * :returns: a file path to the config directory and optional filename
     * :rtype: string
     *
     */

    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLord", "BitLord");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(filename.isEmpty())
        return dir;
    return QDir(dir).absoluteFilePath(filename);
}

QString get_default_download_dir()
{
    /*
     * :returns: the default download directory
     * :rtype: string
     *
     */

    return QStandardPaths::standardLocations(QStandardPaths::DownloadLocation)[0];
}

bool windows_check()
{
    /*
     * Checks if the current platform is Windows
     *
     * :returns: True or False
     * :rtype: bool
     *
     */

#ifdef Q_OS_WIN
    return true;
#else
    return false;
#endif
}

bool windows_xp_check()
{
    /*
     * Checks if the current platform is Windows XP
     *
     * :returns: True or False
     * :rtype: bool
     *
     */

    if(windows_check())
        return QSysInfo::windowsVersion() == QSysInfo::WV_XP;
    return false;
}

bool vista_check()
{
    /*
     * Checks if the current platform is Windows Vista
     *
     * :returns: True or False
     * :rtype: bool
     *
     */

    if(windows_check())
        return QSysInfo::windowsVersion() == QSysInfo::WV_VISTA;
    return false;
}

bool windows7_check()
{
    /*
     * Checks if the current platform is Windows 7
     *
     * :returns: True or False
     * :rtype: bool
     *
     */

    if(windows_check())
        return QSysInfo::windowsVersion() == QSysInfo::WV_WINDOWS7;
    return false;
}

bool windows8_check()
{
    /*
     * Checks if the current platform is Windows 8
     *
     * :returns: True or False
     *:rtype: bool
     *
     */

    if(windows_check())
        return QSysInfo::windowsVersion() == QSysInfo::WV_WINDOWS8;
    return false;
}

bool is_url(QString url)
{
    QRegExp re("^(https?|ftp|udp)://.*");
    return re.exactMatch(url);
}

bool is_magnet(QString magnet)
{
    if(magnet.startsWith("magnet:?xt=urn:btih:"))
        return true;
    return false;
}

QString info_hash_to_string(libtorrent::sha1_hash hash)
{
    char out[41];
    to_hex((char const*)&hash[0], libtorrent::sha1_hash::size, out);
    return QString(out);
}

/* Formatting text functions */

QString fsize(int fsize_b)
{
    /*
     * Formats the bytes value into a string with KiB, MiB or GiB units
     *
     * :param fsize_b: the filesize in bytes
     * :type fsize_b: int
     * :returns: formatted string in KiB, MiB or GiB units
     * :rtype: string
     *
     */

    double fsize_kb = fsize_b / 1024.0;
    if(fsize_kb < 1024)
        return QString::number(fsize_kb, 'f', 1) + " KiB";
    double fsize_mb = fsize_kb / 1024.0;
    if(fsize_mb < 1024)
        return QString::number(fsize_mb, 'f', 1) + " MiB";
    double fsize_gb = fsize_mb / 1024.0;
    return QString::number(fsize_gb, 'f', 1) + " GiB";
}

QString fpcnt(double dec)
{
    /*
     * Formats a string to display a percentage with two decimal places
     *
     * :param dec: the ratio in the range [0.0, 1.0]
     * :type dec: double
     * :returns: a formatted string representing a percentage
     * :rtype: string
     *
     */

    return QString::number(dec * 100, 'f', 2) + '%';
}

QString fspeed(int bps)
{
    /*
     * Formats a string to display a transfer speed utilizing :func:`fsize`
     *
     * :param bps: bytes per second
     * :type bps: int
     * :returns: a formatted string representing transfer speed
     * :rtype: string
     *
     */

    return fsize(bps) + "/s";
}

QString fpeer(int num_peers, int total_peers)
{
    /*
     * Formats a string to show 'num_peers' ('total_peers')
     *
     * :param num_peers: the number of connected peers
     * :type num_peers: int
     * :param total_peers: the total number of peers
     * :type total_peers: int
     * :returns: a formatted string: num_peers (total_peers), if total_peers < 0, then it will not be shown
     * :rtype: string
     *
     */

    if(total_peers > -1)
        return QString::number(num_peers) + " (" + QString::number(total_peers) + ")";
    else
        return QString::number(num_peers);
}

QString ftime_from_seconds(int seconds)
{
    /*
     * Formats a string to show time in a human readable form
     * :param seconds: the number of seconds
     * :type seconds: int
     * :returns: a formatted time string, will return '' if seconds == 0
     * :rtype: string
     *
     */

    if(seconds == 0)
        return "";
    if(seconds < 60)
        return QString::number(seconds) + "s";
    int minutes = seconds / 60;
    if(minutes < 60)
    {
        seconds = seconds % 60;
        return QString::number(minutes) + "m " + QString::number(seconds) + "s";
    }
    int hours = minutes / 60;
    if(hours < 24)
    {
        minutes = minutes % 60;
        return QString::number(hours) + "h " + QString::number(minutes) + "m";
    }
    int days = hours / 24;
    if(days < 7)
    {
        hours = hours % 24;
        return QString::number(days) + "d " + QString::number(hours) + "h";
    }
    int weeks = days / 7;
    if(weeks < 52)
    {
        days = days % 7;
        return QString::number(weeks) + "w " + QString::number(days) + "d";
    }
    int years = weeks / 52;
    weeks = weeks % 52;
    return QString::number(years) + "y " + QString::number(weeks) + "w";
}

QString fdate(double seconds)
{
    /*
     * Formats a date string in the locale's date representation based on the systems timezone
     *
     * :param seconds: time in seconds since the Epoch
     * :type seconds: double
     * :returns: a string in the locale's date representation or "" if seconds < 0
     * :rtype: string
     *
     */

    if(seconds < 0)
        return "";
    return QDateTime::fromTime_t(seconds).toString("MM/dd/yy");
}

void set_run_at_startup()
{
#ifdef Q_OS_WIN

    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

    settings.setValue ("BitLordQt", "\"" +
                       QDir::toNativeSeparators(QCoreApplication::applicationFilePath())
                       + "\"");
#endif
}

void remove_run_at_startup()
{
#ifdef Q_OS_WIN

    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);

    settings.remove("BitLordQt");

#endif
}

bool check_torrent_files_association()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Classes", QSettings::NativeFormat);
    if(settings.value(".torrent/.", "").toString() == "BitLordQt")
        return true;
    return false;
}

bool check_magnet_links_association()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Classes", QSettings::NativeFormat);
    if(settings.value("Magnet/.", "").toString() == "BitLordQt magnet URI")
        return true;
    return false;
}

bool set_torrent_files_association()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Classes", QSettings::NativeFormat);

    settings.setValue (".torrent/.", "BitLordQt");
    settings.setValue (".torrent/Content Type", "application/x-bittorrent");
    settings.setValue ("BitLordQt/.", "BitLordQt");
    settings.setValue ("BitLordQt/shell/open/command/.", "\"" +
                       QDir::toNativeSeparators(QCoreApplication::applicationFilePath())
                       + "\"" + " \"%1\"");

    return true;
}

bool set_magnet_links_association()
{
    QSettings settings("HKEY_CURRENT_USER\\Software\\Classes", QSettings::NativeFormat);

    settings.setValue("Magnet/.", "BitLordQt magnet URI");
    settings.setValue("Magnet/URL Protocol", "");
    settings.setValue("Magnet/Content Type", "application/x-magnet");
    settings.setValue("Magnet/shell/open/command/.", "\"" +
                      QDir::toNativeSeparators(QCoreApplication::applicationFilePath())
                      + "\"" + " \"%1\"");

    return true;
}
