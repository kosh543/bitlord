#ifndef IPCCLASS_H
#define IPCCLASS_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QSharedMemory>
#include <QLocalServer>
#include <QLocalSocket>

class Components;

class IPCClass : public QObject
{
    Q_OBJECT
private:
    Components *components;
    QSharedMemory *shared_mem;
    QLocalServer *server;
    QLocalSocket *socket;
    QString ipc_file;

public:
    bool is_running;
    explicit IPCClass(Components *comp, QObject *parent = 0);
    void shutdown_ipc();
    void send_message(QByteArray msg);

private slots:
    void receive_message();
};

#endif // IPCCLASS_H
