#include "updateqmap.h"

void updateQMap(QMap<QString, QVariant> &first, QMap<QString, QVariant> &second)
{
    foreach (QString key, second.keys())
    {
        first[key] = second[key];
    }
}
