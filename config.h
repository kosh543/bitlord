#ifndef CONFIG_H
#define CONFIG_H

#include <QJsonDocument>
#include <QObject>
#include <QVector>
#include <QFile>
#include <QDir>
#include <QMap>
#include <QVariant>
#include <QString>
#include <QTimer>

#include "updateqmap.h"
#include "common.h"

void convert_to_qsettings();

class ConfigEvents : public QObject
{
    Q_OBJECT

public:
    enum config_signals_t {autoadd_enable, autoadd_location, prevent_hibernation, max_connections_per_torrent,
                           max_upload_slots_per_torrent, max_upload_speed_per_torrent, max_download_speed_per_torrent,
                           show_rate_in_title, run_at_startup, ui_language, classic_mode, enable_system_tray,
                           torrentfiles_location, listen_ports, listen_interface, random_port, outgoing_ports,
                           random_outgoing_ports, peer_tos, dht, upnp, natpmp, utpex, lsd, enc_in_policy,
                           enc_out_policy, enc_level, enc_prefer_rc4, max_connections_global, max_upload_speed,
                           max_download_speed, max_upload_slots_global, max_half_open_connections,
                           max_connections_per_second, ignore_limits_on_local_network, share_ratio_limit,
                           seed_time_ratio_limit, seed_time_limit, max_active_downloading, max_active_seeding,
                           max_active_limit, dont_count_slow_torrents, send_info, proxies, new_release_check,
                           rate_limit_ip_overhead, geoip_db_location, cache_size, cache_expiry};

    ConfigEvents(QObject *parent = 0);
    QMap<QString, config_signals_t> signals_map;
    void emit_signal(QString key, QVariant value);
    void init_signals_map();

signals:
    void config_changed_signal(QString key, QVariant value);
    void autoadd_enable_signal(QString key, QVariant value);
    void autoadd_location_signal(QString key, QVariant value);
    void prevent_hibernation_signal(QString key, QVariant value);
    void max_connections_per_torrent_signal(QString key, QVariant value);
    void max_upload_slots_per_torrent_signal(QString key, QVariant value);
    void max_upload_speed_per_torrent_signal(QString key, QVariant value);
    void max_download_speed_per_torrent_signal(QString key, QVariant value);
    void show_rate_in_title_signal(QString key, QVariant value);
    void run_at_startup_signal(QString key, QVariant value);
    void ui_language_signal(QString key, QVariant value);
    void classic_mode_signal(QString key, QVariant value);
    void enable_system_tray_signal(QString key, QVariant value);
    void torrentfiles_location_signal(QString key, QVariant value);
    void listen_ports_signal(QString key, QVariant value);
    void listen_interface_signal(QString key, QVariant value);
    void random_port_signal(QString key, QVariant value);
    void outgoing_ports_signal(QString key, QVariant value);
    void random_outgoing_ports_signal(QString key, QVariant value);
    void peer_tos_signal(QString key, QVariant value);
    void dht_signal(QString key, QVariant value);
    void upnp_signal(QString key, QVariant value);
    void natpmp_signal(QString key, QVariant value);
    void utpex_signal(QString key, QVariant value);
    void lsd_signal(QString key, QVariant value);
    void enc_in_policy_signal(QString key, QVariant value);
    void enc_out_policy_signal(QString key, QVariant value);
    void enc_level_signal(QString key, QVariant value);
    void enc_prefer_rc4_signal(QString key, QVariant value);
    void max_connections_global_signal(QString key, QVariant value);
    void max_upload_speed_signal(QString key, QVariant value);
    void max_download_speed_signal(QString key, QVariant value);
    void max_upload_slots_global_signal(QString key, QVariant value);
    void max_half_open_connections_signal(QString key, QVariant value);
    void max_connections_per_second_signal(QString key, QVariant value);
    void ignore_limits_on_local_network_signal(QString key, QVariant value);
    void share_ratio_limit_signal(QString key, QVariant value);
    void seed_time_ratio_limit_signal(QString key, QVariant value);
    void seed_time_limit_signal(QString key, QVariant value);
    void max_active_downloading_signal(QString key, QVariant value);
    void max_active_seeding_signal(QString key, QVariant value);
    void max_active_limit_signal(QString key, QVariant value);
    void dont_count_slow_torrents_signal(QString key, QVariant value);
    void send_info_signal(QString key, QVariant value);
    void proxies_signal(QString key, QVariant value);
    void new_release_check_signal(QString key, QVariant value);
    void rate_limit_ip_overhead_signal(QString key, QVariant value);
    void geoip_db_location_signal(QString key, QVariant value);
    void cache_size_signal(QString key, QVariant value);
    void cache_expiry_signal(QString key, QVariant value);
};

#endif // CONFIG_H
