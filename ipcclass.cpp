#include "ipcclass.h"
#include "components.h"

IPCClass::IPCClass(Components *comp, QObject *parent) :
    QObject(parent)
{
    this->components = comp;

    QSettings ini(QSettings::IniFormat, QSettings::UserScope, "BitLordQt", "BitLordQt");
    QString dir = QFileInfo(ini.fileName()).absolutePath();
    if(!QDir(dir).exists())
        QDir().mkdir(dir);
    this->ipc_file = QDir(dir).absoluteFilePath("bitlord-ipc");
    this->socket = new QLocalSocket(this);
    this->socket->connectToServer(this->ipc_file, QIODevice::WriteOnly);
    if(this->socket->waitForConnected(1000))
        this->is_running = true;
    else
    {
        this->is_running = false;
        this->server = new QLocalServer(this);
        this->connect(this->server, SIGNAL(newConnection()), this, SLOT(receive_message()));
        if(QFileInfo(this->ipc_file).exists())
            QFile(this->ipc_file).remove();
        this->server->listen(this->ipc_file);
    }
}

void IPCClass::shutdown_ipc()
{
    if(!this->is_running && QFileInfo(this->ipc_file).exists())
        QFile(this->ipc_file).remove();
}

void IPCClass::send_message(QByteArray msg)
{

    this->socket->write(msg);
    if(!this->socket->waitForBytesWritten(1000))
        return;
    this->socket->disconnectFromServer();
}

void IPCClass::receive_message()
{
    QLocalSocket *socket = this->server->nextPendingConnection();
    if(!socket->waitForReadyRead(1000))
        return;
    this->components->addTorrentDialog->receive_message(QString(socket->readAll()));
}
